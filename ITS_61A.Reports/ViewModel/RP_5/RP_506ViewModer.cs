﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_5
{
    public class RP_506ViewModer
    {
        public string condoBuilding { get; set; }
        public string projectName { get; set; }
        public string take { get; set; }
        public string lend { get; set; }
        public string night { get; set; }
        public string transfer { get; set; }
        public RP_506ViewModer(string condoBuilding,string projectName,string take,string lend,string night,string transfer)
        {
            this.condoBuilding = condoBuilding;
            this.projectName = projectName;
            this.take = take;
            this.lend = lend;
            this.night = night;
            this.transfer = transfer;
        }
    }
}
