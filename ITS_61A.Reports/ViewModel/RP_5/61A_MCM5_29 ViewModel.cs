﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_5
{
    public class MCM5_29_ViewModel
    {
        public string roomNumber { get; set; }
        public string buildingNumbers { get; set; }
        public string floorNumbers { get; set; }
        public string name { get; set; }
    }
}
