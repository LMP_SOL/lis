﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_5
{
    public class RP_505_01ViewModel
    {

        public string part { get; set; }
        public string take { get; set; }
        public string lend { get; set; }
        public string takeBack { get; set; }
        public string transfer { get; set; }
        public string balance { get; set; }
        public RP_505_01ViewModel(string part,string take,string lend,string takeBack,string transfer,string balance)
        {
            this.part = part;
            this.take = take;
            this.lend = lend;
            this.takeBack = takeBack;
            this.transfer = transfer;
            this.balance = balance;
        }
    }
    public class RP_505_02ViewModel
    {

        public string pile { get; set; }
        public string Take { get; set; }
        public string take { get; set; }
        public string lend { get; set; }
        public string takeBack { get; set; }
        public string transfer { get; set; }
        public string balance { get; set; }
        public RP_505_02ViewModel(string pile, string take, string lend, string takeBack, string transfer, string balance)
        {
            this.pile = pile;
            this.take = take;
            this.lend = lend;
            this.takeBack = takeBack;
            this.transfer = transfer;
            this.balance = balance;
        }
    }
}
