﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_5
{
    public class MCM5_30ViewModel
    {
        public string homeNumber { get; set; }
        public string landNumbers { get; set; }
        public string deedNumbers { get; set; }
        public string name { get; set; }
    }
}
