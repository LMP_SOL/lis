﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_5
{
    public class RP_504ViewModel
    {
            public string nameSurname { get; set; }
            public string suiteNumber { get; set; }
            public string floor { get; set; }
            public string building { get; set; }
            public string totalArea { get; set; }
            public string dateOfLoan { get; set; }
            public string typeOfLoan { get; set; }
            public string objective { get; set; }
            public string numberOfTheDay { get; set; }
            public string note { get; set; }
            public string i00022 { get; set; }
        public RP_504ViewModel()
        { }
    }
}
