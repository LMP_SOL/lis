﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_5
{
    class MCM5_24ViewModel
    {
        public string plotLand { get; set; }
        public string deedNumber { get; set; }
        public string nameSurName { get; set; }
        public string note { get; set; }
        public MCM5_24ViewModel(string plotLand, string deedNumber, string nameSurName, string note)
        {
            this.plotLand = plotLand;
            this.deedNumber = deedNumber;
            this.nameSurName = nameSurName;
            this.note = note;
        }

    }
}
