﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_5
{
    public class MCM5_31_ViewModel
    {
        public string roomNumber { get; set; }
        public string buildingNumber { get; set; }
        public string floorNumber { get; set; }
        public string name { get; set; }
    }


}
