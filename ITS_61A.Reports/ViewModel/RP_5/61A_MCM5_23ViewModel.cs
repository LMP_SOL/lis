﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_5
{
    public class MCM5_23ViewModel
    {
        public string houseNumber { get; set; }
        public string deedNumber { get; set; }
        public string nameSurName { get; set; }
        public MCM5_23ViewModel()
        { }
    }
}