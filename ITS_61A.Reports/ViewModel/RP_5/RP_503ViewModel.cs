﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_5
{
    public class RP_503ViewModel
    {
            public string nameSurname { get; set; }
            public string houseNumber { get; set; }
            public string deedNumber { get; set; }
            public string landNumber { get; set; }
            public string surveyPage { get; set; }
            public string dateOfLoan { get; set; }
            public string typeOfLoan { get; set; }
            public string objective { get; set; }
            public string numberOfTheDay { get; set; }
            public string note { get; set; }
            public string i00022 { get; set; }
        public RP_503ViewModel()
        { }

    }
}
