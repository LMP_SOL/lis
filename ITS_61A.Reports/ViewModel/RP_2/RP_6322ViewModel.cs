﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.Surveyors
{
    public class RP_6322ViewModel
    {
        public string natureOfSurvey { get; set; }
        public string surveyType { get; set; }
        public string totalJobs { get; set; }
        public string completeJobs { get; set; }
        public string incompleteJobs { get; set; }
       
    }
}
