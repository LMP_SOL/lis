﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_2
{
    public class RP_203ViewModel
    {
        public string recordNumber { get; set; }
        public string prejectName { get; set; }
        public string numberOfUnitConstruction { get; set; }
        public string numberOfUnitConvertedToSubdivision { get; set; }
        public string operatorName { get; set; }
        public string filedSurveyors { get; set; }
        public string takeSurveyors { get; set; }
        public string Register { get; set; }
        public string Deposit { get; set; }
        public string GetTheDeed { get; set; }
       
    }
}
