﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_2
{
    public class RP_201ViewModel
    {
        public string recordNumber { get; set; }
        public string prejectName { get; set; }
        public string sv { get; set; }
        public string surveyType { get; set; }
        public string departmentalNumber { get; set; }
        public string dateOfCase { get; set; }
        public string operatorName { get; set; }
        public string progress { get; set; }
        public string history { get; set; }
        public string dateOfComplete { get; set; }
        public string remark { get; set; }
    }
}
