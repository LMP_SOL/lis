﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.Surveyors
{
    public class RP_6324ViewModel
    {
        public string recordNumber { get; set; }
        public string prejectName { get; set; }
        public string unit { get; set; }
        public string company { get; set; }
        public string agentName { get; set; }
        public string contractPeriodNumberDate { get; set; }
        public string pushProcedure { get; set; }
        public string takeProcedure { get; set; }
        public string writeProcedure { get; set; }
        public string getTheDeed { get; set; }
        public string sendReport { get; set; }
    }
}
