﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_1
{
    public class RP_106ViewModel
    {
        public string dateSele { get; set; }
        public string nameAddressSele { get; set; }
        public string addressSele { get; set; }
        public string area { get; set; }
        public string province { get; set; }
        public string landPriceSele { get; set; }
        public string priceSele { get; set; }
        public string rai { get; set; }
        public string garn { get; set; }
        public string wa { get; set; }
    }
}
