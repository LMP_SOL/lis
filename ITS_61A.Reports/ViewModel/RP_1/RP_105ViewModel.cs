﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_1
{
    public class RP_105ViewModel
    {
        public string landLocation { get; set; }
        public string landNumber { get; set; }
        public string deedNumber { get; set; }
        public string freight { get; set; }
        public string landSize { get; set; }
        public string rai { get; set; }
        public string garn { get; set; }
        public string wa { get; set; }
        public string landPrice { get; set; }
        public string landValue { get; set; }
        public string categoryLandUse { get; set; }
        public string LandTax { get; set; }
        public string notation { get; set; }

        //public RP_105ViewModel(string landLocation, string landNumber, string deedNumber, string freight, string landSize, string landPrice,
        //   string landValue, string categoryLandUse, string LandTax, string notation)
        //{
        //    this.landLocation = landLocation;
        //    this.landNumber = landNumber;
        //    this.deedNumber = deedNumber;
        //    this.freight = freight;
        //    this.landSize = landSize;
        //    this.landPrice = landPrice;
        //    this.landValue = landValue;
        //    this.categoryLandUse = categoryLandUse;
        //    this.LandTax = LandTax;
        //    this.notation = notation;
        //}
    }
}
