﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_1
{
    public class RP_104ViewModel
    {
        public string projectID { get; set; }
        public string projectName { get; set; }
        public string rai { get; set; }
        public string garn { get; set; } 
        public string wa { get; set; }
        public string pricePerRai { get; set; }
        public string offerer { get; set; }
        public string responsibleConstructionAgency { get; set; }
        public string spaceAgency { get; set; }
    }
}
