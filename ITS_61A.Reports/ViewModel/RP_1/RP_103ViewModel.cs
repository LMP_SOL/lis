﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_1
{
    public class RP_103ViewModel
    {
        public string nameOfOrganization { get; set; }
        public string deedNumber { get; set; }
        public string registerAt { get; set; }
        public string individualRegistration { get; set; }
        public string rai { get; set; }
        public string garn { get; set; }
        public string wa { get; set; }
        public string contractStartDate { get; set; }
        public string contractTerm { get; set; }
        public string buildingStyle { get; set; }
        public string agencyResponsible { get; set; }
        public string category { get; set; }
        public string sqWa { get; set; }
    }
}
