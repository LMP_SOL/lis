﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_1
{
    public class RP_101ViewModel
    {
        public string fiscalYear { get; set; }
        public string numberOfConversions { get; set; }
        public string rai { get; set; }
        public string garn { get; set; }
        public string wa { get; set; }
        public string number { get; set; }


        //public RP_101ViewModel(string fiscalYear, string numberOfConversions, string rai, string garn, string wa, string number)
        //{
        //    this.fiscalYear = fiscalYear;
        //    this.numberOfConversions = numberOfConversions;
        //    this.rai = rai;
        //    this.garn = garn;
        //    this.wa = wa;
        //    this.number = number;
        //}
    }
}
