﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_1
{
    public class RP_102ViewModel
    {

        public string projectID { get; set; }
        public string projectName { get; set; }
        public string projectLocation { get; set; }
        public string offerer { get; set; }
        public string numberOfUnits { get; set; }
        public string rai { get; set; }
        public string garn { get; set; }
        public string wa { get; set; }
        public string pricePerRai { get; set; }
        public string approvalDate { get; set; }
        public string dateOfLandTransfer { get; set; }
        public string spaceManager { get; set; }
        //public RP_102ViewModel(string projectID, string projectName, string projectLocation, string offerer, string numberOfUnits, string rai, string garn, string wa, string pricePerRai, string approvalDate, string dateOfLandTransfer, string spaceManager)
        //{
        //    this.projectID = projectID;
        //    this.projectName = projectName;
        //    this.projectLocation = projectLocation;
        //    this.offerer = offerer;
        //    this.numberOfUnits = numberOfUnits;
        //    this.rai = rai;
        //    this.garn = garn;
        //    this.wa = wa;
        //    this.pricePerRai = pricePerRai;
        //    this.approvalDate = approvalDate;
        //    this.dateOfLandTransfer = dateOfLandTransfer;
        //    this.spaceManager = spaceManager;
        //}
        //public RP_102ViewModel()
        //{

        //}
    }
}
