﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_1
{

    public class RP_108ViewModel
    {
        public string idProject { get; set; }
        public string agency { get; set; }
        public string deedNumber { get; set; }
        public string province { get; set; }
        public string promiseNumber { get; set; }
        public string promiseDate { get; set; }
        public string subject { get; set; }
        //public RP_108ViewModel(string idProject, string agency, string deedNumber, string province, string promiseNumber,
        //    string promiseDate, string subject)
        //{
        //    this.idProject = idProject;
        //    this.agency = agency;
        //    this.deedNumber = deedNumber;
        //    this.province = province;
        //    this.promiseNumber = promiseNumber;
        //    this.promiseDate = promiseDate;
        //    this.subject = subject;
        //}
    }
}
