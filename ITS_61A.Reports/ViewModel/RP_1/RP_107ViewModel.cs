﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_1
{
    public class RP_107ViewModel
    {
        public string idProject { get; set; }
        public string projectName { get; set; }
        public string deedNumber { get; set; }
        public string rai { get; set; }
        public string garn { get; set; }
        public string wa { get; set; }
        public string province { get; set; }
        public string promiseNumber { get; set; }
        public string promiseDateStart { get; set; }
        public string promiseDateOver { get; set; }
        public string landPriceMonth { get; set; }
        public string LandAdmin { get; set; }
        public string landDepartment { get; set; }
        //public RP_107ViewModel(string idProject, string deedNumber, string Area, string province, string promiseNumber, 
        //    string promiseDateStart, string promiseDateOver, string landPriceMonth, string LandAdmin, string landDepartment)
        //{
        //    this.idProject = idProject;
        //    this.deedNumber = deedNumber;
        //    this.Area = Area;
        //    this.province = province;
        //    this.promiseNumber = promiseNumber;
        //    this.promiseDateStart = promiseDateStart;
        //    this.promiseDateOver = promiseDateOver;
        //    this.landPriceMonth = landPriceMonth;
        //    this.LandAdmin = LandAdmin;
        //    this.landDepartment = landDepartment;
        //}
    }
}
