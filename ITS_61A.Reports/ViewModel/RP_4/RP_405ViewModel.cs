﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_4
{
    public class RP_405_01ViewModel
    {
        public string projectType { get; set; }
        public string condoAllNumber { get; set; }
        public string propertyArea { get; set; }
        public string condoTransferNumberY { get; set; }
        public string condoTransferNumberN { get; set; }
    }
    public class RP_405_02ViewModel
    {
        public string projectType { get; set; }
        public string condoAllNumber { get; set; }
        public string deedNumber { get; set; }
        public string propertyArea { get; set; }
        public string condoTransferNumberY { get; set; }
        public string condoTransferNumberN { get; set; }
        public RP_405_02ViewModel (string projectType,string condoAllNumber,string deedNumber,string propertyArea,string condoTransferNumberY,string condoTransferNumberN)
        {
            this.projectType = projectType;
            this.condoAllNumber = condoAllNumber;
            this.deedNumber = deedNumber;
            this.propertyArea = propertyArea;
            this.condoTransferNumberY = condoTransferNumberY;
            this.condoTransferNumberN = condoTransferNumberN;
        }
    }
}
