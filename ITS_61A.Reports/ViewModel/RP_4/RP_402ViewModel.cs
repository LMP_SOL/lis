﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_4
{
    public class RP_402ViewModel
    {
        public string order { get; set; }
        public string nameRegisterCondo { get; set; }
        public string buildingRegistrationNumber { get; set; }
        public string dateOfRegistrationCondo { get; set; }
        public string dateOfReceiptOfTitle { get; set; }
        public string corporateRegister { get; set; }
        public string dateOfIncorporation { get; set; }
        public string officeNumber { get; set; }
        public string publicLand { get; set; }
        public string area { get; set; }
        public string numberOfSuites { get; set; }
        public string projectType { get; set; }
        public string projectName { get; set; }
        public string numberOfCondominiumUnits { get; set; }
        public string numberOfProjects { get; set; }
        public string condoBuilding { get; set; }
        public string pile { get; set; }
        public string rai { get; set; }
        public string garn { get; set; }
        public string wa { get; set; }
        public RP_402ViewModel
            (string nameRegisterCondo,string buildingRegistrationNumber,string dateOfRegistrationCondo,string dateOfReceiptOfTitle,string corporateRegister,string dateOfIncorporation,string officeNumber,string publicLand, string area, string numberOfSuites,string projectType,string projectName,string numberOfCondominiumUnits,string numberOfProjects,string condoBuilding,string pile,string rai,string garn,string wa)
        {
            this.nameRegisterCondo= nameRegisterCondo;
            this.buildingRegistrationNumber= buildingRegistrationNumber;
            this.dateOfRegistrationCondo= dateOfRegistrationCondo;
            this.dateOfReceiptOfTitle= dateOfReceiptOfTitle;
            this.corporateRegister= corporateRegister;
            this.dateOfIncorporation= dateOfIncorporation;
            this.officeNumber= officeNumber;
            this.publicLand= publicLand;
            this.area= area;
            this.numberOfSuites= numberOfSuites;
            this.projectType= projectType; 
            this.projectName= projectName;
            this.numberOfCondominiumUnits= numberOfCondominiumUnits;
            this.numberOfProjects= numberOfProjects;
            this.condoBuilding= condoBuilding;
            this.pile= pile;
            this.rai= rai;
            this.garn= garn;
            this.wa= wa;
        }
    }
}