﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_4
{
    public class RP_404_01ViewModel
    {
        public string project { get; set; }
        public string condoName { get; set; }
        public RP_404_01ViewModel(string project,string condoName)
        {
            this.project = project;
            this.condoName = condoName;
        }
    }
        public class RP_404_02ViewModel
    {
        public string apartmentlayout { get; set; }
        public string apartmentSpace { get; set; }
        public string balconySpace { get; set; }
        public string area { get; set; }
        public RP_404_02ViewModel(string apartmentlayout,string apartmentSpace,string balconySpace,string area)
        {
            this.apartmentlayout = apartmentlayout;
            this.apartmentSpace = apartmentSpace;
            this.balconySpace = balconySpace;
            this.area = area;
        }
    }
        public class RP_404_03ViewModel
    {
        public string apartmentLayout { get; set; }
        public string buildingNumber { get; set; }
        public string floor { get; set; }
        public string apartmentNumber { get; set; }
        public string apartmentSpace { get; set; }
        public string balconySpace { get; set; }
        public string area { get; set; }
        public string materialCode { get; set; }
        public RP_404_03ViewModel (string apartmentLayout,string buildingNumber,string floor, string apartmentNumber,string apartmentSpace,string balconySpace,string area,string materialCode)
        {
            this.apartmentLayout = apartmentLayout;
            this.buildingNumber = buildingNumber;
            this.floor = floor;
            this.apartmentNumber = apartmentNumber;
            this.apartmentSpace = apartmentSpace;
            this.balconySpace = balconySpace;
            this.area = area;
            this.materialCode = materialCode;
        }
    }
}