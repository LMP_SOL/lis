﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_4
{
    public class RP_403ViewModel
    {
        public string condoNumber { get; set; }
        public string condoName { get; set; }
        public string condoDate { get; set; }
        public string corporateNumber { get; set; }
        public string corporateDate { get; set; }
        public string corporateRoom { get; set; }
        public string apartmentNumber { get; set; }
        public string projectName { get; set; }
        public string allapartmentNumber { get; set; }
        public string apartmentNumbertransfer { get; set; }
        public string apartmentNumberRemain { get; set; }
        public string remark { get; set; }
        public RP_403ViewModel
            (string condoNumber,string condoName,string condoDate,string corporateNumber,string corporateDate,string corporateRoom,string apartmentNumber,string projectName,string allapartmentNumber,string apartmentNumbertransfer,string apartmentNumberRemain,string remark)
        {
            this.condoNumber = condoNumber;
            this.condoName = condoName;
            this.condoDate = condoDate;
            this.corporateNumber = corporateNumber;
            this.corporateDate = corporateDate;
            this.corporateRoom = corporateRoom;
            this.apartmentNumber = apartmentNumber;
            this.projectName = projectName;
            this.allapartmentNumber = allapartmentNumber;
            this.apartmentNumbertransfer = apartmentNumbertransfer;
            this.apartmentNumberRemain = apartmentNumberRemain;
            this.remark = remark;
        }

    }
}
