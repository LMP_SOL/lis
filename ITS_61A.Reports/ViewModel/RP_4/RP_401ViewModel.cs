﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_4
{
    public class RP_401_01ViewModel
    {
        public string group { get; set; }
        public string division { get; set; }
        public string condominium { get; set; }
        public string projectName { get; set; }
        public string condoName { get; set; }
    }
    public class RP_401_02ViewModel
    {
        public string name { get; set; }
        public string roomNumber { get; set; }
        public string floor { get; set; }
        public string buildingNumber { get; set; }
        public string room { get; set; }
        public string building { get; set; }
        public string balcony { get; set; }
        public string all { get; set; }
        public string takeDate { get; set; }
        public string tranferDate { get; set; }
        public string tranfer { get; set; }
        public RP_401_02ViewModel(string name, string roomNumber, string floor, string buildingNumber, string room, string building, string balcony, string all, string takeDate, string tranferDate, string tranfer)
        {
            this.name=name;
            this.roomNumber=roomNumber;
            this.floor=floor;
            this.buildingNumber=buildingNumber;
            this.room=room;
            this.building=building;
            this.balcony=balcony;
            this.all=all;
            this.takeDate=takeDate;
            this.tranferDate=tranferDate;
            this.tranfer=tranfer;
        
        }
    }

}
