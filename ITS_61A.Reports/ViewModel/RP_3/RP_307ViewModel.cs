﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_307ViewModel
    {
        public string fileNumbering { get; set; }
        public string projectID { get; set; }
        public string projectName { get; set; }
        public string spaceOfSurveyor { get; set; }
        public string home { get; set; }
        public string land { get; set; }
        public string condominium { get; set; }
        public string dateOfPreparation { get; set; }
        public string surveyResults { get; set; }
        //public RP_307ViewModel(string fileNumbering,string projectID,string projectName,string spaceOfSurveyor,string home,string land,string condominium,string dateOfPreparation,string surveyResults)
        //{
        //    this.fileNumbering = fileNumbering;
        //    this.projectID = projectID;
        //    this.projectName = projectName;
        //    this.spaceOfSurveyor = spaceOfSurveyor;
        //    this.home = home;
        //    this.land = land;
        //    this.condominium = condominium;
        //    this.dateOfPreparation = dateOfPreparation;
        //    this.surveyResults = surveyResults;

        //}
    }
}
