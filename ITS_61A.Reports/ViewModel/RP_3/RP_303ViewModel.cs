﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_303_01ViewModel
    {
        public string party { get; set; }
        public string fileNumber { get; set; }
        public string project { get; set; }
        public string landMark { get; set; }
        public string deedNumber { get; set; }
        public string areaTotle { get; set; }
        public string landTransferDate { get; set; }
        //public RP_303_01ViewModel(string party, string fileNumber, string project, string landMark, string deedNumber,
        //   string areaTotle, string landTransferDate)
        //{
        //    this.party = party;
        //    this.fileNumber = fileNumber;
        //    this.project = project;
        //    this.landMark = landMark;
        //    this.deedNumber = deedNumber;
        //    this.areaTotle = areaTotle;
        //    this.landTransferDate = landTransferDate;
        //}
    }
    public class RP_303_02ViewModel
    {
        public string projectName { get; set; }
        public string styleHouse { get; set; }
        public string construction { get; set; } //หน่วยก่อสร้าง
        public string landValue { get; set; }
        public string deedAreaProject { get; set; } //เนื้อที่โฉนดใช้ทำโครงการ 
        public string surveyDate { get; set; }
        public string landBalance { get; set; }
        //public RP_303_02ViewModel(string projectName, string styleHouse, string construction, string landValue, string deedAreaProject,
        //   string surveyDate, string landBalance)
        //{
        //    this.projectName = projectName;
        //    this.styleHouse = styleHouse;
        //    this.construction = construction;
        //    this.landValue = landValue;
        //    this.deedAreaProject = deedAreaProject;
        //    this.surveyDate = surveyDate;
        //    this.landBalance = landBalance;
        //}
    }
}
