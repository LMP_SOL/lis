﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_306_01ViewModel
    {
        public string names { get; set; }
        public string landGroup { get; set; }
        public string homeNumber { get; set; }
        public string deedNumber { get; set; }
        public string landNumber { get; set; }
        public string survey { get; set; } //หน้าสำรวจ
        public string deedArea { get; set; }
        public string takeDate { get; set; }
        public string transferDate { get; set; }
        public string status { get; set; }
        public string useLand { get; set; }
        public string leftLand { get; set; }
        public string remark { get; set; }
        public string condominiumBuilding { get; set; }
        public string project { get; set; }
        //public RP_306_01ViewModel(string names, string landGroup, string homeNumber, string deedNumber, string landNumber,
        //   string survey, string deedArea, string takeDate, string transferDate, string status, string useLand, string leftLand,
        //   string remark, string condominiumBuilding, string project)
        //{
        //    this.names = names;
        //    this.landGroup = landGroup;
        //    this.homeNumber = homeNumber;
        //    this.deedNumber = deedNumber;
        //    this.landNumber = landNumber;
        //    this.survey = survey;
        //    this.deedArea = deedArea;
        //    this.takeDate = takeDate;
        //    this.transferDate = transferDate;
        //    this.status = status;
        //    this.useLand = useLand;
        //    this.leftLand = leftLand;
        //    this.remark = remark;
        //    this.condominiumBuilding = condominiumBuilding;
        //    this.project = project;
        //}
    }
    public class RP_306_02ViewModel
    {
        public string party { get; set; } //ฝ่าย
        public string division { get; set; } //กอง
        public string projectType { get; set; }
        //public RP_306_02ViewModel(string party, string division, string projectType)
        //{
        //    this.party = party;
        //    this.division = division;
        //    this.projectType = projectType;
        //}
    }
}
