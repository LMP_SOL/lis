﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_314ViewModel //รายงานโฉนดที่ดินทางบัญชี
    {
        public string materialCode { get; set; }
        public string homeNumber { get; set; }
        public string function { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public string deedNumber { get; set; }
        public string landNumber { get; set; }
        public string survey { get; set; }
        public string rai_ngan_wa { get; set; }
        public string area { get; set; }
        public string transferStatus { get; set; }
        public string condominiumRegisterDate { get; set; }
        public string remark { get; set; }
        public string sumType { get; set; }
    }
}
