﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_312ViewModel
    {
        public string projectID { get; set; }
        public string projectName { get; set; }
        public string totalDeed { get; set; }
        public string transferAmount { get; set; }
        public string statusSurveyor { get; set; }
        //public RP_312ViewModel(string projectID, string projectName, string totalDeed, string transferAmount, string statusSurveyor)
        //{
        //    this.projectID = projectID;
        //    this.projectName = projectName;
        //    this.totalDeed = totalDeed;
        //    this.transferAmount = transferAmount;
        //    this.statusSurveyor = statusSurveyor;

        //}
    }
}
