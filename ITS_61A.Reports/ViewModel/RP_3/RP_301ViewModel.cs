﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_301_01ViewModel
    {
        public string fileNumber { get; set; }
        public string project { get; set; }
        public string landMark { get; set; }
        public string obtain { get; set; } //การได้มา
        public string transferFrom { get; set; } //รับโอนจาก
        public string spaceValue { get; set; }
        public string totalPurchase { get; set; }
        public string promiseProject { get; set; }
        public string promiseNumber { get; set; }
        public string date { get; set; }
        public string Offer { get; set; } //ผู้เสนอขาย
        public string tradingPrice { get; set; }
        public string raiPrice { get; set; }
        public string onWah { get; set; }
        //public RP_301_01ViewModel(string fileNumber, string project, string landMark, string obtain, string transferFrom,
        //    string spaceValue, string totalPurchase, string promiseProject, string promiseNumber, string date, string Offer, string tradingPrice,
        //    string raiPrice, string onWah)
        //{
        //    this.fileNumber = fileNumber;
        //    this.project = project;
        //    this.landMark = landMark;
        //    this.obtain = obtain;
        //    this.transferFrom = transferFrom;
        //    this.spaceValue = spaceValue;
        //    this.totalPurchase = totalPurchase;
        //    this.promiseProject = promiseProject;
        //    this.promiseNumber = promiseNumber;
        //    this.date = date;
        //    this.Offer = Offer;
        //    this.tradingPrice = tradingPrice;
        //    this.raiPrice = raiPrice;
        //    this.onWah = onWah;
        //}

    }
    public class RP_301_02ViewModel
    {
        public string deedNumber { get; set; }
        public string landNumber { get; set; }
        public string surveyPage { get; set; }
        public string area { get; set; }
        public string transferDate { get; set; } //วันที่โอนให้ กคช.
        public string landownerName { get; set; }
        public string Obligation { get; set; } //ภาระผูกพัน
        //public RP_301_02ViewModel(string deedNumber, string landNumber, string surveyPage, string area, string transferDate,
        //    string landownerName, string subject)
        //{
        //    this.deedNumber = deedNumber;
        //    this.landNumber = landNumber;
        //    this.surveyPage = surveyPage;
        //    this.area = area;
        //    this.transferDate = transferDate;
        //    this.landownerName = landownerName;
        //    this.Obligation = Obligation;
        //}
    }
}
