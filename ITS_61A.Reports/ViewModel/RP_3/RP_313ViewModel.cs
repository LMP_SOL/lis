﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_313ViewModel
    {
        public string projectID { get; set; }
        public string deedAll { get; set; }
        public string transferOwner { get; set; }
        public string status { get; set; }
    }
}
