﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_310ViewModel
    {
        public string projectID { get; set; }
        public string landID { get; set; }
        public string deed { get; set; }
        public string landNumber { get; set; } 
        public string sueveyPage { get; set; } 
        public string areaAll { get; set; }
        public string utilityType { get; set; }
        public string materialCode { get; set; }
        public string contractParty { get; set; } //คู่สัญญา
        public string yearNum { get; set; }
        public string promiseFinal { get; set; } 
        public string space { get; set; }
        public string spaceBalance { get; set; }
    }
}
