﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_308ViewModel
    {
        public string projectType { get; set; }
        public string deedValue { get; set; }
        public string deedTransferValue { get; set; }
        public string deedArea { get; set; }
        public string useArea { get; set; }
        public string remainArea { get; set; }
    }
}
