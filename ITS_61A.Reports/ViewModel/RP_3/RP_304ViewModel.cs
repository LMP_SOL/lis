﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_304_01ViewModel
    {
        public string fiscalYear { get; set; }
        public string quarter { get; set; }
        public string monthOfWaitProject { get; set; }
        public string yearOfWaitProject { get; set; }
        public string overall { get; set; }
        public string areaOfOverall { get; set; }
        public string projectVicinity { get; set; }
        public string areaOfProjectVicinity { get; set; }
        public string regionalProjects { get; set; }
        public string areaOfRegionalProjects { get; set; }
        public string monthOfUpdate { get; set; }
        public string yearOfUpdate { get; set; }
        //public RP_304_01ViewModel(string fiscalYear,string quarter,string monthOfWaitProject,string yearOfWaitProject ,string overall,string areaOfOverall,string projectVicinity,string areaOfProjectVicinity,string regionalProjects,string areaOfRegionalProjects,string monthOfUpdate,string yearOfUpdate)
        //{
        //    this.fiscalYear = fiscalYear;
        //    this.quarter = quarter;
        //    this.monthOfWaitProject = monthOfWaitProject;
        //    this.yearOfWaitProject = yearOfWaitProject;
        //    this.overall = overall;
        //    this.areaOfOverall = areaOfOverall;
        //    this.projectVicinity = projectVicinity;
        //    this.areaOfProjectVicinity = areaOfProjectVicinity;
        //    this.regionalProjects = regionalProjects;
        //    this.areaOfRegionalProjects = areaOfRegionalProjects;
        //    this.monthOfUpdate = monthOfUpdate;
        //    this.yearOfUpdate = yearOfUpdate;


        //}
    }

    public class RP_304_02ViewModel
    {
        public string projectName { get; set; }
        public string location { get; set; }
        public string projectArea { get; set; }
        public string landUsed { get; set; }
        public string remainingArea { get; set; }
        public string numOfPlot { get; set; }
        public string purchasedInFiscalYear { get; set; }
        public string purchasedPrice { get; set; }
        public string totalValuePurchased { get; set; }
        public string numOfTitleDeed { get; set; }
        public string note { get; set; }
        //public RP_304_02ViewModel(string projectName,string location,string projectArea,string landUsed,string remainingArea,string numOfPlot,string purchasedInFiscalYear,string purchasedPrice,string totalValuePurchased,string numOfTitleDeed,string note)
        //{
        //    this.projectName = projectName;
        //    this.location = location;
        //    this.projectArea = projectArea;
        //    this.landUsed = landUsed;
        //    this.remainingArea = remainingArea;
        //    this.numOfPlot = numOfPlot;
        //    this.purchasedInFiscalYear = purchasedInFiscalYear;
        //    this.purchasedPrice = purchasedPrice;
        //    this.totalValuePurchased = totalValuePurchased;
        //    this.numOfTitleDeed = numOfTitleDeed;
        //    this.note = note;

        //}
    }
}
