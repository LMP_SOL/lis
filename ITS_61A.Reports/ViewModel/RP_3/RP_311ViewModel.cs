﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_311ViewModel
    {
        public string projectID { get; set; }
        public string projectName { get; set; }
        public string plotNumber { get; set; }
        public string titleDeed { get; set; }
        public string landNumber { get; set; }
        public string surveyPage { get; set; }
        public string areaOfTheDeed { get; set; }
        public string obligation { get; set; }
        public string description { get; set; }
        //public RP_311ViewModel(string projectID,string projectName,string plotNumber,string titleDeed,string landNumber,string surveyPage,string areaOfTheDeed,string obligation,string description)
        //{
        //    this.projectID = projectID;
        //    this.projectName = projectName;
        //    this.plotNumber = plotNumber;
        //    this.titleDeed = titleDeed;
        //    this.landNumber = landNumber;
        //    this.surveyPage = surveyPage;
        //    this.areaOfTheDeed = areaOfTheDeed;
        //    this.obligation = obligation;
        //    this.description = description;

        //}
    }
}
