﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_302_01ViewModel
    {
        public string fileNumber { get; set; }
        public string project { get; set; }
        public string landMark { get; set; }
        //public RP_302_01ViewModel(string fileNumber, string project, string landMark)
        //{
        //    this.fileNumber = fileNumber;
        //    this.project = project;
        //    this.landMark = landMark;
        //}

        //public rp_302_01viewmodel()
        //{
        //}
    }
    public class RP_302_02ViewModel
    {
        public string deedNumber { get; set; }
        public string landNumber { get; set; }
        public string surveyPage { get; set; }
        public string area { get; set; }
        public string surveyDate { get; set; }
        public string deedDate { get; set; }
        public string surveyDetail { get; set; }
        //public RP_302_02ViewModel(string deedNumber, string landNumber, string surveyPage, string area, string surveyDate,
        //    string deedDate, string surveyDetail)
        //{
        //    this.deedNumber = deedNumber;
        //    this.landNumber = landNumber;
        //    this.surveyPage = surveyPage;
        //    this.area = area;
        //    this.surveyDate = surveyDate;
        //    this.deedDate = deedDate;
        //    this.surveyDetail = surveyDetail;
        //}

    }
}
