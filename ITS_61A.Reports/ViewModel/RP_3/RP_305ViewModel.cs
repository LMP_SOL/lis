﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel.RP_3
{
    public class RP_305_01ViewModel
    {
        public string fiscalYear { get; set; }
        public string quarter { get; set; }
        public string monthOfWaitProject { get; set; }
        public string yearOfWaitProject { get; set; }
        public string overall { get; set; }
        public string areaOfOverall { get; set; }
        public string projectVicinity { get; set; }
        public string areaOfProjectVicinity { get; set; }
        public string regionalProjects { get; set; }
        public string areaOfRegionalProject { get; set; }
        public string monthOfUpdate { get; set; }
        public string yearOfUpdate { get; set; }
        //public RP_305_01ViewModel(string fiscalYear,string quarter,string monthOfWaitProject,string yearOfWaitProject,string overall,string areaOfOverall,string projectVicinity,string areaOfProjectVicinity,string regionalProjects,string areaOfRegionalProject,string monthOfUpdate,string yearOfUpdate)
        //{ this.fiscalYear = fiscalYear;
        //    this.quarter = quarter;
        //    this.monthOfWaitProject = monthOfWaitProject;
        //    this.yearOfWaitProject = yearOfWaitProject;
        //    this.overall = overall;
        //    this.areaOfOverall = areaOfOverall;
        //    this.projectVicinity = projectVicinity;
        //    this.areaOfProjectVicinity = areaOfProjectVicinity;
        //    this.regionalProjects = regionalProjects;
        //    this.areaOfRegionalProject = areaOfRegionalProject;
        //    this.monthOfUpdate = monthOfUpdate;
        //    this.yearOfUpdate = yearOfUpdate;
        //}
    }
    public class RP_305_02ViewModel
    {
        public string deedNumber { get; set; }
        public string landNumber { get; set; }
        public string surveyPage { get; set; }
        public string landUnderDeed { get; set; }
        public string landDetails { get; set; }
        public string remainingArea { get; set; }
        public string numOfChart { get; set; }
        //public RP_305_02ViewModel(string deedNumber,string landNumber,string surveyPage,string landUnderDeed,string landDetails,string remainingArea,string numOfChart)
        //{
        //    this.deedNumber = deedNumber;
        //    this.landNumber = landNumber;
        //    this.surveyPage = surveyPage;
        //    this.landUnderDeed = landUnderDeed;
        //    this.landDetails = landDetails;
        //    this.remainingArea = remainingArea;
        //    this.numOfChart = numOfChart;

        //}
    }
}
