﻿using ITS_61A.Core.ServiceDataModel.MasterData;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.Services
{
    public class MasterDataManagementDataservice : BaseService
    {
        public DataSet getSectionOfMasterMenu(string id)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = string.Format("SELECT RAWTOHEX(a.ID) AS mastersession_id ,a.MASTER_SESSION_NAME AS mastersession_name ,a.SORT AS sort FROM MASTER_SESSION  a WHERE a.MASTER_MENU_ID = '{0}' ORDER BY a.SORT", id);

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }
        public DataSet getDataIntab(string menuId, string sessionId)
        {

            DataSet ds = new DataSet();

            string sql = "GETDATA_MASTER_INTAB";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SESSION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_MASSTERTOPIC", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_TOPIC", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_TABLE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                    cmd.Parameters.Add("MENU_ID", OracleDbType.NVarchar2).Value = menuId;
                    cmd.Parameters.Add("SESSION_ID", OracleDbType.NVarchar2).Value = sessionId;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    ds.Tables[0].TableName = "mastersession";
                    ds.Tables[1].TableName = "mastertopic";
                    ds.Tables[2].TableName = "list_topic";
                    ds.Tables[3].TableName = "list_table";

                }
            }

            return ds;
        }


        public DataSet getMastertopic(string sessionId)
        {

            DataSet ds = new DataSet();

            string sql = String.Format("SELECT RAWTOHEX(a.ID) mastertopic_id, a.MASTER_TOPIC_NAME mastertopic_name, a.SORT mastertopic_sort FROM MASTER_TOPIC a WHERE a.MASTER_SESSION_ID = '{0}'", sessionId);

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;


                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    ds.Tables[0].TableName = "mastertopic";
                }
            }

            return ds;
        }

        public DataSet getDataInTopic(string Doc_Id)
        {

            DataSet ds = new DataSet();

            string sql = "GETDATA_MASTER_DATAINTOPIC";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_DOCUMENT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_DATAINTOPIC", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                    cmd.Parameters.Add("DOC_ID", OracleDbType.NVarchar2).Value = Doc_Id;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    ds.Tables[0].TableName = "document";
                    ds.Tables[1].TableName = "mastertopic";
                }
            }

            return ds;
        }


        public bool saveSortData(masterDataSort[] masterDataList)
        {
            using (var con = OpenOracleConnection())
            {
                con.Open();
                OracleTransaction trx = con.BeginTransaction(IsolationLevel.ReadCommitted);

                try
                {
                    foreach (var item in masterDataList)
                    {

                        string sql = String.Format("UPDATE DOCUMENT SET SORT = {0} WHERE ID = '{1}'",item.sort.ToString(),item.docId);

                        using (var cmd = new OracleCommand(sql, con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();
                        }

                    }

                    trx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                    trx.Rollback();
                    return false;
                }

            }

        }

        public bool saveMasterData(masterDataSave masterDataSave)
        {
            using (var con = OpenOracleConnection())
            {
              
                OracleTransaction trx = con.BeginTransaction(IsolationLevel.ReadCommitted);
                DataSet ds = new DataSet();
                try
                {
                    if (masterDataSave.doc_id == string.Empty || masterDataSave.doc_id == null)
                    {
                        //insert
                        var doc_id = getSYS_GUID();
                        masterDataSave.doc_id = doc_id;

                        string timestamp = String.Format("TO_TIMESTAMP('{0}', 'YYYY-MM-DD HH24:MI:SS.FF')", GetDateTimeString(DateTime.Now));
                        string P_DOCUMENT_ID =  masterDataSave.doc_id_parent == null ? "null" : "'"+masterDataSave.doc_id_parent+"'";
                        string P_ISUSE = masterDataSave.doc_isuse == true ? "T" : "F";

                        string sql_doc = string.Format("INSERT INTO DOCUMENT (ID,MASTER_SESSION_ID,DOCUMENT_ID,CREATE_BY,CREATE_DATE,ISUSE, SORT) "
                                                   +"VALUES( '{0}','{1}', {2}, '{3}', {4},'{5}',TO_NUMBER((SELECT COUNT(a.ID) + 1 SORT FROM DOCUMENT a WHERE a.MASTER_SESSION_ID = '{1}'))) ",
                                                   doc_id, masterDataSave.master_session_id, P_DOCUMENT_ID, masterDataSave.user_id, timestamp, P_ISUSE
                                                   );

                        using (var cmd = new OracleCommand(sql_doc, con))
                        {
                            cmd.CommandType = CommandType.Text;
                            var res= cmd.ExecuteNonQuery();

                           
                            foreach (var item in masterDataSave.topic_list_Data) {

                                string sql_data = string.Format("INSERT INTO DATA_TOPIC (DOCUMENT_ID, MASTER_TOPIC_ID,DATA_TOPIC,UPDATE_BY,UPDATE_DATE)"
                                                                +" VALUES( '{0}', '{1}','{2}','{3}',{4})"
                                                                , doc_id
                                                                , item.topicId
                                                                , item.asnwerData
                                                                , masterDataSave.user_id
                                                                , timestamp
                                                                );


                                    using (var cmd2 = new OracleCommand(sql_data, con))
                                    {
                                        cmd2.CommandType = CommandType.Text;
                                        cmd2.ExecuteNonQuery();
                                    }

                                }

                        }

                    }
                    else {
                        //update

                        string timestamp = String.Format("TO_TIMESTAMP('{0}', 'YYYY-MM-DD HH24:MI:SS.FF')", GetDateTimeString(DateTime.Now));
                        string P_DOCUMENT_ID = masterDataSave.doc_id_parent == null ? "null" : "'" + masterDataSave.doc_id_parent + "'";
                        string P_ISUSE = masterDataSave.doc_isuse == true ? "T" : "F";

                        string sql_doc = string.Format("UPDATE DOCUMENT SET ISUSE = '{0}' WHERE ID = '{1}'", P_ISUSE, masterDataSave.doc_id);

                        using (var cmd = new OracleCommand(sql_doc, con))
                        {
                            cmd.CommandType = CommandType.Text;
                            var res = cmd.ExecuteNonQuery();


                            foreach (var item in masterDataSave.topic_list_Data)
                            {

                                string sql_data = string.Format("UPDATE DATA_TOPIC SET DATA_TOPIC='{0}', UPDATE_BY ='{3}', UPDATE_DATE={4}  WHERE MASTER_TOPIC_ID='{1}' AND DOCUMENT_ID ='{2}' "
                                                                , item.asnwerData
                                                                , item.topicId
                                                                , masterDataSave.doc_id
                                                                , masterDataSave.user_id
                                                                , timestamp );


                                using (var cmd2 = new OracleCommand(sql_data, con))
                                {
                                    cmd2.CommandType = CommandType.Text;
                                    cmd2.ExecuteNonQuery();
                                }

                            }
                        }

                    }
                  

                    trx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                    trx.Rollback();
                    return false;
                }

            }


        }


        public static String GetDateTimeString(DateTime value)
        {
            return value.ToString("yyyy-MM-dd HH:mm:ss.ffff");
        }

        public DataSet getDataSubdistrictOffset(int limit, int offset, string keyword, string district)
        {

            string key = keyword ==null? "%%":"%" + keyword.ToLower().Replace(" ", "") + "%";
            district = district==null || district== "undefined" ? "%%":district;

            DataSet ds = new DataSet();
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand("GETDATA_SUBDISTRICT_OFFSET", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("LIMIT_DATA", OracleDbType.NVarchar2).Value = limit.ToString();
                    cmd.Parameters.Add("OFFSET_DATA", OracleDbType.NVarchar2).Value = offset.ToString();
                    cmd.Parameters.Add("KEYWORD", OracleDbType.NVarchar2).Value = key;
                    cmd.Parameters.Add("DISTRICT", OracleDbType.NVarchar2).Value = district;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                   

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    ds.Tables[0].TableName = "subdistrict";
                }
            }

            return ds;
        }

        public DataSet getDataDistrictAndProvince()
        {
           
            DataSet ds = new DataSet();
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand("GETDATA_DISTRICT_AND_PROVINCE", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_DISTRICT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_PROVINCE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    ds.Tables[0].TableName = "district";
                    ds.Tables[1].TableName = "province";
                }
            }

            return ds;
        }


        public string getSYS_GUID()
        {
            string id = string.Empty;
            string sql = "SELECT RAWTOHEX (SYS_GUID()) id FROM DUAL";
            DataSet ds = new DataSet();
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        id = dr["ID"].ToString();
                    }
                    return id;
                }
            }

        }


        public DataSet GetDataDepartment()
        {

            DataSet ds = new DataSet();

            string sql = "GETDATA_MASTER_DEPARTMENT";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    ds.Tables[0].TableName = "DATA_SELECT";
                }
            }

            return ds;
        }

    }
}
