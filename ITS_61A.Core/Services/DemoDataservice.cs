﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITS_61A.Core.Entities;

namespace ITS_61A.Core.Services
{
    public class DemoDataservice
    {
        public List<demoModel> demo()
        {
            List<demoModel> res = new List<demoModel>();
            for (var i = 0; i < 10; i++)
            {
                res.Add(new demoModel
                {
                    Name = "File-Name-" + i
                });
            }

            return res;
        }
    }
}
