﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITS_61A.Core.Entities;
using System.Data;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;
using Newtonsoft.Json;
using System.Globalization;
using ITS_61A.Core.Utils;
using ITS_61A.Core.ServiceDataModel.SystemManagement;

namespace ITS_61A.Core.Services
{
    public class SystemManagementDataservicecs : BaseService
    {

        private string Randompassword()
        {
            var rndText = new StringBuilder();
            int lengthpass = 8;
            const string text = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

            var rnd = new Random();
            while (0 < lengthpass--)
            {
                rndText.Append(text[rnd.Next(text.Length)]);
            }
            return rndText.ToString();
        }

        public bool SendEmailforChangepassword(string email)
        {

            bool isMessageSent = false;
            string mailTo = email;
            var passwordtext = Randompassword();

            DataSet ds = new DataSet();
            ds = getSMTP();

            var mail_user = ds.Tables[0].Rows[0]["USER_EMAIL"].ToString();
            var massage_mail = ds.Tables[0].Rows[0]["MASSAGE_EMAIL"].ToString();
            var subject_mail = ds.Tables[0].Rows[0]["SUBJECT_EMAIL"].ToString();
            var pw_email = ds.Tables[0].Rows[0]["PASSWORD_EMAIL"].ToString();
            var host_mail = ds.Tables[0].Rows[0]["EMAIL_SERVER"].ToString();
            var port = ds.Tables[0].Rows[0]["PORT_SERVER"].ToString();

            if (string.IsNullOrEmpty(mailTo))
            {
                return isMessageSent = false;
            }
            //int failedLoginSendMail = userService.GetFailedLoginSendMail();
            var ciTh = new CultureInfo("th");
            var dt = DateTime.Now;
            var from = mail_user;
            var fromName = "LIS SYSTEM";
            var subject = subject_mail;
            string body = string.Format(@"{0}<br>{1}</br>", massage_mail, passwordtext);
            try
            {
                SendMail.Instance.SendEmail(
                from,
                fromName,
                mailTo,
                subject,
                body,
                pw_email,
                host_mail,
                port
                );
                isMessageSent = true;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                isMessageSent = false;
            }
            return isMessageSent;
        }

        public DataSet getSMTP()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = string.Format("select * from IDENTITY_SMTP");

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }


        public DataSet GetdataIdentitySetPassword()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETDATA_IDENTITY_SET_PASSWORD";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }
        
        public DataSet GetdataInsertWorker(string id)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETDATA_IDENTITY_USER";

            using (var con = OpenOracleConnection())
            {
                var ID = id;
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_SELECT_GROUP", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("USER_ID", OracleDbType.NVarchar2).Value = ID; //ParameterDirection.Input;
                    //cmd.Parameters.Add("USER_ID", OracleDbType.NVarchar2).Value = "AA18BA7B99FF403FAFD8529B35F94370";
                    //cmd.Parameters.Add("ID_", OracleDbType.NVarchar2).Value = "73EB039BF2A6595AE05408002713F738";
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[""];
                    }
                }
            }

            return ds;
        }

        public DataSet GetdataFromMasterInsertWorker(string session_id, string topic_id)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETDATA_MASTER_DROPDOWN";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add("SESSION_ID", OracleDbType.NVarchar2).Value = "44CB0A25CB8A435CB48477DBFEF153F1";
                    //cmd.Parameters.Add("USERTOPIC_ID_ID", OracleDbType.NVarchar2).Value = "A0F1527332E14B20AD00308E0A749F81";
                    cmd.Parameters.Add("SESSION_ID", OracleDbType.NVarchar2).Value = session_id;
                    cmd.Parameters.Add("USERTOPIC_ID_ID", OracleDbType.NVarchar2).Value = topic_id;
                    //cmd.Parameters.Add("ID_", OracleDbType.NVarchar2).Value = "73EB039BF2A6595AE05408002713F738";
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }
        public DataSet GetdataIdentityGroup()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETDATA_IDENTITY_GROUP_LIST";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }

        public DataSet GetSettingConnectdatabase()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETDATA_IDENTITY_SET_SYSTEM";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }

        public DataSet GetDataIdentitySMTP()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETDATA_IDENTITY_SMTP";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }

        public DataSet getdataFromSettingAlertUser(string bigTopic, string startDate, string endDate, string notifier)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "SEARCHDATA_IDENTITY_ALERT_USER";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("S_MAIN", OracleDbType.NVarchar2).Value = bigTopic;
                    cmd.Parameters.Add("A_TIME", OracleDbType.NVarchar2).Value = startDate;
                    cmd.Parameters.Add("A_E_TIME", OracleDbType.NVarchar2).Value = endDate;
                    cmd.Parameters.Add("N_NAME", OracleDbType.NVarchar2).Value = notifier;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }
        public DataSet getdataForsearchUser(string PersonCode, string PersonName, string Username, string Office, int Status, string Position)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "SEARCHDATA_IDENTITY_USER";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("ID_CARD", OracleDbType.NVarchar2).Value = PersonCode;
                    cmd.Parameters.Add("NAME_LASTNAME", OracleDbType.NVarchar2).Value = PersonName;
                    cmd.Parameters.Add("USERNAME", OracleDbType.NVarchar2).Value = Username;
                    cmd.Parameters.Add("DEPARTMENT_ID", OracleDbType.NVarchar2).Value = Office;
                    cmd.Parameters.Add("CASE_SENT", OracleDbType.NVarchar2).Value = Status;
                    cmd.Parameters.Add("POSITION_ID", OracleDbType.NVarchar2).Value = Position;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }


        public DataSet getCriteriaforsearcUser()
        {

            DataSet ds = new DataSet();

            string sql = "GETCRITERIA_FOR_SEARCHUSER";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_SELECT_GROUP", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    //if (ds.Tables.Count > 0)
                    //{
                    //    dt = ds.Tables[""];
                    //}
                }
            }

            return ds;
        }


        public DataSet DeleteDataSettingAlertUser(string alert_id)
        {

            DataSet ds = new DataSet();


            string sql = "DELETEDATA_IDENTITY_ALERT_USER";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("ALERT_ID", OracleDbType.NVarchar2).Value = alert_id;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }

            return ds;
        }

        public DataSet save_SMTP(SMTP_dataModel SMTP, string userID)
        {
            DataSet ds = new DataSet();

            string sql = "UPDATE_IDENTITY_SMTP";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("ID", OracleDbType.NVarchar2).Value = SMTP.ID;
                    cmd.Parameters.Add("EMAIL_SERVER", OracleDbType.NVarchar2).Value = SMTP.EMAIL_SERVER;
                    cmd.Parameters.Add("PORT_SERVER", OracleDbType.NVarchar2).Value = SMTP.PORT_SERVER;
                    cmd.Parameters.Add("USER_EMAIL", OracleDbType.NVarchar2).Value = SMTP.USER_EMAIL;
                    cmd.Parameters.Add("PASSWORD_EMAIL", OracleDbType.NVarchar2).Value = SMTP.PASSWORD_EMAIL;
                    cmd.Parameters.Add("SUBJECT_EMAIL", OracleDbType.NVarchar2).Value = SMTP.SUBJECT_EMAIL;
                    cmd.Parameters.Add("MASSAGE_EMAIL", OracleDbType.NVarchar2).Value = SMTP.MASSAGE_EMAIL;
                    cmd.Parameters.Add("SUBJECT_OTP", OracleDbType.NVarchar2).Value = SMTP.SUBJECT_OTP;
                    cmd.Parameters.Add("MASSAGE_OTP", OracleDbType.NVarchar2).Value = SMTP.MASSAGE_OTP;
                    cmd.Parameters.Add("UPDATE_BY", OracleDbType.NVarchar2).Value = userID;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }
            return ds;
        }

        public DataSet setting_password_dataModel(setting_password_dataModel SPD, string userID)
        {
            DataSet ds = new DataSet();

            string sql = "UPDATE_IDENTITY_SET_PASSWORD";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("ID", OracleDbType.NVarchar2).Value = SPD.ID;
                    cmd.Parameters.Add("AGE_DATE", OracleDbType.Int32).Value = Convert.ToInt32(SPD.AGE_DATE);
                    cmd.Parameters.Add("CHECK_PASS", OracleDbType.Int32).Value = Convert.ToInt32(SPD.CHECK_PASS);
                    cmd.Parameters.Add("UNLOCK_TIME", OracleDbType.Int32).Value = Convert.ToInt32(SPD.UNLOCK_TIME);
                    cmd.Parameters.Add("DONT_PASS_OLD", OracleDbType.Int32).Value = Convert.ToInt32(SPD.DONT_PASS_OLD);
                    cmd.Parameters.Add("MIN_CHARACTER", OracleDbType.Int32).Value = Convert.ToInt32(SPD.MIN_CHARACTER);
                    cmd.Parameters.Add("MAX_CHARACTER", OracleDbType.Int32).Value = Convert.ToInt32(SPD.MAX_CHARACTER);
                    cmd.Parameters.Add("SAME_CHARACTER", OracleDbType.Int32).Value = Convert.ToInt32(SPD.SAME_CHARACTER);
                    cmd.Parameters.Add("UPPER_CASE", OracleDbType.Int32).Value = Convert.ToInt32(SPD.UPPER_CASE);
                    cmd.Parameters.Add("LOWER_CASE", OracleDbType.Int32).Value = Convert.ToInt32(SPD.LOWER_CASE);
                    cmd.Parameters.Add("INPUT_NUMBER", OracleDbType.Int32).Value = Convert.ToInt32(SPD.INPUT_NUMBER);
                    cmd.Parameters.Add("SPACIAL_CHARACTER", OracleDbType.Int32).Value = Convert.ToInt32(SPD.SPACIAL_CHARACTER);
                    cmd.Parameters.Add("USER_PASS_NAME", OracleDbType.Int32).Value = Convert.ToInt32(SPD.USER_PASS_NAME);
                    cmd.Parameters.Add("ALERT_EXPIRE", OracleDbType.Int32).Value = Convert.ToInt32(SPD.ALERT_EXPIRE);
                    cmd.Parameters.Add("AGE_OTP", OracleDbType.Int32).Value = Convert.ToInt32(SPD.AGE_OTP);
                    cmd.Parameters.Add("SESSION_TIMEOUT", OracleDbType.Int32).Value = Convert.ToInt32(SPD.SESSION_TIMEOUT);
                    cmd.Parameters.Add("UPDATE_BY", OracleDbType.NVarchar2).Value = userID;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }
            return ds;
        }

        public DataSet setting_connectDatabase_dataModel(setting_connectDatabase_dataModel SCDB, string userID)
        {
            DataSet ds = new DataSet();

            string sql = "UPDATE_IDENTITY_SET_SYSTEM";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("ID", OracleDbType.NVarchar2).Value = SCDB.ID;
                    cmd.Parameters.Add("HOSTNAME_IP", OracleDbType.NVarchar2).Value = SCDB.HOSTNAME_IP;
                    cmd.Parameters.Add("PORT_SERVER", OracleDbType.NVarchar2).Value = SCDB.PORT_SERVER;
                    cmd.Parameters.Add("SERVICE_NAME", OracleDbType.NVarchar2).Value = SCDB.SERVICE_NAME;
                    cmd.Parameters.Add("ISSID", OracleDbType.NVarchar2).Value = SCDB.ISSID;
                    cmd.Parameters.Add("SET_USER_NAME", OracleDbType.NVarchar2).Value = SCDB.SET_USER_NAME;
                    cmd.Parameters.Add("SET_PASSWORD", OracleDbType.NVarchar2).Value = SCDB.SET_PASSWORD;
                    cmd.Parameters.Add("DATABASE_NAME", OracleDbType.NVarchar2).Value = SCDB.DATABASE_NAME;
                    cmd.Parameters.Add("UPDATE_BY", OracleDbType.NVarchar2).Value = userID;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }
            return ds;
        }

        public DataSet update_worker_dataModel(update_worker_dataModel UPDW, string userID)
        {
            DataSet ds = new DataSet();

            string sql = "UPDATE_IDENTITY_USER";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("ID", OracleDbType.NVarchar2).Value = UPDW.ID;
                    cmd.Parameters.Add("IDENTITY_USER_TYPE_ID", OracleDbType.NVarchar2).Value = UPDW.IDENTITY_USER_TYPE_ID;
                    cmd.Parameters.Add("EMPLOYEE_CODE", OracleDbType.NVarchar2).Value = UPDW.EMPLOYEE_CODE;
                    cmd.Parameters.Add("ID_CARD", OracleDbType.Int32).Value = Convert.ToInt64(UPDW.ID_CARD);
                    cmd.Parameters.Add("PREFIX_ID", OracleDbType.NVarchar2).Value = UPDW.PREFIX_ID;
                    cmd.Parameters.Add("NAME_TH", OracleDbType.NVarchar2).Value = UPDW.NAME_TH;
                    cmd.Parameters.Add("LASTNAME_TH", OracleDbType.NVarchar2).Value = UPDW.LASTNAME_TH;
                    cmd.Parameters.Add("USER_CODE", OracleDbType.NVarchar2).Value = UPDW.USER_CODE;
                    cmd.Parameters.Add("POSITION_ID", OracleDbType.NVarchar2).Value = UPDW.POSITION_ID;
                    cmd.Parameters.Add("DEPARTMENT_ID", OracleDbType.NVarchar2).Value = UPDW.DEPARTMENT_ID;
                    cmd.Parameters.Add("MANAGEMENT_POSITION_ID", OracleDbType.NVarchar2).Value = UPDW.MANAGEMENT_POSITION_ID;
                    cmd.Parameters.Add("LEVEL_USER", OracleDbType.NVarchar2).Value = UPDW.LEVEL_USER;
                    cmd.Parameters.Add("WORK_START_DATE", OracleDbType.NVarchar2).Value = UPDW.WORK_START_DATE;
                    cmd.Parameters.Add("WORK_END_DATE", OracleDbType.NVarchar2).Value = UPDW.WORK_END_DATE;
                    cmd.Parameters.Add("COUNT_NUM_LOCK", OracleDbType.Int32).Value = Convert.ToInt32(UPDW.COUNT_NUM_LOCK);
                    cmd.Parameters.Add("NUM_LOCK_DATE", OracleDbType.NVarchar2).Value = UPDW.NUM_LOCK_DATE;
                    cmd.Parameters.Add("OTP_NUMBER", OracleDbType.NVarchar2).Value = UPDW.OTP_NUMBER;
                    cmd.Parameters.Add("OTP_DATE", OracleDbType.NVarchar2).Value = UPDW.OTP_DATE;
                    cmd.Parameters.Add("UPDATE_BY", OracleDbType.NVarchar2).Value = userID;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }
            return ds;
        }
        public DataSet getIdentitySetSystemById(string ID)
        {
            DataSet ds = new DataSet();

            string sql = string.Format("SELECT RAWTOHEX(ID) ID,HOSTNAME_IP,PORT_SERVER,SERVICE_NAME,ISSID,SET_USER_NAME,SET_PASSWORD, DATABASE_NAME,SYSTEM_TYPE_ID,HEADER_NAME FROM IDENTITY_SET_SYSTEM WHERE ID = '{0}'", ID);

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }
            return ds;
        }

        public DataSet GetdataIdentityAlertUser(string id)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETDATA_IDENTITY_ALERT_USER";

            using (var con = OpenOracleConnection())
            {
                var ID = id;
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("ALERT_ID", OracleDbType.NVarchar2).Value = ID;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[""];
                    }
                }
                return ds;
            }
        }

        ///test post
        ///
        public bool saveNotificationMessage(notificationMessage notificationMessage)
        {
            using (var con = OpenOracleConnection())
            {

                OracleTransaction trx = con.BeginTransaction(IsolationLevel.ReadCommitted);
                DataSet ds = new DataSet();
                try
                {
                    if (true) /// w8 for paramitor, change to else  //(masterDataSave.doc_id == string.Empty || masterDataSave.doc_id == null)
                    {
                        string id = new MasterDataManagementDataservice().getSYS_GUID();
                        string timestamp = String.Format("TO_TIMESTAMP('{0}', 'YYYY-MM-DD HH24:MI:SS.FF')", "2018-08-30 11:04:01.629952900");
                        string sql_data = string.Format("INSERT INTO IDENTITY_ALERT_USER (ID,ALERT_END_TIME, ALERT_MASSAGE, ALERT_START_TIME, NOTIFIER_NAME, SUBJECT_MAIN, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, SORT)"
                                                        + " VALUES( '{10}',{0}, '{1}', {2}, '{3}', '{4}', '{5}', {6}, '{7}', {8}, '{9}')"
                                                        , notificationMessage.ALERT_END_TIME,
                                                           //timestamp,
                                                           notificationMessage.ALERT_MASSAGE,
                                                           notificationMessage.ALERT_START_TIME,
                                                           //timestamp,
                                                           notificationMessage.NOTIFIER_NAME,
                                                           notificationMessage.SUBJECT_MAIN,
                                                           notificationMessage.CREATE_BY,
                                                           notificationMessage.CREATE_DATE,
                                                           //timestamp,
                                                           notificationMessage.UPDATE_BY,
                                                           notificationMessage.UPDATE_DATE,
                                                           //timestamp,
                                                           notificationMessage.SORT,
                                                           id
                                                        );
                        using (var cmd = new OracleCommand(sql_data, con))
                        {
                            cmd.CommandType = CommandType.Text;
                            var res = cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        /// update w8 for update

                        //string timestamp = String.Format("TO_TIMESTAMP('{0}', 'YYYY-MM-DD HH24:MI:SS.FF')", GetDateTimeString(DateTime.Now));
                        //string P_DOCUMENT_ID = masterDataSave.doc_id_parent == null ? "null" : "'" + masterDataSave.doc_id_parent + "'";
                        //string P_ISUSE = masterDataSave.doc_isuse == true ? "T" : "F";

                        //string sql_doc = string.Format("UPDATE DOCUMENT SET ISUSE = '{0}' WHERE ID = '{1}'", P_ISUSE, masterDataSave.doc_id);

                        //using (var cmd = new OracleCommand(sql_doc, con))
                        //{
                        //    cmd.CommandType = CommandType.Text;
                        //    var res = cmd.ExecuteNonQuery();


                        //    foreach (var item in masterDataSave.topic_list_Data)
                        //    {

                        //        string sql_data = string.Format("UPDATE DATA_TOPIC SET DATA_TOPIC='{0}', UPDATE_BY ='{3}', UPDATE_DATE={4}  WHERE MASTER_TOPIC_ID='{1}' AND DOCUMENT_ID ='{2}' "
                        //                                        , item.asnwerData
                        //                                        , item.topicId
                        //                                        , masterDataSave.doc_id
                        //                                        , masterDataSave.user_id
                        //                                        , timestamp);


                        //        using (var cmd2 = new OracleCommand(sql_data, con))
                        //        {
                        //            cmd2.CommandType = CommandType.Text;
                        //            cmd2.ExecuteNonQuery();
                        //        }

                        //    }
                    }


                    trx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                    trx.Rollback();
                    return false;
                }

            }


        }


        public DataSet InsertWorkerSave(insertWorkerMessage insertWorkerMessage)
        {

            DataSet ds = new DataSet();

            string sql = "INSERT_IDENTITY_USER";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("IDENTITY_USER_TYPE_ID", OracleDbType.NVarchar2).Value = insertWorkerMessage.IDENTITY_USER_TYPE_ID;
                    cmd.Parameters.Add("EMPLOYEE_CODE", OracleDbType.NVarchar2).Value = insertWorkerMessage.EMPLOYEE_CODE;
                    cmd.Parameters.Add("ID_CARD", OracleDbType.Int32).Value = insertWorkerMessage.ID_CARD;//Convert.ToInt32(insertWorkerMessage.ID_CARD);
                    cmd.Parameters.Add("PREFIX_ID", OracleDbType.NVarchar2).Value = insertWorkerMessage.PREFIX_ID;
                    cmd.Parameters.Add("NAME_TH", OracleDbType.NVarchar2).Value = insertWorkerMessage.NAME_TH;
                    cmd.Parameters.Add("LASTNAME_TH", OracleDbType.NVarchar2).Value = insertWorkerMessage.LASTNAME_TH;
                    cmd.Parameters.Add("PASSWORD_USER", OracleDbType.NVarchar2).Value = insertWorkerMessage.PASSWORD_USER;
                    cmd.Parameters.Add("PASSWORD_USER_DATE", OracleDbType.NVarchar2).Value = insertWorkerMessage.PASSWORD_USER_DATE;
                    cmd.Parameters.Add("IS_GENERATE_PASSWORD", OracleDbType.NVarchar2).Value = insertWorkerMessage.IS_GENERATE_PASSWORD;
                    cmd.Parameters.Add("USER_CODE", OracleDbType.NVarchar2).Value = insertWorkerMessage.USER_CODE;
                    cmd.Parameters.Add("POSITION_ID", OracleDbType.NVarchar2).Value = insertWorkerMessage.POSITION_ID;
                    cmd.Parameters.Add("DEPARTMENT_ID", OracleDbType.NVarchar2).Value = insertWorkerMessage.DEPARTMENT_ID;
                    cmd.Parameters.Add("MANAGEMENT_POSITION_ID", OracleDbType.NVarchar2).Value = insertWorkerMessage.MANAGEMENT_POSITION_ID;
                    cmd.Parameters.Add("LEVEL_USER", OracleDbType.NVarchar2).Value = insertWorkerMessage.LEVEL_USER;
                    cmd.Parameters.Add("EMAIL", OracleDbType.NVarchar2).Value = insertWorkerMessage.EMAIL;
                    cmd.Parameters.Add("WORK_START_DATE", OracleDbType.NVarchar2).Value = insertWorkerMessage.WORK_START_DATE;
                    cmd.Parameters.Add("WORK_END_DATE", OracleDbType.NVarchar2).Value = insertWorkerMessage.WORK_END_DATE;
                    cmd.Parameters.Add("COUNT_NUM_LOCK", OracleDbType.Int32).Value = insertWorkerMessage.COUNT_NUM_LOCK;//Convert.ToInt32(insertWorkerMessage.COUNT_NUM_LOCK);
                    cmd.Parameters.Add("NUM_LOCK_DATE", OracleDbType.NVarchar2).Value = insertWorkerMessage.NUM_LOCK_DATE;
                    cmd.Parameters.Add("OTP_NUMBER", OracleDbType.NVarchar2).Value = insertWorkerMessage.OTP_NUMBER;
                    cmd.Parameters.Add("OTP_DATE", OracleDbType.NVarchar2).Value = insertWorkerMessage.OTP_DATE;
                    cmd.Parameters.Add("CREATE_BY", OracleDbType.NVarchar2).Value = insertWorkerMessage.CREATE_BY;
                    //cmd.Parameters.Add("CREATE_DATE", OracleDbType.NVarchar2).Value = insertWorkerMessage.CREATE_DATE;

                    var da = new OracleDataAdapter();

                    da.SelectCommand = cmd;

                    da.Fill(ds);
                }
                return ds;
            }
            
        }
        
        public DataSet GetDataSortAlertUser()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETDATA_SORT_ALERT_USER";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }

        public DataSet GetdataGroupPrivilege(string groupName)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "SEARCHDATA_IDENTITY_GROUP";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("GROUP_NAME_SEARCH", OracleDbType.NVarchar2).Value = groupName; //ParameterDirection.Input;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }

        public DataSet DeleteDataGroupPrivilege(string group_id, string group_update_by)
        {

            DataSet ds = new DataSet();

            string sql = "DELETEDATA_IDENTITY_GROUP";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("GROUP_ID", OracleDbType.NVarchar2).Value = group_id;
                    cmd.Parameters.Add("GROUP_UPDATE_BY", OracleDbType.NVarchar2).Value = group_update_by;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }

            return ds;
        }

        public DataSet GetdataLogStatusPerson(string Name, string startDateFrom, string startDateTo)
        {
            
            DataSet ds = new DataSet();
         
            string sql = "GETDATA_LOG_STATUS";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("NAME_INPUT", OracleDbType.NVarchar2).Value = Name=="undefined"?null:Name;
                    cmd.Parameters.Add("DATE_FORM", OracleDbType.NVarchar2).Value = startDateFrom== "NaN-undefined-NaN"?null: startDateFrom;
                    cmd.Parameters.Add("DATE_TO", OracleDbType.NVarchar2).Value = startDateTo == "NaN-undefined-NaN" ? null : startDateTo;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    //ds.Tables[0].TableName = "listLog";
                }
                return ds;
            }
        }

        public DataSet GetdataLogByDate(string startDateFrom, string startDateTo)
        {
            
            DataSet ds = new DataSet();


            string sql = "GETDATA_LOG_BYDATE";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    //cmd.Parameters.Add("DATE_FORM", OracleDbType.NVarchar2).Value = startDateFrom;
                    cmd.Parameters.Add("DATE_FORM", OracleDbType.NVarchar2).Value = startDateFrom == "NaN-undefined-NaN" ? null : startDateFrom;
                    //cmd.Parameters.Add("DATE_TO", OracleDbType.NVarchar2).Value = startDateTo;
                    cmd.Parameters.Add("DATE_TO", OracleDbType.NVarchar2).Value = startDateTo == "NaN-undefined-NaN" ? null : startDateTo;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    //ds.Tables[0].TableName = "listLog";
                }
                return ds;
            }
        }

        
        public DataSet GetdataLogByUser(string id, string DateFrom, string DateTo)
        {
            DataSet ds = new DataSet();
            string sql = "GETDATA_LOG_BYUSER";
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("userID", OracleDbType.NVarchar2).Value = id;                    
                    cmd.Parameters.Add("date_from", OracleDbType.NVarchar2).Value = DateFrom == "NaN-undefined-NaN" ? null : DateFrom;                    
                    cmd.Parameters.Add("date_to", OracleDbType.NVarchar2).Value = DateTo == "NaN-undefined-NaN" ? null : DateTo;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    //ds.Tables[0].TableName = "listLog";
                }
                return ds;
            }
        }

        public DataSet GetdataUser(string Name, string Username, string SelectDepartment)
        {
            DataSet ds = new DataSet();
            string sql = "GETDATA_USER";
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_USER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("NAME_LASTNAME", OracleDbType.NVarchar2).Value = Name;
                    cmd.Parameters.Add("USERNAME", OracleDbType.NVarchar2).Value = Username;
                    cmd.Parameters.Add("DEPARTMENT_ID_SELECT", OracleDbType.NVarchar2).Value = SelectDepartment;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    
                }
                return ds;
            }
        }

        public DataSet getCriteriaforsearcPurchasingProject()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETCRITERIA_FOR_P_PROJECT";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_SELECT_GROUP", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }

            return ds;
        }
    }
}
