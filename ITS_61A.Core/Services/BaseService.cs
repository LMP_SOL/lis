﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.Services
{
    public abstract class BaseService
    {
        protected virtual SqlConnection SqlConnection(string connectionString)
        {
            //if (ConfigurationManager.ConnectionStrings[connectionName] == null)
            //{
            //    throw new Exception($"Connection string : {connectionName} not found.");
            //}
            //var connStr = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            //var connectionString = "Data Source=203.170.145.63; User ID=sa; password=ITS_Research@01;Initial Catalog=61A_LIS_HRIS;Integrated Security=false; MultipleActiveResultSets=true;";
           
            var conn = new SqlConnection(connectionString);
            if (conn.State != System.Data.ConnectionState.Open)
            {
                conn.Open();
            }
            return conn;
        }

        protected virtual OracleConnection OracleConnection(string connectionName)
        {
            if (ConfigurationManager.ConnectionStrings[connectionName] == null)
            {
                throw new Exception($"Connection string : {connectionName} not found.");
            }
            var connStr = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
          

            var conn = new OracleConnection(connStr);
            if (conn.State != System.Data.ConnectionState.Open)
            {
                conn.Open();
            }
            return conn;
        }


        public SqlConnection OpenSqlConnection(string connStr)
        {
            return SqlConnection(connStr);
        }
        public OracleConnection OpenOracleConnection()
        {
            return OracleConnection("LISConnection");
        }

    }
}
