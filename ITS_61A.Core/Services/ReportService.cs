﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.Services
{
    public class ReportService : BaseService
    {
        public DataSet getDataProvince()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            var sql = String.Format(@"SELECT * FROM MASTER_PROVINCE WHERE ISUSE = 'T'");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }

        public DataSet getDataDistrict()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            var sql = String.Format(@"SELECT * FROM MASTER_DISTRICT WHERE ISUSE = 'T'");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }

        public DataSet getDataSubdistrict()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            var sql = String.Format(@"SELECT * FROM MASTER_SUBDISTRICT WHERE ISUSE = 'T'");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }

        public DataSet getDataTAX_RECEIVE_DEPARTMENT_ID()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            var sql = String.Format(@"SELECT TAX_RECEIVE_DEPARTMENT_ID FROM P_TAX_LAND");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }

        public DataSet getDataOffersalename()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            var sql = String.Format(@"SELECT OFFER_SALE_NAME FROM P_ANNOUNCEMENT_LAND");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }

        //public DataSet getDataANNUM()
        //{
        //    DataSet ds = new DataSet();
        //    DataTable dt = new DataTable();

        //    var sql = String.Format(@"SELECT ANNUM FROM P_TAX_LAND");

        //    using (var con = OpenOracleConnection())
        //    {
        //        using (var cmd = new OracleCommand(sql, con))
        //        {
        //            cmd.CommandType = CommandType.Text;
        //            var da = new OracleDataAdapter();
        //            da.SelectCommand = cmd;
        //            da.Fill(ds);
        //            if (ds.Tables.Count > 0)
        //            {
        //                dt = ds.Tables[0];
        //            }
        //        }
        //    }
        //    return ds;
        //}

        public DataSet getDataDeedID()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            var sql = String.Format(@"SELECT DEED_ID FROM D_DEED");

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }

        public DataSet getDataParcelNO()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            var sql = String.Format(@"SELECT PARCEL_NO FROM P_ANNOUNCEMENT_LAND_DEED");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }

        public DataSet getDataAnnoucementNo()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            var sql = String.Format(@"SELECT ANNOUNCEMENT_NO FROM P_ANNOUNCEMENT_LAND");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }

        public DataSet getDataREGISTRY()
        {
            DataSet ds = new DataSet();
           

            var sql = String.Format(@"SELECT RAWTOHEX(d.ID) as ID, d.REGISTRY_NO FROM p_use_land d");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
          
                }
            }
            return ds;
        }
        public DataSet getDatadocument_id()
        {
            DataSet ds = new DataSet();


            var sql = String.Format(@"SELECT * FROM DATA_TOPIC WHERE MASTER_TOPIC_ID = '098B5A17EDAA47A9ADA5274861668401' ");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }
            return ds;
        }

        public DataSet getDataPROJECT_ID()
        {
            DataSet ds = new DataSet();


            var sql = String.Format(@"SELECT pc.PROJECT_ID FROM P_COST_LAND pc ");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }
            return ds;
        }

        public DataSet getDataMAINTENANCE_DEPARTMENT_ID()
        {
            DataSet ds = new DataSet();


            var sql = String.Format(@"SELECT * FROM P_COST_LAND ");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }
            return ds;
        }

        public DataSet getDataSINGLEREGISTRY()
        {
            DataSet ds = new DataSet();


            var sql = String.Format(@"SELECT * FROM p_use_land ");


            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                }
            }
            return ds;
        }

        public DataSet GetDataForRP201(string examID)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            var sql = String.Format(@"SELECT RAWTOHEX(a.ID) AS mastersession_id 
                ,a.MASTER_SESSION_NAME AS mastersession_name ,a.SORT AS sort FROM MASTER_SESSION  a 
                WHERE a.MASTER_MENU_ID = '{0}' ORDER BY a.SORT", examID);

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }
            return ds;
        }
        public DataSet getDataIntab(string menuId, string sessionId)
        {
            DataSet ds = new DataSet();

            string sql = "GETDATA_MASTER_INTAB";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SESSION", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_MASSTERTOPIC", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_TOPIC", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_TABLE", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("MENU_ID", OracleDbType.NVarchar2).Value = menuId;
                    cmd.Parameters.Add("SESSION_ID", OracleDbType.NVarchar2).Value = sessionId;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }

            return ds;
        }
        public DataSet GetDataRT103(
            string PROVINCE_ID , 
            string REGISTRY_NO , 
            string SINGLE_REGISTRY , 
            string LANDLORD_DEPARTMENT, 
            string CONTRACT_FROM_DATE_ST , 
            string CONTRACT_FROM_DATE_ED ,
            string CONTRACT_TO_DATE_ST ,
            string CONTRACT_TO_DATE_ED,
            string document_id)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "RP_103";
            document_id = (document_id == "0" || document_id == "undefined" || document_id == null) ? "" : document_id;
            //CONTRACT_FROM_DATE_ST = (CONTRACT_FROM_DATE_ST == null) ? null : CONTRACT_FROM_DATE_ST;
            //CONTRACT_FROM_DATE_ED = (CONTRACT_FROM_DATE_ED == null) ? null : CONTRACT_FROM_DATE_ED;
            //CONTRACT_TO_DATE_ST = (CONTRACT_TO_DATE_ST == null) ? null : CONTRACT_TO_DATE_ST;
            //CONTRACT_TO_DATE_ED = (CONTRACT_TO_DATE_ED == null) ? null : CONTRACT_TO_DATE_ED;
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("PROVINCE_ID", OracleDbType.NVarchar2).Value = PROVINCE_ID;
                    //cmd.Parameters.Add("DEED_NO", OracleDbType.NVarchar2).Value = DEED_NO;
                    cmd.Parameters.Add("REGISTRY_NO", OracleDbType.NVarchar2).Value = REGISTRY_NO;
                    cmd.Parameters.Add("SINGLE_REGISTRY", OracleDbType.NVarchar2).Value = SINGLE_REGISTRY;
                    cmd.Parameters.Add("LANDLORD_DEPARTMENT", OracleDbType.NVarchar2).Value = LANDLORD_DEPARTMENT;
                    //cmd.Parameters.Add("CONTRACT_AREA_RAI", OracleDbType.NVarchar2).Value = CONTRACT_AREA_RAI;
                    //cmd.Parameters.Add("CONTRACT_AREA_NGAN", OracleDbType.NVarchar2).Value = CONTRACT_AREA_NGAN;
                    //cmd.Parameters.Add("CONTRACT_AREA_wa", OracleDbType.NVarchar2).Value = CONTRACT_AREA_wa;
                    cmd.Parameters.Add("CONTRACT_FROM_DATE_ST", OracleDbType.NVarchar2).Value = CONTRACT_FROM_DATE_ST;
                    cmd.Parameters.Add("CONTRACT_FROM_DATE_ED", OracleDbType.NVarchar2).Value = CONTRACT_FROM_DATE_ED;
                    cmd.Parameters.Add("CONTRACT_TO_DATE_ST", OracleDbType.NVarchar2).Value = CONTRACT_TO_DATE_ST;
                    cmd.Parameters.Add("CONTRACT_TO_DATE_ED", OracleDbType.NVarchar2).Value = CONTRACT_TO_DATE_ED;
                    //cmd.Parameters.Add("LANDLORD_DEPARTMENT", OracleDbType.NVarchar2).Value = CONTRACT_TO_DATE;
                    //cmd.Parameters.Add("LANDLORD_DEPARTMENT", OracleDbType.NVarchar2).Value = "2";
                    //cmd.Parameters.Add("ORG_NAME", OracleDbType.NVarchar2).Value = ORG_NAME;
                    cmd.Parameters.Add("document_id", OracleDbType.NVarchar2).Value = document_id;
                    //cmd.Parameters.Add("COST_OF_WA_FOR_RENT", OracleDbType.NVarchar2).Value = COST_OF_WA_FOR_RENT;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }
            return ds;
        }

        //public DataSet GetDataRT104
        //    (
        //        string PROJECT_CODE_ID_IN,
        //        string PROJECT_NAME_IN,
        //        string PROVINCE_ID,
        //        string OFFER_SALE_NAME_ID,
        //        string AGENCY_RESPONSIBLE_ID,
        //        string AGENCY_PURCHASE_ID
        //    )
        //{
        //    DataSet ds = new DataSet();
        //    DataTable dt = new DataTable();

        //    string sql = "RP_104";

        //    using (var con = OpenOracleConnection())
        //    {
        //        using (var cmd = new OracleCommand(sql, con))
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.Add("REPORT_OUT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
        //            cmd.Parameters.Add("PROJECT_CODE_ID_IN", OracleDbType.NVarchar2).Value = PROJECT_CODE_ID_IN;
        //            cmd.Parameters.Add("PROJECT_NAME_IN", OracleDbType.NVarchar2).Value = PROJECT_NAME_IN;
        //            cmd.Parameters.Add("PROVINCE_ID", OracleDbType.NVarchar2).Value = PROVINCE_ID;
        //            cmd.Parameters.Add("OFFER_SALE_NAME_ID", OracleDbType.NVarchar2).Value = OFFER_SALE_NAME_ID;
        //            cmd.Parameters.Add("AGENCY_RESPONSIBLE_ID", OracleDbType.NVarchar2).Value = AGENCY_RESPONSIBLE_ID;
        //            cmd.Parameters.Add("AGENCY_PURCHASE_ID", OracleDbType.NVarchar2).Value = AGENCY_PURCHASE_ID;
        //            var da = new OracleDataAdapter();
        //            da.SelectCommand = cmd;
        //            da.Fill(ds);
        //        }
        //    }
        //    return ds;
        //}

        public DataSet GetDataRT105(string PROVINCE_ID, string PROJECT_ID, string DEED_ID, string TO_DEED_ID, string ANNUM/*, string TAX_RECEIVE_DEPARTMENT_ID*/)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //PROVINCE_ID = (PROVINCE_ID == "undefined" || PROVINCE_ID == null) ? "" : PROVINCE_ID;

            string sql = "RP_105";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("PROVINCE_ID", OracleDbType.NVarchar2).Value = PROVINCE_ID;
                    cmd.Parameters.Add("ANNUM", OracleDbType.NVarchar2).Value = ANNUM;
                    cmd.Parameters.Add("PROJECT_ID", OracleDbType.NVarchar2).Value = PROJECT_ID;
                    cmd.Parameters.Add("DEED_ID", OracleDbType.NVarchar2).Value = DEED_ID;
                    cmd.Parameters.Add("TO_DEED_ID", OracleDbType.NVarchar2).Value = TO_DEED_ID;
                    //cmd.Parameters.Add("TAX_RECEIVE_DEPARTMENT_ID", OracleDbType.NVarchar2).Value = TAX_RECEIVE_DEPARTMENT_ID;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }
            return ds;
        }
        public DataSet GetDataRT106
            (
                string PROVINCE_ID, 
                string DISTRICT_ID, 
                string SUBDISTRICT_ID, 
                string OFFER_SALE_NAME, 
                string DEED_NO,
                string ToDEED_NO,
                string PARCEL_NO, 
                string ANNOUNCEMENT_NO,
                string OFFER_SALE_DATE_ST,
                string OFFER_SALE_DATE_ED
            )
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "RP_106";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("PROVINCE_ID", OracleDbType.NVarchar2).Value = PROVINCE_ID;
                    cmd.Parameters.Add("DISTRICT_ID", OracleDbType.NVarchar2).Value = DISTRICT_ID;
                    cmd.Parameters.Add("SUBDISTRICT_ID", OracleDbType.NVarchar2).Value = SUBDISTRICT_ID;
                    cmd.Parameters.Add("OFFER_SALE_DATE_ST", OracleDbType.NVarchar2).Value = OFFER_SALE_DATE_ST;
                    cmd.Parameters.Add("OFFER_SALE_DATE_ED", OracleDbType.NVarchar2).Value = OFFER_SALE_DATE_ED;
                    cmd.Parameters.Add("OFFER_SALE_NAME", OracleDbType.NVarchar2).Value = OFFER_SALE_NAME;
                    //cmd.Parameters.Add("OFFER_SALE_ADDRESS", OracleDbType.NVarchar2).Value = OFFER_SALE_ADDRESS;
                    //cmd.Parameters.Add("SALE_LAND_RAI", OracleDbType.NVarchar2).Value = SALE_LAND_RAI;
                    //cmd.Parameters.Add("SALE_LAND_NGAN", OracleDbType.NVarchar2).Value = SALE_LAND_NGAN;
                    //cmd.Parameters.Add("SALE_LAND_WA", OracleDbType.NVarchar2).Value = SALE_LAND_WA;
                    //cmd.Parameters.Add("SUBDISTRICT_NAME_TH", OracleDbType.NVarchar2).Value = SUBDISTRICT_NAME_TH;
                    //cmd.Parameters.Add("SALE_AMOUNT_OF_RAI", OracleDbType.NVarchar2).Value = SALE_AMOUNT_OF_RAI;
                    //cmd.Parameters.Add("DISTRICT_NAME_TH", OracleDbType.NVarchar2).Value = DISTRICT_NAME_TH;
                    //cmd.Parameters.Add("PROVINCE_NAME_TH", OracleDbType.NVarchar2).Value = PROVINCE_NAME_TH;
                    cmd.Parameters.Add("DEED_NO", OracleDbType.NVarchar2).Value = DEED_NO;
                    cmd.Parameters.Add("ToDEED_NO", OracleDbType.NVarchar2).Value = ToDEED_NO;
                    cmd.Parameters.Add("PARCEL_NO", OracleDbType.NVarchar2).Value = PARCEL_NO;
                    cmd.Parameters.Add("ANNOUNCEMENT_NO", OracleDbType.NVarchar2).Value = ANNOUNCEMENT_NO;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }
            return ds;
        }
        public DataSet GetDataRT107(
            string PROVINCE_ID,
            string MAINTENANCE_DEPARTMENT_ID,
            string PROJECT_ID,
            string LAND_ADMINISTRATOR,
            string CONTRACT_START_DATE_ST,
            string CONTRACT_START_DATE_ED,
            string CONTRACT_NO, 
            string DEED_TO_NO, 
            string DEED_NO)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "RP_107";
            //document_id = (document_id == "0" || document_id == "undefined" || document_id == null) ? "" : document_id;
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("MAINTENANCE_DEPARTMENT_ID", OracleDbType.NVarchar2).Value = MAINTENANCE_DEPARTMENT_ID;
                    cmd.Parameters.Add("PROJECT_ID", OracleDbType.NVarchar2).Value = PROJECT_ID;
                    cmd.Parameters.Add("LAND_ADMINISTRATOR", OracleDbType.NVarchar2).Value = LAND_ADMINISTRATOR;
                    cmd.Parameters.Add("CONTRACT_START_DATE_ST", OracleDbType.NVarchar2).Value = CONTRACT_START_DATE_ST;
                    cmd.Parameters.Add("CONTRACT_START_DATE_ED", OracleDbType.NVarchar2).Value = CONTRACT_START_DATE_ED;
                    cmd.Parameters.Add("CONTRACT_NO", OracleDbType.NVarchar2).Value = CONTRACT_NO;
                    cmd.Parameters.Add("PROVINCE_ID", OracleDbType.NVarchar2).Value = PROVINCE_ID;
                    cmd.Parameters.Add("DEED_NO", OracleDbType.NVarchar2).Value = DEED_NO;
                    cmd.Parameters.Add("DEED_TO_NO", OracleDbType.NVarchar2).Value = DEED_TO_NO;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }
            return ds;
        }
        public DataSet GetDataRT108(
            string PROJECT_ID_IN,
            string PROJECT_NAME_IN,
            string PROJECT_PROVINCE_ID,
            string D_DEED_NO1,
            string D_DEED_NO2,
            string CONTRACT_NO_IN,
            string CONTRACT_DATE1_IN,
            string CONTRACT_DATE2_IN,
            string INFORMER_IN,
            string SUBJECT_NAME_IN)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "RP_108";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("PROJECT_OUT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("PROJECT_ID_IN", OracleDbType.NVarchar2).Value = PROJECT_ID_IN;
                    cmd.Parameters.Add("PROJECT_NAME_IN", OracleDbType.NVarchar2).Value = PROJECT_NAME_IN;
                    cmd.Parameters.Add("PROJECT_PROVINCE_ID", OracleDbType.NVarchar2).Value = PROJECT_PROVINCE_ID;
                    cmd.Parameters.Add("D_DEED_NO1", OracleDbType.NVarchar2).Value = D_DEED_NO1;
                    cmd.Parameters.Add("D_DEED_NO2", OracleDbType.NVarchar2).Value = D_DEED_NO2;
                    cmd.Parameters.Add("CONTRACT_NO_IN", OracleDbType.NVarchar2).Value = CONTRACT_NO_IN;
                    cmd.Parameters.Add("CONTRACT_DATE1_IN", OracleDbType.NVarchar2).Value = CONTRACT_DATE1_IN;
                    cmd.Parameters.Add("CONTRACT_DATE2_IN", OracleDbType.NVarchar2).Value = CONTRACT_DATE2_IN;
                    cmd.Parameters.Add("INFORMER_IN", OracleDbType.NVarchar2).Value = INFORMER_IN;
                    cmd.Parameters.Add("SUBJECT_NAME_IN", OracleDbType.NVarchar2).Value = SUBJECT_NAME_IN;

     

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }
            return ds;
        }
    }
}
