﻿using ITS_61A.Core.ServiceDataModel.LogDataModel;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.Services
{
    public class LogDataService : BaseService
    {
        public void saveLog(LisLogDataModel item)
        {
            DataSet ds = new DataSet();
            string sql = "INSERT_LOG";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("DATA_LOG_HOST", OracleDbType.NVarchar2).Value = item.LOG_HOST;
                    cmd.Parameters.Add("DATA_CLIENT_IP", OracleDbType.NVarchar2).Value = item.CLIENT_IP;
                    cmd.Parameters.Add("DATA_USER_AGENT_STRING", OracleDbType.NVarchar2).Value = item.USER_AGENT_STRING;
                    cmd.Parameters.Add("DATA_MENU_ID", OracleDbType.NVarchar2).Value = item.MENU_ID;
                    cmd.Parameters.Add("DATA_BUTTON_ID", OracleDbType.NVarchar2).Value = item.BUTTON_ID;
                    cmd.Parameters.Add("DATA_OBJECT_ID", OracleDbType.NVarchar2).Value = item.OBJECT_ID;
                    cmd.Parameters.Add("DATA_IDENTITY_USER_ID", OracleDbType.NVarchar2).Value = item.IDENTITY_USER_ID;

                    // cmd.ExecuteNonQuery();
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }


        }

    }
}
