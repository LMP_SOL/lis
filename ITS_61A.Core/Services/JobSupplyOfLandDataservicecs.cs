﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITS_61A.Core.Entities;
using System.Data;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;
using Newtonsoft.Json;
using System.Globalization;
using ITS_61A.Core.ServiceDataModel.JobSupplyOfLandDataModel;

namespace ITS_61A.Core.Services
{
    public class JobSupplyOfLandDataservicecs : BaseService
    {
        public DataSet GetCriteriaForSearchPayTaxLand()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETCRITERIA_FOR_SEARCHTAX_LAND";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_SELECT_GROUP", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_SELECT_GROUP", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }

            return ds;
        }

        public DataSet GetDataForPayTaxLand(string year_tax,string project_id,string deed_id_from,string deed_id_to,string province_id,string taxReceive_Department)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "SEARCHDATA_TAX_LAND";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("TAX_YEAR", OracleDbType.NVarchar2).Value = year_tax;
                    cmd.Parameters.Add("PROJECT_ID_NAME", OracleDbType.NVarchar2).Value = project_id;
                    cmd.Parameters.Add("DEED_ID_FROM", OracleDbType.NVarchar2).Value = deed_id_from;
                    cmd.Parameters.Add("DEED_ID_TO", OracleDbType.NVarchar2).Value = deed_id_to;
                    cmd.Parameters.Add("PROVINCE_SELECT", OracleDbType.NVarchar2).Value = province_id;
                    cmd.Parameters.Add("DEPARTMENT_PAY_TAX", OracleDbType.NVarchar2).Value = taxReceive_Department;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    
                } 
            }
            return ds;
        }
        public DataSet GetDataForAnnouncementLand()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
        
            string sql = "GETDATA_P_ANNOUNCEMENT_LAND";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }

            return ds;
        }
        public DataSet GetDataForLandRegistration(string ANNOUNCEMENT_LAND_ID,string PROJECT_NAME, string DEED_NO_FROM, string DEED_NO_TO, string PROVINCE_ID)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ANNOUNCEMENT_LAND_ID = (ANNOUNCEMENT_LAND_ID == "undefined" || ANNOUNCEMENT_LAND_ID == "null") ? "" : ANNOUNCEMENT_LAND_ID;
            PROJECT_NAME = (PROJECT_NAME == "undefined" || PROJECT_NAME == "null") ? "" : PROJECT_NAME;
            DEED_NO_FROM = (DEED_NO_FROM == "undefined" || DEED_NO_FROM == "null") ? "" : DEED_NO_FROM;
            DEED_NO_TO = (DEED_NO_TO == "undefined" || DEED_NO_TO == "null") ? "" : DEED_NO_TO;
            PROVINCE_ID = (PROVINCE_ID == "undefined" || PROVINCE_ID == null) ? "" : PROVINCE_ID;
            string sql = "GETDATA_LAND_REGISTRATION";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("ANNOUNCEMENT_LAND_ID", OracleDbType.NVarchar2).Value = ANNOUNCEMENT_LAND_ID;
                    cmd.Parameters.Add("PROJECT_NAME", OracleDbType.NVarchar2).Value = PROJECT_NAME;
                    cmd.Parameters.Add("DEED_NO_FROM", OracleDbType.NVarchar2).Value = DEED_NO_FROM;
                    cmd.Parameters.Add("DEED_NO_TO", OracleDbType.NVarchar2).Value = DEED_NO_TO;
                    cmd.Parameters.Add("PROVINCE_ID", OracleDbType.NVarchar2).Value = PROVINCE_ID;
                 
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }

            return ds;
        }

        public DataSet GetDataForSearchPurchasingProject(string PROJECT_ID, string PROVINCE_ID, string ANNOUNCEMENTS_NO, string DEED_NO_FROM, string DEED_NO_TO, string PARCEL_NO, string DEALING_FILE_NO)
        {

            DataSet ds = new DataSet();
            
            string sql = "SEARCHDATA_PURCHASING_PROJECT";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("PROJECT_ID_NAME", OracleDbType.NVarchar2).Value = PROJECT_ID== "undefined"?"": PROJECT_ID;
                    cmd.Parameters.Add("PROVINCE_SELECT", OracleDbType.NVarchar2).Value = PROVINCE_ID == "undefined" ? "" : PROVINCE_ID;
                    cmd.Parameters.Add("ANNOUNCEMENTS_NO", OracleDbType.NVarchar2).Value = ANNOUNCEMENTS_NO == "undefined" ? "" : ANNOUNCEMENTS_NO;
                    cmd.Parameters.Add("DEED_NO_FROM", OracleDbType.NVarchar2).Value = DEED_NO_FROM == "undefined" ? "" : DEED_NO_FROM;
                    cmd.Parameters.Add("DEED_NO_TO", OracleDbType.NVarchar2).Value = DEED_NO_TO == "undefined" ? "" : DEED_NO_TO;
                    cmd.Parameters.Add("PARCEL_NO", OracleDbType.NVarchar2).Value = PARCEL_NO == "undefined" ? "" : PARCEL_NO;
                    cmd.Parameters.Add("DEALING_FILE_NO", OracleDbType.NVarchar2).Value = DEALING_FILE_NO == "undefined" ? "" : DEALING_FILE_NO;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
                return ds;
            }
        }

        public DataSet getCriteriaforsearcPurchasingProject()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETCRITERIA_FOR_P_PROJECT";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_SELECT_GROUP", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }

            return ds;
        }

        public DataSet DeleteDataTaxLand(string id_p_tax_land)
        {
            DataSet ds = new DataSet();
            string sql = "DELETEDATA_TAX_LAND";
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("ID_TAX_LAND", OracleDbType.NVarchar2).Value = id_p_tax_land;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }

            return ds;
        }

        public DataSet GetDataSearchAnnouncementLand(AnnoucementLandSearchDataModel model)
        {
            DataSet ds = new DataSet();
            string sql = "SEARCHDATA_ANNOUNCEMENT_LAND";
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("NOTICE_DATE", OracleDbType.NVarchar2).Value = model.NOTICE_DATE;
                    cmd.Parameters.Add("NOTICE_DATE_TO", OracleDbType.NVarchar2).Value = model.NOTICE_DATE_TO;
                    cmd.Parameters.Add("ANNOUNCEMENTS_NO", OracleDbType.NVarchar2).Value = model.ANNOUNCEMENTS_NO;
                    cmd.Parameters.Add("OFFER_SALE_NAME", OracleDbType.NVarchar2).Value = model.OFFER_SALE_NAME;
                    cmd.Parameters.Add("OFFER_SALE_DATE", OracleDbType.NVarchar2).Value = model.OFFER_SALE_DATE;
                    cmd.Parameters.Add("OFFER_SALE_DATE_TO", OracleDbType.NVarchar2).Value = model.OFFER_SALE_DATE_TO;
                    cmd.Parameters.Add("APPROVE_SALE", OracleDbType.NVarchar2).Value = model.APPROVE_SALE;
                    cmd.Parameters.Add("DEED_NO", OracleDbType.NVarchar2).Value = model.DEED_NO;
                    cmd.Parameters.Add("LAND_NO", OracleDbType.NVarchar2).Value = model.LAND_NO;
                    cmd.Parameters.Add("SURVEY_PAGE", OracleDbType.NVarchar2).Value = model.SURVEY_PAGE;
                    cmd.Parameters.Add("OWNERSHIP_ORIGINAL", OracleDbType.NVarchar2).Value = model.OWNERSHIP_ORIGINAL;
                    cmd.Parameters.Add("PROVINCE_ID", OracleDbType.NVarchar2).Value = model.PROVINCE_ID;
                    cmd.Parameters.Add("DISTRICT_ID", OracleDbType.NVarchar2).Value = model.DISTRICT_ID;
                    cmd.Parameters.Add("SUBDISTRICT_ID", OracleDbType.NVarchar2).Value = model.SUBDISTRICT_ID;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    ds.Tables[0].TableName = "announcements";
                }
            }

            return ds;
        }
        public DataSet DeleteAnnouncementLand(string id)
        {
            DataSet ds = new DataSet();
            string sql = "DELETEDATA_ANNOUNCEMENT_LAND";
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;                  
                    cmd.Parameters.Add("ANNOUNCEMENT_LAND_ID", OracleDbType.NVarchar2).Value = id;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);                    
                }
            }
            return ds;

        }
        public DataSet DeleteDDeed(string id)
        {
            DataSet ds = new DataSet();
            string sql = "DELETEDATA_D_DEED";
            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("D_DEED_ID", OracleDbType.NVarchar2).Value = id;
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }
            return ds;

        }
    }
}
