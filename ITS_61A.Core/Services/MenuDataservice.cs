﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.Services
{
    public class MenuDataservice: BaseService
    {
        public DataSet getMenuOfUser(string id)
        {

            DataSet ds = new DataSet();
            string sql = "GETMENU_USER_PERMISSION";

            using (var con = OpenOracleConnection())
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_MENU", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("DATA_MENUMASTER", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                    cmd.Parameters.Add("USER_ID", OracleDbType.NVarchar2).Value = id;
                  
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    ds.Tables[0].TableName = "menu";
                    ds.Tables[1].TableName = "masterDataMenu";
                }
            }

            return ds;
        }
    }
}
