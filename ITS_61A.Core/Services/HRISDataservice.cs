﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.Services
{
    public class HRISDataservice:BaseService
    {
        public DataSet getUser()
        {

            DataSet ds = new DataSet();

            string sql = "SELECT [emcode],[prefixID] ,[prefixname],[firstenname],[lastenname], CONCAT([prefixname],[firstenname],[lastenname]) name_lastname, [PositionWorkline],[PositionWorklineShortName],[PositionExcutive],[PositionExecutiveName],[Level],[unitcodecode],[email],[DisplayShortName],[UnitCodeName],[Rank],[UnitCodeID] ,[PositionTypeCode_Line],[PositionTypeCode_Executive],[PositionLevelID_rank],[PersonnelGradeID_Level] FROM [Vw_name_HR2]";

           // string sql = "SELECT [empcode] as [emcode],[prefixID],[prefixname],[firstenname],[lastenname], CONCAT([prefixname],[firstenname],[lastenname]) name_lastname,  [PositionWorkline],  [PositionWorklineShortName],  [PositionExecutive] AS [PositionExcutive],  [PositionExecutiveShortName] AS [PositionExecutiveName], [Level],[unitcodecode],  [email],[DisplayShortName],  [UnitCodeName],  [Rank],  [UnitCodeID] ,  [PositionTypeCode_Line],  [PositionTypeCode_Executive],  [PositionLevelID_rank],[PersonnelGradeID_Level] FROM[Vw_name_HR]";


            string conStr = gennerartSqlConnectionString();

            using (var con = OpenSqlConnection(conStr))
            {
                using (var cmd = new SqlCommand(sql, con))
                {
                    cmd.CommandType = CommandType.Text;
                    var da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                  
                }
            }

            return ds;
        }

        private string gennerartSqlConnectionString() {
            SystemManagementDataservicecs sysService = new SystemManagementDataservicecs();
            var ds = sysService.getIdentitySetSystemById("75C68040806C4652A3498120BD737190");

            string conStr = "";

            foreach (DataRow row in ds.Tables[0].Rows) {
                conStr = string.Format("Data Source={0}; User ID={1}; password={2};Initial Catalog={3};Integrated Security=false; MultipleActiveResultSets=true;"
                    , row["HOSTNAME_IP"].ToString()
                    , row["SET_USER_NAME"].ToString()
                    , row["SET_PASSWORD"].ToString()
                    , row["DATABASE_NAME"].ToString());
            }

            return conStr;

        }
    }
}
