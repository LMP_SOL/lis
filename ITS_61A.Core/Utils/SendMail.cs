﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.Utils
{

    public class SendMail
    {
        private static SendMail _instance;
        public static SendMail Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SendMail();
                }
                return _instance;
            }
        }

        protected SendMail()
        {

        }
    

        private SmtpClient CreateSmtpClient(string from,string password,string hots_mail,string portStr)
        {

            var host = hots_mail;          
            var port = string.IsNullOrEmpty(portStr) ? 25 : int.Parse(portStr);
            var useSslStr = "true";
            var useSsl = bool.Parse(useSslStr);
            var mailFrom = from;
            var mailAccountPassword = password;
            var server = new SmtpClient(host);
            server.Port = port;
            server.EnableSsl = useSsl;
            server.UseDefaultCredentials = false;
            server.Credentials = new System.Net.NetworkCredential(mailFrom, mailAccountPassword);
            server.DeliveryMethod = SmtpDeliveryMethod.Network;
            return server;
        }


        public void SendEmail(string from,string fromDisplayName,string to,string subject,string body,string password,string hots_mail,string port)
        {        
            using (MailMessage message = new MailMessage())
            {
                message.From = new MailAddress(from, fromDisplayName);
                message.To.Add(to);
                message.Subject = subject;
                message.Body = body;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;

                using (SmtpClient smtp = CreateSmtpClient(from,password,hots_mail,port))
                {
                    smtp.Send(message);
                    
                }
            }

        }
    }
}
