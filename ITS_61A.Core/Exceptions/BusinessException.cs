﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.Exceptions
{
    public class BusinessException : Exception
    {
        public JObject ErrorJson { get; set; }

        public BusinessException(string message):base(message)
        {

        }
    }
}
