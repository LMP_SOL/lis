﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.ServiceDataModel.JobSupplyOfLandDataModel
{
    public class AnnoucementLandDataModel
    {
    }
    public class AnnoucementLandSearchDataModel
    {
        public string NOTICE_DATE { get; set; }
        public string NOTICE_DATE_TO { get; set; }
        public string ANNOUNCEMENTS_NO { get; set; }
        public string OFFER_SALE_NAME { get; set; }
        public string OFFER_SALE_DATE { get; set; }
        public string OFFER_SALE_DATE_TO { get; set; }
        public string  APPROVE_SALE { get; set; }
        public string DEED_NO { get; set; }
        public string LAND_NO { get; set; }
        public string SURVEY_PAGE { get; set; }
        public string OWNERSHIP_ORIGINAL { get; set; }
        public string PROVINCE_ID { get; set; }
        public string DISTRICT_ID { get; set; }
        public string SUBDISTRICT_ID { get; set; }
    }
}
