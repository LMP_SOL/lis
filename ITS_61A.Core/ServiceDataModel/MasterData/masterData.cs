﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.ServiceDataModel.MasterData
{
    public class masterDataSort
    {
        public string docId { get; set; }
        public int sort { get; set; }
    }
    public class masterDataSave
    {
        public string doc_id { get; set; }
        public string doc_id_parent { get; set; }
        public bool doc_isuse { get; set; }
        public string master_session_id { get; set; }
        public string user_id { get; set; }
        public string menu_id { get; set; }
        public topicListData[] topic_list_Data { get; set; }
    }
    public class topicListData {
        public string asnwerData { get; set; }
        public string topicId { get; set; }
    }

}
