﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.ServiceDataModel.SystemManagement
{
    public class setting_password_dataModel
    {
        public string ID { get; set; }
        public string AGE_DATE { get; set; }
        public string CHECK_PASS {get; set;}
        public string UNLOCK_TIME { get; set; }
        public string DONT_PASS_OLD { get; set; }
        public string MIN_CHARACTER { get; set; }
        public string MAX_CHARACTER { get; set; }
        public string SAME_CHARACTER { get; set; }
        public string UPPER_CASE { get; set; }
        public string LOWER_CASE { get; set; }
        public string INPUT_NUMBER { get; set; }
        public string SPACIAL_CHARACTER { get; set; }
        public string USER_PASS_NAME { get; set; }
        public string ALERT_EXPIRE { get; set; }
        public string AGE_OTP { get; set; }
        public string SESSION_TIMEOUT { get; set; }
    }
}


