﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.ServiceDataModel.SystemManagement
{
    public class setting_connectDatabase_dataModel
    {
        public string DATABASE_NAME { get; set; }
        public string HEADER_NAME { get; set; }
        public string HOSTNAME_IP { get; set; }
        public string ISSID { get; set; }
        public string PORT_SERVER { get; set; }
        public string ID { get; set; }
        public string SERVICE_NAME { get; set; }
        public string SET_PASSWORD { get; set; }
        public string SET_USER_NAME { get; set; }
        public string SORT { get; set; }
        public string SYSTEM_TYPE_ID { get; set; }
    }
}
