﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.ServiceDataModel.SystemManagement
{
    public class notification
    {
    }

    public class notificationMessage
    {
        public string SUBJECT_MAIN { get; set; }
        public string ALERT_END_TIME { get; set; }
        public string ALERT_START_TIME { get; set; }
        public string ALERT_MASSAGE { get; set; }
        public string NOTIFIER_NAME { get; set; }

        public string CREATE_BY { get; set; }
        public string CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string SORT { get; set; }


    }
}
