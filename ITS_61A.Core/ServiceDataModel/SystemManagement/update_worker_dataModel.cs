﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.ServiceDataModel.SystemManagement
{
    public class update_worker_dataModel
    {
        public string ID { get; set; }
        public string IDENTITY_USER_TYPE_ID { get; set; }
        public string EMPLOYEE_CODE { get; set; }
        public string ID_CARD { get; set; }
        public string PREFIX_ID { get; set; }
        public string NAME_TH { get; set; }
        public string LASTNAME_TH { get; set; }
        public string USER_CODE { get; set; }
        public string POSITION_ID { get; set; }
        public string DEPARTMENT_ID { get; set; }
        public string MANAGEMENT_POSITION_ID { get; set; }
        public string LEVEL_USER { get; set; }
        public string EMAIL { get; set; }
        public string WORK_START_DATE { get; set; }
        public string WORK_END_DATE { get; set; }
        public string COUNT_NUM_LOCK { get; set; }
        public string NUM_LOCK_DATE { get; set; }
        public string OTP_NUMBER { get; set; }
        public string OTP_DATE { get; set; }
        public string CREATE_BY { get; set; }
    }
}
