﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.ServiceDataModel.SystemManagement
{
    public class SMTP_dataModel
    {
        public string EMAIL_SERVER { get; set; }
        public string MASSAGE_EMAIL { get; set; }
        public string MASSAGE_OTP { get; set; }
        public string PASSWORD_EMAIL { get; set; }
        public string PORT_SERVER { get; set; }
        public string ID { get; set; }
        public string SUBJECT_EMAIL { get; set; }
        public string SUBJECT_OTP { get; set; }
        public string USER_EMAIL { get; set; }
    }
}
