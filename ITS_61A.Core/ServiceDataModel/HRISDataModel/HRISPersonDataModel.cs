﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.ServiceDataModel.HRISDataModel
{
    public class HRISPersonDataModel
    {
        public string emcode { get; set; }
        public string prefixID { get; set; }
        public string prefixname { get; set; }
        public string firstenname { get; set; }
        public string lastenname { get; set; }
        public string PositionWorkline { get; set; }
        public string PositionWorklineShortName { get; set; }
        public string PositionExcutive { get; set; }
        public string PositionExecutiveName { get; set; }
        public string Level { get; set; }
        public string unitcodecode { get; set; }
        public string email { get; set; }
        public string DisplayShortName { get; set; }
        public string UnitCodeName { get; set; }
        public string Rank { get; set; }
        public string UnitCodeID { get; set; }
        public string PositionTypeCode_Line { get; set; }
        public string PositionTypeCode_Executive { get; set; }
        public string PositionLevelID_rank { get; set; }
        public string PersonnelGradeID_Level { get; set; }
    }
}
