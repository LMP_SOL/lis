﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITS_61A.Core.ServiceDataModel.LogDataModel
{
    public class LisLogDataModel
    {
        public string ID { get; set; }
        public string LOG_DATE { get; set; }
        public string LOG_HOST { get; set; }
        public string CLIENT_IP { get; set; }
        public string USER_AGENT_STRING { get; set; }
        public string MENU_ID { get; set; }
        public string BUTTON_ID { get; set; }
        public string OBJECT_ID { get; set; }
        public string IDENTITY_USER_ID { get; set; }
    }
}
