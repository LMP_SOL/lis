var DateConverter = (function () {
    function DateConverter() {
    }
    DateConverter.prototype.getFormatDate = function (date) {
        var day = date.getDate();
        if (day < 10)
            day = '0' + day;
        var month = date.getMonth() + 1;
        if (month < 10)
            month = '0' + month;
        var year = date.getFullYear();
        var result = year + "-" + month + "-" + day;
        return result;
    };
    DateConverter.prototype.getFormatDateTime = function (date) {
        var day = date.getDate();
        if (day < 10)
            day = '0' + day;
        var month = date.getMonth() + 1;
        if (month < 10)
            month = '0' + month;
        var year = date.getFullYear();
        var hour = date.getHours();
        var min = date.getMinutes();
        var sec = date.getSeconds();
        var millisec = date.getMilliseconds();
        var result = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec + "." + millisec;
        return result;
    };
    DateConverter.prototype.getFormatDateThai = function (date) {
        var day = date.getDate();
        if (day < 10)
            day = '0' + day;
        var month = date.getMonth() + 1;
        if (month < 10)
            month = '0' + month;
        var year = date.getFullYear();
        var result = day + "/" + month + "/" + year + 543;
        return result;
    };
    DateConverter.prototype.getFormatDateMonthShotThai = function (date) {
        var shortMonths = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear() + 543;
        var result = day + "-" + month + "-" + year;
        return result;
    };
    DateConverter.prototype.getFormatDateMonthFullThai = function (date) {
        var shortMonths = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear() + 543;
        var result = day + "-" + month + "-" + year;
        return result;
    };
    DateConverter.prototype.getFormatDateMonthShotEng = function (date) {
        var shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear();
        var result = day + "-" + month + "-" + year;
        return result;
    };
    DateConverter.prototype.getFormatDateMonthFullEng = function (date) {
        var shortMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear();
        var result = day + "-" + month + "-" + year;
        return result;
    };
    return DateConverter;
}());
//# sourceMappingURL=DateConverter.js.map