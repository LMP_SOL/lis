﻿class GridManagementController implements ng.IController {
    public gridOptions: customOption;
    public onCallbackView: any;
    public onCallbackEdit: any;
    public onCallbackDel: any;
    public gOption: customOption;
    public select: any;
    public kendoGrid: any;
    private grid: any;

    constructor(private authorizationService: AuthorizationService,
        private _commonService: CommonService,
        private $location: ng.ILocationService,
        //private dialogService: DialogService,
    ) { }

    public $onInit() {
        this.gridOptions = this.gOption;
        console.log('this.gridOptions', this.gridOptions)
        this.gridOptions.columns = this.setCommandManagement(this.gridOptions);

    }

    private setCommandManagement(option: any) {
        let res = [];
        let btnStatus: any;
        let iconStatus: any;

        if (option) {
            res = option.column;
        }      
        if (option.showIndex) {
            res.splice(0, 0, {
                field: "rowNumber",
                title: "ลำดับ",
                attributes: { "data-title": "ลำดับ" },
                template: "<div>{{gmCtrl.setIndex(dataItem)}}</div>",
            })
        }
        if (option.management) {
            btnStatus = {
                command: this.setOperation(option.operation),
                title: "การจัดการ",
                attributes: { "data-title": "การจัดการ", },
            }
            res.splice(0, 0, btnStatus);
            //res.push(btnStatus)
        }
        return res;
    }

    private setOperation(operation: operationModel) {
        let arr = [];
        arr.splice(0, 0,
            { template: `<div class="wrapper-btn-grid" layout="row" layout-align="center center">`}
        )
        if (operation.view) {
            arr.push({ template: `
                <img ng-click="gmCtrl.onView(dataItem)" ng-src="Content/button/btn-view-data.svg">
            ` })
        }
        if (operation.edit) {
            arr.push({ template: `
            <img ng-click="gmCtrl.onEdit(dataItem)" ng-src="Content/button/btn-edit-data.svg">
            ` })
        }
        if (operation.del) {
            arr.push({ template: `
            <img ng-click="gmCtrl.onDel(dataItem)" ng-src="Content/button/btn-delete-data.svg">
            ` })
        }
        arr.push ({template: `</div>`})
        return arr;
    }

    //public showErrorDialog(dataItem) {
    //    if (dataItem.STATUS_UPDATE_ID === '5ea2c38a-b59b-4455-9966-6d13c16835e3') {
    //        this.dialogService.errorDialogServicemornitoring('แจ้งเตือนการล้มเหลว', dataItem.ERROR_MASSAGE);
    //    }
    //}

    private setIndex(item) {
        let index = this.kendoGrid.dataSource.indexOf(item) + 1;
        return index;
    }
    private setShowDateThai(t_date) {
        return this._commonService.formatDateThai(this._commonService.formatDate(t_date, "YYYY-MM-DD"), "dd MMMM yyyy")
    }

   

    public onEdit(item) {
        if (this.onCallbackEdit) {
            this.onCallbackEdit(item);
        }
    }

    public onDel(item) {       
        if (this.onCallbackDel) {
            this.onCallbackDel(item);
        }
    }
    public onView(item) {
        if (this.onCallbackView) {
            this.onCallbackView(item);
        }
    }
   
    /////////////////////////////////////
}
class operationModel {
    public view: boolean = false;
    public edit: boolean = false;
    public del: boolean = false;
    public doc: boolean = false; 
}
interface customOption extends kendo.ui.GridOptions {
    operation: operationModel;
    column: Array<string>;
    management: boolean;
    showIndex: boolean;
    showStatus: boolean;
}
