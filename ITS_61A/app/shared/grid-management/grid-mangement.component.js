var GridManagementController = (function () {
    function GridManagementController(authorizationService, _commonService, $location) {
        this.authorizationService = authorizationService;
        this._commonService = _commonService;
        this.$location = $location;
    }
    GridManagementController.prototype.$onInit = function () {
        this.gridOptions = this.gOption;
        console.log('this.gridOptions', this.gridOptions);
        this.gridOptions.columns = this.setCommandManagement(this.gridOptions);
    };
    GridManagementController.prototype.setCommandManagement = function (option) {
        var res = [];
        var btnStatus;
        var iconStatus;
        if (option) {
            res = option.column;
        }
        if (option.showIndex) {
            res.splice(0, 0, {
                field: "rowNumber",
                title: "ลำดับ",
                attributes: { "data-title": "ลำดับ" },
                template: "<div>{{gmCtrl.setIndex(dataItem)}}</div>",
            });
        }
        if (option.management) {
            btnStatus = {
                command: this.setOperation(option.operation),
                title: "การจัดการ",
                attributes: { "data-title": "การจัดการ", },
            };
            res.splice(0, 0, btnStatus);
            //res.push(btnStatus)
        }
        return res;
    };
    GridManagementController.prototype.setOperation = function (operation) {
        var arr = [];
        arr.splice(0, 0, { template: "<div class=\"wrapper-btn-grid\" layout=\"row\" layout-align=\"center center\">" });
        if (operation.view) {
            arr.push({ template: "\n                <img ng-click=\"gmCtrl.onView(dataItem)\" ng-src=\"Content/button/btn-view-data.svg\">\n            " });
        }
        if (operation.edit) {
            arr.push({ template: "\n            <img ng-click=\"gmCtrl.onEdit(dataItem)\" ng-src=\"Content/button/btn-edit-data.svg\">\n            " });
        }
        if (operation.del) {
            arr.push({ template: "\n            <img ng-click=\"gmCtrl.onDel(dataItem)\" ng-src=\"Content/button/btn-delete-data.svg\">\n            " });
        }
        arr.push({ template: "</div>" });
        return arr;
    };
    //public showErrorDialog(dataItem) {
    //    if (dataItem.STATUS_UPDATE_ID === '5ea2c38a-b59b-4455-9966-6d13c16835e3') {
    //        this.dialogService.errorDialogServicemornitoring('แจ้งเตือนการล้มเหลว', dataItem.ERROR_MASSAGE);
    //    }
    //}
    GridManagementController.prototype.setIndex = function (item) {
        var index = this.kendoGrid.dataSource.indexOf(item) + 1;
        return index;
    };
    GridManagementController.prototype.setShowDateThai = function (t_date) {
        return this._commonService.formatDateThai(this._commonService.formatDate(t_date, "YYYY-MM-DD"), "dd MMMM yyyy");
    };
    GridManagementController.prototype.onEdit = function (item) {
        if (this.onCallbackEdit) {
            this.onCallbackEdit(item);
        }
    };
    GridManagementController.prototype.onDel = function (item) {
        if (this.onCallbackDel) {
            this.onCallbackDel(item);
        }
    };
    GridManagementController.prototype.onView = function (item) {
        if (this.onCallbackView) {
            this.onCallbackView(item);
        }
    };
    return GridManagementController;
}());
var operationModel = (function () {
    function operationModel() {
        this.view = false;
        this.edit = false;
        this.del = false;
        this.doc = false;
    }
    return operationModel;
}());
//# sourceMappingURL=grid-mangement.component.js.map