﻿class FileUploadController implements ng.IController {


    public uploadFile: any;
    public listupload: any;
    public operation: any;
    public type: any;
    public ismulti: any;

    public txtlabel: string = "";
    public textcase: string = '';
    public havePicInGallery: boolean = false;
    public isedit: boolean = false;

    public checkChange: any;
    public deleteImageFile: any;
    public viewImage: any;
    public downloadImageFile: any;
    public viewDoc: any;
    public deleteDocFile: any;
    public downloadDocFile: any;
    public checkShowLine: any;
    public onKeyup: any;
    public check: any;
    public checkShowBlackLine: any;
    public showcontent: any;

    constructor(private $scope: ng.IScope,
        private $window: ng.IWindowService,
        private _dialogService: DialogService) {

    }
     
    public $onInit() {

        ///// Init variable
      
        if (this.type == 'img') {
            this.txtlabel = 'IMPORT';
            this.textcase = 'รองรับไฟล์ประเภท .jpg, .png, .gif';
        } else {
            this.txtlabel = 'IMPORT';
            this.textcase = 'รองรับไฟล์ประเภท .doc, .docx, .pdf, .avi, .mp4';
        }
        if (this.operation == 'add' || this.operation == 'edit') {
            this.isedit = true;
        } else {
            this.isedit = false;
        }
        this.checkChange = function (index, list) {
            for (let i = 0; i < list.length; i++) {
                if (i != index) {
                    list[i].useProfile = false;
                }
            }
        }
        this.check = function (index, list) {
            (list[index].useProfile == true) ? list[index].useProfile = false : list[index].useProfile = true;;
        }
        this.checkShowBlackLine = function (idx) {
            this.haveAfterImageFile = false;
            if (this.$scope.type == 'img') {
                if (this.uploadFile.imageFiles.files[idx].state != "delete") {
                    for (let j = idx; j < this.uploadFile.imageFiles.files.length; j++) {
                        if (j < this.uploadFile.imageFiles.files.length - 1) {
                            if (this.uploadFile.imageFiles.files[j + 1].state != "delete") {
                                this.haveAfterImageFile = true;
                            }
                        }
                    }
                }
            } else {
                if (this.uploadFile.documentFiles.files[idx].state != "delete") {
                    for (let j = idx; j < this.uploadFile.documentFiles.files.length; j++) {
                        if (j < this.uploadFile.documentFiles.files.length - 1) {
                            if (this.uploadFile.documentFiles.files[j + 1].state != "delete") {
                                this.haveAfterImageFile = true;
                            }
                        }
                    }
                }
            }

            if (this.haveAfterImageFile) {
                return true;
            }
            else {
                return false;
            }
        }

        this.showcontent = function (idx) {
            if (this.type == 'img') {
                if (this.uploadFile.imageFiles.files[idx].state == 'add') {
                    //var path = $scope.uploadFile.documentFiles.files[idx].result
                    var path = this.uploadFile.imageFiles.files[idx].url;
                } else {
                    var path = this.uploadFile.imageFiles.files[idx].src;
                }
                this.$window.open(path, '_blank');
                //this.havePicInGallery = true;
                //this.galleryFunc.isshow = true;
                //this.galleryFunc.showphoto(idx, this.$scope.uploadFile.imageFiles.files);
            } else {
                if (getFileExtension(this.uploadFile.documentFiles.files[idx].file.name) == "pdf"
                    || (getFileExtension(this.uploadFile.documentFiles.files[idx].file.name) == "doc")
                    || (getFileExtension(this.uploadFile.documentFiles.files[idx].file.name) == "docx")
                    || (getFileExtension(this.uploadFile.documentFiles.files[idx].file.name) == "avi")
                    || (getFileExtension(this.uploadFile.documentFiles.files[idx].file.name) == "mp4")
                ) {
                    if (this.uploadFile.documentFiles.files[idx].state == 'add') {
                        //var path = $scope.uploadFile.documentFiles.files[idx].result
                        var path = this.uploadFile.documentFiles.files[idx].url;
                    } else {
                        var path = this.uploadFile.documentFiles.files[idx].src;
                    }
                    this.$window.open(path, '_blank');
                } else {
                    this.galleryFunc.isshow = true;
                    this.galleryFunc.showphoto(idx, this.uploadFile.documentFiles.files);
                }
            }
        }

        function getFileExtension(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2).toLowerCase();
        }

        this.viewImage = function (idx) {
            var path = this.$scope.WebService + this.uploadFile.imageFiles.files[idx].path
            this.$window.open(path, '_blank');
        }

        this.deleteImageFile = function (idx) {
            debugger
            if (this.uploadFile.imageFiles.files[idx].state == "edit") {
                //$scope.uploadFile.imageFiles.files.splice(idx, 1);
                this.uploadFile.imageFiles.files[idx].state = "delete";
            }
            else if (this.uploadFile.imageFiles.files[idx].state == "add") {
                this.uploadFile.imageFiles.files.splice(idx, 1);
            }
        };
        this.downloadImageFile = function (idx) {
            console.log(this.uploadFile.imageFiles.files[idx].path, this.uploadFile.imageFiles.files[idx].file.name);
        };
        ////////////////////////////DOC///////////////////////////////////////////
        this.viewDoc = function (idx) {
            var path = this.$scope.WebService + this.uploadFile.documentFiles.files[idx].path
            this.$window.open(path, '_blank');
        }
        this.deleteDocFile = function (idx) {
            if (this.uploadFile.documentFiles.files[idx].state == "edit") {

                //$scope.uploadFile.documentFiles.files.splice(idx, 1);
                this.$scope.uploadFile.documentFiles.files[idx].state = "delete";
            }
            else if (this.uploadFile.documentFiles.files[idx].state == "add") {
                this.uploadFile.documentFiles.files.splice(idx, 1);
            }
        };
        this.downloadDocFile = function (idx) {
            console.log(this.uploadFile.documentFiles.files[idx].path, this.uploadFile.documentFiles.files[idx].file.name);
        };
        ////////////////////////////////////////////////////////////////////////////////////

        this.checkShowLine = function () {
            this.haveNotDeleteDocumentFile = false;
            if (this.type == 'img') {
                for (let i = 0; i < this.uploadFile.imageFiles.files.length; i++) {
                    if (this.uploadFile.imageFiles.files[i].state != "delete") {
                        this.haveNotDeleteDocumentFile = true;
                    }
                }
            } else {
                for (let i = 0; i < this.uploadFile.documentFiles.files.length; i++) {
                    if (this.uploadFile.documentFiles.files[i].state != "delete") {
                        this.haveNotDeleteDocumentFile = true;
                    }
                }
            }

            if (this.haveNotDeleteDocumentFile) {
                return true;
            }
            else {
                return false;
            }
        }

        this.onKeyup = function (key) {
            if (key.keyCode == 27) {
                this.havePicInGallery = false;
            }
        }

        //this.$scope.$watchCollection(this.uploadFile, (newValue : any, oldValue : any) => {
        //    try {
        //        if (newValue != undefined) {
        //            debugger
        //            getFile(newValue);
        //        }
        //    } catch (e) {
        //        console.log(e);
        //    }
        //});

        this.$scope.$watch(() => { return this.listupload }, (newValue: Array<any>, oldValue: Array<any>) => {
            try {
                if (newValue != undefined) {
                    getFile(newValue);
                }
            } catch (e) {
                console.log(e);
            }
        });

        var getFile = (item: any) => {
        var valid = true;
        switch (this.type) {
            case 'img':

                if (item) {
                    if (this.ismulti != 0 && item.length > 1) {
                        this._dialogService.showMessage(Constants.messageDialog.systemMesage, "จำกัดอัพโหลดไฟล์");
                        return false;
                    } else {
                        debugger
                        for (var i = 0; i < item.length; i++) {
                            for (var j = 0; j < this.uploadFile.imageFiles.files.length; j++) {
                                if (item[i].file == this.uploadFile.imageFiles.files[j].file.name && this.uploadFile.imageFiles.files[j].state != 'delete') {
                                    this._dialogService.showMessage(Constants.messageDialog.systemMesage, "มีการอัพโหลดชื่อซ้ำกัน");
                                    valid = false;
                                    break;
                                }
                            }
                            if (item[i].type == "image/png"
                                || item[i].type == "image/PNG"
                                || item[i].type == "image/jpg"
                                || item[i].type == "image/JPG"
                                || item[i].type == "image/jpeg"
                                || item[i].type == "image/JPEG"
                                || item[i].type == "image/gif") {
                                if (item[i].size <= 5242880 && valid == true) { //5242880 = 5MB, 10485760 = 10MB
                                    this.uploadFile.imageFiles.files.push({
                                        file: item[i]._file,
                                        //result: item[i].result,
                                        result: item[i].url,
                                        url: item[i].url,
                                        state: "add",
                                        path: "",
                                        ID: ""
                                    });
                                } else {
                                    if (valid) {
                                        this._dialogService.showMessage(Constants.messageDialog.systemMesage, "ไฟล์ขนาดใหญ่เกินไป กรุณาเลือกไฟล์ที่มีขนาดไม่เกิน 5MB");
                                        valid = false;
                                    }
                                }
                            } else {
                                if (valid) {
                                    this._dialogService.showMessage(Constants.messageDialog.systemMesage, "แนบไฟล์ผิดประเภท กรุณาเลือกไฟล์ตามคำแนะนำ");
                                    valid = false;
                                }
                            }

                        }
                    }

                }
                break;
            case 'doc':
                if (this.ismulti != 0 && item.length > 1) {
                    this._dialogService.showMessage(Constants.messageDialog.systemMesage, "จำกัดอัพโหลดไฟล์");
                    return false;
                }
                if (item) {
                    for (var i = 0; i < item.length; i++) {
                        for (var j = 0; j < this.uploadFile.documentFiles.files.length; j++) {
                            if (item[i].file == this.uploadFile.documentFiles.files[j].file.name && this.uploadFile.documentFiles.files[j].state != 'delete') {
                                this._dialogService.showMessage(Constants.messageDialog.systemMesage, "มีการอัพโหลดชื่อซ้ำกัน");
                                valid = false;
                                break;
                            }
                        }
                        if (item[i].type == "application/pdf"
                            || item[i].type == "application/x-pdf"
                            || item[i].type == "application/msword"
                            || item[i].type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                            || item[i].type == "video/mp4"
                            || item[i].type == "video/avi"
                        ) {
                            if (item[i].size <= 31457280 && valid == true) { //5242880 = 5MB, 10485760 = 10MB
                                this.uploadFile.documentFiles.files.push({
                                    file: item[i]._file,
                                    result: item[i].result,
                                    url: item[i].url,
                                    state: "add",
                                    path: "",
                                    ID: ""
                                });
                            } else {
                                if (valid) {
                                    this._dialogService.showMessage(Constants.messageDialog.systemMesage, "ไฟล์ขนาดใหญ่เกินไป กรุณาเลือกไฟล์ที่มีขนาดไม่เกิน 10MB");
                                    valid = false;
                                }
                            }
                        } else {
                            if (valid) {
                                this._dialogService.showMessage(Constants.messageDialog.systemMesage, "แนบไฟล์ผิดประเภท กรุณาเลือกไฟล์ตามคำแนะนำ");
                                valid = false;
                            }
                        }

                    }
                }
                break;
            default:
                this._dialogService.showMessage(Constants.messageDialog.systemMesage, "กรุณาเลือกไฟล์ตามคำแนะนำ");
        }
    }


    }
    


}



interface IMyDirectiveScope extends ng.IScope {

}
class FileInput implements ng.IDirective {
    restrict = 'A';
    scope = {
        data: "=",
    }

    constructor() {

    }

    link = (scope: IMyDirectiveScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) => {

        element.bind("change", function (changeEvent: any) {
            let files = changeEvent.target.files;
            var values = [];
            var reader = new FileReader();
            angular.forEach(files, function (item) {
                var value = {
                    // File Name 
                    file: item.name,
                    //File Size 
                    size: item.size,
                    //File URL to view 
                    url: URL.createObjectURL(item),
                    //
                    result: reader.result,
                    // File Input Value 
                    _file: item,
                    // Type File
                    type: item.type
                };
                values.push(value);
            });
            reader.onload = function (loadEvent) {
                //scope.$apply(function (e) {
                //    scope.data.listupload = values;
                //    //scope.data.haveFile = true;
                //    angular.element(element[0]).val(null);
                //});
                scope.$apply(() => {
                    scope.data = values;
                    //scope.data.listupload = values;
                    //scope.data.haveFile = true;
                    angular.element(element[0]).val(null);
                });
            };
            reader.readAsDataURL(changeEvent.target.files[0]);
        });



        //if (scope.config.isMultiple) {
        //    element.attr('multiple', '');
        //}

        //if (scope.config.showOnlyAcceptFiles) {
        //    if (scope.config.acceptFiles != "") {
        //        element.attr('accept', scope.config.acceptFiles);
        //    }
        //}

        //scope.config.resetValue = function () {
        //    element["0"].value = null;
        //}

        //scope.config.disable = function () {
        //    element.attr('disabled', '');
        //}

        //scope.config.enable = function () {
        //    element.removeAttr('disabled');
        //}


        //element.bind("change", function (e: any) {
        //    let files = e.target.files; // Return as FileList type

        //    scope.data = [];
        //    let fileList = [];
        //    for (let i = 0; i < files.length; i++) {
        //        fileList.push(files[i]);
        //    }
        //    scope.$apply(() => {
        //        scope.data = fileList;
        //    });
      
        //});

        //element.bind('click', function () {
        //    this.value = null;
        //});
    }

    static factory(): ng.IDirectiveFactory {
        const directive = () => new FileInput();
        return directive;
    }
}
