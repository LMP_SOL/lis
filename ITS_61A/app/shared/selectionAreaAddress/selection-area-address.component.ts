﻿class selectionAreaAddressController implements ng.IController {

    public form: any;
    public mode: any; //1 = add, 2 = edit, 3 = view  
    public required: any;
    public provinceSelected: any;
    public districtSelected: any;
    public subdistrictSelected: any;


    public provinceList: any;
    public districtList: any;
  
    public subdistrictList: any;
    public textSearchSubdistrict_current: string;
    public textSearchDistrict: string;
    public textSearchProvince: string;
    public onClear: any;

    private configServerSideSubdistrict = {
        textSearchSubdistrict: "",
        selectedItem: undefined,
        limit: 40,
        offset:1
    }

    private configServerSideDistrict = {
        textSearch: "",
        selectedItem: undefined,
      
    }
    private configServerSideProvince = {
        textSearch: "",
        selectedItem: undefined,

    }


    constructor(
        private _masterDataService: MasterDataService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
        private $element: ng.IRootElementService,

    ) {

    }

    $onInit() {
        
        this.getSubDistric();
        this.getProvinceAndDistrict();
        this.$element.find('input').on('keydown', (ev) => {
            ev.stopPropagation();
        });

        this.onClear = {};
        this.onClear.clear = this.clear;
 
    }


    public getProvinceAndDistrict() {
        if (this._masterDataService.DistrictAndProvinceData != undefined) {
            this.provinceList = this._masterDataService.DistrictAndProvinceData.province;
            this.districtList = this._masterDataService.DistrictAndProvinceData.district;

            if (this.mode != 1)
                this.setProvinceAndDistrictSelected();

        } else {
            this._masterDataService.getDataDistrictAndProvince().then((res) => {

                console.log(res);
                this.provinceList = res.data.province;
                this.districtList = res.data.district;

                if (this.mode != 1)
                    this.setProvinceAndDistrictSelected();
            })
        }
    }

    public setProvinceAndDistrictSelected() {
        let district = this.districtList.find((x) => { return x.ID == this.districtSelected });
        if (district) 
            this.onChangeDistrict(district, false);
        

        let province = this.provinceList.find((x) => { return x.ID == this.provinceSelected; })
        
        if (province)
            this.onChangeProvince(province, false);

    }

    //******************** district **************************//
    public onChangeDistrict(item, select) {
        this.districtSelected = item.ID;
        this.configServerSideDistrict.selectedItem = item;
        if (select) {         
           
            this.subdistrictSelected = undefined
            this.configServerSideSubdistrict.selectedItem = undefined;
            this.getSubDistric();
        }
        
        let province = this.provinceList.find((x) => { return x.ID == item.MASTER_PROVINCE_ID; })
        
        if (province)
            this.onChangeProvince(province, false);

    }


    //******************** district **************************//


    //******************** Province **************************//
    public onChangeProvince(item,select) {
        this.provinceSelected = item.ID;
        this.configServerSideProvince.selectedItem = item;

        if (select) {
            //clear
            this.districtSelected = undefined;
            this.configServerSideDistrict.selectedItem = undefined;

            this.subdistrictSelected = undefined
            this.configServerSideSubdistrict.selectedItem = undefined;

            this.filterDistrict(item);
        }
    }

    public filterDistrict(item) {
        this.districtList = this._masterDataService.DistrictAndProvinceData.district.filter((x) => { return x.MASTER_PROVINCE_ID == item.ID;  });
    }

     //******************** Province **************************//

    //**********************SubDistric***********************//
    public getSubDistric() {
        this.textSearchSubdistrict_current = angular.copy(this.configServerSideSubdistrict.textSearchSubdistrict);
        this._masterDataService.getDataSubdistrictOffset(this.configServerSideSubdistrict.limit, 1, this.configServerSideSubdistrict.textSearchSubdistrict, this.districtSelected)
            .then((res) => {
                console.log(res);
                this.subdistrictList = res.data.subdistrict;
                if (this.mode != 1) {
                    this.setSubDistricSelected();
                }
            });

    }


    public getSubDistricMore() {
       
        this._masterDataService.getDataSubdistrictOffset(this.configServerSideSubdistrict.limit, this.configServerSideSubdistrict.offset, this.configServerSideSubdistrict.textSearchSubdistrict, this.districtSelected)
            .then((res) => {
                console.log(res);
                this.subdistrictList = this.subdistrictList.concat(res.data.subdistrict);

            });

    }

    public setSubDistricSelected() {
        let subdistrict = this.subdistrictList.find((x) => { return x.ID == this.subdistrictSelected });       
        if (subdistrict)
            this.configServerSideSubdistrict.selectedItem = subdistrict;
    }


    public loadMoreSubdistrictList(config) {
        console.log(config);
        if (this.textSearchSubdistrict_current != config.textSearchSubdistrict) {
            config.offset = 1;
            this.configServerSideSubdistrict = config;
            this.getSubDistricMore();
        } else {
            config.offset = config.offset + config.limit;
            this.getSubDistricMore();
        }
     
    }
    public onChangeSubdistrict(item) {
        this.subdistrictSelected = item.ID;
        //find district
        let district = this.districtList.find((x) => { return x.ID == item.MASTER_DISTRICT_ID });
        if (district) {
            this.onChangeDistrict(district, false);
        }

    }
     //**********************End SubDistric***********************//

    public clear=()=> {
        
        this.districtSelected = undefined;
        this.configServerSideDistrict.selectedItem = undefined;

        this.subdistrictSelected = undefined
        this.configServerSideSubdistrict.selectedItem = undefined;

        this.provinceSelected = undefined;
        this.configServerSideProvince.selectedItem = undefined;

    }

}