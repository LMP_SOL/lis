var selectionAreaAddressController = (function () {
    function selectionAreaAddressController(_masterDataService, $location, $http, $element) {
        var _this = this;
        this._masterDataService = _masterDataService;
        this.$location = $location;
        this.$http = $http;
        this.$element = $element;
        this.configServerSideSubdistrict = {
            textSearchSubdistrict: "",
            selectedItem: undefined,
            limit: 40,
            offset: 1
        };
        this.configServerSideDistrict = {
            textSearch: "",
            selectedItem: undefined,
        };
        this.configServerSideProvince = {
            textSearch: "",
            selectedItem: undefined,
        };
        //**********************End SubDistric***********************//
        this.clear = function () {
            _this.districtSelected = undefined;
            _this.configServerSideDistrict.selectedItem = undefined;
            _this.subdistrictSelected = undefined;
            _this.configServerSideSubdistrict.selectedItem = undefined;
            _this.provinceSelected = undefined;
            _this.configServerSideProvince.selectedItem = undefined;
        };
    }
    selectionAreaAddressController.prototype.$onInit = function () {
        this.getSubDistric();
        this.getProvinceAndDistrict();
        this.$element.find('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });
        this.onClear = {};
        this.onClear.clear = this.clear;
    };
    selectionAreaAddressController.prototype.getProvinceAndDistrict = function () {
        var _this = this;
        if (this._masterDataService.DistrictAndProvinceData != undefined) {
            this.provinceList = this._masterDataService.DistrictAndProvinceData.province;
            this.districtList = this._masterDataService.DistrictAndProvinceData.district;
            if (this.mode != 1)
                this.setProvinceAndDistrictSelected();
        }
        else {
            this._masterDataService.getDataDistrictAndProvince().then(function (res) {
                console.log(res);
                _this.provinceList = res.data.province;
                _this.districtList = res.data.district;
                if (_this.mode != 1)
                    _this.setProvinceAndDistrictSelected();
            });
        }
    };
    selectionAreaAddressController.prototype.setProvinceAndDistrictSelected = function () {
        var _this = this;
        var district = this.districtList.find(function (x) { return x.ID == _this.districtSelected; });
        if (district)
            this.onChangeDistrict(district, false);
        var province = this.provinceList.find(function (x) { return x.ID == _this.provinceSelected; });
        if (province)
            this.onChangeProvince(province, false);
    };
    //******************** district **************************//
    selectionAreaAddressController.prototype.onChangeDistrict = function (item, select) {
        this.districtSelected = item.ID;
        this.configServerSideDistrict.selectedItem = item;
        if (select) {
            this.subdistrictSelected = undefined;
            this.configServerSideSubdistrict.selectedItem = undefined;
            this.getSubDistric();
        }
        var province = this.provinceList.find(function (x) { return x.ID == item.MASTER_PROVINCE_ID; });
        if (province)
            this.onChangeProvince(province, false);
    };
    //******************** district **************************//
    //******************** Province **************************//
    selectionAreaAddressController.prototype.onChangeProvince = function (item, select) {
        this.provinceSelected = item.ID;
        this.configServerSideProvince.selectedItem = item;
        if (select) {
            //clear
            this.districtSelected = undefined;
            this.configServerSideDistrict.selectedItem = undefined;
            this.subdistrictSelected = undefined;
            this.configServerSideSubdistrict.selectedItem = undefined;
            this.filterDistrict(item);
        }
    };
    selectionAreaAddressController.prototype.filterDistrict = function (item) {
        this.districtList = this._masterDataService.DistrictAndProvinceData.district.filter(function (x) { return x.MASTER_PROVINCE_ID == item.ID; });
    };
    //******************** Province **************************//
    //**********************SubDistric***********************//
    selectionAreaAddressController.prototype.getSubDistric = function () {
        var _this = this;
        this.textSearchSubdistrict_current = angular.copy(this.configServerSideSubdistrict.textSearchSubdistrict);
        this._masterDataService.getDataSubdistrictOffset(this.configServerSideSubdistrict.limit, 1, this.configServerSideSubdistrict.textSearchSubdistrict, this.districtSelected)
            .then(function (res) {
            console.log(res);
            _this.subdistrictList = res.data.subdistrict;
            if (_this.mode != 1) {
                _this.setSubDistricSelected();
            }
        });
    };
    selectionAreaAddressController.prototype.getSubDistricMore = function () {
        var _this = this;
        this._masterDataService.getDataSubdistrictOffset(this.configServerSideSubdistrict.limit, this.configServerSideSubdistrict.offset, this.configServerSideSubdistrict.textSearchSubdistrict, this.districtSelected)
            .then(function (res) {
            console.log(res);
            _this.subdistrictList = _this.subdistrictList.concat(res.data.subdistrict);
        });
    };
    selectionAreaAddressController.prototype.setSubDistricSelected = function () {
        var _this = this;
        var subdistrict = this.subdistrictList.find(function (x) { return x.ID == _this.subdistrictSelected; });
        if (subdistrict)
            this.configServerSideSubdistrict.selectedItem = subdistrict;
    };
    selectionAreaAddressController.prototype.loadMoreSubdistrictList = function (config) {
        console.log(config);
        if (this.textSearchSubdistrict_current != config.textSearchSubdistrict) {
            config.offset = 1;
            this.configServerSideSubdistrict = config;
            this.getSubDistricMore();
        }
        else {
            config.offset = config.offset + config.limit;
            this.getSubDistricMore();
        }
    };
    selectionAreaAddressController.prototype.onChangeSubdistrict = function (item) {
        this.subdistrictSelected = item.ID;
        //find district
        var district = this.districtList.find(function (x) { return x.ID == item.MASTER_DISTRICT_ID; });
        if (district) {
            this.onChangeDistrict(district, false);
        }
    };
    return selectionAreaAddressController;
}());
//# sourceMappingURL=selection-area-address.component.js.map