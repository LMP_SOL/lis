﻿class DateConverter {
    public getFormatDate(date) {
        let day = date.getDate();
        if (day < 10) day = '0' + day;
        let month = date.getMonth() + 1;
        if (month < 10) month = '0' + month;
        let year = date.getFullYear();
        let result = year + "-" + month + "-" + day;
        return result;
    } 

    public getFormatDateTime(date) {
        let day = date.getDate();
        if (day < 10) day = '0' + day;
        let month = date.getMonth() + 1;
        if (month < 10) month = '0' + month;
        let year = date.getFullYear();
        let hour = date.getHours();
        let min = date.getMinutes();
        let sec = date.getSeconds();
        let millisec = date.getMilliseconds();
        let result = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec + "." + millisec;
        
        return result;
    }

    public getFormatDateThai(date) {
        let day = date.getDate();
        if (day < 10) day = '0' + day;
        let month = date.getMonth() + 1;
        if (month < 10) month = '0' + month;
        let year = date.getFullYear();
        let result =  day+ "/" + month + "/" + year+543 ;
        return result;
    } 

    private getFormatDateMonthShotThai(date) {
        let shortMonths = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear()+543;
        let result = `${day}-${month}-${year}`;
        return result;
    }   
    private getFormatDateMonthFullThai(date) {
        let shortMonths = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear()+543;
        let result = `${day}-${month}-${year}`;
        return result;
    }
    private getFormatDateMonthShotEng(date) {
        let shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear();
        let result = `${day}-${month}-${year}`;
        return result;
    }
    private getFormatDateMonthFullEng(date) {
        let shortMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear();
        let result = `${day}-${month}-${year}`;
        return result;
    }    
}