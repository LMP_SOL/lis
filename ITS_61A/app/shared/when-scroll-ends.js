var WhenScrollEnds = (function () {
    function WhenScrollEnds() {
        this.restrict = 'A';
        this.link = function (scope, element, attrs, ctrl) {
            var container = element.find('md-content');
            container.css('height', '600px');
            //var visibleHeight = 600; // fix visibleHeight.
            // User for compare remaining scrollable
            var threshold = 1;
            container.on('scroll', function (e) {
                //var scrollableHeight = e.target.scrollHeight;
                var scrollableHeight = container.prop('scrollHeight');
                // We can use clientHeight property instead of fix visibleHeight
                var visibleHeight = container.prop('clientHeight');
                var hiddenContentHeight = scrollableHeight - visibleHeight;
                if (hiddenContentHeight - container[0].scrollTop <= threshold) {
                    scope.$apply(attrs.whenscrollends);
                }
                else if (container[0].scrollTop <= threshold) {
                    if (element[0].attributes["readytoscroll"] !== undefined && attrs.whenscrolltop !== undefined) {
                        scope.$apply(attrs.whenscrolltop);
                    }
                }
            });
        };
    }
    // Register in main.ts -> .directive('whenscrollends', WhenScrollEnds.factory())
    WhenScrollEnds.factory = function () {
        var directive = function () { return new WhenScrollEnds(); };
        return directive;
    };
    // Register in main.ts -> .directive('whenscrollends', WhenScrollEnds.instance)
    WhenScrollEnds.instance = function () {
        return new WhenScrollEnds();
    };
    return WhenScrollEnds;
}());
//# sourceMappingURL=when-scroll-ends.js.map