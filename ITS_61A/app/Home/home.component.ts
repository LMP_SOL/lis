﻿class HomeController implements ng.IController {
    public mainMenuList: any;
    public mainMenuSelected: string;

    public subMenuList: any;
    public masterDataMenuList: any;
    public menuOpen: boolean = true;
    public activeMenuMobile: boolean = false;


    constructor(
        private _webConfigService: WebConfigService,
        private $scope: ng.IScope,
        private $location: ng.ILocationService,
        private $rootScope: ng.IRootScopeService,
        private _routeParams: ng.route.IRouteParamsService,
        private $compile: ng.ICompileService,
        private $http: ng.IHttpService,
    ) {
    }
    $onInit() {
        this.getMenu();
        this.$scope.$on('accordionA:onReady', function () {
            console.log('accordionA is ready!');
        });

    }
    public getMenu() {
        this.$http.get(`api/Menu/getMenuUser`).then((res) => {
            console.log('getMenu', res);
            this.mainMenuList = res.data['menu'].filter(x => x.MENU_LEVEL == 0);
            console.log('mainMenuList', this.mainMenuList);
            let menuLV1 = res.data['menu'].filter(x => x.MENU_LEVEL == 1);


            for (let menu1 in menuLV1) {
                let menuLV2 = res.data['menu'].filter(x => x.MENU_LEVEL == 2 && x.MENU_ID == menuLV1[menu1].ID);
                menuLV1[menu1].submenus = menuLV2;
            }

            this.masterDataMenuList = res.data['masterDataMenu'];
            this.subMenuList = menuLV1;

            let isMainMenu = localStorage.getItem('mainMenuSelected');
            if (isMainMenu)
                this.mainMenuSelected = isMainMenu;
            else
                this.mainMenuSelected = this.mainMenuList[0].ID;

            console.log('subMenuList', this.subMenuList);

        }).catch((err) => {

        }).finally(() => {

        });
    }

    public onClickMenu(path, item) {
        console.log('path', path);
        console.log('item', item);

        if (item.submenus == undefined) {
            localStorage.setItem("onMenuID", item.ID);
            this.$location.path(path);
        } else if (item.submenus.length == 0) {
            localStorage.setItem("onMenuID", item.ID);
            this.$location.path(path);
        }
    }
    public onClickMasterDataMenu(item) {
        console.log('item', item);
        let path = 'master-data/' + item.ID;
        this.$location.path(path);
    }


    public onSelectMainMenu(mainMenuId: string) {
        console.log('mainMenuId', mainMenuId);
        this.mainMenuSelected = mainMenuId;
        localStorage.setItem("mainMenuSelected", this.mainMenuSelected);
    }

    public expandCallback(index, item) {
        console.log('expand:', index, item);
    };

    public collapseCallback(index, item) {
        console.log('collapse:', index, item);
    };

    public showMenu() {
        return true;
    }

    public toggleMenuMobile() {
        this.activeMenuMobile = !this.activeMenuMobile;
    }

    public closeMenuMobile() {
        this.activeMenuMobile = !this.activeMenuMobile;
    }

    public gotoHome() {
        this.$location.path('index.html#');
    }

}