var HomeController = (function () {
    function HomeController(_webConfigService, $scope, $location, $rootScope, _routeParams, $compile, $http) {
        this._webConfigService = _webConfigService;
        this.$scope = $scope;
        this.$location = $location;
        this.$rootScope = $rootScope;
        this._routeParams = _routeParams;
        this.$compile = $compile;
        this.$http = $http;
        this.menuOpen = true;
        this.activeMenuMobile = false;
    }
    HomeController.prototype.$onInit = function () {
        this.getMenu();
        this.$scope.$on('accordionA:onReady', function () {
            console.log('accordionA is ready!');
        });
    };
    HomeController.prototype.getMenu = function () {
        var _this = this;
        this.$http.get("api/Menu/getMenuUser").then(function (res) {
            console.log('getMenu', res);
            _this.mainMenuList = res.data['menu'].filter(function (x) { return x.MENU_LEVEL == 0; });
            console.log('mainMenuList', _this.mainMenuList);
            var menuLV1 = res.data['menu'].filter(function (x) { return x.MENU_LEVEL == 1; });
            var _loop_1 = function (menu1) {
                var menuLV2 = res.data['menu'].filter(function (x) { return x.MENU_LEVEL == 2 && x.MENU_ID == menuLV1[menu1].ID; });
                menuLV1[menu1].submenus = menuLV2;
            };
            for (var menu1 in menuLV1) {
                _loop_1(menu1);
            }
            _this.masterDataMenuList = res.data['masterDataMenu'];
            _this.subMenuList = menuLV1;
            var isMainMenu = localStorage.getItem('mainMenuSelected');
            if (isMainMenu)
                _this.mainMenuSelected = isMainMenu;
            else
                _this.mainMenuSelected = _this.mainMenuList[0].ID;
            console.log('subMenuList', _this.subMenuList);
        }).catch(function (err) {
        }).finally(function () {
        });
    };
    HomeController.prototype.onClickMenu = function (path, item) {
        console.log('path', path);
        console.log('item', item);
        if (item.submenus == undefined) {
            localStorage.setItem("onMenuID", item.ID);
            this.$location.path(path);
        }
        else if (item.submenus.length == 0) {
            localStorage.setItem("onMenuID", item.ID);
            this.$location.path(path);
        }
    };
    HomeController.prototype.onClickMasterDataMenu = function (item) {
        console.log('item', item);
        var path = 'master-data/' + item.ID;
        this.$location.path(path);
    };
    HomeController.prototype.onSelectMainMenu = function (mainMenuId) {
        console.log('mainMenuId', mainMenuId);
        this.mainMenuSelected = mainMenuId;
        localStorage.setItem("mainMenuSelected", this.mainMenuSelected);
    };
    HomeController.prototype.expandCallback = function (index, item) {
        console.log('expand:', index, item);
    };
    ;
    HomeController.prototype.collapseCallback = function (index, item) {
        console.log('collapse:', index, item);
    };
    ;
    HomeController.prototype.showMenu = function () {
        return true;
    };
    HomeController.prototype.toggleMenuMobile = function () {
        this.activeMenuMobile = !this.activeMenuMobile;
    };
    HomeController.prototype.closeMenuMobile = function () {
        this.activeMenuMobile = !this.activeMenuMobile;
    };
    HomeController.prototype.gotoHome = function () {
        this.$location.path('index.html#');
    };
    return HomeController;
}());
//# sourceMappingURL=home.component.js.map