﻿class CustomDialogController {
    // จะใส่ implements ng.IController หรือไม่ก็ได้

    public theForm: any;

    // - ถ้าจะอ้างถึง Service อะไรภายใน Class นี้ ก็ให้ Reference ที่ Constructor
    // - ถ้าจะส่งตัวแปรอะไรผ่านทางแอตทริบิวต์ locals แล้วไม่กำหนดแอตทริบิวต์ bindToController: true จะต้อง inject ตัวแปรนั้นที่ constructor ด้วย
    constructor(private $mdDialog: ng.material.IDialogService, private data: any, private mode: any, private okCallback: any, private cancelCallback: any, private dialogService: DialogService) {
        //console.log("--->CustomDialogController");
        //console.log("data", this.data);
    }

    public onClickOK(event) {
        //console.log('theForm', this.theForm);

        if (this.okCallback) {
            let res = this.okCallback(this.data, this.theForm, null);
            if (angular.isDefined(res) && res == true) {
                this.$mdDialog.hide();
            }
            else {
                // Nested Dialog
                /*
                this.$mdDialog.show({
                    controller: SimpleDialogController,
                    controllerAs: 'dialogCtrl',
                    targetEvent: event,
                    parent: angular.element(document.body),
                    autoWrap: true,
                    preserveScope: true,
                    multiple: true,
                    template: '<md-dialog class="md-complex-dialog"><md-conent><md-button >Close</md-button></md-conent></md-dialog>'
                });
                */
            }
        }
        else {
            this.$mdDialog.hide();
        }
    }

    public onClickCancel(event) {
        if (this.cancelCallback) {
            let res = this.cancelCallback(this.data);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    }
}

class CustomAlertDialogController {

    constructor(private $mdDialog: ng.material.IDialogService, private okCallback: any, private cancelCallback: any, private dialogService: DialogService, ) {
        //console.log("--->CustomAlertDialogController");
    }

    public onClickOK() {
       
        if (this.okCallback) {
            let res = this.okCallback();
            if (res) {              
                this.$mdDialog.hide();
            } else {
                this.$mdDialog.hide();
            }
        }
        else {
            
            this.$mdDialog.hide();
        }
    }

    public onClickCancel() {
        if (this.cancelCallback) {
            let res = this.cancelCallback();
            if (res) {
                this.$mdDialog.hide();
            }
            else {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    }
}

class CustomComfirmDialogController {

    constructor(private $mdDialog: ng.material.IDialogService, private data: any, private okCallback: any, private cancelCallback: any, private dialogService: DialogService, ) {
        //console.log("--->CustomComfirmDialogController");
        //console.log("data", this.data);
    }

    public onClickOK() {

        if (this.okCallback) {
            let res = this.okCallback(this.data);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    }

    public onClickCancel() {
        if (this.cancelCallback) {
            let res = this.cancelCallback(this.data);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    }
}

class selectEmployeeDataDialogController {

    constructor(private $mdDialog: ng.material.IDialogService, private data: any, private okCallback: any, private cancelCallback: any, private dialogService: DialogService, ) {

    }

    public onClickOK(DataSave) {
       
        if (this.okCallback) {
            let res = this.okCallback(DataSave);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    }

    public onClickCancel() {
        if (this.cancelCallback) {
            let res = this.cancelCallback(this.data);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    }
}