var CustomDialogController = (function () {
    // - ถ้าจะอ้างถึง Service อะไรภายใน Class นี้ ก็ให้ Reference ที่ Constructor
    // - ถ้าจะส่งตัวแปรอะไรผ่านทางแอตทริบิวต์ locals แล้วไม่กำหนดแอตทริบิวต์ bindToController: true จะต้อง inject ตัวแปรนั้นที่ constructor ด้วย
    function CustomDialogController($mdDialog, data, mode, okCallback, cancelCallback, dialogService) {
        this.$mdDialog = $mdDialog;
        this.data = data;
        this.mode = mode;
        this.okCallback = okCallback;
        this.cancelCallback = cancelCallback;
        this.dialogService = dialogService;
        //console.log("--->CustomDialogController");
        //console.log("data", this.data);
    }
    CustomDialogController.prototype.onClickOK = function (event) {
        //console.log('theForm', this.theForm);
        if (this.okCallback) {
            var res = this.okCallback(this.data, this.theForm, null);
            if (angular.isDefined(res) && res == true) {
                this.$mdDialog.hide();
            }
            else {
                // Nested Dialog
                /*
                this.$mdDialog.show({
                    controller: SimpleDialogController,
                    controllerAs: 'dialogCtrl',
                    targetEvent: event,
                    parent: angular.element(document.body),
                    autoWrap: true,
                    preserveScope: true,
                    multiple: true,
                    template: '<md-dialog class="md-complex-dialog"><md-conent><md-button >Close</md-button></md-conent></md-dialog>'
                });
                */
            }
        }
        else {
            this.$mdDialog.hide();
        }
    };
    CustomDialogController.prototype.onClickCancel = function (event) {
        if (this.cancelCallback) {
            var res = this.cancelCallback(this.data);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    };
    return CustomDialogController;
}());
var CustomAlertDialogController = (function () {
    function CustomAlertDialogController($mdDialog, okCallback, cancelCallback, dialogService) {
        this.$mdDialog = $mdDialog;
        this.okCallback = okCallback;
        this.cancelCallback = cancelCallback;
        this.dialogService = dialogService;
        //console.log("--->CustomAlertDialogController");
    }
    CustomAlertDialogController.prototype.onClickOK = function () {
        if (this.okCallback) {
            var res = this.okCallback();
            if (res) {
                this.$mdDialog.hide();
            }
            else {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    };
    CustomAlertDialogController.prototype.onClickCancel = function () {
        if (this.cancelCallback) {
            var res = this.cancelCallback();
            if (res) {
                this.$mdDialog.hide();
            }
            else {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    };
    return CustomAlertDialogController;
}());
var CustomComfirmDialogController = (function () {
    function CustomComfirmDialogController($mdDialog, data, okCallback, cancelCallback, dialogService) {
        this.$mdDialog = $mdDialog;
        this.data = data;
        this.okCallback = okCallback;
        this.cancelCallback = cancelCallback;
        this.dialogService = dialogService;
        //console.log("--->CustomComfirmDialogController");
        //console.log("data", this.data);
    }
    CustomComfirmDialogController.prototype.onClickOK = function () {
        if (this.okCallback) {
            var res = this.okCallback(this.data);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    };
    CustomComfirmDialogController.prototype.onClickCancel = function () {
        if (this.cancelCallback) {
            var res = this.cancelCallback(this.data);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    };
    return CustomComfirmDialogController;
}());
var selectEmployeeDataDialogController = (function () {
    function selectEmployeeDataDialogController($mdDialog, data, okCallback, cancelCallback, dialogService) {
        this.$mdDialog = $mdDialog;
        this.data = data;
        this.okCallback = okCallback;
        this.cancelCallback = cancelCallback;
        this.dialogService = dialogService;
    }
    selectEmployeeDataDialogController.prototype.onClickOK = function (DataSave) {
        if (this.okCallback) {
            var res = this.okCallback(DataSave);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    };
    selectEmployeeDataDialogController.prototype.onClickCancel = function () {
        if (this.cancelCallback) {
            var res = this.cancelCallback(this.data);
            if (res) {
                this.$mdDialog.hide();
            }
        }
        else {
            this.$mdDialog.hide();
        }
    };
    return selectEmployeeDataDialogController;
}());
//# sourceMappingURL=custom-dialog-controller.js.map