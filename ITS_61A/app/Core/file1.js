var InputDirective = (function () {
    function InputDirective($sce) {
        var _this = this;
        this.$sce = $sce;
        this.restrict = 'A';
        this.replace = true;
        this._templates = {
            "projectId": '<md-input-container class="input-container" flex-gt-sm="50" flex-gt-md="33"><label>รหัสโครงการ, ชื่อโครงการ</label><input ng-model="ctrl.model.PROJECT_ID" type="text"></md-input-container>'
        };
        this.link = function (scope, element, attrs, ctrl) {
            //element.append(scope.ctrl.$compile(this._templates[attrs.inputDir])(scope));
            //element.html('').append( scope.ctrl.$compile(this._templates[attrs.inputDir])(scope) );
            //scope.myInput = this.$sce.trustAsHtml(scope.ctrl.$compile(this._templates[attrs.inputDir])(scope));
            //scope.myInput = this.$sce.trustAsHtml(element.html()); //element.html('').append(scope.ctrl.$compile(this._templates[attrs.inputDir])(scope)).contents();
            //element.html(this._templates[attrs.inputDir]);
            //scope.ctrl.$compile(element.contents())(scope);
            //this.template = this._templates[attrs.inputDir];
            //element.html(this._templates[attrs.inputDir]);
            //let elm: any = scope.ctrl.$compile(element.contents())(scope);
            //element.replaceWith(elm);
            ////element = elm;
            //console.log(element);
            //element.html(this._templates[attrs.inputDir]);
            var elm = scope.ctrl.$compile(_this._templates[attrs.inputDir])(scope);
            element.replaceWith(elm);
            element = $(elm);
            //console.log(scope.$watch)
            //scope.$digest();
            scope.$watch('ctrl.model.PROJECT_ID', function (value) {
                console.log(value);
            });
        };
        //controller = ['$scope', '$element', ($scope, $element) => {
        //    console.log(999, $scope);
        //}];
        this.controller = function ($scope, $element) {
            console.log($element.find("input"));
        };
    }
    InputDirective.factory = function () {
        var directive = function ($sce) { return new InputDirective($sce); };
        directive.$inject = ['$sce'];
        return directive;
    };
    return InputDirective;
}());
//# sourceMappingURL=file1.js.map