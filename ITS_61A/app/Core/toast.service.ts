﻿class ToastService {

    constructor(private $mdToast: ng.material.IToastService) {
    }

    public showToast(text, pos="top right") {
        this.$mdToast.show(
            this.$mdToast.simple()
                .textContent(text)
                .position(pos)
                .hideDelay(2000)
        );
    }
}