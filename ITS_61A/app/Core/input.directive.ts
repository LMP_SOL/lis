﻿class InputDirective implements ng.IDirective {

    restrict = 'A';
    replace = true;
    _templates = {
        ATTORNET: '<md-input-container class="input-container"><label>ผู้รับมอบอำนาจ</label><input type="text" $replace/></md-input-container>',
        BTN_SAVE_DEFAULT: '<div><md-button ng-click="ctrl.Search()"><img ng-src="Content/btn/search_btn.png"></md-button><md-button ng-click="ctrl.Default()"><img ng-src="Content/btn/reset_btn.png"></md-button></div>',
        BTN_SAVE_RESET_BACK: '<div><md-button ng-click="ctrl.Save()"><img ng-src="Content/btn/save_btn.png"></md-button><md-button ng-click="ctrl.Reset()"><img ng-src="Content/btn/reset_btn.png"></md-button><md-button ng-click="ctrl.Back()"><img ng-src="Content/btn/back_btn.png"></md-button></div>',
        CONTACT_TYPE_ID: '<md-input-container class="input-container"><label>ประเภทผู้ทำสัญญา</label><input type="text" $replace/></md-input-container>',
        CUSTOMER: '<md-input-container class="input-container" flex-gt-md="33"><label>ชื่อ - นามสกุลผู้เช่าซื้อ/บริษัท</label><input type="text" $replace/></md-input-container>',
        DEALING_FILE_NO: '<md-input-container class="input-container" flex-gt-md="33"><label>หน้าสำรวจ</label><input type="text" $replace/></md-input-container>',
        DEED_NGAN: '<md-input-container class="input-container"><label>งาน</label><input max="3" min="0" step="1" type="number" $replace/></md-input-container>',
        DEED_NO: '<md-input-container class="input-container" flex-gt-sm="50" flex-gt-md="33"><label>โฉนดเลขที่</label><input step="1" type="number" $replace/></md-input-container>',
        DEED_RAI: '<md-input-container class="input-container"><label>ไร่</label><input min="0" step="1" type="number" $replace/></md-input-container>',
        DEED_WA: '<md-input-container class="input-container"><label>วา</label><input max="99.99" min="0" step="0.01" type="number" $replace/></md-input-container>',
        DISTRICT_ID: '<md-input-container class="input-container" flex-gt-sm="50" flex-gt-md="33"><label>เขต/อำเภอ</label><input md-hide-icons="triangle" $replace/></md-input-container>',
        EDITOR_NAME: '<md-input-container class="input-container"><label>ชื่อผู้แก้ไข</label><input type="text" $replace/></md-input-container>',
        FOLDER_NO: '<md-input-container class="input-container" flex-gt-sm="100"><label>เลขที่แฟ้ม</label><input type="text" $replace/></md-input-container>',
        FUNCTION_LAND_ID: '<md-input-container class="input-container" flex-gt-md="33"><label>กลุ่มที่ดิน (FUNCTION)</label><input type="text" $replace/></md-input-container>',
        HOUSE_NO: '<md-input-container class="input-container" flex-xs="100" flex="50"><label>บ้านเลขที่</label><input min="1" step="1" type="number" $replace/></md-input-container>',
        HOUSE_NO2: '<md-input-container class="input-container" flex-xs="100" flex="50"><label>ทับบ้านเลขที่</label><input min="1" placeholder="*" step="1" type="number" $replace/></md-input-container>',
        ID_CARD: '<md-input-container class="input-container"><label>เลขที่ประจำตัวประชาชน</label><input type="text" $replace/></md-input-container>',
        LAND_DESC: '<md-input-container class="input-container" flex-gt-md="33"><label>รายละเอียดที่ดิน</label><input type="text" $replace/></md-input-container>',
        LAND_USE_TYPE_ID: '<md-input-container class="input-container" flex-gt-md="33"><label>ประเภทที่ดิน</label><input type="text" $replace/></md-input-container>',
        LASTNAME_TH: '<md-input-container class="input-container"><label>นามสกุลผู้เช่าซื้อ</label><input type="text" $replace/></md-input-container>',
        MANAGEMENT_POSITION_ID: '<md-input-container class="input-container"><label>ตำแหน่ง</label><input type="text" $replace/></md-input-container>',
        MAPSHEET_NEW: '<md-input-container class="input-container"><label>ระวางแบบใหม่</label><input type="text" $replace/></md-input-container>',
        MAPSHEET_OLD: '<md-input-container class="input-container"><label>ระวางแบบเก่า</label><input type="text" $replace/></md-input-container>',
        NAME_TH: '<md-input-container class="input-container"><label>ชื่อผู้เช่าซื้อ</label><input type="text" $replace/></md-input-container>',
        OFFICE_NAME: '<md-input-container class="input-container"><label>ชื่อบริษัท</label><input type="text" $replace/></md-input-container>',
        PARCEL_NO: '<md-input-container class="input-container" flex-gt-md="33"><label>เลขที่ดิน</label><input type="text" $replace/></md-input-container>',
        PREFIX_ID: '<md-input-container class="input-container"><label>คำนำหน้า</label><input type="text" $replace/></md-input-container>',
        PROJECT: {
            template: '<md-input-container class="input-container" flex-gt-sm="50" flex-gt-md="33"><label>รหัสโครงการ, ชื่อโครงการ</label><input type="text" $replace/></md-input-container>',
            watcher: (value: any) => {
                console.log('watcher: ' + value);
            }
        },
        PROVINCE_ID: '<md-input-container class="input-container" flex-gt-sm="50" flex-gt-md="33"><label>จังหวัด</label><input md-hide-icons="triangle" $replace/></md-input-container>',
        SUBDISTRICT_ID: '<md-input-container class="input-container" flex-gt-sm="50" flex-gt-md="33"><label>แขวง/ตำบล</label><input type="text" $replace/></md-input-container>',
        TRANSFER_DATE: '<md-input-container class="input-container" flex-gt-md="33"><label>วันที่โอน</label><input type="date" $replace/></md-input-container>',
        TRANSFER_STATUS: '<md-input-container class="input-container" flex-gt-md="33"><label>สถานะการโอน</label><input type="text" $replace/></md-input-container>',
        TRANSFEROR_NAME: '<md-input-container class="input-container"><label>ชื่อผู้โอนกรรมสิทธิ์</label><input type="text" $replace/></md-input-container>',
    };

    constructor(private $compile: ng.ICompileService) {
        
    }

    link = (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, ctrl: any) => {
        let isDataObject = (typeof this._templates[attrs.inputDir] === 'object' ? true : false)
        let hasModel = (attrs.model ? true : false) 
        let hasRefer = (attrs.refer ? true : false)

        var $replace = ''
        if (hasModel) { $replace += ` ng-model="${attrs.model}"` }
        if (hasRefer) { $replace += ` ng-refer="${attrs.refer}"` }

        let model = isDataObject ?
            this._templates[attrs.inputDir].template.replace('$replace', $replace) : 
            this._templates[attrs.inputDir].replace('$replace', $replace)

        let elm = this.$compile(model)(scope);
        element.replaceWith(elm);
        element = $(elm);
  
        scope.$watch(attrs.model, this._templates[attrs.inputDir].watcher);
    }

    controller = ($scope: ng.IScope, $element: ng.IAugmentedJQuery) => {
        console.log($element.find("input"));
    }

    static factory(): ng.IDirectiveFactory {
        const directive = ($compile: ng.ICompileService) => new InputDirective($compile);
        directive.$inject = ['$compile'];
        return directive;
    }
}