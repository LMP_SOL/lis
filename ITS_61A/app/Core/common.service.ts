﻿class CommonService {

    constructor() {
    }

    private arrMonthTwoDigit = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    private arrMonthThaiFull = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
    private arrMonthThaiShort = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];

    private mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    private mS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    private getYearInter(year: string | number, isShortYear: boolean = false): string {
        let numYear = angular.isString(year) ? Number(year) : year;
        numYear -= 543;

        let strYear = "";
        if (isShortYear)
            strYear = numYear.toString().substring(2);
        else
            strYear = numYear.toString();

        return strYear;
    }
    private getYearThai(year: string | number, isShortYear: boolean = false): string {
        let numYear = angular.isString(year) ? Number(year) : year;
        numYear += 543;

        let strYear = "";
        if (isShortYear)
            strYear = numYear.toString().substring(2);
        else
            strYear = numYear.toString();

        return strYear;
    }
    private getMonthThai(month: string | number, isShortName: boolean = false): string {


        let numMonth = angular.isString(month) ? Number(month) : month;
        let indexMonth = numMonth - 1;
        let monthName;
        if (isShortName !== undefined && isShortName == true)
            monthName = this.arrMonthThaiShort[indexMonth];
        else
            monthName = this.arrMonthThaiFull[indexMonth];

        return monthName;
    }

    private formatDateThaiStyle1(strYear: string, strMonth: string, strDay: string): string {
        if (!strYear || !strMonth || !strDay)
            return ""

        let year = this.getYearThai(strYear, false);
        let month = this.getMonthThai(strMonth, true);
        let day = strDay;

        if (day.length < 2) day = '0' + day;

        return `${day} ${month} ${year}`;
    }
    private formatDateThaiStyle2(strYear: string, strMonth: string, strDay: string): string {
        if (!strYear || !strMonth || !strDay) 
            return ""

        let year = this.getYearThai(strYear, false);
        let month = this.getMonthThai(strMonth, false);
        let day = strDay;

        if (day.length < 2) day = '0' + day;

        return `${day} ${month} ${year}`;
    }


    //---------------------------------------------------------//


    public formatDateThai(strDate: string, format: string): string {
        let result = "";

        if (!strDate || strDate == null)
            return result;

        let arrDate = strDate.trim().split('-');
        let strYear = arrDate[0];
        let strMonth = arrDate[1];
        let strDay = arrDate[2];

        switch (format) {
            case "dd MMM yyyy":
                result = this.formatDateThaiStyle1(strYear, strMonth, strDay);
                break;
            case "dd MMMM yyyy":
                result = this.formatDateThaiStyle2(strYear, strMonth, strDay);
                break;
            case "yyyy":
                result = this.getYearThai(strDate, false);
                break;  
            case "MMMM":
                result = this.getMonthThai(strDate);
                break;
            case "MMM":
                result = this.getMonthThai(strDate, true);  
                break;    
            default:
                result = this.formatDateThaiStyle1(strYear, strMonth, strDay);
        }

        return result;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    private getYearEn(year: string | number, isShortYear: boolean = false): string {
        let numYear = angular.isString(year) ? Number(year) : year;

        let strYear = "";
        if (isShortYear)
            strYear = numYear.toString().substring(2);
        else
            strYear = numYear.toString();

        return strYear;
    }
    private getMonthEn(month: string | number, isShortName: boolean = false): string {


        let numMonth = angular.isString(month) ? Number(month) : month;
        let indexMonth = numMonth - 1;
        let monthName;
        if (isShortName !== undefined && isShortName == true)
            monthName = this.mS[indexMonth];
        else
            monthName = this.mL[indexMonth];

        return monthName;
    }

    private formatDateEnShort(strYear: string, strMonth: string, strDay: string): string {
        if (!strYear || !strMonth || !strDay)
            return ""

        let year = this.getYearEn(strYear, true);
        let month = this.getMonthEn(strMonth, true);
        let day = strDay;

        if (day.length < 2) day = '0' + day;

        return `${day}-${month}-${year}`;
    }

    private formatDateEnLong(strYear: string, strMonth: string, strDay: string): string {
        if (!strYear || !strMonth || !strDay)
            return ""

        let year = this.getYearEn(strYear, false);
        let month = this.getMonthEn(strMonth, false);
        let day = strDay;

        if (day.length < 2) day = '0' + day;

        return `${day} ${month} ${year}`;
    }

    public formatDateEn(strDate: string, format: string): string {
        let result = "";

        if (!strDate || strDate == null)
            return result;

        let arrDate = strDate.trim().split('-');
        let strYear = arrDate[0];
        let strMonth = arrDate[1];
        let strDay = arrDate[2];

        switch (format) {
            case "dd MMM yyyy":
                result = this.formatDateEnShort(strYear, strMonth, strDay);
                break;
            case "dd MMMM yyyy":
                result = this.formatDateEnLong(strYear, strMonth, strDay);
                break;
            case "yyyy":
                result = this.getYearEn(strDate, false);
                break;
            case "MMMM":
                result = this.getMonthEn(strDate);
                break;
            case "MMM":
                result = this.getMonthEn(strDate, true);
                break;
            default:
                result = this.formatDateEnShort(strYear, strMonth, strDay);
        }

        return result;
    }
    ///////////////////////////////////////////////////////////////////////////////////////

    public formatDate(date: Date, format: string): string {
        return moment(date).format('YYYY-MM-DD');
    }
    public stringToDate(date: string, format?: string): Date {
        if (!date || date == null) {
            return null;
        }
        else {
            if (format) {
                return moment(date, format).toDate() 
            }
            else {
                return moment(date).toDate() 
            }
        }
    }

    public ConvertIntToNumberWithCommas(n: number)
    {
        return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");//int 2000->string 2,000 
    }
   
    public ConvertNumberWithCommasToInt(n:string)
    {
        return Number(n.replace(/[^0-9\.]+/g, ""));//string 2,000->int 2000
    }

    public getMonthListThai(format: string = "MM"): Array<any> {
        let monthSource = [];
        let monthList = [];

        if (format == "MM") {
            monthSource = this.arrMonthTwoDigit;
        }
        else if (format == "MMM") {
            monthSource = this.arrMonthThaiShort;
        }
        else if (format == "MMMM") {
            monthSource = this.arrMonthThaiFull;
        }
        
        for (let i = 0; i < monthSource.length; i++) {
            let item = { ID: ("0" + (i + 1)).slice(-2), Name: monthSource[i] };
            monthList.push(item);
        }

        return monthList;
    }
    public convertYear(year: string, culture: string = "en-US"): string {
        if (!year)
            return "";
        if (culture.toLowerCase() == "en-us")
            return this.getYearInter(year);
        if (culture.toLowerCase() == "th-th")
            return this.getYearThai(year);
    }
}