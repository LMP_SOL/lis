var DialogService = (function () {
    function DialogService($mdBottomSheet, $mdDialog) {
        this.$mdBottomSheet = $mdBottomSheet;
        this.$mdDialog = $mdDialog;
    }
    DialogService.prototype.showBusyBottomSheet = function (parentElement) {
        this.$mdBottomSheet.show({
            template: "<md-bottom-sheet><md-progress-linear md-mode=\"indeterminate\"></md-progress-linear></md-bottom-sheet>",
            parent: angular.isDefined(parentElement) ? parentElement : null,
            escapeToClose: false,
            clickOutsideToClose: false
        });
    };
    DialogService.prototype.hideBusyBottomSheet = function () {
        this.$mdBottomSheet.hide();
    };
    DialogService.prototype.showMessage = function (title, message) {
        var alert = this.$mdDialog.alert()
            .title(title)
            .textContent(message)
            .ok('Close');
        return this.$mdDialog
            .show(alert)
            .finally(function () {
            alert = undefined;
        });
    };
    DialogService.prototype.showConfirm = function (title, message) {
        var confirm = this.$mdDialog.confirm()
            .title(title)
            .textContent(message)
            .ok('Yes')
            .cancel('No');
        return this.$mdDialog.show(confirm);
    };
    DialogService.prototype.showPrompt = function (title, message) {
        var confirm = this.$mdDialog.prompt()
            .title(title)
            .textContent(message)
            .placeholder('Test')
            .ariaLabel('Name')
            .initialValue('Buddy')
            .ok('Okay!')
            .cancel('I\'m a name person');
        return this.$mdDialog.show(confirm);
    };
    DialogService.prototype.dialogMessage = function (title, message) {
        var alert = this.$mdDialog.alert()
            .title(title)
            .textContent(message)
            .ok('Close');
        return this.$mdDialog
            .show(alert)
            .finally(function () {
            alert = undefined;
        });
    };
    DialogService.prototype.dialogConfirm = function (title, message, okCallback, noCallback) {
        var confirm = this.$mdDialog.confirm()
            .title(title)
            .textContent(message)
            .ok('Yes')
            .cancel('No');
        return this.$mdDialog.show(confirm).then(function () {
            console.log("Confirm");
            if (okCallback) {
                okCallback();
            }
        }, function () {
            console.log("Cancel");
            if (noCallback) {
                noCallback();
            }
        });
    };
    DialogService.prototype.dialogPrompt = function (title, message, okCallback, noCallback) {
        var confirm = this.$mdDialog.prompt()
            .title(title)
            .textContent(message)
            .placeholder('Test')
            .ariaLabel('Name')
            .initialValue('Buddy')
            .ok('Okay!')
            .cancel('I\'m a name person');
        return this.$mdDialog.show(confirm).then(function (result) {
            console.log(result);
            if (okCallback) {
                okCallback();
            }
        }, function () {
            console.log("function ()");
            if (noCallback) {
                noCallback();
            }
        });
    };
    DialogService.prototype.showCustomDialog = function (title, data, mode, okCallback, cancelCallback) {
        //let title: string = 'เพิ่มข้อมูล';
        var template = '<div class="render-topic"><render-topic topics="dlgCtrl.data.CHILD" form="dlgCtrl.theForm" mode="dlgCtrl.mode"></render-topic></div>';
        // DATA MANAGEMENT
        var content = '<md-dialog class="md-complex-dialog event-detail-dialog">' +
            '<md-header>' + title + '<div class="close-btn-md-dialog" ng-click="dlgCtrl.onClickCancel($event)"></div></md-header>' + '<form name="dlgCtrl.theForm" novalidate>' +
            '<md-content>' + template +
            '</md-content>' +
            '<md-footer>' +
            '<md-input-container class="footer-dialog" md-no-float layout="row" layout-align="center center">' +
            '<md-button type="submit" layout="row" layout-align="center center" class="md-raised md-primary" ng-click="dlgCtrl.onClickOK($event)"> ' + 'บันทึก' + ' </md-button>' +
            '<md-button layout="row" layout-align="center center" class="md-raised md-warn" ng-click="dlgCtrl.onClickCancel($event)"> ' + 'ยกเลิก' + ' </md-button>' +
            '</md-input-container>' +
            '</md-footer>' +
            '</form>' +
            '</md-dialog>';
        //controller == null ? CustomDialogController : controller;
        var controller = CustomDialogController;
        return this.$mdDialog.show({
            controller: controller,
            controllerAs: "dlgCtrl",
            template: content,
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            autoWrap: false,
            escapeToClose: false,
            locals: {
                data: data,
                mode: mode,
                okCallback: okCallback,
                cancelCallback: cancelCallback,
                dialogService: this
            }
            //,bindToController: true
        });
    };
    DialogService.prototype.showCustomAlert = function (title, btnAgree, Message, okCallback, cancelCallback) {
        var content = '<md-dialog class="md-complex-dialog">' +
            '<md-header>' + title + '<div class="close-btn-md-dialog" ng-click="dlgCtrl.onClickCancel()"></div></md-header>' +
            '<md-content> <div class="text-center -dialog" layout="row" layout-align="center center">' + Message + ' </div> </md-content>' +
            '<div> ' +
            '<md-input-container class="footer-dialog" md-no-float layout="row" layout-align="center center">' +
            '<md-button layout="row" layout-align="center center" class="md-raised md-primary" ng-click="dlgCtrl.onClickOK()"> ' + btnAgree + ' </md-button>' +
            '</md-input-container>' +
            '</div>' +
            '</md-dialog>';
        var controller = CustomAlertDialogController;
        return this.$mdDialog.show({
            controller: controller,
            controllerAs: "dlgCtrl",
            template: content,
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            autoWrap: false,
            multiple: true,
            locals: {
                okCallback: okCallback,
                cancelCallback: cancelCallback,
                dialogService: this
            }
        });
    };
    DialogService.prototype.showCustomConfirm = function (title, Message, data, okCallback, cancelCallback) {
        var content = '<md-dialog class="md-complex-dialog">' +
            '<md-header>' + title + '<div class="close-btn-md-dialog" ng-click="dlgCtrl.onClickCancel()"></div></md-header>' +
            '<md-content> <div class="text-center -dialog" layout="row" layout-align="center center">' + Message + ' </div> </md-content>' +
            '<div> ' +
            '<md-input-container class="footer-dialog" md-no-float layout="row" layout-align="center center">' +
            '<md-button layout="row" layout-align="center center" class="main-btn-dialog" ng-click="dlgCtrl.onClickOK()"> ' + '<img ng-src="Content/btn/accept_btn.png">' + '</md-button>' +
            '<md-button layout="row" layout-align="center center" class="main-btn-dialog" ng-click="dlgCtrl.onClickCancel()"> ' + '<img ng-src="Content/btn/cancel_btn.png">' + '</md-button>' +
            '</md-input-container>' +
            '</div>' +
            '</md-dialog>';
        var controller = CustomComfirmDialogController;
        return this.$mdDialog.show({
            controller: controller,
            controllerAs: "dlgCtrl",
            template: content,
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            autoWrap: false,
            multiple: true,
            locals: {
                data: data,
                okCallback: okCallback,
                cancelCallback: cancelCallback,
                dialogService: this
            }
        });
    };
    DialogService.prototype.selectEmployeeDataContentDialog = function (title, btnAgree, btnDisagree, template, data, okCallback, cancelCallback) {
        var content = '<md-dialog class="md-complex-dialog">' +
            '<md-header>' + title + '<div class="close-btn-md-dialog" ng-click="dlgCtrl.onClickCancel()"></div></md-header>' +
            '' + template + '' +
            '</md-dialog>';
        var controller = selectEmployeeDataDialogController;
        return this.$mdDialog.show({
            controller: controller,
            controllerAs: "dlgCtrl",
            template: content,
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            autoWrap: false,
            locals: {
                data: data,
                okCallback: okCallback,
                cancelCallback: cancelCallback,
                dialogService: this
            }
        });
    };
    return DialogService;
}());
//# sourceMappingURL=dialog.service.js.map