var ToastService = (function () {
    function ToastService($mdToast) {
        this.$mdToast = $mdToast;
    }
    ToastService.prototype.showToast = function (text, pos) {
        if (pos === void 0) { pos = "top right"; }
        this.$mdToast.show(this.$mdToast.simple()
            .textContent(text)
            .position(pos)
            .hideDelay(2000));
    };
    return ToastService;
}());
//# sourceMappingURL=toast.service.js.map