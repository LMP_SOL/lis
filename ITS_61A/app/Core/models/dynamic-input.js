var TopicTypeIds = (function () {
    function TopicTypeIds() {
    }
    return TopicTypeIds;
}());
TopicTypeIds.MultiAnswer = "1D4C1A98-16A5-4C91-8C1B-93F2A741B56B";
TopicTypeIds.ShortText = "FE4F942C-14FD-4E1C-ABAD-BAF0F96C5979";
TopicTypeIds.ParagraphText = "E0E86B40-D302-4E9C-AE2F-A5D16F561BC0";
TopicTypeIds.Upload = "DDF5DF99-326E-428F-ABFE-39E9EA53AB1C";
TopicTypeIds.Dropdown = "E2E85E87-D993-4417-B5C2-F89F3272BDE7";
TopicTypeIds.Header = "062326D3-F680-4EB1-AE8B-B91440E57EC6";
TopicTypeIds.Master = "9C25A252-B862-4EF2-A5E9-8C3922C1068F";
TopicTypeIds.Datetime = "62BBABEF-5565-44E8-B09A-649E6E459CDA";
TopicTypeIds.Datatable = "0CC43894-183A-4241-BB00-1F84FD37B6E7";
TopicTypeIds.SingleAnswer = "D65C5B56-A5C3-4408-A8F0-07C96BFADC46";
var menuState = (function () {
    function menuState() {
    }
    return menuState;
}());
//# sourceMappingURL=dynamic-input.js.map