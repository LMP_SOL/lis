﻿class TopicTypeIds {
    static MultiAnswer = "1D4C1A98-16A5-4C91-8C1B-93F2A741B56B";
    static ShortText = "FE4F942C-14FD-4E1C-ABAD-BAF0F96C5979";
    static ParagraphText = "E0E86B40-D302-4E9C-AE2F-A5D16F561BC0";
    static Upload = "DDF5DF99-326E-428F-ABFE-39E9EA53AB1C";
    static Dropdown = "E2E85E87-D993-4417-B5C2-F89F3272BDE7";
    static Header = "062326D3-F680-4EB1-AE8B-B91440E57EC6";
    static Master = "9C25A252-B862-4EF2-A5E9-8C3922C1068F";
    static Datetime = "62BBABEF-5565-44E8-B09A-649E6E459CDA";
    static Datatable = "0CC43894-183A-4241-BB00-1F84FD37B6E7";
    static SingleAnswer = "D65C5B56-A5C3-4408-A8F0-07C96BFADC46";
}

interface TopicCategory {
    CategoryId: string;
    CategoryName: string;
    ParentCategoryId: string;
    SortOrder: number;
    RecordCount: number;
}

interface TopicCat {
    CategoryId: string;
    CategoryName: string;
    CategorySort: number;
}

interface TopicType {
    TopicTypeId: string;
    TopicTypeName: string;
    TopicSort: number;
}

interface ValidateType {
    TypeValidationId: string;
    TypeValidationName: string;
    TypemasterValidtionId: string;
    TypeValidationSort: number;
}

interface DatetimeType {
    TypeDatetimeId: string;
    TypeDatetimeDesign: string;
    TypeDatetimeSort: string;
}

interface UploadType {
    TypeUploadId: string;
    TypeUploadFileTypeName: string;
    TypeUploadSort: number;
}

interface Topic {
    TopicId: string;
    TopicIdParent: string;
    TopicTypeDropdownId: string;
    TopicName: string;
    TopicTypeId: string;
    TopicSectionId: string;
    TopicSort: number;
}

interface TopicSetting {
    checkGrid: boolean;
    checkSearch: boolean;
    checkdisableGrid: boolean;
    checkdisableSearch: boolean;
    selectPrivate: number;
}

interface TopicSection {
    SectionId: string;
    SectionName: string;
    FormId: string;
    SectionDescription: string;
    SortOrder: number;
}

interface Checkbox {
    CheckboxId: string;
    TopicId: string;
    CheckboxName: string;
    SortOrder: number;
}

interface ResultDataConnect {
    topicCateID: string;
    topicFormID: string;
    SectionID: string;
    TopicID: string;
    TopicID2: string;
    TopicID3: string;
    TopicDataID: string;
}
interface TopicForm {
    formId: string,
    catId: string,
    topicFormName: string,
    sortOrder: number,
    rowVs:number
}
class menuState {
    public stateName: string   
}