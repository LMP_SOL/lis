﻿class DatabaseCriteriaDataId {

    //MENU - ความตกลงและ สนธิสัญญา
    static menu_agreement = "58d55228-ef2f-480a-b15e-8e10c9ce6c4d";//รายการความตกลงและสนธิสัญญา
    static menu_contract = "06e1a8a6-6855-414f-906c-8a4be7b3722c";//ตัวอย่างสัญญาและความตกลงมาตรฐาน
    static menu_cabinetRes = "123386dd-27fe-4731-bbec-76addea70e01";//มติคณะรัฐมนตรี
    static menu_agreementCon = "c83472d1-dcb4-429f-9db6-07819547977a";//เขตแดนไทยกับประเทศเพื่อนบ้าน
    static menu_dataRef = "4beb77c5-3300-4abf-af1f-9235e3af5093";//เอกสารอ้างอิง*
    //MENU - พิธีการทูต
    static menu_entryWater = "0309d890-5c8d-45d7-9836-11cb06a3a655";//การขออนุญาตผ่านน่านน้ำ
    static menu_support = "3cbded93-4bd1-4dee-8e02-b64f1701d3fb";//การรับรองการเยือน*
    static menu_supportInter = "4018d3b8-8289-429f-97d3-1b7eea022e76";//การอำนวยความสะดวก (ต่างประเทศ)*
    static menu_supportThai = "6065f61d-b0d6-4cba-8cda-ee668455959e";//การอำนวยความสะดวก (ไทย)*
    static menu_metting = "3f29a583-60f4-4f3a-bc1a-87c5686a7beb";//การประชุมเตรียมการ*
    static menu_party = "0615d1aa-6a69-449a-ba37-c6801a7421be";//การจัดเลี้ยงรับรอง*
    static menu_present = "a92d541d-56a6-48f4-9e38-8c4af5cb1f89";//การบรรยาย*
    static menu_eventSp = "ad858ca4-f8c3-4870-90ef-41f82e44df39";//กิจกรรมพิเศษ*
    static menu_reportJoin = "9d6a85b7-a299-4b2e-b067-f9791bc299cb";//การออกหนังสือประสานงาน*
    static menu_vision = "8b81b04d-ff8c-474d-8ea3-ef77c6b932c6";// วิสัยทัศน์ / ยุทธศาสตร์
    static menu_ambassador_visit = "ca1ae43d-6e39-437e-a44e-45b7b55169c3";// โครงการศึกษาดูงานด้านพิธีการทูตในต่างประเทศ
    static menu_ambassadorAndspouse_visit = "d909da34-4da9-45b7-b193-2a4e89cbc8cb";// โครงการนำคณะทูตและคู่สมรสไปทัศนศึกษา 
    static menu_manual_ambassador = "74017a3a-050e-432b-9061-5fd298739e98"; // คู่มือปฏิบัติงานด้านพิธีการทูต

    //MENU - ทรัพย์สิน
    static menu_realty = "7b530c96-71a2-4969-986b-3b41263a49f6";//ข้อมูลทะเบียนอสังหาริมทรัพย์
    static menu_saveRequest = "b03a325f-205d-433a-908b-3eb2cd2a354b";//บันทึกการตั้งคำขอ
    static menu_saveDesign = "1e0577cb-ca13-44a7-a602-7d1315bfdf87";//บันทึกการออกแบบ
    static menu_saveBulider = "b01bd5f6-afb7-4fd9-8392-ccaf7fb40507";//บันทึกการก่อสร้าง
    static menu_saveRepair = "c4549cca-85d4-4c9d-a7ba-8a16354fdd73";//บันทึกการซ่อมแซม
    static menu_tempService = "f0cf2795-07e4-4d40-bfd9-a2332d5470d5"//ลูกจ้างเหมาบริการ
    //MENU - การกงสุล
    static menu_amountThaiInter = "b77134de-3e49-48b4-882c-6c635e44b7e7";//จำนวนคนไทยในต่างประเทศ
    static menu_helpThaiInter = "534e86b4-3a1b-43eb-9ec1-62413b84c529";//สถิติการให้ความช่วยเหลือ คนไทยในต่างประเทศ
    static menu_exceptVisa = "ee457755-a655-4d2c-bcfe-6345bf30ebb8";//การยกเว้นการตรวจลงตราและการขอ Visa on Arrival (VoA)
    static menu_approveStatic = "8e9baaf9-222f-4af8-be49-5a816f13e7e4";//สถิติการตรวจลงตรา
    static menu_approveSta = "9ae75796-3992-4159-9f49-d8d4019abaad";//สถิติการรับรองเอกสารและนิติกรณ์
    static menu_approveRep = "1f2191b1-3bde-443e-b40a-3538ba029fce";//แนวทางการรับรองเอกสารและนิติกรณ์
    static menu_visaSta = "f01a39db-6049-4598-9b7b-0deeb294ee55";//สถิติการออกหนังสือเดินทาง
    //MENU - กรอบความร่วมมือ
    static menu_cooperation = "0aeb4ace-8cce-4365-b1a9-028f9b7a5a24"//กรอบความร่วมมือ
    static menu_cooMeeting = "7390eca0-fcfb-4c01-84fa-a3af7beee24b"  //กลไกการประชุม

    //MENU - งบประมาณ
    static menu_budget = '99eb27a3-5120-46b3-a363-70b49fd58ad3' // งบประมาณโครงการ

    //menu class map add-on
    static BS101 = "58d55228-ef2f-480a-b15e-8e10c9ce6c4d";//รายการความตกลงและสนธิสัญญา
    static BS105 = "c83472d1-dcb4-429f-9db6-07819547977a";//เขตแดนไทยกับประเทศเพื่อนบ้าน
    static BS301 = "13e95a8e-e9f5-4371-8363-5bd6545e6d54";//บุคลากรกระทรวง
    static BS30401 = "47f95118-e683-4380-a064-38e423f4d553";//ข้อมูลกงสุลกิตติมศักดิ์
    static BS305 = "ed076b76-81df-4219-b633-9030507c5010";//นักเรียนทุน
    static BS306 = "cc3f7b1f-08f8-4f2e-bfeb-ecf151d6db9e";//ภาพรวมบุคลากร(class map 30x)

    static BS40101 = "325d87ff-fe3c-40a9-b5ff-3708e003f415";//สถานทูต / สถานกงสุล / องค์การระหว่างประเทศในไทย
    static BS40102 = "6d49d904-26e4-4002-90bd-a70a2769efb9";//บุคคลในคณะทูต / กงสุล / องค์การระหว่างประเทศในไทย
    static BS40301 = "d909da34-4da9-45b7-b193-2a4e89cbc8cb";//โครงการนำคณะทูตและคู่สมรสไปทัศนศึกษา 
    static BS40501 = "0309d890-5c8d-45d7-9836-11cb06a3a655";//การขออนุญาตผ่านน่านน้ำ
    static BS40701 = "9ab3325d-18f2-4dc7-b677-10ee455d9866";//โครงการสัญจรฯ
    static BS40702 = "ca1ae43d-6e39-437e-a44e-45b7b55169c3";
    //MENU - กรอบความร่วมมือ
    static BS501 = "0aeb4ace-8cce-4365-b1a9-028f9b7a5a24"//กรอบความร่วมมือ
    static BS503 = "7390eca0-fcfb-4c01-84fa-a3af7beee24b"  //กลไกการประชุม

    static BS601 = "933bc04f-61e5-4078-95f9-e24c89dec81e";//ข้อมูลประเทศ
    static BS603 = "3dcb819e-8b62-420d-8f6a-7f10c726c7e5";//คำแนะนำการเดินทางไปต่างประเทศ (Outbound)
    static BS604 = "ee63c971-626c-4d8e-b013-6a5af3069a59";//คำแนะนำการเดินทางของต่างประเทศต่อประเทศไทย (Inbound)
    static BS605 = "d65dafc9-6e8f-4bea-80e9-79123b38f608";//ประวัติบุคคลสำคัญ
    static BS609 = "f157ae71-cebb-4ca0-aa59-7f532932a9f2";//อนุสรณ์สถาน

    static BS70101 = "63399bd1-4a60-444c-b249-871ea9cd86c8";//ข้อมูลสำนักงาน
    static BS703 = "383b6b16-190c-4036-8230-ae0b2e5df282";//การตรวจราชการในต่างประเทศ

    static BS802 = "d2127c48-59bb-4f07-8637-183ba1d50822";//งบเดือน
    static BS803 = "fdc029ea-b8c2-4e19-baf1-6f0c3171a961";//งบประมาณที่ได้รับการจัดสรร
    static BS80401 = "ec86d792-23b8-40d1-b0dc-5b5c5eba4f46";//รายได้แผ่นดินด้านการกงสุล
    static BS80402 = "3f4c41c2-f1cc-4c11-8d53-fca3ad4b7f9b";//รายได้ค้างนำส่ง

    static BS905 = "ff18c231-54f6-48a4-a665-f6900b55eb06";//ตารางจัดส่งถุงเมล์การทูตออกจากกระทรวงฯ
    static BS907 = "bbf62ac0-13af-4c04-b8c4-31c525c580e4";//โครงการสัญจร
    static BS914 = "07ffb7b3-0a1e-4689-b43e-a840ef7792c4";//สถานที่ศึกษาดูงาน / การจัดประชุม สัมมนา
    static BS915 = "6a1d7567-4022-48ce-b8c5-5a444e36a30a";//Organizer 
    static BS91701 = "37e6bcbc-3e21-4b06-ab86-ffd1c4c0f341";//การติดตามข้อสั่งการ (ภาพรวม)
    static BS91702 = "2a389937-7e30-416a-b369-c0147e1bca90";//การติดตามข้อสั่งการ (แยกตามหน่วยงาน)

    static BS100301 = "ee457755-a655-4d2c-bcfe-6345bf30ebb8";//การยกเว้นการตรวจลงตราและการขอ Visa on Arrival (VoA)
    static BS100302 = "8e9baaf9-222f-4af8-be49-5a816f13e7e4";//สถิติการตรวจลงตรา
    static BS100401 = "9ae75796-3992-4159-9f49-d8d4019abaad";//สถิติการรับรองเอกสารและนิติกรณ์
    static BS1005 = "f01a39db-6049-4598-9b7b-0deeb294ee55";//สถิติการออกหนังสือเดินทาง
    static BS1006 = "bf4a6719-946e-4292-b348-68df4f7159e2";//หน่วยหนังสือเดินทางเคลื่อนที่(สัญจร)
    static BS1008 = "3ae5f3f7-592b-47c2-b4be-13223633b45e";//เหตุการณ์ฉุกเฉินในต่างประเทศ
    static BS1009 = "58e6a1f9-2361-42fe-a429-3d4699a32008";//สำนักงานหนังสือเดินทาง
    //------------------- TOPIC CRITERIA ----------------------------

    //---- กรอบความร่วมมือ
    static cooperation_type = "17C0E9D0-3FC5-4EF1-BF19-BDF6443E6693";  //  ประเภทของกรอบความร่วมมือ
    static cooperation_main = "9C1397E3-A28E-4D2C-AA52-0089B41C8BEC";  //  กรอบความร่วมมือหลัก
    static cooperation_header = "072C1C09-85D6-4365-BD35-2F2AE82DE6D1"  //  หัวข้อหลัก
    static cooperation_detail = "61D898A0-8A03-4284-9531-7C515EEA8707"  //  หัวข้อย่อย
    static cooperation_member_country = "264C99E6-0690-45EB-BDA9-D4DAB9489315"  //  ประเทศสมาชิก
    static cooperation_country = "76C0B0C8-3D2F-4D67-A252-6BD6BBE3013A"  // ประเทศ
    static cooperation_member_org = "A9B7FCD9-7B65-46CC-89A1-5A1032BD6D56"  //  กรอบความร่วมมือ / องค์การ
    static cooperation_watch = "547274BA-0F44-48D7-88A0-BD86A0EE6971"  // ผู้สังเกตการณ์
    static cooperation_watch_country = "F62E1BE7-2E7C-4D50-B95A-95D3E9F4AD90"  //  ประเทศผู้สังเกต
    static cooperation_watch_org = "8F3BFED0-E64E-4226-B728-95239CDB8A5B"  //  กรอบความร่วมมือ / องค์การ
    static cooperation_status = "53C0D97D-0617-4F99-98CE-C2F21FDD2CBF"  //  สถานะประเทศไทย
    static cooperation_short_name_th = "B550BBA9-E97B-44A9-BEF5-02408C7F0382"  //  ชื่อย่อของกรอบความร่วมมือ (ภาษาไทย)
    static cooperation_short_name_en = "134321A4-D3F2-4F9D-AE7A-1144088E8615"  //  ชื่อย่อของกรอบความร่วมมือ (ภาษาอังกฤษ)
    static cooperation_keyword = "C9735700-9A09-42DC-90F2-45E3D5475234"  // keyword
    static operation_cooperation = ['AND', 'AND', 'AND', 'AND', 'AND', 'AND', 'AND', 'AND', 'AND', 'AND', 'AND', 'OR', 'OR', 'OR'];

    //---- กลไลการประชุม
    static cooMeeting_startDate = "29D8BC51-AFE9-413A-A627-86EF03EFB9C9";  // วันเริ่มต้นการประชุม
    static cooMeeting_endDate = "3B90135E-7A42-4486-9FE2-381B99083165";  //  วันสิ้นสุดการประชุม
    static cooMeeting_name_th = "D091A67B-D422-491C-B7AF-C8CBFA13D3FA";  // ชื่อการประชุม ไทย
    static cooMeeting_name_en = "5FCCBE19-E1BD-4B32-A62A-006709CDA5F3";  // ชื่อการประชุม อังกฤษ
    static operation_cooMeeting = ['AND', 'AND', 'OR', 'OR'];

    //---- สัญญา / ความตกลงมาตรฐาน
    static contract_agree_country = "efc2afbf-b814-4c2f-ab96-a991115bea30";   // ประเทศ
    static contract_agree_sot = "2a2c5e53-9ff7-46e5-9221-55284d171e7c"; // สอท. / สกญ.
    static contract_agree_sub_type = "e904321a-2e67-4bdc-835c-2a23f25bc4fc"  // ประเภทย่อยของเรื่อง
    static contract_agree_head = "38c269f7-1028-457e-99a6-19aab37d04d8";      // เรื่อง
    static contract_agree_head_type = "f7551e16-2d61-4dbf-a565-80c07cdf735c";      // ประเภทของเรื่อง

    //---- มติคณะรัฐมนตรี
    static cabinetRes_head = "27d90dbf-92c9-48f1-be91-b05caea324f4";  //  เรื่อง
    static cabinetRes_date = "5b618f7e-5141-49dc-998c-6eda5d687a72"; // วันที่คณะรัฐมนตรีมีมติ
    static cabinetRes_gov = "9020dcff-0948-430a-9ea0-cb3791607ef2";  // ส่วนราชการเจ้าของเรื่อง
    static cabinetRes_key = "91c68f35-1f61-426c-b049-3c63e896800b";  //  keyword

    //---- เขตแดนไทยกับประเทศเพื่อนบ้าน
    static agreementCon_country = "d4d20370-630e-4cbb-acca-adbbac14d853";  //  ประเทศ
    static agreementCon_province = "bd9a2983-655c-4133-9aac-68e797d2e74c"; //  จังหวัด/รัฐ/ภูมิภาค/แขวงติดต่อ (ในเพื่อนบ้าน)
    static agreementCon_mountain = "8471fd76-addc-467a-8537-cbd800d4296a"; //  ชื่อเทือกเขา/ภูเขา
    static agreementCon_river = "6aaf606f-22b4-4751-951e-e3bdba108682";  //  ประเภทเขตแดนแม่น้ำ / ลำคลอง

    //---- ข้อมูลอ้างอิง
    static dataRef_country = "a7755504-3a66-4477-a6bc-6c5bead635cb"  // ประเทศ
    static dataRef_date = "d0bd8891-0914-481c-81bd-52291c8f7400"  //  วันที่ของเอกสาร
    static dataRef_head_type = "17224970-5333-4b28-86af-a825fbbe59df"; //  ประเภทของเรื่อง
    static dataRef_sub_type = "65fb5c10-96ac-41a6-9dce-9e74a0a956cc";  //  ประเภทย่อยของเรื่อง
    static dataRef_head = "dbbb6b31-ad51-4c2d-a81f-8fcec49ff3ad";  //  เรื่อง

    //---- ข้อมูลทะเบียนอสังหาริมทรัพย์
    static realty_region = "608d92b4-546f-480f-8ded-0208faa62916"; //  ภูมิภาค
    static realty_part = "a1dde64e-34bb-4b1d-ba11-fe2f73ff6016"; //  อนุภูมิภาค
    static realty_country = "3dffb73a-c68e-497e-86a2-d0bc854282f1"; // ประเทศ
    static realty_dep = "4d8d2055-0740-49eb-8679-fa0577bd6dc5"; // กรม / สำนัก
    static realty_sot = "d0cac56f-da1b-46e5-83e8-4849ecc1d3d7"; // กอง / สอท
    static realty_group = "2e4d809c-33b8-4dcb-a838-380cbf929214"; // กลุ่มอสังหาริมทรัพย์
    static realty_type = "fdb02a4c-0c05-4e63-9caf-40aab5b3ad92";  // ประเภทอสังหาริมทรัพย์  
    static realty_status = "53d817f6-0fb0-4027-8a34-9fa5c17471a4"; // สถานะการใช้งาน
    static realty_date = "9e0ce172-281e-48af-ace6-3c3ac518f1ce"; // วันที่ได้มา
    static realty_name = "1ccd4c6b-90b6-4746-a179-1a6b2dbfe5b7"; // ชื่ออสังหาริมทรัพย์

    //---- บันทึกการตั้งคำขอ
    static saveRequest_dep = "92614a1f-3c60-41ae-8b16-7d58d3f45b11"; //  กรม / สำนัก
    static saveRequest_sot = "3e74d250-bb2c-4a94-8034-a78ced3e09b4"; //  กอง / สอท
    static saveRequest_budget_year = "6808de13-75d0-43c4-a4f4-810b42b4a97e"; //  ปีงบประมาณที่ได้รับอนุมัติ
    static saveRequest_source_budget = "ae24110a-bbd9-4d84-9efd-afbfb3cd2230";  //  แหล่งงบประมาณ
    static saveRequest_budget_type = "0ba19d05-798a-4dcd-8297-2a4b3a020e57"; //  ประเภทงบประมาณ
    static saveRequest_result = "d277cb6c-c471-46e3-a93f-f8adb0703aa2"; //  ผลการอนุมัติ
    static saveRequest_status = "be3f3a8a-b70a-4d3e-83f7-df7ec5478546"; // สถานะการโอนกรรมสิทธิ์

    //----  บันทึการออกแบบ
    static saveDesign_dep = "ec077377-f63a-4310-b436-52fb5ae9d472";  //  กรม สำนัก
    static saveDesign_sot = "005b7d76-99ac-4ed8-8c3b-b5c5bd6d5ba8";  //  กอง สอท
    static saveDesign_budget_year = "73c18ae7-dbf0-4048-8e08-caeb8b254a2b"; //  ปีงบประมาณ
    static saveDesign_type = "a1fa958d-c0d5-43d6-82da-31424db22166"; //  ประเภทอสังหาริมทรัพย์
    static saveDesign_name = "a0eb3f00-0f6b-4260-b945-8d85c540b346"; //  ชื่ออสังหาริมทรัพย์

    //----  บันทึกการก่อสร้าง
    static saveBuilder_dev = "1016e7b4-72dd-491b-be59-ff8e4007be61";
    static saveBuilder_sot = "e107acb3-5cc4-4297-9083-51d6d1689b61";
    static saveBuilder_type = "d270908a-caf3-4e5b-addb-e657ddc5ee8b";
    static saveBuilder_status = "412b5b43-a59d-4e7c-baa7-1e8799223454";
    static saveBuilder_date = "913b73e1-ec4d-4210-9b56-59eff98d1d31";

    //----  บันทึกการซ่อมแซม
    static saveRepair_dev = "83a2b516-a248-4942-9252-ce2b9b382d5f";
    static saveRepair_sot = "d23a444a-d104-47eb-96b5-1f299241c5ea";
    static saveRepair_budget_year = "78f30b9b-3147-4242-b581-1508568e2202";
    static saveRepair_budget_type = "2cc0e1e3-1d8c-43d5-a25b-6cc6fc962886";
    static saveRepair_list = "f150ab16-7106-47d8-b4d0-62ead9f210cd";

    //----  ลูกจ้างเหมาบริการ
    static tempService_dev = "069beea9-6de0-47a5-92fb-97d00db11efc";  // กรม
    static tempService_sot = "3cf3e08e-ae97-46a7-a311-3f92428e8819";  // กอง
    static tempService_div = "153e9378-c183-4023-8be6-4161d2c7d41e";  // สังกัด
    static tempService_position = "e01717c2-9ea3-4d95-92d3-532acf874e38";  // ตำแหน่ง
    static tempService_status = "8f26eebb-ed01-4989-9d50-d475279e1561";  // สถานะการทำงาน    
    static tempService_prefix_th = "88ebb20b-a341-443f-abe6-75958c166f4d";  // คำนำหน้า (ภาษาไทย)
    static tempService_firstname_th = "38b5f82f-16a9-46bf-b1db-e75c06d0ab9e";  // ชื่อ (ภาษาไทย)
    static tempService_midname_th = "ffdf3d92-5177-4203-9977-2c1d046a3f8f";  // ชื่อกลาง (ภาษาไทย)
    static tempService_lastname_th = "9e3d9a66-1c74-4120-8cd8-55c4a33a4b57";  // นามสกุล (ภาษาไทย)
    static tempService_firstname_en = "a5b37921-b713-4001-951a-0b085cbd3871";  // ชื่อ (ภาษาอังกฤษ)
    static tempService_midname_en = "b66f3362-568f-4c15-81d8-f51353a6b627";  // ชื่อกลาง (ภาษาอังกฤษ)
    static tempService_lastname_en = "bfd55597-1b49-443e-a8d2-e2df0fd72e66";  // นามสกุล (ภาษาอังกฤษ)
    static operation_tempService = ['AND', 'AND', 'AND', 'AND', 'AND', 'OR', 'OR', 'OR', 'OR', 'OR', 'OR', 'OR'];


    //---- การรับรองการมาเยือน ----                  
    static support_date_in = "590e5827-3756-4f5b-8ef0-913f3448e551"; // วันที่เดินทางเข้า
    static support_country = "367e9cb7-eb8b-4897-b0dd-3a9f8aa404f5"; // ประเทศ
    static support_person_level = "45f3705c-79d2-43da-9c3c-16ae138c0e51"; //ระดับบุคคล
    static support_guest_type = "e8c46ba5-a89f-440e-8b3b-936833c5d1b8"; // ประเภทแขกเยือน
    static support_easy_ambas = "a339dbab-0c5c-4fa4-8439-febbbfc45717"; // ขอรับการอำนวยความสะดวกจากพิธีการทูต
    static support_name_thai = "5563558d-0472-49cf-bbe7-8c60b8b22149"; // ชื่อบุคคลสำคัญ (ภาษาไทย)
    static support_name_eng = "d7e9f7a8-d200-4176-a3af-dc94ec469b1c"; // ชื่อบุคคลสำคัญ (อังกฤษ)

    //---- การอำนวยความสะดวก (ต่างประเทศ) -----
    static support_inter_date_in = "88861836-d840-49bc-86b2-6c53a35c8bd9"; // วันที่เดินทางเข้า
    static support_inter_country = "f08e085d-5cbc-40c5-881f-119c66143dd5"; // ประเทศ
    static support_inter_person_level = "f12d5fef-a795-42ae-924f-81f59aa93e19"; // ะดับบุคคล
    static support_inter_guest_type = "4cc5971e-8443-4cf1-8cf6-6f0eac413d4f";  // ประเภทแขกเยือน
    static support_inter_name_thai = "63d1cecf-3119-4ceb-b169-a10c27a59cb9"; // ชื่อบุคคลสำคัญ (ภาษาไทย)
    static support_inter_name_eng = "0fa169ac-0316-4abf-a598-f673b80d587d";  // ชื่อบุคคลสำคัญ (อังกฤษ)

    //---- การอำนวยความสะดวก (ไทย)
    static support_thai_date_out = "c6ef6fc4-48d6-48ea-9a55-f204f35d142a";  // วันที่เดินทางออก
    static support_thai_country = "05d1f843-9699-4892-8144-2e4ae23477a9";  // ประเทศ 
    static support_thai_person_level = "2f41f69f-d0df-4e42-bab6-da4120e1ad22";  //  ระดับบุคคล
    static support_thai_name_thai = "c4881fc8-9784-41c4-8b6a-245d8e4f9ba0";  //  ชื่อบุคคลสำคัญ (ภาษาไทย)
    static support_thai_name_eng = "a3efd3c8-bbd3-422a-8086-26d1ecc193e3";  //  ชื่อบุคคลสำคัญ (อังกฤษ)
    static support_thai_position = "f0ba736b-6701-47b0-b693-e461b838ddab";  //  ตำแหน่ง

    //---- การประชุมเตรียมการ
    static metting_date = '7c6c7328-aff6-4ecc-a2fb-140c1de3ec51';  //  วันที่จัด
    static metting_head = 'cba21d97-dea7-4920-9792-00753d6d3fc4';  //  เรื่อง  

    //---- การบรรยาย
    static speak_date = "df84ab65-949f-4830-97c4-1e10185e056c"; //  วันที่จัด
    static speak_type = "b6d2f5d6-c446-4148-9149-8751611cd816"; //  ประเภทการบรรยาย
    static speak_head = "cfe08935-62c6-4f51-a146-152e2aec80af"; //  เรื่อง
    static speak_dev_request = "31c32665-bed2-4f04-9bf0-cbdb2058bb87";  //  หน่วยงานที่ขอ 

    //---- การจัดเลี้ยงรับรอง
    static party_date = "9cbe09b8-6ccb-486f-9d7b-907cdce0a187";    //  วันที่จัด และ เวลาเริ่มงาน
    static party_country = "d992a419-d21c-4b7b-9ed4-73d081fab456";  //  ประเทศ
    static party_person_level = "5a8add3e-2caa-442b-86f2-398deb119510";  //  ระดับบุคคล
    static party_type = "4de6603f-8fdd-4324-a3e5-e6d8557abd89";  //  ประเภทงานเลี้ยง
    static party_name_thai = "5ce5c5a5-2188-4941-ae0c-1ea1d5354323";  //  ชื่อบุคคลสำคัญ (ภาษาไทย)
    static party_name_eng = "c114c2bf-57e2-40ce-912c-98d43fd40941";  //  ชื่อบุคคลสำคัญ (ภาษาอังกฤษ)
    static party_position = "e8ba5037-670a-49d1-af1c-30f793e4c468";  //  ตำแหน่ง 

    //---- กิจกรรมพิเศษ
    static event_date = "c01aa010-b67a-4e34-90f3-f3f8bb8eb48b";  // วันที่เริ่มกิจกรรม
    static event_type = "a11b0625-1c88-45d4-a690-bf0176b19778";  // ประเภทกิจกรรม
    static event_name = "c7f2ffe6-166c-424b-a84e-e96a66c8365a";  //  ชื่อกิจกรรม

    //---- การออกหนังสือประสานงาน
    static coordinate_date_in = "a4f911d9-7397-4fcb-93a6-4948970ba1b6";  //  วันที่เดินทางเข้า
    static coordinate_date_out = "fe4306a7-612d-4829-94cb-f1282e8307ea";  //  วันที่เดินทางออก
    static coordinate_country = "338eda5b-3d0c-49a2-906b-ec9309887a32";  //  ประเทศ
    static coordinate_person_level = "5a69a55c-a416-4b71-8f8e-b51d836d5b25";  //  ระดับบุคคล
    static coordinate_name_eng = "f1cbdfbe-209d-47d1-98f4-5d60150f9060"; //  ชื่อบุคคลสำคัญ (ภาษาอังกฤษ)
    static coordinate_position = "4eda867b-fe38-4690-8e4e-3eb2987dc84b";  //  ตำแหน่ง

    //---- กงศุล  -- จำนวนคนไทยในต่างประเทศ
    static thai_people_inter_region = "ecbe65b8-8fe5-426f-9cf2-67e053272646";  //  ภูมิภาค
    static thai_people_inter_part = "69858c96-5016-4088-b73a-e4706ac336cb";  //  อนุภูมิภาค
    static thai_people_inter_country = "6f98421b-72c7-4a04-a33d-d35862f7d0b0";  //  ประเทศ
    static thai_people_inter_year = "45807de3-43fe-49bc-bc1d-e067bb03f3a5";  //  ปี
    static thai_people_inter_career = "93117bd2-59f6-4460-9308-935e127f2008";  //  อาชีพ 

    //---- กงศุล  -- สถิติการให้ความช่วยเหลือ คนไทยในต่างประเทศ
    static help_thai_people_region = "c14cd835-0cfb-4412-b2ee-d7fe2f86df60"; // ภูมิภาค
    static help_thai_people_part = "4a79adf1-e917-4b3c-9e52-7662173ef839"; // อนุภูมิภาค
    static help_thai_people_country = "c920ecee-04fa-46b1-93d4-2237a5b1741c";  //  ประเทศ
    static help_thai_people_type = "13d6df13-c692-4bdb-8ea8-55412ba964aa"; // ประเภทการให้ความช่วยเหลือ

    //---- กงศุล  -- การยกเว้นการตรวจลงตราและการขอ Visa on Arrival (VoA)
    static except_visa_country = "77dd51d7-2732-45d3-b581-179258dfbeac";  // ประเทศ  ดินแดน
    static except_visa_agree_type = "87fec96b-d01f-457e-82cd-3c60b72a3015";  // ประเภทข้อตกลง

    //---- กงศุล  -- สถิติการตรวจลงตรา
    static static_inspect_office = "370d8288-e1cb-4ff5-8fd3-3132efe3e935";  //  สำนักงาน

    //---- กงศุล  -- สถิติการรับรองเอกสารและนิติกรณ์
    static static_approve_date = "621a2381-5db7-4bbc-8cb1-f0d82021ea53";  // เดือน ปี
    static static_approve_office = "238d6018-0fc5-4427-993e-9eb8c9dabc46";  // สำนักงาน

    //---- กงศุล  -- แนวทางการรับรองเอกสารและนิติกรณ์ 
    static static_process_approve = "77d07956-ae1c-4465-bc2b-32913894f27b";  // การรับรอง

    //---- กงศุล  -- สถิติการออกหนังสือเดินทาง
    static static_visa_address_office = "a44f5674-2f6e-4ef1-88f2-89b975a357a4"; // ที่ตั้งสำนักงาน
    static static_visa_office = "d78c4a49-d88b-45fd-a3c9-87fdd3c86307";  // สำนักงาน

   
    //---- Bound --------
    static Province = "b79337d5-c7ba-45dc-9c09-5a4bb3cc7f1a";
    static RiverName = "d5c06689-935f-489a-8d27-74802cbdcfa4";

    //---- Entry Water ---------
    static CountryShip = "bffefdef-145a-4dd7-ab8a-2bf1a5f168ba";
    static CountryEntry = "5a9463e8-339a-4173-b0b8-5d764a5b2d58";
    static Objective = "cca74055-d674-43dc-a70e-9f87df1ec82f";

   
    //---- วิสัยทัศน์ / ยุทธศาสตร์
    static namecard = "e18f0e59-e110-40d1-ad94-783a2e6f42f3"; // ชื่อ
    static year = "4277de6a-af45-4c7b-8453-ac877ccd4c28";  // ปี
    static doc = "065334e6-9f03-403f-bae3-eb5b55b02fcf";  // เอกสารแนบ


    
    //---- โครงการศึกษาดูงานด้านพิธีการทูตในต่างประเทศ
    static fiscal_year = "05247bfb-c095-4a18-a6b0-3772e1f95767";  // ปีงบประมาณ
    static ProjectType = "60a9419e-3f11-43dc-8d86-79cf26989530"; // ประเภทโครงการ
    static region = "608d92b4-546f-480f-8ded-0208faa62916"; //  ภูมิภาค
    static part = "a1dde64e-34bb-4b1d-ba11-fe2f73ff6016"; //  อนุภูมิภาค
    static country = "3dffb73a-c68e-497e-86a2-d0bc854282f1"; // ประเทศ
   
   


    




    //-------------------- TOPIC LIST -----------------------

    //------ กรอบความร่วมมือ
    static topicList_card_cooperation = [
        "D9A88FC7-2DE1-4144-BDF3-A74C74198C33",  // รูป
        "792F4B52-111B-418A-8334-B11F1DA5D2CA",  // ชื่อกรอบความร่วมมือ (ภาษาอังกฤษ)
        "134321A4-D3F2-4F9D-AE7A-1144088E8615",  //  ชื่อย่อของกรอบความร่วมมือ (ภาษาอังกฤษ)
        "77352F86-D68C-4F32-8AB3-241BAFBD7DCB",  //  วันก่อตั้งองค์กร
        "04AE3BEB-9044-4318-B0F3-7F04219B8E87",  //  วันเข้าเป็นภาคี
        "264C99E6-0690-45EB-BDA9-D4DAB9489315",  //  ประเทศสมาชิก
        "53C0D97D-0617-4F99-98CE-C2F21FDD2CBF"   //  สถานะประเทศไทย
    ];
    static sort_cooperation = ['1', '2', '2', '4', '5', '3', '6'];

    //----- กลไลการประชุม
    static topicList_card_cooMeeting = [
        "5FCCBE19-E1BD-4B32-A62A-006709CDA5F3",  // ชื่อการประชุม (ภาษาอังกฤษ)
        "F289EACD-A185-45FC-B2E0-8B664AF23B8E",  // เมืองที่ประชุม
        "7FA6EDF2-F51C-484F-A782-BE4435C730EC",  // ประเทศเจ้าภาพ
        "29D8BC51-AFE9-413A-A627-86EF03EFB9C9",  // วันเริ่มต้นการประชุม
        "3B90135E-7A42-4486-9FE2-381B99083165",  // วันสิ้นสุดการประชุม
        "294ECADE-02EC-4A58-8DC5-AB3E563D14F0"  //  ระดับการประชุม
    ];
    static sort_cooMeeting = ['1', '2', '3', '4', '5', '6'];

    //----- รายการความตกลงและสนธิสัญญา
    static topicList_card_s_agreementList = [
        "7ae8df13-9ae3-4341-9bbc-b7854c8bc922",  // ชื่อความตกลง / สนธิสัญญา (ภาษาอังกฤษ)
        "4eb9d6b4-be46-4567-bfe1-4ea1d927f154",  // ประเทศ
        "6b418533-0cc1-47aa-9fdf-048ade824503",  // สถานะของความตกลง / สนธิสัญญา อยู่ระหว่างการพิจารณาของ
        "7b6cf6c5-43ac-48aa-afec-04186b825b35",  // วันลงนาม วันสิ้นผล การต่ออายุ (ปี)        
    ];

    //----- สัญญา / ความตกลงมาตรฐาน
    static topicList_card_s_contractStd = [
        "38c269f7-1028-457e-99a6-19aab37d04d8",  // เรื่อง
        "f7551e16-2d61-4dbf-a565-80c07cdf735c",  // ประเภทของเรื่อง
        "e904321a-2e67-4bdc-835c-2a23f25bc4fc",  // ประเภทย่อยของเรื่อง
        "efc2afbf-b814-4c2f-ab96-a991115bea30",  // ประเทศ  
        "2a2c5e53-9ff7-46e5-9221-55284d171e7c",  // สอท. / สกญ.
        "ff60966b-a879-443d-b520-17595b0fb639"   // เอกสารแนบ   
    ];

    //----- มติคณะรัฐมนตรี
    static topicList_card_s_cabinetRes = [
        "27d90dbf-92c9-48f1-be91-b05caea324f4",  // เรื่อง
        "5b618f7e-5141-49dc-998c-6eda5d687a72",  // วันที่คณะรัฐมนตรีมีมติ
        "9020dcff-0948-430a-9ea0-cb3791607ef2"  // ส่วนราชการเจ้าของเรื่อง       
    ];

    //----- เขตแดนไทยกับประเทศเพื่อนบ้าน
    static topicList_card_s_agreementCon = [
        "d4d20370-630e-4cbb-acca-adbbac14d853",  // ธงชาติ
        "bc4cc79c-4615-4733-a07b-228084354d80",  // ชื่อเขตแดน
        "13adad46-bccd-4a99-a5b4-e931f3c91057",  // ความยาวเส้นเขตแดน (กม.)
        "f55a045e-f16c-46ce-91ed-219a299f07ec",  // กลไกเจรจาหลัก (ภาษาไทย)
        "d1a61d8e-b85d-4af1-85fb-f38e2800b813"   // กลไกเจรจาหลัก (ภาษาอังกฤษ)
    ];

    //----- ข้อมูลอ้างอิง
    static topicList_card_s_dataRef = [
        "dbbb6b31-ad51-4c2d-a81f-8fcec49ff3ad",  // เรื่อง
        "17224970-5333-4b28-86af-a825fbbe59df",  // ประเภทของเรื่อง
        "65fb5c10-96ac-41a6-9dce-9e74a0a956cc",  // ประเภทย่อยของเรื่อง
        "a7755504-3a66-4477-a6bc-6c5bead635cb",  // ประเทศ
        "4ed057ad-b894-46fe-bc44-14fd7a3b1bb8"   // ประเภทของเอกสาร
    ];

    //-----การขออนุญาตผ่านน่านน้ำ
    static topicList_card_s_entryWater = [
        "bffefdef-145a-4dd7-ab8a-2bf1a5f168ba",  // ธงชาติเจ้าของเรือ
        "f786434f-32f3-4a64-9ec4-775ed48ac13e",  // ประเทศเจ้าของเรือ
        //"bffefdef-145a-4dd7-ab8a-2bf1a5f168ba",  // จำนวนเรือที่มี   *******
        //"bffefdef-145a-4dd7-ab8a-2bf1a5f168ba"   // จำนวนครั้งที่ขอ  *******      
    ];

    //----- การรับรองการเยือน
    static topicList_card_s_support = [
        "367e9cb7-eb8b-4897-b0dd-3a9f8aa404f5",  // ธงชาติ
        "d7e9f7a8-d200-4176-a3af-dc94ec469b1c",  // ชื่อบุคคลสำคัญ (อังกฤษ) 
        "590e5827-3756-4f5b-8ef0-913f3448e551",  // วันที่เดินทางเข้า        
        "e8c46ba5-a89f-440e-8b3b-936833c5d1b8"  // ประเภทแขกเยือน        
    ];

    //----- อำนวนความสะดวก ต่างประเทศ
    static topicList_card_s_supportInter = [
        "f08e085d-5cbc-40c5-881f-119c66143dd5",  // ประเทศ
        "0fa169ac-0316-4abf-a598-f673b80d587d",  // ชื่อบุคคลสำคัญ (อังกฤษ)
        "4cc5971e-8443-4cf1-8cf6-6f0eac413d4f"  // ประเภทการเยือน
    ];

    //----- อำนวยความสะดวก ไทย
    static topicList_card_s_supportThai = [
        "05d1f843-9699-4892-8144-2e4ae23477a9",  // ประเทศ
        "c4881fc8-9784-41c4-8b6a-245d8e4f9ba0",  // ชื่อบุคคลสำคัญ (ภาษาไทย) 
        "f0ba736b-6701-47b0-b693-e461b838ddab",  // ตำแหน่ง (ภาษาไทย)
        "c6ef6fc4-48d6-48ea-9a55-f204f35d142a"  // วันที่เดินทางออก      
    ];

    //----- การบรรยาย
    static topicList_card_s_present = [
        "cfe08935-62c6-4f51-a146-152e2aec80af",  // เรื่อง
        "df84ab65-949f-4830-97c4-1e10185e056c",  // วันที่จัด
        "b6d2f5d6-c446-4148-9149-8751611cd816",  // ประเภทการบรรยาย
        "31c32665-bed2-4f04-9bf0-cbdb2058bb87"  // หน่วยงานที่ขอ    
    ];

    //----- meeting
    static topicList_card_s_meeting = [
        "cba21d97-dea7-4920-9792-00753d6d3fc4",  // เรื่อง            *******
        "7c6c7328-aff6-4ecc-a2fb-140c1de3ec51",  // วันที่จัด          *******
        "570ce313-4492-4c2f-97b1-837f89baca9c"  // หน่วยงานเจ้าภาพ     *******      
    ];

    //----- การจัดเลี้ยงรับรอง
    static topicList_card_s_party = [
        "d992a419-d21c-4b7b-9ed4-73d081fab456",  // ประเทศ
        "9cbe09b8-6ccb-486f-9d7b-907cdce0a187",  // วันที่จัด  *******
        "4de6603f-8fdd-4324-a3e5-e6d8557abd89",  // ประเภทงานเลี้ยง
        "5a8add3e-2caa-442b-86f2-398deb119510"  // ระดับบุคคล        
    ];

    //----- กิจกรรมพิเศษ
    static topicList_card_eventSp = [
        "c7f2ffe6-166c-424b-a84e-e96a66c8365a",  // ชื่อกิจกรรม
        "c01aa010-b67a-4e34-90f3-f3f8bb8eb48b",  // วันเริ่มกิจกรรม
        "a11b0625-1c88-45d4-a690-bf0176b19778",  // ประเภทกิจกรรม
        "85e05b1d-0178-4d85-bf12-a0c49eef0be0"  // หน่วยงานเจ้าภาพ      
    ];


    //----- การออกหนังสือประสานงาน
    static topicList_card_reportJoin = [
        "338eda5b-3d0c-49a2-906b-ec9309887a32",  // ประเทศ
        "f1cbdfbe-209d-47d1-98f4-5d60150f9060",  // ชื่อบุคคลสำคัญ (ภาษาอังกฤษ)
        "a4f911d9-7397-4fcb-93a6-4948970ba1b6",  // วันที่เดินทางเข้า
        "fe4306a7-612d-4829-94cb-f1282e8307ea",  // วันที่เดินทางออก    
        "4eda867b-fe38-4690-8e4e-3eb2987dc84b"  //  ตำแหน่ง
    ];



    //----- ข้อมูลทะเบียนอสังหาริมทรัพย์
    static topicList_card_realty = [
        "01a24fae-6d52-4c4c-819a-086143e31bc5",  // แนบรูป
        "1ccd4c6b-90b6-4746-a179-1a6b2dbfe5b7",  // ชื่ออสังหาริมทรัพย์
        "9e0ce172-281e-48af-ace6-3c3ac518f1ce",  // วันที่ได้มา
        "0db9e3de-4a6a-4892-a136-025f7a696cda",  // มูลค่า    
        "2e4d809c-33b8-4dcb-a838-380cbf929214",  // กลุ่มอสังหาริมทรัพย์
        "7d74159f-4d90-49d4-b3e9-6a490c4b7092"   //  สถานะกรรมสิทธิ์
    ];

    //----- บันทึกการตั้งคำขอ
    static topicList_card_saveRequest = [
        "3e74d250-bb2c-4a94-8034-a78ced3e09b4",  // กอง / สนง.นสดท./ สอท. / สกญ. / คผถ.
        "260158ce-7dd5-468b-ac34-6efa5b0003c0",  // ชื่ออสังหาริมทรัพย์
        "1ba9bed9-afdc-4a78-b4cb-92c9553fec26",  // ปีงบประมาณ
        "58611462-791b-409c-963e-c58df927ba57",  // วงเงิน    
        //"3e74d250-bb2c-4a94-8034-a78ced3e09b4"  // กอง / สนง.นสดท./สอท. / สกญ. / คผถ.     
    ];

    //----- บันทึกการออกแบบ
    static topicList_card_saveDesign = [
        "005b7d76-99ac-4ed8-8c3b-b5c5bd6d5ba8",  // กอง / สนง.นสดท./สอท. / สกญ. / คผถ.
        "a0eb3f00-0f6b-4260-b945-8d85c540b346",  // ชื่ออสังหาริมทรัพย์
        //"005b7d76-99ac-4ed8-8c3b-b5c5bd6d5ba8",  // กอง / สนง.นสดท./สอท. / สกญ. / คผถ.
        "fd76a933-239a-414c-9506-10d28a2b8dcf",  // มูลค่า (บาท)  
        "a1fa958d-c0d5-43d6-82da-31424db22166"  // ประเภทอสังหาริมทรัพย์    
    ];

    //----- บันทึกการก่อสร้าง
    static topicList_card_saveBulider = [
        "bca02593-77ef-44c5-958d-612711d6176f",  // อ้างอิงอสังหาริมทรัพย์
        "e107acb3-5cc4-4297-9083-51d6d1689b61",  // กอง / สนง.นสดท./สอท. / สกญ. / คผถ.
        "a03582e8-be67-4d97-92fd-07531a25ed0e",  // ปีงบประมาณ
        "fa95612a-01a0-4d72-b075-8656be5fbfc9"  // วงเงิน             
    ];


    //-----  บันทึกการซ่อมแซม
    static topicList_card_saveRepair = [
        "708741cb-6547-425e-8cd0-983427630772",  // ค้นหาอสังหาริมทรัพย์
        //"708741cb-6547-425e-8cd0-983427630772",  // ค้นหาอสังหาริมทรัพย์
        "78f30b9b-3147-4242-b581-1508568e2202",  // ปีงบประมาณ
        "d23a444a-d104-47eb-96b5-1f299241c5ea",  // กอง / สนง.นสดท./สอท. / สกญ. / คผถ.
        // "d23a444a-d104-47eb-96b5-1f299241c5ea"    // กอง / สนง.นสดท./สอท. / สกญ. / คผถ.
    ];

    //-----  ลูกจ้างเหมาบริการ
    static topicList_card_tempService = [
        "88ebb20b-a341-443f-abe6-75958c166f4d",  // คำนำหน้า (ภาษาไทย)
        "38b5f82f-16a9-46bf-b1db-e75c06d0ab9e",  // ชื่อ (ภาษาไทย)
        "ffdf3d92-5177-4203-9977-2c1d046a3f8f",  // ชื่อกลาง (ภาษาไทย)
        "9e3d9a66-1c74-4120-8cd8-55c4a33a4b57",  // นามสกุล (ภาษาไทย)
        "a5b37921-b713-4001-951a-0b085cbd3871",  // ชื่อ (ภาษาอังกฤษ)
        "b66f3362-568f-4c15-81d8-f51353a6b627",  // ชื่อกลาง (ภาษาอังกฤษ)
        "bfd55597-1b49-443e-a8d2-e2df0fd72e66",  // นามสกุล (ภาษาอังกฤษ)

        "e01717c2-9ea3-4d95-92d3-532acf874e38",  // ตำแหน่ง
        "069beea9-6de0-47a5-92fb-97d00db11efc",  // กรม
        "3cf3e08e-ae97-46a7-a311-3f92428e8819",  // กอง
        "153e9378-c183-4023-8be6-4161d2c7d41e",  // สังกัด

        "7e31cefe-0a8d-46cb-8938-481f53769369",  // อีเมล์
        "122bcb19-f49d-457c-8970-d0ff9d29c3bd",  // โทรศัพท์มือถือ
    ];
    static sort_tempService = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13'];


    //-----  จำนวนคนไทยในต่างประเทศ
    static topicList_card_amountThaiInter = [
        "6f98421b-72c7-4a04-a33d-d35862f7d0b0",  // ประเทศ
        "45807de3-43fe-49bc-bc1d-e067bb03f3a5",  // ปี
        "19e5f2f0-d3db-4711-85d0-fe59f437165a",  // จำนวนชาวไทยเพศหญิง
        "a2abac7a-84b1-4be1-adea-9320a80bce01",  // จำนวนชาวไทยเพศชาย
        "afca69fd-1fe6-4023-a254-fd9d51e0a9ad",    // จำนวนรวมทั้งหมด
        "ab644a24-0c8b-4ed2-a88e-f15b6aa21c8a"  // วันที่อัพเดทข้อมูล
    ];

    //-----  สถิติการให้ความช่วยเหลือ คนไทยในต่างประเทศ
    static topicList_card_helpThaiInter = [
        "c920ecee-04fa-46b1-93d4-2237a5b1741c",  // ประเทศ
        "543afe02-5704-437b-b793-c9dcf44534ff",  // จำนวนรวมทั้งหมด
        "cc7faa6e-cc3d-42cb-8605-c26936b81a7b",  // จำนวนการให้ความช่วยเหลือเพศหญิง
        "65ae8396-ece4-4a94-b88e-00d731ec329c",  // จำนวนการให้ความช่วยเหลือเพศชาย
        "a70987d4-1353-432e-b827-84fa787bce08"    // วันที่อัพเดทข้อมูล    
    ];

    //-----  การยกเว้นการตรวจลงตราและการขอ Visa on Arrival (VoA)
    static topicList_card_exceptVisa = [
        "77dd51d7-2732-45d3-b581-179258dfbeac",  // ประเทศ
        "87fec96b-d01f-457e-82cd-3c60b72a3015",  // จำนวนรวมทั้งหมด
        "944a941e-c41a-4dd3-9a61-03a8acdd1c3f"  // จำนวนการให้ความช่วยเหลือเพศหญิง        
    ];

    //-----  สถิติการตรวจลงตรา
    static topicList_card_approveStatic = [
        "370d8288-e1cb-4ff5-8fd3-3132efe3e935",  // สำนักงาน
        "ffb0d6ef-dad1-404d-8a2b-9c9028678bd9",  // ปี
        "0260eeba-a7d3-4a53-9f5a-217746e12519"  // ผลรวมการตรวจลงตรา (รายปี)       
    ];

    //-----  สถิติการรับรองเอกสารและนิติกรณ์
    static topicList_card_approveSta = [
        "238d6018-0fc5-4427-993e-9eb8c9dabc46",  // สำนักงาน
        "621a2381-5db7-4bbc-8cb1-f0d82021ea53",  // เดือน/ปี
        "d1a2d0f0-8117-432d-a882-e707564313ab",  // จำนวนคำร้องที่ได้รับ (เรื่อง)   
        "bbe92cdb-6483-436f-ac34-8aea2f5564ad",  // จำนวนนิติกรณ์เอกสารปกติที่ผ่านการรับรอง(ฉบับ) 
        "d2cf8e1c-f9da-451b-a879-96a88920678a"  // จำนวนนิติกรณ์เอกสารด่วนที่ผ่านการรับรอง (ฉบับ)  
    ];

    //-----  แนวทางการรับรองเอกสารและนิติกรณ์ 
    static topicList_card_approveRep = [
        "77d07956-ae1c-4465-bc2b-32913894f27b"  // การรับรอง             
    ];

    //-----  สถิติการออกหนังสือเดินทาง
    static topicList_card_visaSta = [
        "d78c4a49-d88b-45fd-a3c9-87fdd3c86307",  // สำนักงาน
        "a44f5674-2f6e-4ef1-88f2-89b975a357a4",  // ที่ตั้งสำนักงาน
        "c32b310f-045d-4070-a554-8867c7dbc7dd"  // จำนวนผู้ขอหนังสือเดินทาง (ราย) 
    ];

    //-----  วิสัยทัศน์ / ยุทธศาสตร์
    static topicList_vision = [
        "e18f0e59-e110-40d1-ad94-783a2e6f42f3",  // ชื่อ
         "4277de6a-af45-4c7b-8453-ac877ccd4c28",  // ปี
        "065334e6-9f03-403f-bae3-eb5b55b02fcf"  // เอกสารแนบ
    ];


    //----- โครงการศึกษาดูงานด้านพิธีการทูตในต่างประเทศ
    static topicList_ambassador_visit = [
        "69ac8858-53a2-4bfa-866a-cf6e647904f9", // ชื่อโครงการ
        "58bcc443-b6a6-4015-92d8-35877e27ebad",  // ครั้งที่
        "05247bfb-c095-4a18-a6b0-3772e1f95767",  // ปีงบประมาณ
        "71ed7b16-5216-43b9-8772-c718b0f4034b",  // วันเริ่มดำเนินโครงการ
        "b79e550c-7598-4942-9db8-ddefd951f4f1", // วันสิ้นสุดดำเนินโครงการ
        "2BCEC4C8-546F-407F-88DE-47A64D4AE435",   // ประเทศ
        "47C554C4-10C2-403B-8D71-E8870F215D60"  // จังหวัด
    ];

    //----- โครงการนำคณะทูตและคู่สมรสไปทัศนศึกษา
    static topicList_ambassadorAndspouse_visit = [
        "1cabd481-a460-4063-8903-e30191754318", // ชื่อโครงการ
        "b5343530-bcca-4336-85eb-50e3d55a5811",  // ครั้งที่
        "69493d05-d210-4fa2-bf12-53c6a41f487d",  // ปีงบประมาณ
        "23b478a0-2ca3-4a84-8e8f-48892dfa60c3",  // วันเริ่มดำเนินโครงการ
        "b62a9767-e583-4dcd-908d-e6cf06802342", // วันสิ้นสุดดำเนินโครงการ
        "4FC72175-8E3F-4502-A699-6BA473E340B0",   // ประเทศ
        "13232A1B-8A8C-4D5D-A158-B8864BD741F4"  // จังหวัด
    ];

    //-----คู่มือปฏิบัติงานด้านพิธีการทูต
    static topicList_manual_ambassador = [
        "1bd972e0-edef-4a86-b377-172ffaeb33e3", // ชื่อคู่มือ
        "4df37ae8-b370-4e52-87ff-b35a6b255018",  // ปี
        "d4ce6831-9b4f-4f5f-9423-362a59aab8cc",  // ชื่อประเภท
        "e25059f2-ebf6-4e0d-9e47-cfb4f139fbcf"  // กองเจ้าของเอกสาร
       
    ];

    //-----การขออนุญาติผ่านน่านน้ำ
    static topicList_entryWater = [
        "f786434f-32f3-4a64-9ec4-775ed48ac13e", // ประเทศเจ้าของเรือ
        //"4df37ae8-b370-4e52-87ff-b35a6b255018",  // ปี
        //"d4ce6831-9b4f-4f5f-9423-362a59aab8cc",  // ชื่อประเภท
        //"e25059f2-ebf6-4e0d-9e47-cfb4f139fbcf"  // กองเจ้าของเอกสาร

    ];

    //-----งบประมาณโครงการ
    static topicList_budget = [
        "879cc381-4430-493f-b69f-1ed08240ae4f", // ชื่อโครงก่าร
        "9403d318-cfa5-4289-9fd0-f79a64827e04",  //หน่วยงานผู้ของบประมาณ
        "a850ce83-1c3d-4829-9a26-ec4b6648abdc",  // ปีงบประมาณ
        "9795980b-bce1-40f9-9ed2-747b4789e3d0",  // หมวดค่าใช้จ่าย
        "75fa54ee-1a3e-43e4-81a7-ccbf1f3905a5",  // สถานะโครงการ
        //"fff4a0e7-33f2-4f33-98e1-b5a4a43e1cd6"   // ประเทศของหน่วยงานผู้ขอ

    ];

    static topiclist_cardL_test = [

        "05247bfb-c095-4a18-a6b0-3772e1f95767", // ปีงบประมาณ  
        "58bcc443-b6a6-4015-92d8-35877e27ebad", // ครั้งที่
        "69ac8858-53a2-4bfa-866a-cf6e647904f9"  // ชื่อโครงการ
    ]


}

interface DatabaseBound {
    Country: string;
    Province: string;
    name: string;
}

interface DatabaesThrougWaterCriterial {
    CountryShip: string;
    CountryEntry: string;
    objective: string;
    start_date: string;
    end_date: string;
    ship_name: string;
}

interface DatabaseSupport {
    start_date: string;
    end_date: string;
    Country: string;
    personLevel: string;
    guestType: string;
    easyAmbas: string;
    name: string;
}

interface DatabaseSupportInter {
    start_date: string;
    end_date: string;
    Country: string;
    personLevel: string;
    visitType: string;
    name: string;
}

interface DatabaseSupportThai {
    start_date: string;
    end_date: string;
    Country: string;
    personLevel: string;
    position: string;
    name: string;
}

interface DatabaseMeeting {
    start_date: string;
    end_date: string;
    subject: string;
}

interface DatabaseParty {
    start_date: string;
    end_date: string;
    Country: string;
    personLevel: string;
    position: string;
    partyType: string;
}
