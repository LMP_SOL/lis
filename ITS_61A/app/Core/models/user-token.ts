﻿interface IUserToken {
    access_token: string;
    expires_in: string;
    firstNameEn: string;
    firstNameTh: string;
    lastNameEn: string;
    lastNameTh: string;
    userId: string;
    userName: string;
}


