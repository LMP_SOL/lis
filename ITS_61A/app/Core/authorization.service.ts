﻿class AuthorizationService {
    constructor(private $http: ng.IHttpService,
        private $location: ng.ILocationService,
        private $q: ng.IQService) {
    }

    public getAuthorizationData(): IUserToken {
        var json = localStorage.getItem(Constants.localStorageKeys.AuthorizationData);
        if (!json) {
            return null;
        }
        return angular.fromJson(json);
    }

    public getLoginCaptcha(): ng.IPromise<ILoginCaptchaData> {
        return this.$http.get("api/user/GetLoginCaptcha")
            .then((resp: any) => {
                if (resp && resp.data) {
                    return angular.fromJson(resp.data);
                }
                return this.$q.reject();
            });
    }

    public logout() {

        this.$http.get("api/User/SignOut").then((resp: any) => {

            console.log(resp);
            localStorage.removeItem(Constants.localStorageKeys.AuthorizationData);
            this.$location.path("/login");
        });

    }

    public getInitDataForUserRegister(): ng.IPromise<any> {
        return this.$http.get("api/User/GetInitDataForUserRegister")
            .then((resp: any) => {
                if (resp && resp.data) {
                    var jsObjs = angular.fromJson(resp.data);
                    return this.$q.resolve(jsObjs);
                }
                return this.$q.reject();
            });
    }



    public login(username: string, password: string, userCaptcha: string, captchaEnc: string): ng.IPromise<any> {
        var data = "grant_type=password&username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password)
            + "&encodeCaptcha=" + encodeURIComponent(captchaEnc) + "&userCaptcha=" + encodeURIComponent(userCaptcha);        
        return this.$http.post("/Token", data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
            .then((resp: any) => {
                localStorage.removeItem(Constants.localStorageKeys.AuthorizationData);
                if (resp && resp.data) {
                    var json = angular.toJson(resp.data);
                    localStorage.setItem(Constants.localStorageKeys.AuthorizationData, json);
                    //workerCallSocket.send();
                }
            });
    }
}
class ModelAuthorizationData {
    public access_token: string = "";
    public token_type: string = "";
    public userId: Guid;
    public userName: string = "";
}
