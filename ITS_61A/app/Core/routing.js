app.config(['$locationProvider', '$routeProvider',
    function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider
            .when('/login', {
            template: "<h1>Login</h1>"
        })
            .when('/setting-password', {
            template: '<setting-password> </setting-password>'
        })
            .when('/insert-worker', {
            template: '<insert-worker> </insert-worker>'
        })
            .when('/insert-employee', {
            template: '<insert-employee> </insert-employee>'
        })
            .when('/insert-employee-popup', {
            template: '<insert-employee-popup> </insert-employee-popup>'
        })
            .when('/setting-connectdatabase', {
            template: '<setting-connectdatabase> </setting-connectdatabase>'
        })
            .when('/setting-email', {
            template: '<setting-email> </setting-email>'
        })
            .when('/users-management', {
            template: '<search-user> </search-user>'
        })
            .when('/master-data/:id', {
            template: '<master-data-management></master-data-management>'
        })
            .when('/setting-alert', {
            template: '<setting-alert> </setting-alert>'
        })
            .when('/insert-user', {
            template: '<insert-user> </insert-user>'
        })
            .when('/setting-notification', {
            template: '<setting-notification> </setting-notification>'
        })
            .when('/privilege-management', {
            template: '<privilege-management> </privilege-management>'
        })
            .when('/sort-data', {
            template: '<sort-data> </sort-data>'
        })
            .when('/add-group-privilege-management', {
            template: '<add-group-privilege-management> </add-group-privilege-management>'
        })
            .when('/history-person', {
            template: '<history-person> </history-person>'
        })
            .when('/daily-history', {
            template: '<daily-history> </daily-history>'
        })
            .when('/view-history-person', {
            template: '<view-history-person> </view-history-person>'
        })
            .when('/purchasing-for-project', {
            template: '<purchasing-for-project> </purchasing-for-project>'
        })
            .when('/desc-purchasing-for-project', {
            template: '<desc-purchasing-for-project> </desc-purchasing-for-project>'
        })
            .when('/announcement-management', {
            template: '<announcement-management> </announcement-management>'
        })
            .when('/pay-tax-land', {
            template: '<pay-tax-land> </pay-tax-land>'
        })
            .when('/land-registration', {
            template: '<land-registration> </land-registration>'
        })
            .when('/insert-data-Pay-tax-land', {
            template: '<insert-data-Pay-tax-land> </insert-data-Pay-tax-land>'
        })
            .otherwise('/');
    }
]);
//# sourceMappingURL=routing.js.map