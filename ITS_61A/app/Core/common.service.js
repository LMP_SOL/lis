var CommonService = (function () {
    function CommonService() {
        this.arrMonthTwoDigit = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        this.arrMonthThaiFull = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
        this.arrMonthThaiShort = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];
        this.mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.mS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    }
    CommonService.prototype.getYearInter = function (year, isShortYear) {
        if (isShortYear === void 0) { isShortYear = false; }
        var numYear = angular.isString(year) ? Number(year) : year;
        numYear -= 543;
        var strYear = "";
        if (isShortYear)
            strYear = numYear.toString().substring(2);
        else
            strYear = numYear.toString();
        return strYear;
    };
    CommonService.prototype.getYearThai = function (year, isShortYear) {
        if (isShortYear === void 0) { isShortYear = false; }
        var numYear = angular.isString(year) ? Number(year) : year;
        numYear += 543;
        var strYear = "";
        if (isShortYear)
            strYear = numYear.toString().substring(2);
        else
            strYear = numYear.toString();
        return strYear;
    };
    CommonService.prototype.getMonthThai = function (month, isShortName) {
        if (isShortName === void 0) { isShortName = false; }
        var numMonth = angular.isString(month) ? Number(month) : month;
        var indexMonth = numMonth - 1;
        var monthName;
        if (isShortName !== undefined && isShortName == true)
            monthName = this.arrMonthThaiShort[indexMonth];
        else
            monthName = this.arrMonthThaiFull[indexMonth];
        return monthName;
    };
    CommonService.prototype.formatDateThaiStyle1 = function (strYear, strMonth, strDay) {
        if (!strYear || !strMonth || !strDay)
            return "";
        var year = this.getYearThai(strYear, false);
        var month = this.getMonthThai(strMonth, true);
        var day = strDay;
        if (day.length < 2)
            day = '0' + day;
        return day + " " + month + " " + year;
    };
    CommonService.prototype.formatDateThaiStyle2 = function (strYear, strMonth, strDay) {
        if (!strYear || !strMonth || !strDay)
            return "";
        var year = this.getYearThai(strYear, false);
        var month = this.getMonthThai(strMonth, false);
        var day = strDay;
        if (day.length < 2)
            day = '0' + day;
        return day + " " + month + " " + year;
    };
    //---------------------------------------------------------//
    CommonService.prototype.formatDateThai = function (strDate, format) {
        var result = "";
        if (!strDate || strDate == null)
            return result;
        var arrDate = strDate.trim().split('-');
        var strYear = arrDate[0];
        var strMonth = arrDate[1];
        var strDay = arrDate[2];
        switch (format) {
            case "dd MMM yyyy":
                result = this.formatDateThaiStyle1(strYear, strMonth, strDay);
                break;
            case "dd MMMM yyyy":
                result = this.formatDateThaiStyle2(strYear, strMonth, strDay);
                break;
            case "yyyy":
                result = this.getYearThai(strDate, false);
                break;
            case "MMMM":
                result = this.getMonthThai(strDate);
                break;
            case "MMM":
                result = this.getMonthThai(strDate, true);
                break;
            default:
                result = this.formatDateThaiStyle1(strYear, strMonth, strDay);
        }
        return result;
    };
    ///////////////////////////////////////////////////////////////////////////////////
    CommonService.prototype.getYearEn = function (year, isShortYear) {
        if (isShortYear === void 0) { isShortYear = false; }
        var numYear = angular.isString(year) ? Number(year) : year;
        var strYear = "";
        if (isShortYear)
            strYear = numYear.toString().substring(2);
        else
            strYear = numYear.toString();
        return strYear;
    };
    CommonService.prototype.getMonthEn = function (month, isShortName) {
        if (isShortName === void 0) { isShortName = false; }
        var numMonth = angular.isString(month) ? Number(month) : month;
        var indexMonth = numMonth - 1;
        var monthName;
        if (isShortName !== undefined && isShortName == true)
            monthName = this.mS[indexMonth];
        else
            monthName = this.mL[indexMonth];
        return monthName;
    };
    CommonService.prototype.formatDateEnShort = function (strYear, strMonth, strDay) {
        if (!strYear || !strMonth || !strDay)
            return "";
        var year = this.getYearEn(strYear, true);
        var month = this.getMonthEn(strMonth, true);
        var day = strDay;
        if (day.length < 2)
            day = '0' + day;
        return day + "-" + month + "-" + year;
    };
    CommonService.prototype.formatDateEnLong = function (strYear, strMonth, strDay) {
        if (!strYear || !strMonth || !strDay)
            return "";
        var year = this.getYearEn(strYear, false);
        var month = this.getMonthEn(strMonth, false);
        var day = strDay;
        if (day.length < 2)
            day = '0' + day;
        return day + " " + month + " " + year;
    };
    CommonService.prototype.formatDateEn = function (strDate, format) {
        var result = "";
        if (!strDate || strDate == null)
            return result;
        var arrDate = strDate.trim().split('-');
        var strYear = arrDate[0];
        var strMonth = arrDate[1];
        var strDay = arrDate[2];
        switch (format) {
            case "dd MMM yyyy":
                result = this.formatDateEnShort(strYear, strMonth, strDay);
                break;
            case "dd MMMM yyyy":
                result = this.formatDateEnLong(strYear, strMonth, strDay);
                break;
            case "yyyy":
                result = this.getYearEn(strDate, false);
                break;
            case "MMMM":
                result = this.getMonthEn(strDate);
                break;
            case "MMM":
                result = this.getMonthEn(strDate, true);
                break;
            default:
                result = this.formatDateEnShort(strYear, strMonth, strDay);
        }
        return result;
    };
    ///////////////////////////////////////////////////////////////////////////////////////
    CommonService.prototype.formatDate = function (date, format) {
        return moment(date).format('YYYY-MM-DD');
    };
    CommonService.prototype.stringToDate = function (date, format) {
        if (!date || date == null) {
            return null;
        }
        else {
            if (format) {
                return moment(date, format).toDate();
            }
            else {
                return moment(date).toDate();
            }
        }
    };
    CommonService.prototype.ConvertIntToNumberWithCommas = function (n) {
        return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); //int 2000->string 2,000 
    };
    CommonService.prototype.ConvertNumberWithCommasToInt = function (n) {
        return Number(n.replace(/[^0-9\.]+/g, "")); //string 2,000->int 2000
    };
    CommonService.prototype.getMonthListThai = function (format) {
        if (format === void 0) { format = "MM"; }
        var monthSource = [];
        var monthList = [];
        if (format == "MM") {
            monthSource = this.arrMonthTwoDigit;
        }
        else if (format == "MMM") {
            monthSource = this.arrMonthThaiShort;
        }
        else if (format == "MMMM") {
            monthSource = this.arrMonthThaiFull;
        }
        for (var i = 0; i < monthSource.length; i++) {
            var item = { ID: ("0" + (i + 1)).slice(-2), Name: monthSource[i] };
            monthList.push(item);
        }
        return monthList;
    };
    CommonService.prototype.convertYear = function (year, culture) {
        if (culture === void 0) { culture = "en-US"; }
        if (!year)
            return "";
        if (culture.toLowerCase() == "en-us")
            return this.getYearInter(year);
        if (culture.toLowerCase() == "th-th")
            return this.getYearThai(year);
    };
    return CommonService;
}());
//# sourceMappingURL=common.service.js.map