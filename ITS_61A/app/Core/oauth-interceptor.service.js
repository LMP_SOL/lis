var OAuthInterceptorService = (function () {
    function OAuthInterceptorService($q, $location) {
        var _this = this;
        this.$q = $q;
        this.$location = $location;
        this.responseError = function (rejection) {
            if (rejection.status === 401) {
                _this.$location.path("/login");
            }
            return _this.$q.reject(rejection);
        };
    }
    OAuthInterceptorService.prototype.request = function (config) {
        if (!config.headers) {
            config.headers = {};
        }
        var authDataJson = localStorage.getItem(Constants.localStorageKeys.AuthorizationData);
        if (authDataJson) {
            var authData = angular.fromJson(authDataJson);
            config.headers["Authorization"] = 'Bearer ' + authData.access_token;
        }
        return config;
    };
    return OAuthInterceptorService;
}());
//# sourceMappingURL=oauth-interceptor.service.js.map