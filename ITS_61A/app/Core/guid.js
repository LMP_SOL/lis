var Guid = (function () {
    function Guid(id) {
        if (Guid.isGuid(id)) {
            this.id = id.toLowerCase();
        }
        else {
            this.id = Guid.emptyGuid.valueOf();
        }
    }
    Guid.compare = function (guid1, guid2) {
        if (!guid1 || !guid2) {
            return !guid1 === !guid2;
        }
        return guid1.toUpperCase() === guid2.toUpperCase();
    };
    Object.defineProperty(Guid, "empty", {
        get: function () {
            return Guid.emptyGuid;
        },
        enumerable: true,
        configurable: true
    });
    Guid.isNullOrEmpty = function (id) {
        return !id || Guid.compare(id, Guid.emptyGuid.toString());
    };
    Guid.isGuid = function (guid) {
        return (/^(\{){0,1}[0-9a-fA-F]{8}(\-){0,1}[0-9a-fA-F]{4}(\-){0,1}[0-9a-fA-F]{4}(\-){0,1}[0-9a-fA-F]{4}(\-){0,1}[0-9a-fA-F]{12}(\}){0,1}$/gi).test(guid);
    };
    Guid.newGuid = function (isSequence) {
        if (isSequence === void 0) { isSequence = true; }
        var newGuid = new Guid(Guid.s4() +
            Guid.s4() +
            '-' +
            Guid.s4() +
            '-' +
            Guid.s4() +
            '-' +
            Guid.s4() +
            '-' +
            Guid.s4() +
            Guid.s4() +
            Guid.s4());
        if (isSequence) {
            var timePart = (moment)
                ? moment().utc().toDate().getTime().toString(16)
                : new Date().getTime().toString(16);
            while (timePart.length < 12) {
                timePart = "0" + timePart;
            }
            return new Guid(newGuid.toString().substr(0, 24) + timePart);
        }
        else {
            return newGuid;
        }
    };
    Guid.s4 = function () {
        return Math
            .floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };
    Guid.prototype.isEmpty = function () {
        return this.id === Guid.emptyGuid.valueOf();
    };
    Guid.prototype.isEquals = function (guid) {
        return this.valueOf() === guid.valueOf();
    };
    Guid.prototype.toString = function (format) {
        if (format) {
            format = format.toUpperCase();
        }
        switch (format) {
            case "B":
                return "{" + this.id + "}";
            case "N":
                return this.id.replace(/-/g, "");
            case "P":
                return "(" + this.id + ")";
            default:
                return this.id;
        }
    };
    Guid.prototype.valueOf = function () {
        return this.id;
    };
    return Guid;
}());
Guid.emptyGuid = new Guid("00000000-0000-0000-0000-000000000000");
//# sourceMappingURL=guid.js.map