﻿class DialogService {

    constructor(private $mdBottomSheet: ng.material.IBottomSheetService,
        private $mdDialog: ng.material.IDialogService) {

    }

    public showBusyBottomSheet(parentElement?: any) {
        this.$mdBottomSheet.show({
            template: `<md-bottom-sheet><md-progress-linear md-mode="indeterminate"></md-progress-linear></md-bottom-sheet>`,
            parent: angular.isDefined(parentElement) ? parentElement : null,
            escapeToClose: false,
            clickOutsideToClose: false
        });
    }

    public hideBusyBottomSheet() {
        this.$mdBottomSheet.hide();
    }

    public showMessage(title: string, message: string): ng.IPromise<any> {
        var alert = this.$mdDialog.alert()
            .title(title)
            .textContent(message)
            .ok('Close');

        return this.$mdDialog
            .show(alert)
            .finally(() => {
                alert = undefined;
            });
    }

    public showConfirm(title: string, message: string): ng.IPromise<any> {
        var confirm = this.$mdDialog.confirm()
            .title(title)
            .textContent(message)
            .ok('Yes')
            .cancel('No');
        return this.$mdDialog.show(confirm);

    }

    public showPrompt(title: string, message: string): ng.IPromise<any> {
        var confirm = this.$mdDialog.prompt()
            .title(title)
            .textContent(message)
            .placeholder('Test')
            .ariaLabel('Name')
            .initialValue('Buddy')
            .ok('Okay!')
            .cancel('I\'m a name person');
        return this.$mdDialog.show(confirm);
    }

    public dialogMessage(title: string, message: string): ng.IPromise<any> {
        var alert = this.$mdDialog.alert()
            .title(title)
            .textContent(message)
            .ok('Close');

        return this.$mdDialog
            .show(alert)
            .finally(() => {
                alert = undefined;
            });
    }

    public dialogConfirm(title: string, message: string, okCallback?, noCallback?): ng.IPromise<any> {
        var confirm = this.$mdDialog.confirm()
            .title(title)
            .textContent(message)
            .ok('Yes')
            .cancel('No');
        return this.$mdDialog.show(confirm).then(() => {
            console.log("Confirm");
            if (okCallback) {
                okCallback();
            }
        }, function () {
            console.log("Cancel");
            if (noCallback) {
                noCallback();
            }
        });

    }

    public dialogPrompt(title: string, message: string, okCallback?, noCallback?): ng.IPromise<any> {
        var confirm = this.$mdDialog.prompt()
            .title(title)
            .textContent(message)
            .placeholder('Test')
            .ariaLabel('Name')
            .initialValue('Buddy')
            .ok('Okay!')
            .cancel('I\'m a name person');
        return this.$mdDialog.show(confirm).then(function (result) {
            console.log(result);
            if (okCallback) {
                okCallback();
            }
        }, function () {
            console.log("function ()");
            if (noCallback) {
                noCallback();
            }
        });
    }


    public showCustomDialog(title ,data, mode, okCallback, cancelCallback): ng.IPromise<any> {

        //let title: string = 'เพิ่มข้อมูล';
        let template: string = '<div class="render-topic"><render-topic topics="dlgCtrl.data.CHILD" form="dlgCtrl.theForm" mode="dlgCtrl.mode"></render-topic></div>';

        // DATA MANAGEMENT

        let content: string = '<md-dialog class="md-complex-dialog event-detail-dialog">' +
            '<md-header>' + title + '<div class="close-btn-md-dialog" ng-click="dlgCtrl.onClickCancel($event)"></div></md-header>' + '<form name="dlgCtrl.theForm" novalidate>' +
            '<md-content>' + template +
            '</md-content>' +
            '<md-footer>' +
            '<md-input-container class="footer-dialog" md-no-float layout="row" layout-align="center center">' +
            '<md-button type="submit" layout="row" layout-align="center center" class="md-raised md-primary" ng-click="dlgCtrl.onClickOK($event)"> ' + 'บันทึก' + ' </md-button>' +
            '<md-button layout="row" layout-align="center center" class="md-raised md-warn" ng-click="dlgCtrl.onClickCancel($event)"> ' + 'ยกเลิก' + ' </md-button>' +
            '</md-input-container>' +
            '</md-footer>' +
            '</form>' +
            '</md-dialog>'


        //controller == null ? CustomDialogController : controller;
        let controller = CustomDialogController;

        return this.$mdDialog.show({
            controller: controller,
            controllerAs: "dlgCtrl",
            template: content,
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            autoWrap: false,
            escapeToClose: false,
            locals: {
                data: data,
                mode: mode,
                okCallback: okCallback,
                cancelCallback: cancelCallback,
                dialogService: this
            }
            //,bindToController: true
        });
    }


    public showCustomAlert(title, btnAgree, Message, okCallback, cancelCallback): ng.IPromise<any> {

        let content: string = '<md-dialog class="md-complex-dialog">' +
            '<md-header>' + title + '<div class="close-btn-md-dialog" ng-click="dlgCtrl.onClickCancel()"></div></md-header>' +
            '<md-content> <div class="text-center -dialog" layout="row" layout-align="center center">' + Message + ' </div> </md-content>' +
            '<div> ' +
            '<md-input-container class="footer-dialog" md-no-float layout="row" layout-align="center center">' +
            '<md-button layout="row" layout-align="center center" class="md-raised md-primary" ng-click="dlgCtrl.onClickOK()"> '  + btnAgree + ' </md-button>' +
            '</md-input-container>' +
            '</div>' +
            '</md-dialog>'

        let controller = CustomAlertDialogController;

        return this.$mdDialog.show(<any>{
            controller: controller,
            controllerAs: "dlgCtrl",
            template: content,
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            autoWrap: false,
            multiple: true,
            locals: {
                okCallback: okCallback,
                cancelCallback: cancelCallback,
                dialogService: this
            }
        });

    }

    public showCustomConfirm(title, Message, data, okCallback, cancelCallback): ng.IPromise<any> {
        let content: string = '<md-dialog class="md-complex-dialog">' +
            '<md-header>' + title + '<div class="close-btn-md-dialog" ng-click="dlgCtrl.onClickCancel()"></div></md-header>' +
            '<md-content> <div class="text-center -dialog" layout="row" layout-align="center center">' + Message + ' </div> </md-content>' +
            '<div> ' +
            '<md-input-container class="footer-dialog" md-no-float layout="row" layout-align="center center">' +
            '<md-button layout="row" layout-align="center center" class="main-btn-dialog" ng-click="dlgCtrl.onClickOK()"> ' + '<img ng-src="Content/btn/accept_btn.png">'+'</md-button>' +
            '<md-button layout="row" layout-align="center center" class="main-btn-dialog" ng-click="dlgCtrl.onClickCancel()"> ' + '<img ng-src="Content/btn/cancel_btn.png">'+'</md-button>' +
            '</md-input-container>' +
            '</div>' +
            '</md-dialog>'

        let controller = CustomComfirmDialogController;

        return this.$mdDialog.show(<any>{
            controller: controller,
            controllerAs: "dlgCtrl",
            template: content,
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            autoWrap: false,
            multiple: true,
            locals: {
                data: data,
                okCallback: okCallback,
                cancelCallback: cancelCallback,
                dialogService: this
            }
        });
    }

    public selectEmployeeDataContentDialog(title, btnAgree, btnDisagree, template, data, okCallback, cancelCallback): ng.IPromise<any> {
        let content: string = '<md-dialog class="md-complex-dialog">' +
            '<md-header>' + title + '<div class="close-btn-md-dialog" ng-click="dlgCtrl.onClickCancel()"></div></md-header>' +
            '' + template + '' +
            '</md-dialog>'

        let controller = selectEmployeeDataDialogController;

        return this.$mdDialog.show({
            controller: controller,
            controllerAs: "dlgCtrl",
            template: content,
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            autoWrap: false,
            locals: {
                data: data,
                okCallback: okCallback,
                cancelCallback: cancelCallback,
                dialogService: this
            }

        });
    }
}