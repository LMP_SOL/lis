﻿class Guid {
    private id: string;
    private static emptyGuid = new Guid("00000000-0000-0000-0000-000000000000");

    constructor(id: string) {
        if (Guid.isGuid(id)) {
            this.id = id.toLowerCase();
        } else {
            this.id = Guid.emptyGuid.valueOf();
        }
    }

    static compare(guid1: string, guid2: string): boolean {
        if (!guid1 || !guid2) {
            return !guid1 === !guid2;
        }
        return guid1.toUpperCase() === guid2.toUpperCase();
    }

    static get empty() {
        return Guid.emptyGuid;
    }

    static isNullOrEmpty(id: string): boolean {
        return !id || Guid.compare(id, Guid.emptyGuid.toString());
    }

    static isGuid(guid: string): boolean {
        return (
            /^(\{){0,1}[0-9a-fA-F]{8}(\-){0,1}[0-9a-fA-F]{4}(\-){0,1}[0-9a-fA-F]{4}(\-){0,1}[0-9a-fA-F]{4}(\-){0,1}[0-9a-fA-F]{12}(\}){0,1}$/gi).test(guid);
    }

    static newGuid(isSequence: boolean = true) {
        const newGuid = new Guid(
            Guid.s4() +
            Guid.s4() +
            '-' +
            Guid.s4() +
            '-' +
            Guid.s4() +
            '-' +
            Guid.s4() +
            '-' +
            Guid.s4() +
            Guid.s4() +
            Guid.s4()
        );
        if (isSequence) {
            let timePart = (moment)
                ? moment().utc().toDate().getTime().toString(16)
                : new Date().getTime().toString(16);
            while (timePart.length < 12) {
                timePart = `0${timePart}`;
            }
            return new Guid(newGuid.toString().substr(0, 24) + timePart);
        } else {
            return newGuid;
        }
    }

    private static s4() {
        return Math
            .floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    isEmpty() {
        return this.id === Guid.emptyGuid.valueOf();
    }

    isEquals(guid: Guid) {
        return this.valueOf() === guid.valueOf();
    }

    toString(format?: string) {
        if (format) {
            format = format.toUpperCase();
        }
        switch (format) {
            case "B":
                return `{${this.id}}`;
            case "N":
                return this.id.replace(/-/g, "");
            case "P":
                return `(${this.id})`;
            default:
                return this.id;
        }
    }

    valueOf() {
        return this.id;
    }
}