var AuthorizationService = (function () {
    function AuthorizationService($http, $location, $q) {
        this.$http = $http;
        this.$location = $location;
        this.$q = $q;
    }
    AuthorizationService.prototype.getAuthorizationData = function () {
        var json = localStorage.getItem(Constants.localStorageKeys.AuthorizationData);
        if (!json) {
            return null;
        }
        return angular.fromJson(json);
    };
    AuthorizationService.prototype.getLoginCaptcha = function () {
        var _this = this;
        return this.$http.get("api/user/GetLoginCaptcha")
            .then(function (resp) {
            if (resp && resp.data) {
                return angular.fromJson(resp.data);
            }
            return _this.$q.reject();
        });
    };
    AuthorizationService.prototype.logout = function () {
        var _this = this;
        this.$http.get("api/User/SignOut").then(function (resp) {
            console.log(resp);
            localStorage.removeItem(Constants.localStorageKeys.AuthorizationData);
            _this.$location.path("/login");
        });
    };
    AuthorizationService.prototype.getInitDataForUserRegister = function () {
        var _this = this;
        return this.$http.get("api/User/GetInitDataForUserRegister")
            .then(function (resp) {
            if (resp && resp.data) {
                var jsObjs = angular.fromJson(resp.data);
                return _this.$q.resolve(jsObjs);
            }
            return _this.$q.reject();
        });
    };
    AuthorizationService.prototype.login = function (username, password, userCaptcha, captchaEnc) {
        var data = "grant_type=password&username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password)
            + "&encodeCaptcha=" + encodeURIComponent(captchaEnc) + "&userCaptcha=" + encodeURIComponent(userCaptcha);
        return this.$http.post("/Token", data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
            .then(function (resp) {
            localStorage.removeItem(Constants.localStorageKeys.AuthorizationData);
            if (resp && resp.data) {
                var json = angular.toJson(resp.data);
                localStorage.setItem(Constants.localStorageKeys.AuthorizationData, json);
                //workerCallSocket.send();
            }
        });
    };
    return AuthorizationService;
}());
var ModelAuthorizationData = (function () {
    function ModelAuthorizationData() {
        this.access_token = "";
        this.token_type = "";
        this.userName = "";
    }
    return ModelAuthorizationData;
}());
//# sourceMappingURL=authorization.service.js.map