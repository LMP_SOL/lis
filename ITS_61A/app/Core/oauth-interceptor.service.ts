﻿class OAuthInterceptorService implements ng.IHttpInterceptor{
    constructor(private $q: ng.IQService,
        private $location: ng.ILocationService) {
    }

    public request(config: ng.IRequestConfig) {
        if (!config.headers) {
            config.headers = {};
        }
        var authDataJson = localStorage.getItem(Constants.localStorageKeys.AuthorizationData);               
        if (authDataJson) {
            var authData: IUserToken = angular.fromJson(authDataJson);
            config.headers["Authorization"] = 'Bearer ' + authData.access_token;
        }
        return config;
    }

    public responseError = (rejection: any) => {
        if (rejection.status === 401) {
            this.$location.path("/login");
        }
        return this.$q.reject(rejection);
    };
}