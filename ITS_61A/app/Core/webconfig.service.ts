﻿class WebConfigService {
    constructor(private $location: ng.ILocationService) {
        
    }

    public getBaseUrl() {
        return this.$location.protocol() + '://' + this.$location.host() + ':' + this.$location.port() + '/'
    }
}