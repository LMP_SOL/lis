﻿class Constants {
    public static modules = {
        AppName: "LISApp"
    };

    public static components = {
        HomePage: "homePage",
        FileUpload: "fileUpload",
        SettingPassword: "settingPassword",
        InsertWorker: "insertWorker",
        InsertEmployee: "insertEmployee",
        InsertEmployeePopup: "insertEmployeePopup"
        , SettingConnectdatabase: "settingConnectdatabase"
        , SettingEmail: "settingEmail",
        MasterDataManagement:"masterDataManagement",
        GridManagement: "gridManagement",
        SearchUser: "searchUser",
        SettingAlert: "settingAlert",
        InsertUser: "insertUser",
        SettingNotification: "settingNotification",
        PrivilegeManagement: "privilegeManagement",
        AddGroupPrivilegeManagement: "addGroupPrivilegeManagement",
        SortData: "sortData",
        HistoryPerson: "historyPerson",
        DailyHistory: "dailyHistory",
        ViewHistoryPerson: "viewHistoryPerson",
        PurchasingForProject: "purchasingForProject",
        DescPurchasingForProject: "descPurchasingForProject"
        , AnnouncementManagement: "announcementManagement"
        , AnnouncementTabs: "announcementTabs"
        , AnnouncementDescriptionTab: "announcementDescriptionTab"
        , AnnouncementLandTab: "announcementLandTab"
        , AnnouncementDeedTab: "announcementDeedTab"
        , AnnouncementConsiderTab: "announcementConsiderTab"
        , AnnouncementApproveTab: "announcementApproveTab"
        , SelectionAreaAddress:"selectionAreaAddress",        
        PayTaxLand: "payTaxLand",
        LandRegistration: "landRegistration",
        InsertDataPayTaxLand: "insertDataPayTaxLand"
    };

    public static localStorageKeys = {
        CurrentUser: "currentUser",
        AuthorizationData: "authorizationData"
    };

    public static services = {
        Authorization: "authorizationService",
        MasterDataService:"masterDataService",
        Dialog: "dialogService",
        WebConfig: "webConfig",
        SystemManagementService: "SystemManagementService",
        JobSupplyOfLandService: "JobSupplyOfLandService",
        Common: "CommonService",

    };

    public static directives = {
        WhenScrollEnds: "whenscrollends",
        FileInput: "fileInput",
    }

    public static messageDialog = {
        enter: 'ตกลง',
        save: 'บันทึก',
        nosave: 'ไม่บันทึก',
        cancel: 'ยกเลิก',
        confirm: 'ยืนยัน',
        confirmation: 'การยืนยัน',
        alert: 'การแจ้งเตือน',
        systemMesage: 'ข้อความจากระบบ',
        duplicateInfo: 'ข้อมูลซ้ำ',
        addInformation: 'เพิ่มข้อมูล',
        editInformation: 'แก้ไขข้อมูล',

        completeSave: 'บันทึกสำเร็จ',
        notHaveInfo: 'ไม่มีข้อมูล',
        uncompleteSave: 'บันทึกข้อมูลไม่สำเร็จ กรุณาบันทึกอีกครั้ง',
        completeUpdate: 'แก้ไขข้อมูลสำเร็จ',
        uncompleteUpdate: 'ไม่สารมาถแก้ไขข้อมูลได้',
        pleaseEnterInformation: 'กรุณากรอกข้อมูล',
        pleaseEnterAllInformation: 'กรุณากรอกข้อมูลให้ครบ',
        confrim_delete_info: 'ยืนยันการลบข้อมูล',
        completeDelete: 'ลบข้อมูลสำเร็จ',
        uncompleteDelete: 'ไม่สามารถลบข้อมูลได้',
        somethingWrong: 'มีบางอย่างผิดพลาด กรุณาตรวจสอบข้อมูล!',
        rowVersionNotMatch: 'มีคนอื่นกำลังแก้ไขรายการนี้',

        do_you_need_to_save_sort_of_submenu: 'คุณต้องการบันทึกการจัดเรียงลำดับเมนูย่อยใช่หรือไม่?',
        do_you_need_to_save_submenu: 'คุณต้องการบันทึกเมนูย่อยใช่หรือไม่?',
        do_you_need_to_edit_submenu: 'คุณต้องการบันทึกการแก้ไขเมนูย่อยใช่หรือไม่?',

        do_you_need_to_save_sort_of_mainmenu: 'คุณต้องการบันทึกการจัดเรียงลำดับเมนูหลักใช่หรือไม่?',
        do_you_need_to_save_mainmenu: 'คุณต้องการบันทึกเมนูหลักใช่หรือไม่?',
        do_you_need_to_edit_mainmenu: 'คุณต้องการบันทึกการแก้ไขเมนูหลักใช่หรือไม่?',
        do_you_neet_to_cancel: 'ยืนยันการยกเลิก',
        doyouneedtodeleteservice: 'การลบเซอร์วิสจะส่งผลให้ดึงข้อมูลจาก Service หยุดการทำงาน ยืนยันการลบระบบใช่หรือไม่'
    }
}