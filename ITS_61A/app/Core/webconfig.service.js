var WebConfigService = (function () {
    function WebConfigService($location) {
        this.$location = $location;
    }
    WebConfigService.prototype.getBaseUrl = function () {
        return this.$location.protocol() + '://' + this.$location.host() + ':' + this.$location.port() + '/';
    };
    return WebConfigService;
}());
//# sourceMappingURL=webconfig.service.js.map