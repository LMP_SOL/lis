var LoginController = (function () {
    function LoginController(authorizationService, $location) {
        this.authorizationService = authorizationService;
        this.$location = $location;
        this.logInFailed = false;
        this.defaultLoginFailedMessage = "Log in failed.";
    }
    LoginController.prototype.$onInit = function () {
        this.refreshCaptcha();
    };
    LoginController.prototype.refreshCaptcha = function () {
        var _this = this;
        //this.dialogService.showBusyBottomSheet();
        this.authorizationService.getLoginCaptcha()
            .then(function (data) {
            _this.captchaImg = "data:image/png;base64," + data.CaptchaImage;
            _this.captchaEncCode = data.EncodeCaptcha;
        })
            .finally(function () {
            //this.dialogService.hideBusyBottomSheet();
        });
    };
    LoginController.prototype.login = function () {
        var _this = this;
        //this.dialogService.showBusyBottomSheet();
        this.authorizationService.login(this.username, this.password, this.userCaptcha, this.captchaEncCode)
            .then(function () {
            _this.$location.path("/main-menu");
        }, function (error) {
            _this.logInFailed = true;
            _this.logInFailedMessage = _this.defaultLoginFailedMessage;
            if (error && error.data && error.data.error_description) {
                _this.logInFailedMessage = error.data.error_description;
            }
        })
            .finally(function () {
            //this.dialogService.hideBusyBottomSheet();
        });
    };
    LoginController.prototype.register = function () {
        this.$location.path("/user/register");
    };
    LoginController.prototype.forgetPassword = function () {
    };
    return LoginController;
}());
//# sourceMappingURL=login.component.js.map