﻿
class LoginController implements ng.IController {
    public loginForm: ng.IFormController;
    public username: string;
    public password: string;
    public logInFailed: boolean = false;
    private logInFailedMessage: string;
    private defaultLoginFailedMessage = "Log in failed.";
    private captchaEncCode: string;
    public captchaImg: string;
    public userCaptcha: string;
    

    
    constructor(private authorizationService: AuthorizationService,
        private $location: ng.ILocationService,
        //private dialogService: DialogService,
    ) {

    }

    $onInit() {
        this.refreshCaptcha();
    }

    private refreshCaptcha() {
        //this.dialogService.showBusyBottomSheet();
        this.authorizationService.getLoginCaptcha()
            .then((data) => {
                this.captchaImg = "data:image/png;base64," + data.CaptchaImage;
                this.captchaEncCode = data.EncodeCaptcha;
            })
            .finally(() => {
                //this.dialogService.hideBusyBottomSheet();
            });
    }

    public login() {
        //this.dialogService.showBusyBottomSheet();
        this.authorizationService.login(this.username, this.password, this.userCaptcha, this.captchaEncCode)            
            .then(() => {
                this.$location.path("/main-menu");
            }, (error) => {
                this.logInFailed = true;
                this.logInFailedMessage = this.defaultLoginFailedMessage;
                if (error && error.data && error.data.error_description) {
                    this.logInFailedMessage = error.data.error_description;
                }
            })
            .finally(() => {
                //this.dialogService.hideBusyBottomSheet();
            });
    }

    public register() {
        this.$location.path("/user/register");
    }

    public forgetPassword() {

    }

    //public saveLog() {
    //    this.logService.saveLog(this.logCatLoginID)
    //        .then((data) => {
    //            //console.log(data)
    //        })
    //        .finally(() => {
    //        });
    //}
}