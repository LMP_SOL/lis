﻿/// <reference path="jobsupplyofland/purchasingforproject/purchasing-for-project.component.ts" />
/// <reference path="jobsupplyofland/announcementland/announcement-management.component.ts" />
/// <reference path="jobsupplyofland/announcementland/announcement-management.component.ts" />
var app = angular.module(Constants.modules.AppName, ['ngRoute', 'ngCookies', 'ngMaterial', "kendo.directives", "ngSanitize", "ngScrollbars", "angular.vertilize", 'ngMessages', 'md.data.table', 'fixed.table.header', 'dualmultiselect','vAccordion'])
app.service(Constants.services.Authorization, ["$http", "$location", "$q", AuthorizationService])
app.service(Constants.services.MasterDataService, ["$http", Constants.services.WebConfig, "$q", MasterDataService])
app.service(Constants.services.WebConfig, ["$location", WebConfigService])
app.service(Constants.services.SystemManagementService, ["$http", "$location", "$q", SystemManagementService])
app.service(Constants.services.JobSupplyOfLandService, ["$http", "$location", "$q", JobSupplyOfLandService])
app.service(Constants.services.Dialog, ["$mdBottomSheet", "$mdDialog", DialogService])
app.service(Constants.services.Common, [CommonService])
    .directive(Constants.directives.WhenScrollEnds, WhenScrollEnds.factory())
    .directive(Constants.directives.FileInput, FileInput.factory())
    .component(Constants.components.FileUpload, {
        templateUrl: "app/shared/file-upload/file-upload-component.html",
        controllerAs: "fileUploadCtrl",
        controller: ["$scope","$window", Constants.services.Dialog, FileUploadController],
        bindings: {
            func: '=',
            uploadFile: "=",
            operation: "=",
            type: '@',
            ismulti: '=',
            //isIntelligence: '='
        }
    })
    .config(function ($mdDateLocaleProvider) {
        var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
        $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
        $mdDateLocaleProvider.shortMonths = shortMonths;
        $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
        $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
        $mdDateLocaleProvider.monthHeaderFormatter = function (date) {
            return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
        };
        $mdDateLocaleProvider.formatDate = function (date) {
            return (date) ? `${moment(date).format('DD')}/${moment(date).format('MM')}/${moment(date).get('year') + 543}` : null;
        };
        $mdDateLocaleProvider.parseDate = function (dateString) {
            var dateArray = dateString.split("/");
            dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
            var m = moment(dateString, 'L', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
    })
app.component(Constants.components.HomePage, {
    templateUrl: "/app/Home/home.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.WebConfig, "$scope", "$location", "$rootScope", "$routeParams", "$compile", "$http", HomeController]
})
app.component(Constants.components.SettingPassword, {
    templateUrl: "/app/SystemManagement/SystemSetting/setting-password.component/setting-password.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, Constants.services.Dialog,  "$location","$http", SettingPasswordController]
})
app.component(Constants.components.InsertWorker, {
    templateUrl: "/app/SystemManagement/UserManagement/insert-worker.component/insert-worker.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.MasterDataService, Constants.services.SystemManagementService, "$location", "$http", "$element", Constants.services.Dialog, InsertWorkerController],
    bindings: {
        object: "=",   
        mode: "="    
    }
})
app.component(Constants.components.InsertEmployee, {
    templateUrl: "/app/SystemManagement/UserManagement/insert-employee.component/insert-employee.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.MasterDataService, Constants.services.SystemManagementService, "$location", "$http","$element", Constants.services.Dialog, InsertEmployeeController],
    bindings: {
        object: "=",
        mode:"="
    }
})
app.component(Constants.components.InsertEmployeePopup, {
    templateUrl: "/app/SystemManagement/UserManagement/insert-employee.component/insert-employee-popup.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.SystemManagementService, Constants.services.MasterDataService, "$element", "$location",  InsertEmployeePopupController],// employee-popup
    bindings: {
        model: "=",
        saveSelectEmployee: "&",
        cancelSelectEmployee: "&",
    }
})
app.component(Constants.components.SettingConnectdatabase, {
    templateUrl: "/app/SystemManagement/SystemSetting/setting-connectdatabase.component/setting-connectdatabase.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, "$location", "$http", SettingConnectdatabaseController]
})

app.component(Constants.components.SettingEmail, {
    templateUrl: "/app/SystemManagement/SystemSetting/setting-email.component/setting-email.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, Constants.services.Dialog, "$location", "$http", SettingEmailController]
})

 app.component(Constants.components.GridManagement, {
     templateUrl: "app/shared/grid-management/grid-management.component.html",
        controllerAs: "gmCtrl",
        controller: [Constants.services.Authorization, Constants.services.Common, "$location", GridManagementController],
        bindings: {
            gOption: "<",
            onCallbackView: "=",
            onCallbackEdit: "=",
            onCallbackDel: "=",                           
        }
})

app.component(Constants.components.SearchUser, {
    templateUrl: "/app/SystemManagement/UserManagement/search-user/search-user.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, Constants.services.SystemManagementService, Constants.services.Dialog, SearchUserController]
})
app.component(Constants.components.MasterDataManagement, {
    templateUrl: "/app/SystemManagement/MasterDataManagement/master-data-management.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.MasterDataService, Constants.services.Dialog, "$scope", "$location", "$rootScope", "$routeParams", "$compile", masterDataManagementController]
})
app.component(Constants.components.SettingAlert, {
    templateUrl: "/app/SystemManagement/SystemSetting/setting-alert-user/setting-alert-user.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, Constants.services.SystemManagementService,Constants.services.Dialog, "$location", "$http", SettingAlertController]
})

app.component(Constants.components.InsertUser, {
    templateUrl: "/app/SystemManagement/SystemSetting/setting-alert-user/insert-user.component/insert-user.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, "$location", "$http", InsertUserController],
    bindings: {
        object: "=",
    }
})

app.component(Constants.components.PrivilegeManagement, {
    templateUrl: "/app/SystemManagement/privilege-management.component/privilege-management.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, Constants.services.SystemManagementService, Constants.services.Dialog, "$location", "$http", PrivilegeManagementController],
    bindings: {
        object: "=",
    }
})

app.component(Constants.components.AddGroupPrivilegeManagement, {
    templateUrl: "/app/SystemManagement/privilege-management.component/add-group-privilege-management.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, "$location", "$http", Constants.services.SystemManagementService, AddGroupPrivilegeManagementController],
    bindings: {
        object: "=",
    }
})

app.component(Constants.components.SortData, {
    templateUrl: "/app/SystemManagement/SystemSetting/setting-alert-user/sort-data-alert-user.component/sort-data-alert-user.html",
    controllerAs: "ctrl",
    controller: [Constants.services.SystemManagementService, "$scope", "$location", "$http", "$compile", SortDataController],
    bindings: {
        object : "=",
    }
})

app.component(Constants.components.HistoryPerson, {
    templateUrl: "/app/SystemManagement/SystemHistory/person/history-person.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, Constants.services.SystemManagementService, "$location", "$http", HistoryPersonController],
    bindings: {
        object: "=",
    }
})

app.component(Constants.components.DailyHistory, {
    templateUrl: "/app/SystemManagement/SystemHistory/daily/history-daily.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, Constants.services.SystemManagementService, "$location", "$http", HistoryDailyController]
})

app.component(Constants.components.ViewHistoryPerson, {
    templateUrl: "/app/SystemManagement/SystemHistory/person/view-history-person.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Authorization, "$location", "$http", ViewHistoryPersonController],
    bindings: {
        object: "=",
    }
})

app.component(Constants.components.PurchasingForProject, {
    templateUrl: "/app/JobSupplyOfLand/purchasingForProject/purchasing-for-project.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, "$location", "$http", PurchasingForProjectController]
})

app.component(Constants.components.DescPurchasingForProject, {
    templateUrl: "/app/JobSupplyOfLand/purchasingForProject/desc-purchasing-for-project.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, "$location", "$http", DescPurchasingForProjectController]
})


app.component(Constants.components.AnnouncementManagement, {
    templateUrl: "app/JobSupplyOfLand/announcementLand/announcement-management.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, Constants.services.MasterDataService, Constants.services.Common, "$location", "$http","$element", AnnouncementManagementController],
    bindings: {}
})

app.component(Constants.components.AnnouncementTabs, {
    templateUrl: "app/JobSupplyOfLand/announcementLand/announcementTabs/announcement-tabs.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, "$location", "$http", "$element",announcementTabsController],
    bindings: {
        announcementId: "<",
        mode:"="
        
    }
})

app.component(Constants.components.AnnouncementDescriptionTab, {
    templateUrl: "/app/JobSupplyOfLand/announcementLand/announcementTabs/announcementDescriptionTab/announcement-description-tab.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, Constants.services.MasterDataService, "$location", "$http", "$element", announcementDescriptionTabController],
    bindings: {
        form: "=",
        mode: "=",
        modelSave:"=",
    }
})
app.component(Constants.components.AnnouncementDeedTab, {
    templateUrl: "/app/JobSupplyOfLand/announcementLand/announcementTabs/announcementDeedTab/announcement-deed-tab.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, "$location", "$http", announcementDeedTabController],
    bindings: {
        form: "=",
        mode: "=",
        modelSave: "=",
    }
})
app.component(Constants.components.AnnouncementLandTab, {
    templateUrl: "/app/JobSupplyOfLand/announcementLand/announcementTabs/announcementLandTab/announcement-land-tab.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, "$location", "$http", announcementLandTabController],
    bindings: {
        form: "=",
        mode: "=",
        modelSave: "=",
    }
})

app.component(Constants.components.AnnouncementConsiderTab, {
    templateUrl: "/app/JobSupplyOfLand/announcementLand/announcementTabs/announcementConsiderTab/announcement-consider-tab.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, "$location", "$http", announcementConsiderTabController],
    bindings: {
        form: "=",
        mode: "=",
        modelSave: "=",
    }
})

app.component(Constants.components.AnnouncementApproveTab, {
    templateUrl: "/app/JobSupplyOfLand/announcementLand/announcementTabs/announcementApproveTab/announcement-approve-tab.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, "$location", "$http", announcementApproveTabController],
    bindings: {
        form: "=",
        mode: "=",
        model: "=",
    }
})
app.component(Constants.components.SelectionAreaAddress, {
    templateUrl: "/app/shared/selectionAreaAddress/selection-area-address.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.MasterDataService, "$location", "$http", "$element", selectionAreaAddressController],
    bindings: {
        form: "=",
        mode: "=",
        required:"=",
        provinceSelected: "=",
        districtSelected: "=",
        subdistrictSelected: "=",
        onClear:"="
    }
})

//// ใช้สำหรับ Slim Scroll
.config(function (ScrollBarsProvider) {
    ScrollBarsProvider.defaults = {
        scrollButtons: {
            scrollAmount: 'auto', // scroll amount when button pressed
            enable: true // enable scrolling buttons by default
        },
        axis: 'yx', // enable 2 axis scrollbars by default
        autoHideScrollbar: true,
        theme: 'minimal',
        advanced: {
            updateOnContentResize: true
        }
    }
})
app.component(Constants.components.PayTaxLand, {
    templateUrl: "/app/JobSupplyOfLand/payTaxLand/pay-tax-land.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService,Constants.services.Dialog, "$location", "$http", payTaxLandController]
})
app.component(Constants.components.LandRegistration, {
    templateUrl: "app/JobSupplyOfLand/landRegistration/land-registration.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, Constants.services.MasterDataService, "$location", "$http", "$element", Constants.services.Common, LandRegistrationController],
    bindings: {}
})

app.component(Constants.components.InsertDataPayTaxLand, {
    templateUrl: "/app/JobSupplyOfLand/payTaxLand/insertData-pay-tax-land.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.Dialog, Constants.services.JobSupplyOfLandService, "$location", "$http", InsertDataPayTaxLandController],
    bindings: {
        object: "=",
    }
})