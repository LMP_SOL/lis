﻿class OwnershipTransferringController implements ng.IController {
    constructor(

        private _dialogService: DialogService,
        private $http: ng.IHttpService) {

    }

    private gridOptions: any;
    private kendoGrid: any;
    private grid: any;
    private Status = null;
    private dataitem = [];

    public model: any;

    $onInit() {
        this.gridOptions = this.createGridOptions(null);
    }

    private createGridOptions(dataSource: any = null) {
        let template1 = `<div  layout="row" layout-wrap>
                        <div ng-click="ctrl.onEdit(dataItem)"><img ng-src="Content/btn/edit_table_btn.png"></div>
                        <div ng-click="ctrl.onChangePass(dataItem)"><img ng-src="Content/btn/change_password_btn.png"></div>
                        </div>`;

        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "จัดการ", template: template1,
                    attributes: { class: "text-center", "data-title": "จัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "Announcements_No",
                    title: "ประกาศฉบับบที่",
                    attributes: { class: "text-center" },
                     
                },
                {
                    field: "Project_Id",
                    title: "รหัสโครงการ,ชื่อโครงการ",
                    attributes: { class: "text-center" }
                },
                {
                    field: "Province_Id",
                    title: "จังหวัด",
                    attributes: { class: "text-center" },
                },
                {
                    field: "",
                    title: "ขนาดพื้นที่(ไร่-งาน-วา)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "",
                    title: "ราคาที่ซื้อ(บาท/ไร่)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "",
                    title: "ค่าที่ดินทั้งโครงการ(บาท)",
                    attributes: { class: "text-center" },
                },
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;
                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }

    public Search() {
        console.log("this.model", this.model);
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/DeedData/surveyorsHistorySearch', this.model).then((res: any) => {
            if (res.data) {
                console.log('res-->', res.data)
                this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                    Constants.messageDialog.enter,
                    Constants.messageDialog.completeSave,
                    "",
                    "");
            }
        }).catch((err) => {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                Constants.messageDialog.enter,
                Constants.messageDialog.uncompleteSave,
                "", "");
        }).finally(() => {
            this._dialogService.hideBusyBottomSheet();

        })

    }
}