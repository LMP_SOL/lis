﻿class DeedInformationController implements ng.IController {
    constructor(

        private _dialogService: DialogService,
        private $http: ng.IHttpService) {

    }

    private gridOptions: any;
    private kendoGrid: any;
    private grid: any;
    private Status = null;
    private dataitem = [];

    public model: any;

    $onInit() {
        this.gridOptions = this.createGridOptions(null);
    }

    private createGridOptions(dataSource: any = null) {
        let template1 = `<div  layout="row" layout-wrap>
                        <div ng-click="ctrl.onEdit(dataItem)"><img ng-src="Content/btn/edit_table_btn.png"></div>
                        <div ng-click="ctrl.onChangePass(dataItem)"><img ng-src="Content/btn/change_password_btn.png"></div>
                        </div>`;

        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "จัดการ", template: template1,
                    attributes: { class: "text-center", "data-title": "จัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "PROJECT",
                    title: "รหัสโครงการ, ชื่อโครงการ",
                    attributes: { class: "text-center" },
                     
                },
                {
                    field: "CONTACT",
                    title: "ชื่อ - นามสกุลผู้เช่าซื้อ",
                    attributes: { class: "text-center" }
                },
                {
                    field: "FUNCTION_LAND",
                    title: "FUNCTION",
                    attributes: { class: "text-center" },
                },
                {
                    field: "HOUSE_NO",
                    title: "บ้านเลขที่",
                    attributes: { class: "text-center" },
                },
                {
                    field: "DEED_NO",
                    title: "โฉนดที่ดิน",
                    attributes: { class: "text-center" },
                },
                {
                    field: "PARCEL_NO",
                    title: "เลขที่ดิน",
                    attributes: { class: "text-center" },
                },
                {
                    field: "DEALING_FILE_NO",
                    title: "หน้าสำรวจ",
                    attributes: { class: "text-center" },
                },
                {
                    field: "SIZE_IN_DEED",
                    title: "เนื้อที่ตามโฉนด (ไร่-งาน-วา)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "BORROW",
                    title: "วันที่เบิก/ครั้งที่",
                    attributes: { class: "text-center" },
                },
                {
                    field: "RECEIVE_DEED_DATE",
                    title: "วันที่คืน",
                    attributes: { class: "text-center" },
                },
                {
                    field: "TRANSFER_DATE",
                    title: "วันที่โอน",
                    attributes: { class: "text-center" },
                },
                {
                    field: "TRANSFER_STATUS",
                    title: "สถานะการโอน",
                    attributes: { class: "text-center" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;
                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }

    public Search() {
        console.log("this.model", this.model);
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/DeedData/surveyorsHistorySearch', this.model).then((res: any) => {
            if (res.data) {
                console.log('res-->', res.data)
                this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                    Constants.messageDialog.enter,
                    Constants.messageDialog.completeSave,
                    "",
                    "");
            }
        }).catch((err) => {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                Constants.messageDialog.enter,
                Constants.messageDialog.uncompleteSave,
                "", "");
        }).finally(() => {
            this._dialogService.hideBusyBottomSheet();

        })

    }
}