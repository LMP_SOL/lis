var EditDeedInformationController = (function () {
    function EditDeedInformationController(_dialogService, $http) {
        this._dialogService = _dialogService;
        this.$http = $http;
        this.Status = null;
        this.dataitem = [];
    }
    EditDeedInformationController.prototype.$onInit = function () {
        this.gridOptions = this.createGridOptions(null);
    };
    EditDeedInformationController.prototype.createGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template1 = "<div  layout=\"row\" layout-wrap>\n                        <div ng-click=\"ctrl.onEdit(dataItem)\"><img ng-src=\"Content/btn/edit_table_btn.png\"></div>\n                        <div ng-click=\"ctrl.onChangePass(dataItem)\"><img ng-src=\"Content/btn/change_password_btn.png\"></div>\n                        </div>";
        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "จัดการ", template: template1,
                    attributes: { class: "text-center", "data-title": "จัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "Announcements_No",
                    title: "ประกาศฉบับบที่",
                    attributes: { class: "text-center" },
                },
                {
                    field: "Project_Id",
                    title: "รหัสโครงการ,ชื่อโครงการ",
                    attributes: { class: "text-center" }
                },
                {
                    field: "Province_Id",
                    title: "จังหวัด",
                    attributes: { class: "text-center" },
                },
                {
                    field: "",
                    title: "ขนาดพื้นที่(ไร่-งาน-วา)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "",
                    title: "ราคาที่ซื้อ(บาท/ไร่)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "",
                    title: "ค่าที่ดินทั้งโครงการ(บาท)",
                    attributes: { class: "text-center" },
                },
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    EditDeedInformationController.prototype.Search = function () {
        var _this = this;
        console.log("this.model", this.model);
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/DeedData/surveyorsHistorySearch', this.model).then(function (res) {
            if (res.data) {
                console.log('res-->', res.data);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, "", "");
            }
        }).catch(function (err) {
            _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
        }).finally(function () {
            _this._dialogService.hideBusyBottomSheet();
        });
    };
    return EditDeedInformationController;
}());
//# sourceMappingURL=editDeedInformation.component.js.map