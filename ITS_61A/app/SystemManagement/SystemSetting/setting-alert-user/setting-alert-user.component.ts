﻿class SettingAlertController implements ng.IController {
    constructor(private authorizationService: AuthorizationService,
        private systemManagementService: SystemManagementService,
        private dialog: DialogService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
    ) {

    }
    private gridOptions: any;
    private bigTopic: any;
    private startDateFrom: any;
    private startDateTo: any;
    private notifier: any;
    private kendoGrid: any;
    private dataitem = [];
    private paramObj: any;
    private Mode: any;
    $onInit() {

        this.gridOptions = this.CreateGridOptions(null);
        this.Mode = 1;
    }
    //ปุ่ม edit ข้อมูลบน Kendo Grid
    private onEdit(dataItem) {
        this.Mode = 2;
        console.log("Edit--->", dataItem);
        this.paramObj = [];
        this.paramObj.push({
            item: dataItem,
            Mode : true
        })
    }
    //ปุ่มเพิ่มข้อมูล
    private InsertData() {
        this.Mode = 3;
        console.log("Insert Data");
        this.paramObj = [];
        this.paramObj.push({
            Mode: false
        })
    }

    //ปุ่มเรียงข้อความ
    private SortMessage() {
        this.Mode = 4;
        
        //console.log("SortMessage==>", this.dataitem);
        //this.paramObj = [];
        //this.paramObj.push({
        //    item: this.grouplist
        //})
        
    }

    

    //ปุ่ม ค้นหา
    private Search() {
        
        this.startDateFrom = "";
        this.startDateTo = "";
        console.log(this.bigTopic);

        this.systemManagementService.getdataFromSettingAlertUser(this.bigTopic, this.startDateFrom, this.startDateTo, this.notifier).then((res) => {
            this.dataitem = res.Table
            console.log(res.Table);

            let datasource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
            this.kendoGrid.setDataSource(datasource)
        })
    }

    private onDelete(dataItem) {
        //this.dialog.showCustomAlert("ยืนยันการลบข้อมูล", "ตกลง", "ต้องการลบข้อมูลหรือไม่?", undefined, undefined);
        this.dialog.showCustomConfirm("ยืนยันการลบข้อมูล", "ต้องการลบข้อมูลหรือไม่", dataItem, this.Delete, undefined);
        console.log("onDelete", dataItem);
    }

    private Delete = (dataItem) => {
        this.$http.get(`api/SystemManagement/DeleteDataSettingAlertUser?alert_id=${dataItem["RAWTOHEX(ID)"]}`).then((res: any) => {
            console.log(res);
            if (res.status == 200) {
                this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ลบข้อมูลเรียบร้อย", undefined, undefined);
                this.Search();
            }
            else {
                this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ไม่สามารถลบข้อมูลได้", undefined, undefined);
            }
        })
        console.log("Delete", dataItem);
        return true;
    }
    
    private CreateGridOptions(dataSource: any = null) {
        let template_data = `<div  layout="row" layout-wrap>
                        <div ng-click="ctrl.onEdit(dataItem)"><img ng-src="Content/btn/edit_table_btn.png"></div>
                        <div ng-click="ctrl.onDelete(dataItem)"><img ng-src="Content/btn/delete_table_btn.png"></div>
                        </div>`;

        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                { field: "management", title: "การจัดการ", width: "100px", template: template_data, attributes: { class: "text-center", "data-title": "การจัดการ" } },
                {
                    field: "rowNumber",
                    title: "ลำดับ",
                    width: "100px",
                    template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "SUBJECT_MAIN",
                    title: "หัวข้อใหญ่",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "หัวข้อใหญ่" }
                },
                {
                    field: "ALERT_START_TIME",
                    title: "วันที่เริ่มต้น",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "วันที่เริ่มต้น" },
                },
                {
                    field: "ALERT_END_TIME",
                    title: "วันที่สิ้นสุด",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "วันที่สิ้นสุด" },
                },
                {
                    field: "NOTIFIER_NAME",
                    title: "ผู้แจ้งเตือน",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "ผู้แจ้งเตือน" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;

                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }

                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;

                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }

                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }
    
    //ปุ่มค่าเริ่มต้น
    private Clear() {
        this.bigTopic = "";
        this.startDateFrom = "";
        this.startDateTo = "";
        this.notifier = "";
        let data = []
        let datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource)
    }
    
}