var SettingAlertController = (function () {
    function SettingAlertController(authorizationService, systemManagementService, dialog, $location, $http) {
        var _this = this;
        this.authorizationService = authorizationService;
        this.systemManagementService = systemManagementService;
        this.dialog = dialog;
        this.$location = $location;
        this.$http = $http;
        this.dataitem = [];
        this.Delete = function (dataItem) {
            _this.$http.get("api/SystemManagement/DeleteDataSettingAlertUser?alert_id=" + dataItem["RAWTOHEX(ID)"]).then(function (res) {
                console.log(res);
                if (res.status == 200) {
                    _this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ลบข้อมูลเรียบร้อย", undefined, undefined);
                    _this.Search();
                }
                else {
                    _this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ไม่สามารถลบข้อมูลได้", undefined, undefined);
                }
            });
            console.log("Delete", dataItem);
            return true;
        };
    }
    SettingAlertController.prototype.$onInit = function () {
        this.gridOptions = this.CreateGridOptions(null);
        this.Mode = 1;
    };
    //ปุ่ม edit ข้อมูลบน Kendo Grid
    SettingAlertController.prototype.onEdit = function (dataItem) {
        this.Mode = 2;
        console.log("Edit--->", dataItem);
        this.paramObj = [];
        this.paramObj.push({
            item: dataItem,
            Mode: true
        });
    };
    //ปุ่มเพิ่มข้อมูล
    SettingAlertController.prototype.InsertData = function () {
        this.Mode = 3;
        console.log("Insert Data");
        this.paramObj = [];
        this.paramObj.push({
            Mode: false
        });
    };
    //ปุ่มเรียงข้อความ
    SettingAlertController.prototype.SortMessage = function () {
        this.Mode = 4;
        //console.log("SortMessage==>", this.dataitem);
        //this.paramObj = [];
        //this.paramObj.push({
        //    item: this.grouplist
        //})
    };
    //ปุ่ม ค้นหา
    SettingAlertController.prototype.Search = function () {
        var _this = this;
        this.startDateFrom = "";
        this.startDateTo = "";
        console.log(this.bigTopic);
        this.systemManagementService.getdataFromSettingAlertUser(this.bigTopic, this.startDateFrom, this.startDateTo, this.notifier).then(function (res) {
            _this.dataitem = res.Table;
            console.log(res.Table);
            var datasource = new kendo.data.DataSource({ data: _this.dataitem, pageSize: 10 });
            _this.kendoGrid.setDataSource(datasource);
        });
    };
    SettingAlertController.prototype.onDelete = function (dataItem) {
        //this.dialog.showCustomAlert("ยืนยันการลบข้อมูล", "ตกลง", "ต้องการลบข้อมูลหรือไม่?", undefined, undefined);
        this.dialog.showCustomConfirm("ยืนยันการลบข้อมูล", "ต้องการลบข้อมูลหรือไม่", dataItem, this.Delete, undefined);
        console.log("onDelete", dataItem);
    };
    SettingAlertController.prototype.CreateGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template_data = "<div  layout=\"row\" layout-wrap>\n                        <div ng-click=\"ctrl.onEdit(dataItem)\"><img ng-src=\"Content/btn/edit_table_btn.png\"></div>\n                        <div ng-click=\"ctrl.onDelete(dataItem)\"><img ng-src=\"Content/btn/delete_table_btn.png\"></div>\n                        </div>";
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                { field: "management", title: "การจัดการ", width: "100px", template: template_data, attributes: { class: "text-center", "data-title": "การจัดการ" } },
                {
                    field: "rowNumber",
                    title: "ลำดับ",
                    width: "100px",
                    template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "SUBJECT_MAIN",
                    title: "หัวข้อใหญ่",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "หัวข้อใหญ่" }
                },
                {
                    field: "ALERT_START_TIME",
                    title: "วันที่เริ่มต้น",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "วันที่เริ่มต้น" },
                },
                {
                    field: "ALERT_END_TIME",
                    title: "วันที่สิ้นสุด",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "วันที่สิ้นสุด" },
                },
                {
                    field: "NOTIFIER_NAME",
                    title: "ผู้แจ้งเตือน",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "ผู้แจ้งเตือน" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    //ปุ่มค่าเริ่มต้น
    SettingAlertController.prototype.Clear = function () {
        this.bigTopic = "";
        this.startDateFrom = "";
        this.startDateTo = "";
        this.notifier = "";
        var data = [];
        var datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource);
    };
    return SettingAlertController;
}());
//# sourceMappingURL=setting-alert-user.component.js.map