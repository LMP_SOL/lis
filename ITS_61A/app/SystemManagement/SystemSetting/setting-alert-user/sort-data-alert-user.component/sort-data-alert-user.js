var SortDataController = (function () {
    function SortDataController(_systemManagementService, $scope, $location, $http, $compile) {
        var _this = this;
        this._systemManagementService = _systemManagementService;
        this.$scope = $scope;
        this.$location = $location;
        this.$http = $http;
        this.$compile = $compile;
        this._c_date = new DateConverter();
        //********* sort  *****************
        this.sortableOptions = {
            filter: ".k-grid table.dragable tr[data-uid]",
            hint: function (element) {
                return element.clone().addClass("hint");
            },
            cursor: "move",
            ignore: "TD, input",
            placeholder: function (element) {
                var tmp = element.clone();
                tmp.remove("tr:first");
                return tmp
                    .removeAttr("uid")
                    .addClass("k-state-hover")
                    .css("opacity", 0.65);
                //return $('<tr colspan="4" class="placeholder"></tr>');
                //return element.clone().addClass("placeholder");
            },
            container: ".k-grid tbody",
            change: function (e) {
                var grid = _this.$scope.areaGrid, dataItem = grid.dataSource.getByUid(e.item.data("uid"));
                grid.dataSource.remove(dataItem);
                grid.dataSource.insert(e.newIndex, dataItem);
            },
        };
        this.sort_table = function () {
            this.$scope.areaGrid.table.addClass("dragable");
            this.$scope.areaGrid.dataSource.pageSize(100000);
        };
    }
    SortDataController.prototype.$onInit = function () {
        console.log('object', this.object);
        this.getdataForSortAlertUser();
    };
    SortDataController.prototype.getdataForSortAlertUser = function () {
        var _this = this;
        this.$http.get("api/SystemManagement/GetDataSortAlertUser")
            .then(function (res) {
            _this.grouplist = res.data.Table;
            console.log("res===>", _this.grouplist);
            _this.genTemp();
            _this.datasource = new kendo.data.DataSource({
                data: _this.grouplist,
                pageSize: 10,
                serverPaging: false,
                serverSorting: false
            });
            _this.gridOptions = _this.CreateGridOptions(_this.datasource);
            _this.$compile($('#tabSortData'))(_this.$scope);
        }).catch(function (err) {
            console.log('error=>', err);
        }).finally(function () {
            _this.sort_table();
        });
    };
    SortDataController.prototype.genTemp = function () {
        $('#tabSortData').html('<kendo-sortable options="ctrl.sortableOptions" class="tableSizeMasterDetail">'
            + '<kendo-grid k-scope-field="areaGrid" options="ctrl.gridOptions"> </kendo-grid="areaGrid">'
            + '</kendo-sortable><div class="footerBtnCenter">'
            + '<kendo-button ng-click="ctrl.submit_sort_table()" class="btn btnYellow"> <i class="far fa-save"> </i> บันทึก</kendo-button>'
            + '<kendo-button ng-click="ctrl.cancel_sort_table()" class="btn btnGray"> <i class="fas fa-times"> </i> ยกเลิก</kendo-button>');
    };
    SortDataController.prototype.CreateGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var tableRowNo;
        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            resizable: true,
            dataBinding: function () {
                tableRowNo = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            noRecords: {
                template: "ไม่มีข้อมูล"
            },
            columns: [
                //{ field: "management", title: "การจัดการ", width: "100px", attributes: { class: "text-center", "data-title": "การจัดการ" } },
                {
                    title: "ลำดับ",
                    width: "100px",
                    template: " <input type=\"text\" ng-model=\"dataItem.SORT\" >",
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "SUBJECT_MAIN",
                    title: "หัวข้อใหญ่",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "หัวข้อใหญ่" }
                },
                {
                    title: "วันที่เริ่มต้น",
                    width: "auto",
                    template: "{{ctrl.convertDateThai(dataItem.ALERT_START_TIME)}}",
                    attributes: { class: "text-center", "data-title": "วันที่เริ่มต้น" },
                },
                {
                    title: "วันที่สิ้นสุด",
                    width: "auto",
                    template: "{{ctrl.convertDateThai(dataItem.ALERT_END_TIME)}}",
                    attributes: { class: "text-center", "data-title": "วันที่สิ้นสุด" },
                },
                {
                    field: "NOTIFIER_NAME",
                    title: "ผู้แจ้งเตือน",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "ผู้แจ้งเตือน" },
                }
            ],
        };
    };
    SortDataController.prototype.convertDateThai = function (date) {
        return this._c_date.getFormatDateThai(new Date(date));
    };
    //show order 
    SortDataController.prototype.setIndex = function (item) {
        var index = this.gridOptions.dataSource.indexOf(item) + 1;
        return index;
    };
    SortDataController.prototype.changeSortGrid = function (dataItem) {
    };
    return SortDataController;
}());
//# sourceMappingURL=sort-data-alert-user.js.map