﻿class SortDataController implements ng.IController {

    private grouplist: any;
    private gridOptions: any;
    public areaGrid: any;
    public object: any;
    public datasource: any;
    private _c_date: DateConverter = new DateConverter();

    constructor(private _systemManagementService: SystemManagementService,
        private $scope: ng.IScope,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
        private $compile: ng.ICompileService,
    ) {

    }

    $onInit() {
        console.log('object', this.object);
        this.getdataForSortAlertUser();
    }

    public getdataForSortAlertUser() {
        this.$http.get(`api/SystemManagement/GetDataSortAlertUser`)
            .then((res: any) => {

                this.grouplist = res.data.Table;
                console.log("res===>", this.grouplist);

                this.genTemp();
                this.datasource = new kendo.data.DataSource({
                    data: this.grouplist,
                    pageSize: 10,

                    serverPaging: false,
                    serverSorting: false
                });

                this.gridOptions = this.CreateGridOptions(this.datasource);
                this.$compile($('#tabSortData'))(this.$scope);


            }).catch((err) => {
                console.log('error=>', err);
            }).finally(() => {
                this.sort_table();
            });
    }

    private genTemp() {
        $('#tabSortData').html('<kendo-sortable options="ctrl.sortableOptions" class="tableSizeMasterDetail">'
            + '<kendo-grid k-scope-field="areaGrid" options="ctrl.gridOptions"> </kendo-grid="areaGrid">'
            + '</kendo-sortable><div class="footerBtnCenter">'
            + '<kendo-button ng-click="ctrl.submit_sort_table()" class="btn btnYellow"> <i class="far fa-save"> </i> บันทึก</kendo-button>'
            + '<kendo-button ng-click="ctrl.cancel_sort_table()" class="btn btnGray"> <i class="fas fa-times"> </i> ยกเลิก</kendo-button>');
    }

    private CreateGridOptions(dataSource: any = null) {
        var tableRowNo;
        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            resizable: true,
            dataBinding: function () {
                tableRowNo = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            noRecords: {
                template: "ไม่มีข้อมูล"
            },
            columns: [
                //{ field: "management", title: "การจัดการ", width: "100px", attributes: { class: "text-center", "data-title": "การจัดการ" } },
                {

                    title: "ลำดับ",
                    width: "100px",
                    template: " <input type=\"text\" ng-model=\"dataItem.SORT\" >",
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "SUBJECT_MAIN",
                    title: "หัวข้อใหญ่",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "หัวข้อใหญ่" }
                },
                {

                    title: "วันที่เริ่มต้น",
                    width: "auto",
                    template: "{{ctrl.convertDateThai(dataItem.ALERT_START_TIME)}}",
                    attributes: { class: "text-center", "data-title": "วันที่เริ่มต้น" },
                },
                {

                    title: "วันที่สิ้นสุด",
                    width: "auto",
                    template: "{{ctrl.convertDateThai(dataItem.ALERT_END_TIME)}}",
                    attributes: { class: "text-center", "data-title": "วันที่สิ้นสุด" },
                },
                {
                    field: "NOTIFIER_NAME",
                    title: "ผู้แจ้งเตือน",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "ผู้แจ้งเตือน" },
                }
            ],



            //dataBound: function () {
            //    let isPageable = this.options.pageable;
            //    let currentPage = 0;
            //    let itemPerPage = 0;
            //    let lastIndex = 0;

            //    if (isPageable) {
            //        currentPage = this.dataSource.page();
            //        itemPerPage = this.dataSource.pageSize();
            //        lastIndex = (currentPage - 1) * itemPerPage;
            //    }

            //    let rows = this.items();
            //    for (let i = 0; i < rows.length; i++) {
            //        let row = angular.element(rows[i]);
            //        let currentIndex = i + 1;

            //        let rowNum;
            //        if (isPageable) {
            //            rowNum = lastIndex + currentIndex;
            //        }
            //        else {
            //            rowNum = currentIndex;
            //        }

            //        let span = row.find(".row-number");
            //        span.html(rowNum.toString());
            //    }
            //}
        }
    }

    private convertDateThai(date) {
        return this._c_date.getFormatDateThai(new Date(date));
    }


    //show order 
    private setIndex(item) {
        let index = this.gridOptions.dataSource.indexOf(item) + 1;
        return index;
    }




    //********* sort  *****************

    public sortableOptions = {
        filter: ".k-grid table.dragable tr[data-uid]",
        hint: (element) => {
            return element.clone().addClass("hint");
        },
        cursor: "move",
        ignore: "TD, input",
        placeholder: (element) => {

            var tmp = element.clone();
            tmp.remove("tr:first");
            return tmp
                .removeAttr("uid")
                .addClass("k-state-hover")
                .css("opacity", 0.65);
            //return $('<tr colspan="4" class="placeholder"></tr>');
            //return element.clone().addClass("placeholder");
        },
        container: ".k-grid tbody",
        change: (e) => {

            var grid = this.$scope.areaGrid,
                dataItem = grid.dataSource.getByUid(e.item.data("uid"));

            grid.dataSource.remove(dataItem);
            grid.dataSource.insert(e.newIndex, dataItem);
        },
    };

    public sort_table = function () {
        this.$scope.areaGrid.table.addClass("dragable");
        this.$scope.areaGrid.dataSource.pageSize(100000);
    }

    public changeSortGrid(dataItem) {

    }


}