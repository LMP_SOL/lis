var InsertUserController = (function () {
    function InsertUserController(authorizationService, $location, $http) {
        this.authorizationService = authorizationService;
        this.$location = $location;
        this.$http = $http;
        this.model = {
            ALERT_END_TIME: null,
            ALERT_MASSAGE: null,
            ALERT_START_TIME: null,
            NOTIFIER_NAME: null,
            SUBJECT_MAIN: null,
            CREATE_BY: null,
            CREATE_DATE: null,
            UPDATE_BY: null,
            UPDATE_DATE: null,
            SORT: null
        };
        //public editMode = false;
        this.editMode = false;
        this.sentData = function () {
            var data_save = {
                SUBJECT_MAIN: this.model.SUBJECT_MAIN,
                ALERT_START_TIME: this.setTime2,
                ALERT_END_TIME: this.setTime2,
                ALERT_MASSAGE: 'AM',
                NOTIFIER_NAME: 'NN',
                CREATE_BY: 'AA18BA7B99FF403FAFD8529B35F94370',
                CREATE_DATE: this.setTime2,
                UPDATE_BY: 'AA18BA7B99FF403FAFD8529B35F94370',
                UPDATE_DATE: this.setTime2,
                SORT: "8" //this.model.SORT
            };
            this.test = data_save;
            console.log(data_save);
            this.InsertAlertSave(data_save);
        };
    }
    InsertUserController.prototype.$onInit = function () {
        console.log("XXXXXXXXX", this.object);
        this.setTime = "TO_TIMESTAMP('2018-08-30 11:04:01.629952900', 'YYYY-MM-DD HH24:MI:SS.FF')";
        this.setTime2 = this.setTime;
        this.setData();
        this.getdataIdentityAlertUser(this.alertID);
        //this.editMode = this.object[0].Mode
    };
    InsertUserController.prototype.setData = function () {
        if (this.object[0].Mode == false) {
            this.alertID = undefined;
        }
        else {
            this.alertID = this.object["0"].item["RAWTOHEX(ID)"];
        }
    };
    InsertUserController.prototype.getdataIdentityAlertUser = function (alertID) {
        var _this = this;
        var Id = this.alertID;
        if (Id != undefined) {
            this.$http.get("api/SystemManagement/getdataIdentityAlertUser?id=" + Id).then(function (res) {
                console.log('res-->', res);
                _this.modelList = res.data.Table["0"];
                _this.setdata2();
            });
        }
    };
    InsertUserController.prototype.sentDataSave = function () {
        this.sentData();
        //this.InsertAlertSave(this.test);
    };
    InsertUserController.prototype.setdata2 = function () {
        this.model.ALERT_END_TIME = this.modelList.ALERT_END_TIME;
        this.model.ALERT_MASSAGE = this.modelList.ALERT_MASSAGE;
        this.model.ALERT_START_TIME = this.modelList.ALERT_START_TIME;
        this.model.NOTIFIER_NAME = this.modelList.NOTIFIER_NAME;
        this.model.SUBJECT_MAIN = this.modelList.SUBJECT_MAIN;
    };
    InsertUserController.prototype.InsertAlertSave = function (data_save) {
        return this.$http.post("api/SystemManagement/InsertAlertSave", data_save).then(function (res) {
            return res;
        });
    };
    return InsertUserController;
}());
//# sourceMappingURL=insert-user.component.js.map