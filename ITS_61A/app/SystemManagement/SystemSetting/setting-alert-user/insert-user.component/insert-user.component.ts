﻿class InsertUserController implements ng.IController {
    constructor(private authorizationService: AuthorizationService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,

        //private _SystemManagementDataService: InsertUserController,
        //private $scope: ng.IScope,
        //private $rootScope: ng.IRootScopeService,
        //private _routeParams: ng.route.IRouteParamsService,
        //private $compile: ng.ICompileService,
        //private $q: ng.IQService
    ) {

    }

    public model: any = {
        ALERT_END_TIME: null,
        ALERT_MASSAGE: null,
        ALERT_START_TIME: null,
        NOTIFIER_NAME: null,
        SUBJECT_MAIN: null,
        CREATE_BY: null,
        CREATE_DATE: null,
        UPDATE_BY: null,
        UPDATE_DATE: null,
        SORT: null

    }

    public setTime: any
    public setTime2: string

    public test:any
    public modelList: any
    private object;
    private alertID: any
    //public editMode = false;
    public editMode: boolean = false;

    $onInit() {
        console.log("XXXXXXXXX", this.object);
        this.setTime = "TO_TIMESTAMP('2018-08-30 11:04:01.629952900', 'YYYY-MM-DD HH24:MI:SS.FF')";
        this.setTime2 = this.setTime;
        
        this.setData()
        this.getdataIdentityAlertUser(this.alertID)

        //this.editMode = this.object[0].Mode

    }

    public setData() {
        if (this.object[0].Mode == false) {
            this.alertID = undefined;
        }
        else {
            this.alertID = this.object["0"].item["RAWTOHEX(ID)"];
        }
    }

    

    public getdataIdentityAlertUser(alertID) {
        let Id = this.alertID;
        if (Id != undefined) {
            this.$http.get(`api/SystemManagement/getdataIdentityAlertUser?id=${Id}`).then((res: any) => {
            console.log('res-->', res)
            this.modelList = res.data.Table["0"];
            this.setdata2() 
            })
        }
        

    }

    public sentDataSave() {
        this.sentData();
        //this.InsertAlertSave(this.test);
    }

    

    public setdata2() {
        this.model.ALERT_END_TIME = this.modelList.ALERT_END_TIME;
        this.model.ALERT_MASSAGE = this.modelList.ALERT_MASSAGE;
        this.model.ALERT_START_TIME = this.modelList.ALERT_START_TIME;
        this.model.NOTIFIER_NAME = this.modelList.NOTIFIER_NAME;
        this.model.SUBJECT_MAIN = this.modelList.SUBJECT_MAIN;
        
    }

    public sentData = function () {
        var data_save = {
            
            SUBJECT_MAIN: this.model.SUBJECT_MAIN,
            ALERT_START_TIME: this.setTime2,//this.model.ALERT_START_TIME,
            ALERT_END_TIME: this.setTime2,//this.model.ALERT_END_TIME,
    
            ALERT_MASSAGE: 'AM',//this.model.ALERT_MASSAGE,
            NOTIFIER_NAME: 'NN',//this.model.NOTIFIER_NAME,
    
            CREATE_BY: 'AA18BA7B99FF403FAFD8529B35F94370',//this.model.CREATE_BY,
            CREATE_DATE: this.setTime2,//this.model.CREATE_DATE,
            UPDATE_BY: 'AA18BA7B99FF403FAFD8529B35F94370',//this.model.UPDATE_BY,
            UPDATE_DATE: this.setTime2,//this.model.UPDATE_DATE,
            SORT: "8"//this.model.SORT

        }
    this.test = data_save
        
        console.log(data_save);

        this.InsertAlertSave(data_save);
        
    }

    

    public InsertAlertSave(data_save: any): ng.IPromise<any>
    {
        return this.$http.post(`api/SystemManagement/InsertAlertSave`, data_save).then((res: any) => {
            return res;
            });
    }


    
}

