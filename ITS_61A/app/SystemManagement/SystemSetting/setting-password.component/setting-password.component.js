var SettingPasswordController = (function () {
    function SettingPasswordController(authorizationService, _dialogService, $location, $http) {
        this.authorizationService = authorizationService;
        this._dialogService = _dialogService;
        this.$location = $location;
        this.$http = $http;
    }
    SettingPasswordController.prototype.$onInit = function () {
        this.viewMode = true;
        this.viewModeSession = true;
        this.getdataIdentitySetPassword();
        this.setdataIdentitySetPassword(this.ageDate);
    };
    SettingPasswordController.prototype.maxCharacter = function () {
        //this.sumChar; this.model.spacial_character + this.model.upper_case + this.model.lower_case + this.model.input_number
    };
    SettingPasswordController.prototype.getdataIdentitySetPassword = function () {
        var _this = this;
        this.$http.get('api/SystemManagement/GetdataIdentitySetPassword').then(function (res) {
            console.log('res-->', res);
            _this.modelList = res.data;
            _this.modelList = _this.modelList.Table["0"];
            _this.M_modelList = angular.copy(_this.modelList);
        }).catch(function (err) {
        }).finally(function () {
        });
    };
    SettingPasswordController.prototype.setdataIdentitySetPassword = function (ageDate) {
        var age_date = this.ageDate;
        this.$http.get("api/SystemManagement/SetdataIdentitySetPassword?ageDate=" + age_date).then(function (res) {
            console.log('res', res);
        });
    };
    SettingPasswordController.prototype.editViewMode = function () {
        this.viewMode = false;
    };
    SettingPasswordController.prototype.cancelViewMode = function () {
        this.viewMode = true;
        this.modelList.AGE_DATE = angular.copy(this.M_modelList.AGE_DATE);
        this.modelList.CHECK_PASS = angular.copy(this.M_modelList.CHECK_PASS);
        this.modelList.UNLOCK_TIME = angular.copy(this.M_modelList.UNLOCK_TIME);
        this.modelList.DONT_PASS_OLD = angular.copy(this.M_modelList.DONT_PASS_OLD);
        this.modelList.MIN_CHARACTER = angular.copy(this.M_modelList.MIN_CHARACTER);
        this.modelList.MAX_CHARACTER = angular.copy(this.M_modelList.MAX_CHARACTER);
        this.modelList.SAME_CHARACTER = angular.copy(this.M_modelList.SAME_CHARACTER);
        this.modelList.UPPER_CASE = angular.copy(this.M_modelList.UPPER_CASE);
        this.modelList.LOWER_CASE = angular.copy(this.M_modelList.LOWER_CASE);
        this.modelList.INPUT_NUMBER = angular.copy(this.M_modelList.INPUT_NUMBER);
        this.modelList.SPACIAL_CHARACTER = angular.copy(this.M_modelList.SPACIAL_CHARACTER);
        this.modelList.USER_PASS_NAME = angular.copy(this.M_modelList.USER_PASS_NAME);
        this.modelList.ALERT_EXPIRE = angular.copy(this.M_modelList.ALERT_EXPIRE);
        this.modelList.AGE_OTP = angular.copy(this.M_modelList.AGE_OTP);
    };
    SettingPasswordController.prototype.cancelViewModeSession = function () {
        this.viewModeSession = true;
        this.modelList.SESSION_TIMEOUT = angular.copy(this.M_modelList.SESSION_TIMEOUT);
    };
    SettingPasswordController.prototype.saveViewMode = function () {
        var _this = this;
        console.log("this.modelList", this.modelList);
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/settingPasswordDataModel', this.modelList).then(function (res) {
            if (res.data) {
                console.log('res-->', res.data);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, _this.getdataIdentitySetPassword(), "");
            }
        }).catch(function (err) {
            _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
        }).finally(function () {
            _this._dialogService.hideBusyBottomSheet();
        });
    };
    return SettingPasswordController;
}());
//# sourceMappingURL=setting-password.component.js.map