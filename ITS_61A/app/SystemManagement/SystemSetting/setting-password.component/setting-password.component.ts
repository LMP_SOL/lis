﻿
class SettingPasswordController implements ng.IController {
    
    public model: any;
    public modelList: any
    public modelListTo: any
    public M_modelList: any;

    constructor(private authorizationService: AuthorizationService,
        private _dialogService: DialogService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
        //private dialogService: DialogService,
    ) {

    }
    public viewMode: boolean;
    public viewModeSession: boolean;
    public sumChar: any;

    public ageDate: any

    $onInit() {
        this.viewMode = true
        this.viewModeSession = true
        this.getdataIdentitySetPassword();
        this.setdataIdentitySetPassword(this.ageDate)
    }

    public maxCharacter() {
        //this.sumChar; this.model.spacial_character + this.model.upper_case + this.model.lower_case + this.model.input_number
    }
    public getdataIdentitySetPassword() {
        this.$http.get('api/SystemManagement/GetdataIdentitySetPassword').then((res) => {
            console.log('res-->', res)
            this.modelList = res.data;
            this.modelList = this.modelList.Table["0"]
            this.M_modelList = angular.copy(this.modelList);
        }).catch((err) => {

        }).finally(() => {

        })
    }

    public setdataIdentitySetPassword(ageDate) {
        let age_date = this.ageDate;
        this.$http.get(`api/SystemManagement/SetdataIdentitySetPassword?ageDate=${age_date}`).then((res: any) => {
            console.log('res', res)
        })
    }
    debugger

    public editViewMode() {
        this.viewMode = false;
    }
    public cancelViewMode()
    {
        this.viewMode = true;
        this.modelList.AGE_DATE = angular.copy(this.M_modelList.AGE_DATE);
        this.modelList.CHECK_PASS = angular.copy(this.M_modelList.CHECK_PASS);
        this.modelList.UNLOCK_TIME = angular.copy(this.M_modelList.UNLOCK_TIME);
        this.modelList.DONT_PASS_OLD = angular.copy(this.M_modelList.DONT_PASS_OLD);
        this.modelList.MIN_CHARACTER = angular.copy(this.M_modelList.MIN_CHARACTER);
        this.modelList.MAX_CHARACTER = angular.copy(this.M_modelList.MAX_CHARACTER);
        this.modelList.SAME_CHARACTER = angular.copy(this.M_modelList.SAME_CHARACTER);
        this.modelList.UPPER_CASE = angular.copy(this.M_modelList.UPPER_CASE);
        this.modelList.LOWER_CASE = angular.copy(this.M_modelList.LOWER_CASE);
        this.modelList.INPUT_NUMBER = angular.copy(this.M_modelList.INPUT_NUMBER);
        this.modelList.SPACIAL_CHARACTER = angular.copy(this.M_modelList.SPACIAL_CHARACTER);
        this.modelList.USER_PASS_NAME = angular.copy(this.M_modelList.USER_PASS_NAME);
        this.modelList.ALERT_EXPIRE = angular.copy(this.M_modelList.ALERT_EXPIRE);
        this.modelList.AGE_OTP = angular.copy(this.M_modelList.AGE_OTP);
    }
    public cancelViewModeSession()
    {
        this.viewModeSession = true;
        this.modelList.SESSION_TIMEOUT = angular.copy(this.M_modelList.SESSION_TIMEOUT);
    }

    public saveViewMode()
    {
        console.log("this.modelList", this.modelList)
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/settingPasswordDataModel', this.modelList).then((res: any) => {
            if (res.data) {
                console.log('res-->', res.data)
                this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                    Constants.messageDialog.enter,
                    Constants.messageDialog.completeSave,
                    this.getdataIdentitySetPassword(),
                    "");
            }
        }).catch((err) => {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                Constants.messageDialog.enter,
                Constants.messageDialog.uncompleteSave,
                "", "");
        }).finally(() => {
            this._dialogService.hideBusyBottomSheet();

        })

    }


}