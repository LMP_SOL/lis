﻿
class SettingEmailController implements ng.IController {


    public dataIdentiySMTP: any;
    public M_dataIdentiySMTP: any;

    


    private SMTP_editMode: boolean;
    private OTP_editMode: boolean;
    private ChangePass_editMode: boolean;



    public baseUrl: string;

    constructor(private authorizationService: AuthorizationService,
        private _dialogService: DialogService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
    ) {
    }

    $onInit() {
        this.SMTP_editMode = false;
        this.OTP_editMode = false;
        this.ChangePass_editMode = false;
        this.getSettingConnectdatabase();
    }

    public getSettingConnectdatabase() {

        this.$http.get('api/SystemManagement/GetDataIdentitySMTP').then((res: any) => {
            if (res.data) {
                console.log('res-->', res.data)
                this.dataIdentiySMTP = res.data.Table[0];
                this.M_dataIdentiySMTP = angular.copy(this.dataIdentiySMTP);
            }
        }).catch((err) => {

        }).finally(() => {

        })
    }
    public editMode_SMTP()
    {
        this.SMTP_editMode = true;
    }
    public saveMode_SMTP()
    {
        console.log("this.dataIdentiySMTP", this.dataIdentiySMTP)
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/saveDataIdentitySMTP', this.dataIdentiySMTP).then((res: any) => {
            if (res.data) {
                console.log('res-->', res.data)
                this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                    Constants.messageDialog.enter,
                    Constants.messageDialog.completeSave,
                    this.getSettingConnectdatabase(),
                    "");
            }
        }).catch((err) => {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                Constants.messageDialog.enter,
                Constants.messageDialog.uncompleteSave,
                "", "");
        }).finally(() => {
            this._dialogService.hideBusyBottomSheet();

        })

    }
    public CancelEditMode_SMTP() {
        this.SMTP_editMode = false;
        this.dataIdentiySMTP.EMAIL_SERVER = angular.copy(this.M_dataIdentiySMTP.EMAIL_SERVER); 
        this.dataIdentiySMTP.PORT_SERVER = angular.copy(this.M_dataIdentiySMTP.PORT_SERVER); 
        this.dataIdentiySMTP.USER_EMAIL = angular.copy(this.M_dataIdentiySMTP.USER_EMAIL);
        this.dataIdentiySMTP.PASSWORD_EMAIL = angular.copy(this.M_dataIdentiySMTP.USER_EMAIL);
    }

    public editMode_OTP()
    {
        this.OTP_editMode = true;
    }
    public CancelEditMode_OTP()
    {
        this.OTP_editMode = false;
        this.dataIdentiySMTP.SUBJECT_OTP = angular.copy(this.M_dataIdentiySMTP.SUBJECT_OTP);
        this.dataIdentiySMTP.MASSAGE_OTP = angular.copy(this.M_dataIdentiySMTP.MASSAGE_OTP);
    }

    public editMode_ChangePass()
    {
        this.ChangePass_editMode = true;
    }
    public CancelEditMode_ChangePass()
    {
        this.ChangePass_editMode = false;
        this.dataIdentiySMTP.SUBJECT_EMAIL = angular.copy(this.M_dataIdentiySMTP.SUBJECT_EMAIL);
        this.dataIdentiySMTP.MASSAGE_EMAIL = angular.copy(this.M_dataIdentiySMTP.MASSAGE_EMAIL);
    }

}