var SettingEmailController = (function () {
    function SettingEmailController(authorizationService, _dialogService, $location, $http) {
        this.authorizationService = authorizationService;
        this._dialogService = _dialogService;
        this.$location = $location;
        this.$http = $http;
    }
    SettingEmailController.prototype.$onInit = function () {
        this.SMTP_editMode = false;
        this.OTP_editMode = false;
        this.ChangePass_editMode = false;
        this.getSettingConnectdatabase();
    };
    SettingEmailController.prototype.getSettingConnectdatabase = function () {
        var _this = this;
        this.$http.get('api/SystemManagement/GetDataIdentitySMTP').then(function (res) {
            if (res.data) {
                console.log('res-->', res.data);
                _this.dataIdentiySMTP = res.data.Table[0];
                _this.M_dataIdentiySMTP = angular.copy(_this.dataIdentiySMTP);
            }
        }).catch(function (err) {
        }).finally(function () {
        });
    };
    SettingEmailController.prototype.editMode_SMTP = function () {
        this.SMTP_editMode = true;
    };
    SettingEmailController.prototype.saveMode_SMTP = function () {
        var _this = this;
        console.log("this.dataIdentiySMTP", this.dataIdentiySMTP);
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/saveDataIdentitySMTP', this.dataIdentiySMTP).then(function (res) {
            if (res.data) {
                console.log('res-->', res.data);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, _this.getSettingConnectdatabase(), "");
            }
        }).catch(function (err) {
            _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
        }).finally(function () {
            _this._dialogService.hideBusyBottomSheet();
        });
    };
    SettingEmailController.prototype.CancelEditMode_SMTP = function () {
        this.SMTP_editMode = false;
        this.dataIdentiySMTP.EMAIL_SERVER = angular.copy(this.M_dataIdentiySMTP.EMAIL_SERVER);
        this.dataIdentiySMTP.PORT_SERVER = angular.copy(this.M_dataIdentiySMTP.PORT_SERVER);
        this.dataIdentiySMTP.USER_EMAIL = angular.copy(this.M_dataIdentiySMTP.USER_EMAIL);
        this.dataIdentiySMTP.PASSWORD_EMAIL = angular.copy(this.M_dataIdentiySMTP.USER_EMAIL);
    };
    SettingEmailController.prototype.editMode_OTP = function () {
        this.OTP_editMode = true;
    };
    SettingEmailController.prototype.CancelEditMode_OTP = function () {
        this.OTP_editMode = false;
        this.dataIdentiySMTP.SUBJECT_OTP = angular.copy(this.M_dataIdentiySMTP.SUBJECT_OTP);
        this.dataIdentiySMTP.MASSAGE_OTP = angular.copy(this.M_dataIdentiySMTP.MASSAGE_OTP);
    };
    SettingEmailController.prototype.editMode_ChangePass = function () {
        this.ChangePass_editMode = true;
    };
    SettingEmailController.prototype.CancelEditMode_ChangePass = function () {
        this.ChangePass_editMode = false;
        this.dataIdentiySMTP.SUBJECT_EMAIL = angular.copy(this.M_dataIdentiySMTP.SUBJECT_EMAIL);
        this.dataIdentiySMTP.MASSAGE_EMAIL = angular.copy(this.M_dataIdentiySMTP.MASSAGE_EMAIL);
    };
    return SettingEmailController;
}());
//# sourceMappingURL=setting-email.component.js.map