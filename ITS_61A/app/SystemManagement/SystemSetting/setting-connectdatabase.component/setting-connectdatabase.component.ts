﻿
class SettingConnectdatabaseController implements ng.IController {





    public dataIdentiySetSystem: databaseConnectionModel[];
    public M_dataIdentiySetSystem: databaseConnectionModel[];

    //+++ ห้ามลบ
    public baseUrl: string;

    constructor(private _dialogService: DialogService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
    ) {}



    $onInit() {
        this.getSettingConnectdatabase();
    }
    private password: any


    public getSettingConnectdatabase() {
      
        this.$http.get('api/SystemManagement/GetSettingConnectdatabase').then((res: any) => {
            if (res.data) {
                console.log('res-->', res.data)
                for (let i in res.data.Table) {
                    res.data.Table[i].editMode = false;
                }

                this.dataIdentiySetSystem = res.data.Table;
                this.M_dataIdentiySetSystem = angular.copy(this.dataIdentiySetSystem);
            }
        }).catch((err) => {

        }).finally(() => {
            
        })
    }

    public xx() {

    }

    public editMode(item) {
        item.editMode = true;
    }
    //public view() {
        
    //    $(".toggle-password").click(function () {

    //        $(this).toggleClass("fa-eye fa-eye-slash");
    //        var input = $($(this).attr("toggle"));
    //        if (input.attr("type") == "password") {
    //            input.attr("type", "text");
    //        } else {
    //            input.attr("type", "password");
    //        }
    //    });
    //}

    public testConnectionDB() {
        console.log("success!");
    }

    public saveConnectDatabase(item) {
        console.log("dataIdentiySetSystem", item)
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/settingConnectDatabaseDataModel', item).then((res: any) => {
            if (res.data) {
                console.log('res-->', res.data)
                this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                    Constants.messageDialog.enter,
                    Constants.messageDialog.completeSave,
                    this.getSettingConnectdatabase(),
                    "");
            }
        }).catch((err) => {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                Constants.messageDialog.enter,
                Constants.messageDialog.uncompleteSave,
                "", "");
        }).finally(() => {
            this._dialogService.hideBusyBottomSheet();
         
       })

    }
    public cancelConnectDatabase(item, index) {
        item.editMode = false;
        item.HOSTNAME_IP = angular.copy(this.M_dataIdentiySetSystem[index].HOSTNAME_IP);
        item.PORT_SERVER = angular.copy(this.M_dataIdentiySetSystem[index].PORT_SERVER);
        item.SERVICE_NAME = angular.copy(this.M_dataIdentiySetSystem[index].SERVICE_NAME);
        item.ISSID = angular.copy(this.M_dataIdentiySetSystem[index].ISSID);
        item.DATABASE_NAME = angular.copy(this.M_dataIdentiySetSystem[index].DATABASE_NAME);
        item.SET_USER_NAME = angular.copy(this.M_dataIdentiySetSystem[index].SET_USER_NAME);
        item.SET_PASSWORD = angular.copy(this.M_dataIdentiySetSystem[index].SET_PASSWORD);
    }


}