var SettingConnectdatabaseController = (function () {
    function SettingConnectdatabaseController(_dialogService, $location, $http) {
        this._dialogService = _dialogService;
        this.$location = $location;
        this.$http = $http;
    }
    SettingConnectdatabaseController.prototype.$onInit = function () {
        this.getSettingConnectdatabase();
    };
    SettingConnectdatabaseController.prototype.getSettingConnectdatabase = function () {
        var _this = this;
        this.$http.get('api/SystemManagement/GetSettingConnectdatabase').then(function (res) {
            if (res.data) {
                console.log('res-->', res.data);
                for (var i in res.data.Table) {
                    res.data.Table[i].editMode = false;
                }
                _this.dataIdentiySetSystem = res.data.Table;
                _this.M_dataIdentiySetSystem = angular.copy(_this.dataIdentiySetSystem);
            }
        }).catch(function (err) {
        }).finally(function () {
        });
    };
    SettingConnectdatabaseController.prototype.xx = function () {
    };
    SettingConnectdatabaseController.prototype.editMode = function (item) {
        item.editMode = true;
    };
    //public view() {
    //    $(".toggle-password").click(function () {
    //        $(this).toggleClass("fa-eye fa-eye-slash");
    //        var input = $($(this).attr("toggle"));
    //        if (input.attr("type") == "password") {
    //            input.attr("type", "text");
    //        } else {
    //            input.attr("type", "password");
    //        }
    //    });
    //}
    SettingConnectdatabaseController.prototype.testConnectionDB = function () {
        console.log("success!");
    };
    SettingConnectdatabaseController.prototype.saveConnectDatabase = function (item) {
        var _this = this;
        console.log("dataIdentiySetSystem", item);
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/settingConnectDatabaseDataModel', item).then(function (res) {
            if (res.data) {
                console.log('res-->', res.data);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, _this.getSettingConnectdatabase(), "");
            }
        }).catch(function (err) {
            _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
        }).finally(function () {
            _this._dialogService.hideBusyBottomSheet();
        });
    };
    SettingConnectdatabaseController.prototype.cancelConnectDatabase = function (item, index) {
        item.editMode = false;
        item.HOSTNAME_IP = angular.copy(this.M_dataIdentiySetSystem[index].HOSTNAME_IP);
        item.PORT_SERVER = angular.copy(this.M_dataIdentiySetSystem[index].PORT_SERVER);
        item.SERVICE_NAME = angular.copy(this.M_dataIdentiySetSystem[index].SERVICE_NAME);
        item.ISSID = angular.copy(this.M_dataIdentiySetSystem[index].ISSID);
        item.DATABASE_NAME = angular.copy(this.M_dataIdentiySetSystem[index].DATABASE_NAME);
        item.SET_USER_NAME = angular.copy(this.M_dataIdentiySetSystem[index].SET_USER_NAME);
        item.SET_PASSWORD = angular.copy(this.M_dataIdentiySetSystem[index].SET_PASSWORD);
    };
    return SettingConnectdatabaseController;
}());
//# sourceMappingURL=setting-connectdatabase.component.js.map