﻿class databaseConnectionModel {
    public DATABASE_NAME: string
    public  HEADER_NAME :string
    public HOSTNAME_IP: string
    public ISSID: string
    public PORT_SERVER: string
    public ID :string
    public SERVICE_NAME: string
    public SET_PASSWORD: string
    public SET_USER_NAME: string
    public SORT: string
    public SYSTEM_TYPE_ID: string
    public editMode: boolean = false;
}