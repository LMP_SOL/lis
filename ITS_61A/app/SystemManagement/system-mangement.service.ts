﻿class SystemManagementService {

    constructor(private $http: ng.IHttpService,
        private $q: ng.IQService) {
    }


    public getCriteriaforsearcUser(): ng.IPromise<any> {
        return this.$http.get(`api/SystemManagement/getCriteriaforsearcUser?_=${new Date().getTime().toString()}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }

    public getdataForsearchUser(PersonCode: string, PersonName: string, Username: string, Office: string, Status: string, Position: string): ng.IPromise<any> {
        return this.$http.get(`api/SystemManagement/getdataForsearchUser?PersonCode=${PersonCode}&PersonName=${PersonName}&Username=${Username}&Office=${Office}&Status=${Status}&Position=${Position}&_=${new Date().getTime().toString()}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }

    public getdataFromSettingAlertUser(bigTopic: string, startDateFrom: string, startDateTo: string, notifier: string): ng.IPromise<any> {
        return this.$http.get(`api/SystemManagement/getdataFromSettingAlertUser?bigTopic=${bigTopic}&startDate=${startDateFrom}&endDate=${startDateTo}&notifier=${notifier}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
    
    public SendEmailforChangepassword(email : string): ng.IPromise<any> {
        return this.$http.get(`api/SystemManagement/SendEmailforChangepassword?email=${email}&_=${new Date().getTime().toString()}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }

    public getdataGroupPrivilege(groupName: string): ng.IPromise<any> {
        return this.$http.get(`api/SystemManagement/getdataGroupPrivilege?groupName=${groupName}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
    public getdataPersonalHRIS(): ng.IPromise<any> {
        return this.$http.get(`api/IdentityUser/getUserFromHRIS`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
    public getdataIdentityGroup() : ng.IPromise<any> {
        return this.$http.get(`api/SystemManagement/GetdataIdentityGroup`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
    public getdataEmployeeOrWorker(id) {
        return this.$http.get(`api/SystemManagement/GetdataInsertWorker?id=${id}`).then((res: any) => {
            return res;
        })
    }

    public getdataLogStatusPerson(Name: string, startDateFrom: string, startDateTo: string): ng.IPromise<any> {
        
        return this.$http.get(`api/SystemManagement/getdataLogStatusPerson?Name_user=${Name}&startDate_From=${startDateFrom}&start_DateTo=${startDateTo}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }

    public getdataLogByDate(startDateFrom: string, startDateTo: string): ng.IPromise<any> {
        console.log("startDateFrom" , startDateFrom);
        return this.$http.get(`api/SystemManagement/getdataLogByDate?startDate_From=${startDateFrom}&start_DateTo=${startDateTo}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
}