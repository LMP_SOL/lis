var SystemManagementService = (function () {
    function SystemManagementService($http, $q) {
        this.$http = $http;
        this.$q = $q;
    }
    SystemManagementService.prototype.getCriteriaforsearcUser = function () {
        return this.$http.get("api/SystemManagement/getCriteriaforsearcUser?_=" + new Date().getTime().toString())
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    SystemManagementService.prototype.getdataForsearchUser = function (PersonCode, PersonName, Username, Office, Status, Position) {
        return this.$http.get("api/SystemManagement/getdataForsearchUser?PersonCode=" + PersonCode + "&PersonName=" + PersonName + "&Username=" + Username + "&Office=" + Office + "&Status=" + Status + "&Position=" + Position + "&_=" + new Date().getTime().toString())
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    SystemManagementService.prototype.getdataFromSettingAlertUser = function (bigTopic, startDateFrom, startDateTo, notifier) {
        return this.$http.get("api/SystemManagement/getdataFromSettingAlertUser?bigTopic=" + bigTopic + "&startDate=" + startDateFrom + "&endDate=" + startDateTo + "&notifier=" + notifier)
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    SystemManagementService.prototype.SendEmailforChangepassword = function (email) {
        return this.$http.get("api/SystemManagement/SendEmailforChangepassword?email=" + email + "&_=" + new Date().getTime().toString())
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    SystemManagementService.prototype.getdataGroupPrivilege = function (groupName) {
        return this.$http.get("api/SystemManagement/getdataGroupPrivilege?groupName=" + groupName)
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    SystemManagementService.prototype.getdataPersonalHRIS = function () {
        return this.$http.get("api/IdentityUser/getUserFromHRIS")
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    SystemManagementService.prototype.getdataIdentityGroup = function () {
        return this.$http.get("api/SystemManagement/GetdataIdentityGroup")
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    SystemManagementService.prototype.getdataEmployeeOrWorker = function (id) {
        return this.$http.get("api/SystemManagement/GetdataInsertWorker?id=" + id).then(function (res) {
            return res;
        });
    };
    SystemManagementService.prototype.getdataLogStatusPerson = function (Name, startDateFrom, startDateTo) {
        return this.$http.get("api/SystemManagement/getdataLogStatusPerson?Name_user=" + Name + "&startDate_From=" + startDateFrom + "&start_DateTo=" + startDateTo)
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    SystemManagementService.prototype.getdataLogByDate = function (startDateFrom, startDateTo) {
        console.log("startDateFrom", startDateFrom);
        return this.$http.get("api/SystemManagement/getdataLogByDate?startDate_From=" + startDateFrom + "&start_DateTo=" + startDateTo)
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    return SystemManagementService;
}());
//# sourceMappingURL=system-mangement.service.js.map