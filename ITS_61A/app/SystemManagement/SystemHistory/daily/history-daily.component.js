var HistoryDailyController = (function () {
    function HistoryDailyController(authorizationService, systemManagementService, $location, $http) {
        this.authorizationService = authorizationService;
        this.systemManagementService = systemManagementService;
        this.$location = $location;
        this.$http = $http;
        this.dataitem = [];
    }
    HistoryDailyController.prototype.$onInit = function () {
        this.startDateFrom = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
        this.startDateTo = new Date();
        this.gridOptions = this.CreateGridOptions(null);
    };
    HistoryDailyController.prototype.CreateGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template_data = "<div  layout=\"row\" layout-align=\"center center\" layout-wrap>\n                        <div ng-click=\"ctrl.onView(dataItem)\"><img ng-src=\"Content/btn/view_table_btn.png\"></div>\n                        </div>";
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                {
                    field: "ROWN",
                    title: "ลำดับ",
                    width: "80px",
                    template: '<div layout="row" layout-align="center center"><span class="row-number"></span></div>',
                    attributes: { class: "text-center" }
                },
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    width: "auto",
                    attributes: { class: "text-center" }
                },
                {
                    field: "ORG_NAME",
                    title: "หน่วยงาน",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "DATETIME",
                    title: "วัน/เวลา",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "USEDATA",
                    title: "การใช้ข้อมูล",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "IP_ADDRESS",
                    title: "IP Address",
                    width: "auto",
                    attributes: { class: "text-center" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    HistoryDailyController.prototype.Search = function () {
        var _this = this;
        var dateForm = this.getFormatDateEng(this.startDateFrom).toString();
        var dateTo = this.getFormatDateEng(this.startDateTo).toString();
        this.systemManagementService.getdataLogByDate(dateForm, dateTo).then(function (res) {
            _this.dataitem = res.Table;
            console.log('===>res', res);
            var datasource = new kendo.data.DataSource({ data: _this.dataitem, pageSize: 10 });
            console.log('===>datasource', datasource);
            _this.kendoGrid.setDataSource(datasource);
        });
    };
    HistoryDailyController.prototype.Default = function () {
        this.startDateFrom = "";
        this.startDateTo = "";
        var data = [];
        var datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource);
    };
    HistoryDailyController.prototype.getFormatDateEng = function (date) {
        var shortMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        //let day = _date.getDate();
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear();
        var result = year + "-" + month + "-" + day;
        return result;
    };
    HistoryDailyController.prototype.getFormatDateEng2 = function (date) {
        var shortMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        //let day = _date.getDate();
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear();
        var result = day + "/" + month + "/" + year;
        return result;
    };
    HistoryDailyController.prototype.Export = function () {
        console.log(this.dataitem);
        if (this.dataitem.length > 0) {
            var cols = [];
            var sheets = [];
            var dataSource = [];
            var rows = [
                { cells: [{ value: "ประวัติการเข้าใช้งาน", hAlign: "center" },] },
                { cells: [{ value: "ช่วงวันที่" + this.getFormatDateEng2(this.startDateFrom).toString() + "-" + this.getFormatDateEng2(this.startDateTo).toString(), hAlign: "center" },] },
                {
                    cells: [{ value: "ลำดับ", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }, },
                        { value: "ชื่อ-สกุล", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: "หน่วยงาน", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: "วันเวลา", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: "การใช้งาน", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: "ไอพีแอดเดรส", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } }
                    ]
                }
            ];
            this.dataitem.map(function (x) {
                rows.push({
                    cells: [
                        { value: x.ROWN, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.FULLNAME, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.ORG_NAME, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.DATETIME, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.USEDATA, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.IP_ADDRESS, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    ]
                });
            });
            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        mergedCells: ["A1:F1", "A2:F2"],
                        columns: [
                            // Column settings (width)
                            { autoWidth: false },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: false },
                            { autoWidth: true }
                        ],
                        // Title of the sheet
                        title: "ประวัติการเข้าใช้งาน",
                        // Rows of the sheet
                        rows: rows
                    }
                ]
            });
            //save the file as Excel file with extension xlsx
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: "ประวัติการเข้าใช้งาน" + "_" + this.getDate() + ".xlsx" });
        }
    };
    HistoryDailyController.prototype.getDate = function () {
        var today = new Date();
        var dd = today.getDate().toString();
        if (Number(dd) < 10)
            dd = '0' + dd;
        var mm = (today.getMonth() + 1).toString();
        if (Number(mm) < 10)
            mm = '0' + mm;
        //let yyyy = today.getFullYear() + 543;
        var yyyy = today.getFullYear();
        var result = yyyy + mm + dd;
        return result;
    };
    return HistoryDailyController;
}());
//# sourceMappingURL=history-daily.component.js.map