﻿class HistoryDailyController implements ng.IController {
    constructor(
        private authorizationService: AuthorizationService,
        private systemManagementService: SystemManagementService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService, ) {

    }

    private gridOptions: any;
    private startDateFrom: any;
    private startDateTo: any;
    private kendoGrid: any;
    private dataitem = [];

    $onInit() {
        this.startDateFrom = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
        this.startDateTo = new Date();
        this.gridOptions = this.CreateGridOptions(null);
    }

    private CreateGridOptions(dataSource: any = null) {
        let template_data = `<div  layout="row" layout-align="center center" layout-wrap>
                        <div ng-click="ctrl.onView(dataItem)"><img ng-src="Content/btn/view_table_btn.png"></div>
                        </div>`;

        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                
                {
                    field: "ROWN",
                    title: "ลำดับ",
                    width: "80px",
                    template: '<div layout="row" layout-align="center center"><span class="row-number"></span></div>',
                    attributes: { class: "text-center" }
                },
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    width: "auto",
                    attributes: { class: "text-center" }
                },
                {
                    field: "ORG_NAME",
                    title: "หน่วยงาน",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "DATETIME",
                    title: "วัน/เวลา",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "USEDATA",
                    title: "การใช้ข้อมูล",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "IP_ADDRESS",
                    title: "IP Address",
                    width: "auto",
                    attributes: { class: "text-center" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;

                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }

                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;

                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }

                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }
    public Search() {
        var dateForm = this.getFormatDateEng(this.startDateFrom).toString();
        var dateTo = this.getFormatDateEng(this.startDateTo).toString();
        this.systemManagementService.getdataLogByDate(dateForm, dateTo).then((res) => {
            this.dataitem = res.Table
            console.log('===>res', res);

            let datasource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
            console.log('===>datasource', datasource);
            this.kendoGrid.setDataSource(datasource);
        })
    }

    private Default() {
        this.startDateFrom = "";
        this.startDateTo = "";
        let data = []
        let datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource)
    }

    private getFormatDateEng(date) {
        let shortMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        //let day = _date.getDate();
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear();
        let result = `${year}-${month}-${day}`;

        return result;
    }

    private getFormatDateEng2(date) {
        let shortMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        //let day = _date.getDate();
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear();
        let result = `${day}/${month}/${year}`;

        return result;
    }

    public Export() {
        console.log(this.dataitem);

        if (this.dataitem.length > 0) {
            let cols = [];
            let sheets = [];
          
            let dataSource = [];

            var rows = [
                { cells: [{ value: "ประวัติการเข้าใช้งาน", hAlign: "center" },] },
                { cells: [{ value: "ช่วงวันที่" + this.getFormatDateEng2(this.startDateFrom).toString() + "-" + this.getFormatDateEng2(this.startDateTo).toString() , hAlign: "center" },] },

                {
                    cells: [{ value: "ลำดับ", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }, },
                        { value: "ชื่อ-สกุล", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }  },
                        { value: "หน่วยงาน", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }  },
                        { value: "วันเวลา", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }  },
                        { value: "การใช้งาน", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }  },
                        { value: "ไอพีแอดเดรส", hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }  }
                ]
            }];

            this.dataitem.map((x) => {

                rows.push(

                    {
                        cells: [
                            { value: x.ROWN, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } }, 
                            { value: x.FULLNAME, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.ORG_NAME, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.DATETIME, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.USEDATA, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.IP_ADDRESS, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        ]
                    },
                )
            })
            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        mergedCells: ["A1:F1", "A2:F2"],
                        columns: [
                            // Column settings (width)
                            { autoWidth: false },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: false },
                            { autoWidth: true }
                        ],
                        // Title of the sheet
                        title: "ประวัติการเข้าใช้งาน",
                        // Rows of the sheet
                        rows: rows
                    }
                ]
            });
            //save the file as Excel file with extension xlsx
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: `${"ประวัติการเข้าใช้งาน"}_${this.getDate()}.xlsx` });   
        }

    }
private getDate() {
        let today = new Date();
        let dd = today.getDate().toString();
        if (Number(dd) < 10) dd = '0' + dd;
        let mm = (today.getMonth() + 1).toString();
        if (Number(mm) < 10) mm = '0' + mm;
        //let yyyy = today.getFullYear() + 543;
        let yyyy = today.getFullYear();
        let result = yyyy + mm + dd;
        return result;
    }
    

}