﻿class ViewHistoryPersonController implements ng.IController {
    constructor(
        private authorizationService: AuthorizationService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService, ) {

    }
    private gridOptions: any;
    private Name: any;
    private DateFrom: any;
    private DateTo: any;
    private kendoGrid: any;
    private dataitem = [];
    private Mode: any;
    private Id_Person: any;
    private object;
    private dataPerson: any;
    $onInit() {
        this.DateFrom = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
        this.DateTo = new Date();
        this.Mode = 2;
        this.gridOptions = this.CreateGridOptions([]);
        this.setData();
        this.Search();
    }

    private setData() {
        this.dataPerson = this.object["FULLNAME"] + "   " + this.object["ORG_NAME"];
        this.Id_Person = this.object["ID_USER"];
    }

    private CreateGridOptions(dataSource: any = null) {
        
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                {
                    field: "ROWN",
                    title: "ลำดับ",
                    width: "80px",
                    template: '<div layout="row" layout-align="center center"><span class="row-number"></span></div>',
                    attributes: { class: "text-center" }
                },
                {
                    field: "LOG_DATE",
                    title: "วัน/เวลา",
                    width: "auto",
                    attributes: { class: "text-center" }
                },
                {
                    field: "MENU_NAME",
                    title: "การใช้งาน",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "CLIENT_IP",
                    title: "IP Address",
                    width: "auto",
                    attributes: { class: "text-center" },
                }
                
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;

                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }

                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;

                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }

                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }

    private Search() {
        console.log('search_View===>')
        var dateForm = this.getFormatDateEng(this.DateFrom).toString();
        var dateTo = this.getFormatDateEng(this.DateTo).toString();
        this.getdataLogByUser(this.Id_Person, dateForm, dateTo);
    }

    private getFormatDateEng(date) {
        let shortMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear();
        let result = `${year}-${month}-${day}`;
        return result;
    }

    private getdataLogByUser(Id, Date_Form, Date_To) {
        this.$http.get(`api/SystemManagement/getdataLogByUser?id=${Id}&startDate_From=${Date_Form}&startDate_To=${Date_To}`).then((res: any) => {
            this.dataitem = res.data.Table;
            let datasource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
            this.kendoGrid.setDataSource(datasource);
            console.log('getdataLogByUser===>', this.dataitem);
        })
    }

    private Default() {
        let data = [];
        let datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource)
    }

    private Export() {
        if (this.dataitem.length > 0) {
            let cols = [];
            let sheets = [];

            let dataSource = [];

            var rows = [
                { cells: [{ value: "ประวัติการเข้าใช้งาน", hAlign: "center" }] },
                { cells: [{ value: this.dataPerson, hAlign: "center"}] },
                { cells: [{ value: "ช่วงวันที่" + this.getFormatDateEng(this.DateFrom).toString() + "-" + this.getFormatDateEng(this.DateTo).toString(), hAlign: "center" },] },
                { cells: [{ value: "" },] },
                {
                    cells: [{ value: "ลำดับ", hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }},
                        { value: "วันเวลา", hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: "การใช้ระบบ", hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },                   
                        { value: "ไอพีแอดเดรส", hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } }
                    ]
                }];

            this.dataitem.map((x) => {

                rows.push(

                    {
                        cells: [
                            { value: x.ROWN, hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },                           
                            { value: x.LOG_DATE, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.MENU_NAME, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.CLIENT_IP, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        ]
                    },
                )
            })
            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        mergedCells: ["A1:D1", "A2:D2", "A3:D3"],
                        columns: [
                            // Column settings (width)
                            { autoWidth: false },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true }
                        ],
                        // Title of the sheet
                        title: "ประวัติการเข้าใช้งาน",
                        // Rows of the sheet
                        rows: rows
                    }
                ]
            });
            //save the file as Excel file with extension xlsx
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: `${"ประวัติการเข้าใช้งาน"}.xlsx` });
        }
    }
}