var ViewHistoryPersonController = (function () {
    function ViewHistoryPersonController(authorizationService, $location, $http) {
        this.authorizationService = authorizationService;
        this.$location = $location;
        this.$http = $http;
        this.dataitem = [];
    }
    ViewHistoryPersonController.prototype.$onInit = function () {
        this.DateFrom = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
        this.DateTo = new Date();
        this.Mode = 2;
        this.gridOptions = this.CreateGridOptions([]);
        this.setData();
        this.Search();
    };
    ViewHistoryPersonController.prototype.setData = function () {
        this.dataPerson = this.object["FULLNAME"] + "   " + this.object["ORG_NAME"];
        this.Id_Person = this.object["ID_USER"];
    };
    ViewHistoryPersonController.prototype.CreateGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                {
                    field: "ROWN",
                    title: "ลำดับ",
                    width: "80px",
                    template: '<div layout="row" layout-align="center center"><span class="row-number"></span></div>',
                    attributes: { class: "text-center" }
                },
                {
                    field: "LOG_DATE",
                    title: "วัน/เวลา",
                    width: "auto",
                    attributes: { class: "text-center" }
                },
                {
                    field: "MENU_NAME",
                    title: "การใช้งาน",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "CLIENT_IP",
                    title: "IP Address",
                    width: "auto",
                    attributes: { class: "text-center" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    ViewHistoryPersonController.prototype.Search = function () {
        console.log('search_View===>');
        var dateForm = this.getFormatDateEng(this.DateFrom).toString();
        var dateTo = this.getFormatDateEng(this.DateTo).toString();
        this.getdataLogByUser(this.Id_Person, dateForm, dateTo);
    };
    ViewHistoryPersonController.prototype.getFormatDateEng = function (date) {
        var shortMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear();
        var result = year + "-" + month + "-" + day;
        return result;
    };
    ViewHistoryPersonController.prototype.getdataLogByUser = function (Id, Date_Form, Date_To) {
        var _this = this;
        this.$http.get("api/SystemManagement/getdataLogByUser?id=" + Id + "&startDate_From=" + Date_Form + "&startDate_To=" + Date_To).then(function (res) {
            _this.dataitem = res.data.Table;
            var datasource = new kendo.data.DataSource({ data: _this.dataitem, pageSize: 10 });
            _this.kendoGrid.setDataSource(datasource);
            console.log('getdataLogByUser===>', _this.dataitem);
        });
    };
    ViewHistoryPersonController.prototype.Default = function () {
        var data = [];
        var datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource);
    };
    ViewHistoryPersonController.prototype.Export = function () {
        if (this.dataitem.length > 0) {
            var cols = [];
            var sheets = [];
            var dataSource = [];
            var rows = [
                { cells: [{ value: "ประวัติการเข้าใช้งาน", hAlign: "center" }] },
                { cells: [{ value: this.dataPerson, hAlign: "center" }] },
                { cells: [{ value: "ช่วงวันที่" + this.getFormatDateEng(this.DateFrom).toString() + "-" + this.getFormatDateEng(this.DateTo).toString(), hAlign: "center" },] },
                { cells: [{ value: "" },] },
                {
                    cells: [{ value: "ลำดับ", hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: "วันเวลา", hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: "การใช้ระบบ", hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: "ไอพีแอดเดรส", hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } }
                    ]
                }
            ];
            this.dataitem.map(function (x) {
                rows.push({
                    cells: [
                        { value: x.ROWN, hAlign: "center", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.LOG_DATE, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.MENU_NAME, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.CLIENT_IP, hAlign: "left", borderTop: { color: "#000000", size: 1 }, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    ]
                });
            });
            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        mergedCells: ["A1:D1", "A2:D2", "A3:D3"],
                        columns: [
                            // Column settings (width)
                            { autoWidth: false },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true }
                        ],
                        // Title of the sheet
                        title: "ประวัติการเข้าใช้งาน",
                        // Rows of the sheet
                        rows: rows
                    }
                ]
            });
            //save the file as Excel file with extension xlsx
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: "ประวัติการเข้าใช้งาน" + ".xlsx" });
        }
    };
    return ViewHistoryPersonController;
}());
//# sourceMappingURL=view-history-person.component.js.map