var HistoryPersonController = (function () {
    function HistoryPersonController(authorizationService, systemManagementService, $location, $http) {
        this.authorizationService = authorizationService;
        this.systemManagementService = systemManagementService;
        this.$location = $location;
        this.$http = $http;
        this.dataitem = [];
    }
    HistoryPersonController.prototype.$onInit = function () {
        this.Mode = 1;
        this.startDateFrom = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
        this.startDateTo = new Date();
        this.gridOptions = this.CreateGridOptions(null);
    };
    HistoryPersonController.prototype.Search = function () {
        var _this = this;
        //this.startDateFrom = "";
        //this.startDateTo = "";
        var dateForm = this.getFormatDateEng(this.startDateFrom).toString();
        var dateTo = this.getFormatDateEng(this.startDateTo).toString();
        this.systemManagementService.getdataLogStatusPerson(this.Name, dateForm, dateTo).then(function (res) {
            _this.dataitem = res.Table;
            console.log('===>res_Search', _this.dataitem);
            var datasource = new kendo.data.DataSource({ data: _this.dataitem, pageSize: 10 });
            _this.kendoGrid.setDataSource(datasource);
        });
    };
    HistoryPersonController.prototype.onView = function (dataItem) {
        this.Mode = 2;
        console.log("onView--->", dataItem);
        this.paramObj = dataItem;
    };
    HistoryPersonController.prototype.CreateGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template_data = "<div  layout=\"row\" layout-align=\"center center\" layout-wrap>\n                        <div ng-click=\"ctrl.onView(dataItem)\"><img ng-src=\"Content/btn/view_table_btn.png\"></div>\n                        </div>";
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                { field: "management", title: "การจัดการ", width: "100px", template: template_data, attributes: { class: "text-center", "data-title": "การจัดการ" } },
                {
                    field: "rowNumber",
                    title: "ลำดับ",
                    width: "80px",
                    template: '<div layout="row" layout-align="center center"><span class="row-number"></span></div>',
                    attributes: { class: "text-center" }
                },
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    width: "auto",
                    attributes: { class: "text-center" }
                },
                {
                    field: "ORG_NAME",
                    title: "หน่วยงาน",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    template: '<div layout="row" layout-align="center center" class="w100-percen">' +
                        '#if(SIGNOUT == null){# <img class="img-responsive icon-btn" ng-src="Content/btn/icon-use.png"> #} else{# <img class="img-responsive icon-btn" ng-src="Content/btn/icon-unuse.png"> #}#'
                        + '</div>',
                    title: "สถานะ",
                    width: "90px",
                    attributes: { class: "text-center", "data-title": "สถานะ" }
                },
                {
                    field: "SIGNIN",
                    title: "วัน/เวลาเข้าระบบ",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "SIGNOUT",
                    title: "วัน/เวลาออกระบบ",
                    width: "auto",
                    attributes: { class: "text-center" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    HistoryPersonController.prototype.Default = function () {
        this.Name = "";
        this.startDateFrom = "";
        this.startDateTo = "";
        var data = [];
        var datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource);
    };
    HistoryPersonController.prototype.getFormatDateEng = function (date) {
        var shortMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        //let day = _date.getDate();
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear();
        var result = year + "-" + month + "-" + day;
        return result;
    };
    HistoryPersonController.prototype.Export = function () {
        console.log(this.dataitem);
        if (this.dataitem.length > 0) {
            var cols_1 = [];
            var sheets = [];
            var rows = [];
            var dataSource_1 = [];
            var Hearder = {
                cells: [
                    { value: "ลำดับ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "ชื่อ-สกุล", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "หน่วยงาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "สถานะ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "วันเวลาเข้าระบบ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "วันเวลาออกระบบ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "IP Address", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } }
                ]
            };
            dataSource_1.push(Hearder);
            this.dataitem.map(function (x) {
                dataSource_1.push({
                    cells: [
                        { value: x.ROWN, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.FULLNAME, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.ORG_NAME, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.STATUS, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.SIGNIN, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.SIGNIN, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.CLIENT_IP, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } }
                    ]
                });
            });
            rows = (dataSource_1);
            rows.map(function (item) {
                cols_1.push({ autoWidth: true });
            });
            sheets.push({
                columns: cols_1,
                title: "ประวัติการเข้าใช้งานระบบ",
                rows: rows
            });
            var workbook = new kendo.ooxml.Workbook({
                sheets: sheets
            });
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: "ประวัติการเข้าใช้งานระบบ_" + ".xlsx" });
        }
    };
    return HistoryPersonController;
}());
//# sourceMappingURL=history-person.component.js.map