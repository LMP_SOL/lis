﻿class HistoryPersonController implements ng.IController {
    constructor(
        private authorizationService: AuthorizationService,
        private systemManagementService: SystemManagementService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService, )
    {

    }
    private gridOptions: any;
    private Name: any;
    private startDateFrom: any;
    private startDateTo: any;
    private kendoGrid: any;
    private dataitem = [];
    private Mode: any;
    private paramObj: any;
    $onInit() {
        this.Mode = 1;
        this.startDateFrom = new Date(new Date().setFullYear(new Date().getFullYear()-1));
        this.startDateTo = new Date();
        this.gridOptions = this.CreateGridOptions(null);
    }

    private Search() {
        //this.startDateFrom = "";
        //this.startDateTo = "";
        var dateForm = this.getFormatDateEng(this.startDateFrom).toString();
        var dateTo = this.getFormatDateEng(this.startDateTo).toString();
        this.systemManagementService.getdataLogStatusPerson(this.Name, dateForm, dateTo).then((res) => {
            this.dataitem = res.Table
            console.log('===>res_Search', this.dataitem);

            let datasource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });            
            this.kendoGrid.setDataSource(datasource)
        })
    }

    private onView(dataItem) {
        this.Mode = 2;
        console.log("onView--->", dataItem);
        this.paramObj = dataItem;
    }

    private CreateGridOptions(dataSource: any = null) {
        let template_data = `<div  layout="row" layout-align="center center" layout-wrap>
                        <div ng-click="ctrl.onView(dataItem)"><img ng-src="Content/btn/view_table_btn.png"></div>
                        </div>`;

        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                { field: "management", title: "การจัดการ", width: "100px", template: template_data, attributes: { class: "text-center", "data-title": "การจัดการ" } },
                {
                    field: "rowNumber",
                    title: "ลำดับ",
                    width: "80px",
                    template: '<div layout="row" layout-align="center center"><span class="row-number"></span></div>',
                    attributes: { class: "text-center"}
                },
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    width: "auto",
                    attributes: { class: "text-center"}
                },
                {
                    field: "ORG_NAME",
                    title: "หน่วยงาน",
                    width: "auto",
                    attributes: { class: "text-center"},
                },
                {
                    template: '<div layout="row" layout-align="center center" class="w100-percen">' +
                    '#if(SIGNOUT == null){# <img class="img-responsive icon-btn" ng-src="Content/btn/icon-use.png"> #} else{# <img class="img-responsive icon-btn" ng-src="Content/btn/icon-unuse.png"> #}#'
                    + '</div>'
                    , title: "สถานะ"
                    , width: "90px"
                    , attributes: { class: "text-center", "data-title": "สถานะ" }
                },  
                {
                    field: "SIGNIN",
                    title: "วัน/เวลาเข้าระบบ",
                    width: "auto",
                    attributes: { class: "text-center"},
                },
                {
                    field: "SIGNOUT",
                    title: "วัน/เวลาออกระบบ",
                    width: "auto",
                    attributes: { class: "text-center"},
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;

                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }

                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;

                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }

                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }

    private Default() {
        this.Name = "";
        this.startDateFrom = "";
        this.startDateTo = "";
        let data = []
        let datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource)
    }

    private getFormatDateEng(date) {
        let shortMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        //let day = _date.getDate();
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear();
        let result = `${year}-${month}-${day}`;

        return result;
    }

    private Export() {
        console.log(this.dataitem);
        if (this.dataitem.length > 0) {
            let cols = [];
            let sheets = [];
            let rows = [];
            let dataSource = [];

            let Hearder = {
                cells: [
                    { value: "ลำดับ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },                   
                    { value: "ชื่อ-สกุล", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "หน่วยงาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "สถานะ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "วันเวลาเข้าระบบ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "วันเวลาออกระบบ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "IP Address", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } }
                    
                ]
            };

            dataSource.push(Hearder);

            this.dataitem.map((x) => {

                dataSource.push(
                    {
                        cells: [
                            { value: x.ROWN, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },                          
                            { value: x.FULLNAME, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.ORG_NAME, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.STATUS, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.SIGNIN, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.SIGNIN, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                            { value: x.CLIENT_IP, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } }
                            
                        ]
                    },
                )
            })

            rows = (dataSource)

            rows.map((item) => {
                cols.push({ autoWidth: true });
            });

            sheets.push(
                {
                    columns: cols,
                    title: "ประวัติการเข้าใช้งานระบบ",
                    rows: rows
                }
            );

            var workbook = new kendo.ooxml.Workbook({
                sheets: sheets
            });
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: `${"ประวัติการเข้าใช้งานระบบ_"}.xlsx` });

        }
    }
}