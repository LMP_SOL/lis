﻿class GridSystemManagementController implements ng.IController {
    public gridOptions: customOption;
    public onCallbackView: any;
    public onCallbackEdit: any;
    public onCallbackDel: any;
    public gOption: customOption;
    public select: any;
    public kendoGrid: any;
    private grid: any;

    constructor(private authorizationService: AuthorizationService,
        private $location: ng.ILocationService,
        //private dialogService: DialogService,
    ) { }

    public $onInit() {
        this.gridOptions = this.gOption;
        console.log('this.gridOptions', this.gridOptions)
        this.gridOptions.columns = this.setCommandManagement(this.gridOptions);

    }

    private setCommandManagement(option: any) {
        let res = [];
        let btnStatus: any;
        let iconStatus: any;

        if (option) {
            res = option.column;
        }      
        if (option.management) {
            btnStatus = {
                command: this.setOperation(option.operation), title: "การจัดการ", attributes: { "data-title": "การจัดการ" },
            }
            res.push(btnStatus)
        }
        if (option.showIndex) {
            res.splice(0, 0, {
                field: "rowNumber",
                title: "ลำดับ",
                attributes: { "data-title": "ลำดับ" },
                template: "<div>{{gsmCtrl.setIndex(dataItem)}}</div>",
            })
        }
        return res;
    }

    private setOperation(operation: operationModel) {
        let arr = [];
        if (operation.view) {
            arr.push({ template: '<div ng-click="gsmCtrl.onView(dataItem)"><img ng-src="Content/btn/search_btn.png"></div>' })
        }
        if (operation.edit) {
            arr.push({ template: '<div ng-click="gsmCtrl.onEdit(dataItem)"><img ng-src="Content/btn/search_btn.png"></div>' })
        }
        if (operation.del) {
            arr.push({ template: '<div ng-click="gsmCtrl.onDel(dataItem)"><img ng-src="Content/btn/search_btn.png"></div>' })
        }      
        return arr;
    }

    //public showErrorDialog(dataItem) {
    //    if (dataItem.STATUS_UPDATE_ID === '5ea2c38a-b59b-4455-9966-6d13c16835e3') {
    //        this.dialogService.errorDialogServicemornitoring('แจ้งเตือนการล้มเหลว', dataItem.ERROR_MASSAGE);
    //    }
    //}

    private setIndex(item) {
        let index = this.kendoGrid.dataSource.indexOf(item) + 1;
        return index;
    }

    public onEdit(item) {
        if (this.onCallbackEdit) {
            this.onCallbackEdit(item);
        }
    }

    public onDel(item) {
        if (this.onCallbackDel) {
            this.onCallbackDel(item);
        }
    }
    public onView(item) {
        if (this.onCallbackView) {
            this.onCallbackView(item);
        }
    }
   
    /////////////////////////////////////
}
class operationModel {
    public view: boolean = false;
    public edit: boolean = false;
    public del: boolean = false;
    public doc: boolean = false; 
}
interface customOption extends kendo.ui.GridOptions {
    operation: operationModel;
    column: Array<string>;
    management: boolean;
    showIndex: boolean;
    showStatus: boolean;
}
