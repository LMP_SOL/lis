﻿class PrivilegeManagementController implements ng.IController {
    constructor(private authorizationService: AuthorizationService,
        private systemManagementService: SystemManagementService,
        private dialog: DialogService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
    ) {

    }
    private mainGridOptions: any;
    private groupName: any;
    private dataitem = [];
    private kendoGrid: any;
    private Mode: any;
    private paramObj: any;
    
    $onInit() {
        this.Mode = 1;
        this.mainGridOptions = this.CreateGridOptions(null);
    } 

    //Create Grid 
    private CreateGridOptions(dataSource: any = null) {
        let template_data = `<div layout="row" layout-wrap>
                        <div ng-click="ctrl.onEdit(dataItem)"><img ng-src="Content/btn/edit_table_btn.png"></div>
                        <div ng-click="ctrl.onDelete(dataItem)"><img ng-src="Content/btn/delete_table_btn.png"></div>
                        </div>`;

        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "การจัดการ",
                    width: "100px",
                    template: template_data, attributes: { class: "text-center", "data-title": "การจัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ",
                    width: "100px",
                    template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "GROUP_NAME",
                    title: "กลุ่มสิทธิ์การใช้งาน",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "กลุ่มสิทธิ์การใช้งาน" }
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;

                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }

                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;

                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }

                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }

    private Search() {
        //ดึงข้อมูลกลุ่มสิทธ์การใช้งาน
        console.log(this.groupName);
        this.systemManagementService.getdataGroupPrivilege(this.groupName).then((res) => {

            this.dataitem = res.Table
            console.log(res.Table);
            

            let datasource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
            this.kendoGrid.setDataSource(datasource)
            
        })
    }

    private Clear()
    {
        this.groupName = "";
        let data = []
        let datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource)
    }

    private onDelete(dataItem) {
        console.log("onDelete===>", dataItem);
        this.dialog.showCustomConfirm("ยืนยันการลบข้อมูล", "ต้องการลบข้อมูลหรือไม่", dataItem, this.Delete, undefined);
    }

    private Delete = (dataItem) => {
        this.$http.get(`api/SystemManagement/DeleteDataGroupPrivilege?group_id=${dataItem["RAWTOHEX(ID)"]}&group_update_by=${dataItem["RAWTOHEX(CREATE_BY)"]}`).then((res: any) => {
            console.log(res);
            if (res.status == 200) {
                this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ลบข้อมูลเรียบร้อย", undefined, undefined);
                this.Search();
            }
            else {
                this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ไม่สามารถลบข้อมูลได้", undefined, undefined);
            }
        })
        console.log("Delete", dataItem);
        return true;
    }

    private AddGroup() {
        this.Mode = 2;
    }

    private onEdit(dataItem) {
        this.Mode = 3;
        console.log('onEdit===>', dataItem);
        this.paramObj = [];
        this.paramObj.push({
            item: dataItem
        })
    }
}

