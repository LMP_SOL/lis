﻿class AddGroupPrivilegeManagementController implements ng.IController {

    constructor(private authorizationService: AuthorizationService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
        private systemManagementService: SystemManagementService   
    ) {
    }

    private OfficeList: any;
    private allListings: any;
    private selectedContacts: any;
    public selectAll = false;
    private dataitem = [];
    private kendoGrid: any;
    private kendoGrid2: any;
    private gridOptions: any;
    private gridOptions2: any;
    private Name: any;
    private UserName: any;
    private Office: any;
    private selected: any;
    private callBack: any;
    private dataItemGrid2 = [];
    private selected2: any;
    private result: any;

    $onInit() {
        this.OfficeList = [];

        //หน่วยงาน ==> select item
        //this.InitCriteria();
        
        this.gridOptions = this.CreateGridOptions([]);
        this.gridOptions2 = this.CreateGridOptions2([]);
    }

    private CreateGridOptions(dataSource: any) {
        let template_data = "<div><input type='checkbox' ng-model='dataItem.isChecked' ng-click='ctrl.toggle(dataItem, ctrl.selected);' /></div>";
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                {
                    title: "<div><input type='checkbox' ng-click='ctrl.toggleAll(dataItem)'/></div>",
                    width: "40px",
                    template: template_data
                },
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    width: "120px",
                    attributes: { class: "text-center" }
                },
                {
                    field: "USER_CODE",
                    title: "รหัสผู้ใช้งาน",
                    width: "100px",
                    attributes: { class: "text-center" },
                }
            ],
        }
    }

    private toggle(item, list) {
        console.log('toggle===>', item);
        

        let idx = list.indexOf(item);
        console.log('idx===>', idx);
        if (idx > -1) {
            list.splice(idx, 1);
            //list.isChecked = false       
        } else {
            item.isChecked2 = false;
            
            list.push(item);
          
        }
console.log('list===>', list);
        
    }

    private toggle2(item, list) {
        console.log('toggle===>', item);
        let idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        } else {
            item.isChecked = false;

            list.push(item);

        }
    }

    private toggleAll(item) {
        console.log('toggleAll===>');
        for (let i = 0; i < this.dataitem.length; i++) {
            this.dataitem[i].isChecked = true;
        }
    }

    private CreateGridOptions2(dataSource: any = null) {
        let template_data = "<div><input type='checkbox' ng-model='dataItem.isChecked2' ng-click='ctrl.toggle2(dataItem, ctrl.selected2);' /></div>";
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                {
                    width: "40px",
                    template: template_data
                },
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    width: "120px",
                    attributes: { class: "text-center" }
                },
                {
                    field: "ORG_NAME",
                    title: "หน่วยงาน",
                    width: "100px",
                    attributes: { class: "text-center" },
                }
            ],
        }
    }

    private Search() {
        console.log('Search========>');
        if (this.Name == undefined) {
            this.Name = '';
        }
        if (this.UserName == undefined) {
            this.UserName = '';
        }
        if (this.Office == undefined)
        {
            this.Office = '';
        }
        this.getData_User(this.Name, this.UserName, this.Office);
        
    }

    private InitCriteria() {
        this.systemManagementService.getCriteriaforsearcUser().then((res) => {
            console.log('rexxxx', res);
            this.OfficeList = res.Table;
        })
    }
    
    private getData_User(name, username, selectDepartment)
    {
        let datatest = [
            {
                FULLNAME: "WWWW",
                USER_CODE: "XXX",
                ORG_NAME: "QQQ"
            },
            {
                FULLNAME: "aaaa",
                USER_CODE: "bbbb",
                ORG_NAME: "UUU"
            },
            {
                FULLNAME: "ccc",
                USER_CODE: "ddd",
                ORG_NAME: "PPP"
            }
        ]
        //this.$http.get(`api/SystemManagement/getdataUser?Name=${name}&Username=${username}&SelectDepartment=${selectDepartment}`).then((res: any) => {
        //    this.dataitem = res.data.Table;
        //    console.log('getData_User=====>', res);
        //    let datasource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
        //    this.kendoGrid.setDataSource(datasource);
        //    this.selected = [];
        //})

        this.dataitem = datatest;
        this.dataitem.map((x) => {
            x.isChecked = false;
            x.isChecked2 = false;
        })
        //this.result = this.dataitem;
        console.log('getData_User=====>', this.dataitem);
            let datasource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
            this.kendoGrid.setDataSource(datasource);
            this.selected = [];

    }

    private ToRight()
    {
        let datasource: any;
        let dataPushGrid2 = [];
        console.log('ToRight===>', this.selected);


        this.dataItemGrid2.push(this.selected);

        //for (let i = 0; i < this.dataItemGrid2.length; i++) {
            
        //}

        for (let i = 0; i < this.dataItemGrid2.length; i++) {
            console.log('dataItemGrid2===>', this.dataItemGrid2[i]);
            dataPushGrid2[i] = this.dataItemGrid2[i];
        }
        datasource = new kendo.data.DataSource({ data: dataPushGrid2, pageSize: 10 });
        this.kendoGrid2.setDataSource(datasource);

        let data = this.kendoGrid.dataSource._data;
        //for (let i = 0; i < this.dataItemGrid2[0].length; i++){
        this.result = data.filter(x => x.isChecked == false);
        //}

        let datasource2 = new kendo.data.DataSource({ data: this.result, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource2);
        console.log('dataitem====>', this.result);
        
        this.selected2 = [];
        this.selected = [];
    }

    private ToLeft()
    {
        let test = this.result;
        //let data2 = this.selected2;


       // let result2 = this.dataItemGrid2[0];
        //console.log('result2===>', result2);

        for (let i = 0; i < this.selected2.length; i++)
        {
            test.push(this.selected2[i]);
        }
        

        let datasource2 = new kendo.data.DataSource({ data: test, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource2);
        console.log('dataitem====>', test);

        let data = this.kendoGrid2.dataSource._data;

        //for (let i = 0; i < this.selected2.length; i++) {
      let  result2 = data.filter(x => x.isChecked2 == false);
        //}
        

        let datasourcefirst = new kendo.data.DataSource({ data: result2, pageSize: 10 });
        this.kendoGrid2.setDataSource(datasourcefirst);
        this.selected = [];
        this.selected2 = [];
    }
}