var AddGroupPrivilegeManagementController = (function () {
    function AddGroupPrivilegeManagementController(authorizationService, $location, $http, systemManagementService) {
        this.authorizationService = authorizationService;
        this.$location = $location;
        this.$http = $http;
        this.systemManagementService = systemManagementService;
        this.selectAll = false;
        this.dataitem = [];
        this.dataItemGrid2 = [];
    }
    AddGroupPrivilegeManagementController.prototype.$onInit = function () {
        this.OfficeList = [];
        //หน่วยงาน ==> select item
        //this.InitCriteria();
        this.gridOptions = this.CreateGridOptions([]);
        this.gridOptions2 = this.CreateGridOptions2([]);
    };
    AddGroupPrivilegeManagementController.prototype.CreateGridOptions = function (dataSource) {
        var template_data = "<div><input type='checkbox' ng-model='dataItem.isChecked' ng-click='ctrl.toggle(dataItem, ctrl.selected);' /></div>";
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                {
                    title: "<div><input type='checkbox' ng-click='ctrl.toggleAll(dataItem)'/></div>",
                    width: "40px",
                    template: template_data
                },
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    width: "120px",
                    attributes: { class: "text-center" }
                },
                {
                    field: "USER_CODE",
                    title: "รหัสผู้ใช้งาน",
                    width: "100px",
                    attributes: { class: "text-center" },
                }
            ],
        };
    };
    AddGroupPrivilegeManagementController.prototype.toggle = function (item, list) {
        console.log('toggle===>', item);
        var idx = list.indexOf(item);
        console.log('idx===>', idx);
        if (idx > -1) {
            list.splice(idx, 1);
            //list.isChecked = false       
        }
        else {
            item.isChecked2 = false;
            list.push(item);
        }
        console.log('list===>', list);
    };
    AddGroupPrivilegeManagementController.prototype.toggle2 = function (item, list) {
        console.log('toggle===>', item);
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            item.isChecked = false;
            list.push(item);
        }
    };
    AddGroupPrivilegeManagementController.prototype.toggleAll = function (item) {
        console.log('toggleAll===>');
        for (var i = 0; i < this.dataitem.length; i++) {
            this.dataitem[i].isChecked = true;
        }
    };
    AddGroupPrivilegeManagementController.prototype.CreateGridOptions2 = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template_data = "<div><input type='checkbox' ng-model='dataItem.isChecked2' ng-click='ctrl.toggle2(dataItem, ctrl.selected2);' /></div>";
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                {
                    width: "40px",
                    template: template_data
                },
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    width: "120px",
                    attributes: { class: "text-center" }
                },
                {
                    field: "ORG_NAME",
                    title: "หน่วยงาน",
                    width: "100px",
                    attributes: { class: "text-center" },
                }
            ],
        };
    };
    AddGroupPrivilegeManagementController.prototype.Search = function () {
        console.log('Search========>');
        if (this.Name == undefined) {
            this.Name = '';
        }
        if (this.UserName == undefined) {
            this.UserName = '';
        }
        if (this.Office == undefined) {
            this.Office = '';
        }
        this.getData_User(this.Name, this.UserName, this.Office);
    };
    AddGroupPrivilegeManagementController.prototype.InitCriteria = function () {
        var _this = this;
        this.systemManagementService.getCriteriaforsearcUser().then(function (res) {
            console.log('rexxxx', res);
            _this.OfficeList = res.Table;
        });
    };
    AddGroupPrivilegeManagementController.prototype.getData_User = function (name, username, selectDepartment) {
        var datatest = [
            {
                FULLNAME: "WWWW",
                USER_CODE: "XXX",
                ORG_NAME: "QQQ"
            },
            {
                FULLNAME: "aaaa",
                USER_CODE: "bbbb",
                ORG_NAME: "UUU"
            },
            {
                FULLNAME: "ccc",
                USER_CODE: "ddd",
                ORG_NAME: "PPP"
            }
        ];
        //this.$http.get(`api/SystemManagement/getdataUser?Name=${name}&Username=${username}&SelectDepartment=${selectDepartment}`).then((res: any) => {
        //    this.dataitem = res.data.Table;
        //    console.log('getData_User=====>', res);
        //    let datasource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
        //    this.kendoGrid.setDataSource(datasource);
        //    this.selected = [];
        //})
        this.dataitem = datatest;
        this.dataitem.map(function (x) {
            x.isChecked = false;
            x.isChecked2 = false;
        });
        //this.result = this.dataitem;
        console.log('getData_User=====>', this.dataitem);
        var datasource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource);
        this.selected = [];
    };
    AddGroupPrivilegeManagementController.prototype.ToRight = function () {
        var datasource;
        var dataPushGrid2 = [];
        console.log('ToRight===>', this.selected);
        this.dataItemGrid2.push(this.selected);
        //for (let i = 0; i < this.dataItemGrid2.length; i++) {
        //}
        for (var i = 0; i < this.dataItemGrid2.length; i++) {
            console.log('dataItemGrid2===>', this.dataItemGrid2[i]);
            dataPushGrid2[i] = this.dataItemGrid2[i];
        }
        datasource = new kendo.data.DataSource({ data: dataPushGrid2, pageSize: 10 });
        this.kendoGrid2.setDataSource(datasource);
        var data = this.kendoGrid.dataSource._data;
        //for (let i = 0; i < this.dataItemGrid2[0].length; i++){
        this.result = data.filter(function (x) { return x.isChecked == false; });
        //}
        var datasource2 = new kendo.data.DataSource({ data: this.result, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource2);
        console.log('dataitem====>', this.result);
        this.selected2 = [];
        this.selected = [];
    };
    AddGroupPrivilegeManagementController.prototype.ToLeft = function () {
        var test = this.result;
        //let data2 = this.selected2;
        // let result2 = this.dataItemGrid2[0];
        //console.log('result2===>', result2);
        for (var i = 0; i < this.selected2.length; i++) {
            test.push(this.selected2[i]);
        }
        var datasource2 = new kendo.data.DataSource({ data: test, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource2);
        console.log('dataitem====>', test);
        var data = this.kendoGrid2.dataSource._data;
        //for (let i = 0; i < this.selected2.length; i++) {
        var result2 = data.filter(function (x) { return x.isChecked2 == false; });
        //}
        var datasourcefirst = new kendo.data.DataSource({ data: result2, pageSize: 10 });
        this.kendoGrid2.setDataSource(datasourcefirst);
        this.selected = [];
        this.selected2 = [];
    };
    return AddGroupPrivilegeManagementController;
}());
//# sourceMappingURL=add-group-privilege-management.component.js.map