var PrivilegeManagementController = (function () {
    function PrivilegeManagementController(authorizationService, systemManagementService, dialog, $location, $http) {
        var _this = this;
        this.authorizationService = authorizationService;
        this.systemManagementService = systemManagementService;
        this.dialog = dialog;
        this.$location = $location;
        this.$http = $http;
        this.dataitem = [];
        this.Delete = function (dataItem) {
            _this.$http.get("api/SystemManagement/DeleteDataGroupPrivilege?group_id=" + dataItem["RAWTOHEX(ID)"] + "&group_update_by=" + dataItem["RAWTOHEX(CREATE_BY)"]).then(function (res) {
                console.log(res);
                if (res.status == 200) {
                    _this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ลบข้อมูลเรียบร้อย", undefined, undefined);
                    _this.Search();
                }
                else {
                    _this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ไม่สามารถลบข้อมูลได้", undefined, undefined);
                }
            });
            console.log("Delete", dataItem);
            return true;
        };
    }
    PrivilegeManagementController.prototype.$onInit = function () {
        this.Mode = 1;
        this.mainGridOptions = this.CreateGridOptions(null);
    };
    //Create Grid 
    PrivilegeManagementController.prototype.CreateGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template_data = "<div layout=\"row\" layout-wrap>\n                        <div ng-click=\"ctrl.onEdit(dataItem)\"><img ng-src=\"Content/btn/edit_table_btn.png\"></div>\n                        <div ng-click=\"ctrl.onDelete(dataItem)\"><img ng-src=\"Content/btn/delete_table_btn.png\"></div>\n                        </div>";
        return {
            dataSource: dataSource,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "การจัดการ",
                    width: "100px",
                    template: template_data, attributes: { class: "text-center", "data-title": "การจัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ",
                    width: "100px",
                    template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "GROUP_NAME",
                    title: "กลุ่มสิทธิ์การใช้งาน",
                    width: "auto",
                    attributes: { class: "text-center", "data-title": "กลุ่มสิทธิ์การใช้งาน" }
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    PrivilegeManagementController.prototype.Search = function () {
        var _this = this;
        //ดึงข้อมูลกลุ่มสิทธ์การใช้งาน
        console.log(this.groupName);
        this.systemManagementService.getdataGroupPrivilege(this.groupName).then(function (res) {
            _this.dataitem = res.Table;
            console.log(res.Table);
            var datasource = new kendo.data.DataSource({ data: _this.dataitem, pageSize: 10 });
            _this.kendoGrid.setDataSource(datasource);
        });
    };
    PrivilegeManagementController.prototype.Clear = function () {
        this.groupName = "";
        var data = [];
        var datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource);
    };
    PrivilegeManagementController.prototype.onDelete = function (dataItem) {
        console.log("onDelete===>", dataItem);
        this.dialog.showCustomConfirm("ยืนยันการลบข้อมูล", "ต้องการลบข้อมูลหรือไม่", dataItem, this.Delete, undefined);
    };
    PrivilegeManagementController.prototype.AddGroup = function () {
        this.Mode = 2;
    };
    PrivilegeManagementController.prototype.onEdit = function (dataItem) {
        this.Mode = 3;
        console.log('onEdit===>', dataItem);
        this.paramObj = [];
        this.paramObj.push({
            item: dataItem
        });
    };
    return PrivilegeManagementController;
}());
//# sourceMappingURL=privilege-management.component.js.map