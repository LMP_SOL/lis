﻿class MasterDataService {
    public DistrictAndProvinceData: any;

    constructor(private $http: ng.IHttpService,

        private _webConfigService: WebConfigService,
        private $q: ng.IQService) {
    }


    public getSectionOfMenu(menuId: string): ng.IPromise<any> {
        return this.$http.get(this._webConfigService.getBaseUrl() +`api/MasterData/getSectionOfMasterMenu?menuId=${menuId}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
    public getDataIntab(menuId: string, sessionId: string): ng.IPromise<any> {
        return this.$http.get(this._webConfigService.getBaseUrl() +`api/MasterData/getDataIntab?menuId=${menuId}&sessionId=${sessionId}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }

    public getDatatopic(docID: string, menuId: string): ng.IPromise<any> {
        return this.$http.get(this._webConfigService.getBaseUrl() +`api/MasterData/Datatopic?docId=${docID}&menuId=${menuId}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
    public getMastertopic(sessionId: string): ng.IPromise<any> {
        return this.$http.get(this._webConfigService.getBaseUrl() +`api/MasterData/Mastertopic?sessionId=${sessionId}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
    public Sortdata(data: any): ng.IPromise<any>{
        return this.$http.post(this._webConfigService.getBaseUrl() +`api/MasterData/Sortdata`,data)
            .then((res: any) => {
               
                    return res;
                
            });
    }
    public Mastersave(data: any): ng.IPromise<any> {
        return this.$http.post(this._webConfigService.getBaseUrl() +`api/MasterData/Mastersave`, data)
            .then((res: any) => {

                return res;

            });
    }

    public getDataSubdistrictOffset(limit: number, offset: number, keyword: string, district: string): ng.IPromise<any> {
        return this.$http.get(this._webConfigService.getBaseUrl() +`api/MasterData/getDataSubdistrictOffset?limit=${limit}&offset=${offset}&keyword=${keyword}&district=${district}`)
            .then((res: any) => {
                return res;
            });
    }

    public getDataDistrictAndProvince(): ng.IPromise<any> {

        return this.$http.get(this._webConfigService.getBaseUrl() + `api/MasterData/getDataDistrictAndProvince`)
            .then((res: any) => {
                this.DistrictAndProvinceData = res.data;
                return res;
            });
    }


    public getDepartment(): ng.IPromise<any> {
        return this.$http.get(this._webConfigService.getBaseUrl() +`api/MasterData/GetDataDepartment`)
            .then((res: any) => {
                return res;
            });
    }

    public getMasterData(session_id: any, topic_id) {
        return this.$http.get(this._webConfigService.getBaseUrl() +`api/SystemManagement/GetdataFromMasterInsertWorker?session_id=${session_id}&topic_id=${topic_id}`).then((res: any) => {
            return res;
        })
    }

  


}