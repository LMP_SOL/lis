var masterDataManagementController = (function () {
    function masterDataManagementController(_masterDataService, _dialogService, $scope, $location, $rootScope, _routeParams, $compile) {
        var _this = this;
        this._masterDataService = _masterDataService;
        this._dialogService = _dialogService;
        this.$scope = $scope;
        this.$location = $location;
        this.$rootScope = $rootScope;
        this._routeParams = _routeParams;
        this.$compile = $compile;
        this.masterpopupoption = {
            errorTemplate: '',
        };
        this.tabManagerOptions = {
            select: function (e) {
                console.log(e.item);
                var id_tabsession = $(e.item).find("> .k-link > div").prop('id');
                var id_tabsession_id = id_tabsession.toString().replace('tab_master_', '');
                _this.dragdropstatus = false;
                id_tabsession = id_tabsession_id;
                _this.mastersession_id = id_tabsession_id;
                _this.searchText = '';
                _this.list_combo = [];
                _this._masterDataService.getDataIntab(_this.selectMenuId, id_tabsession_id).then(function (result) {
                    console.log("getDataIntab", result);
                    for (var i = 0; i < result.mastersession.length; i++) {
                        $('#tab_master_detail_' + result.mastersession[i].MASTERSESSION_ID).html('');
                    }
                    _this.data_table = result.list_table;
                    _this.data_session = result.mastersession;
                    var tableRowNo;
                    var data_table = [];
                    var count_index = 1;
                    for (var i = 0; i < result['list_table'].length; i++) {
                        if (result['list_table'][i].MASTER_TOPIC_ID == result['mastertopic'][0].ID) {
                            var name_list = [];
                            var id_list = [];
                            name_list.push(result['list_table'][i]['DATA_TOPIC_TEXT']);
                            id_list.push(result['list_table'][i]['DATA_TOPIC_ID']);
                            var string_isuse = 'active';
                            var string_isuse_text = '<i class="k-icon k-i-k-icon k-i-check-circle k-i-checkmark-circle"></i>';
                            if (result['list_table'][i]['ISUSE'] != "T") {
                                string_isuse = '';
                                string_isuse_text = '<i class="k-icon k-i-k-icon k-i-close-circle k-i-x-circle"></i>';
                            }
                            if (result['list_table'][i]['DATA_TOPIC_PARENT'] != null) {
                                var limit_loop = 0;
                                var check_parent = result['list_table'][i]['DATA_TOPIC_PARENT'];
                                do {
                                    for (var j = 0; j < result['list_table'].length; j++) {
                                        if (result['list_table'][j]['DATA_TOPIC_ID'] == check_parent) {
                                            name_list.splice(1, 0, result['list_table'][j]['DATA_TOPIC_TEXT']);
                                            id_list.splice(1, 0, result['list_table'][j]['DATA_TOPIC_ID']);
                                            check_parent = result['list_table'][j]['DATA_TOPIC_PARENT'];
                                        }
                                    }
                                    limit_loop++;
                                } while (check_parent != null && limit_loop < 10);
                            }
                            data_table.push({
                                index: count_index,
                                Name: name_list,
                                id: id_list,
                                isuse: string_isuse,
                                isuse_text: string_isuse_text
                            });
                            count_index++;
                        }
                    }
                    _this.mastersession_id_index = 0;
                    var text_option = '';
                    var text_option_modal = '';
                    for (var i = 0; i < result['mastersession'].length; i++) {
                        if (result['mastersession'][i]['MASTERSESSION_ID'] == id_tabsession) {
                            _this.mastersession_id_index = i;
                            break;
                        }
                        else
                            _this.$scope['combo_select_data_' + i] = [{ id: 0, name: 'ทั้งหมด' }];
                        _this.$scope['modal_combo_select_data_' + i] = [];
                        _this.$scope['combo_select_' + i] = _this.$scope['combo_select_data_' + i][0];
                        _this.$scope['modal_combo_select_' + i] = null;
                        //$scope['modal_combo_select_' + i] = $scope['modal_combo_select_data_' + i][0];
                        if (i == 0) {
                            for (var j = 0; j < result['list_table'].length; j++) {
                                if (result['list_table'][j]['MASTER_TOPIC_ID'] == result['mastersession'][i]['MASTERTOPIP_ID']) {
                                    _this.$scope['combo_select_data_' + i].push({
                                        name: result['list_table'][j]['DATA_TOPIC_TEXT'],
                                        id: result['list_table'][j]['DATA_TOPIC_ID'],
                                    });
                                    _this.$scope['modal_combo_select_data_' + i].push({
                                        name: result['list_table'][j]['DATA_TOPIC_TEXT'],
                                        id: result['list_table'][j]['DATA_TOPIC_ID'],
                                    });
                                }
                            }
                        }
                        text_option += '<div class="col-xs-12 col-sm-12 col-md-6"><div class="input-group comboBoxStyle"><label>' + result['mastersession'][i]['MASTERSESSION_NAME'] + '</label> \
                                <select kendo-combo-box k-ng-model="combo_select_' + i + '" \
                                k-placeholder="\'เลือก' + result['mastersession'][i]['MASTERSESSION_NAME'] + '\'"\
                                k-data-source="combo_select_data_' + i + '" \
                                k-on-change="ctrl.change_search_select(' + i + ')" \
                                k-data-text-field="\'name\'" \
                                k-data-value-field="\'id\'" \
                                k-filter="\'contains\'" \
                                k-auto-bind="false" \
                                k-min-length="1" \
                                ></select></div></div>';
                        text_option_modal += '<div class="comboBoxStyle"><label>' + result['mastersession'][i]['MASTERSESSION_NAME'] + '</label> \
                                <select kendo-combo-box k-ng-model="modal_combo_select_' + i + '" \
                                k-ng-readonly="doc_transfer_id"\
                                k-placeholder="\'เลือก' + result['mastersession'][i]['MASTERSESSION_NAME'] + '\'"\
                                k-data-source="modal_combo_select_data_' + i + '" \
                                k-on-change="ctrl.modal_change_search_select(' + i + ')" \
                                k-data-text-field="\'name\'" \
                                k-data-value-field="\'id\'" \
                                k-filter="\'contains\'" \
                                k-auto-bind="false" \
                                k-min-length="1" \
                                required></select></div>';
                    }
                    var col_size = 6;
                    if (_this.mastersession_id_index == 0 || _this.mastersession_id_index % 2 == 0)
                        col_size = 12;
                    text_option += '<div class="col-xs-12 col-sm-12 col-md-' + col_size + '"><div class="input-group"><label>ค้นหา</label>\
                        <input type="text" ng-model="ctrl.searchText" placeholder="ค้นหา">\
                                    <span class="input-group-btn">\
                                     <kendo-button ng-click="ctrl.search_table()" class="btn btnYellow"><i class="fas fa-search"></i> ค้นหา</kendo-button>\
                                  </span>\
                                   <span class="input-group-btn">\
                                    <kendo-button ng-click="ctrl.re_search_table()" class="btn btnGray"><i class="fas fa-redo-alt"></i> ค่าเริ่มต้น</kendo-button>\
                                  </span>\
                                </div></div>';
                    $('#data_topic_select_div').html(text_option_modal);
                    _this.$compile($('#data_topic_select_div'))(_this.$scope);
                    $('#tab_master_detail_' + id_tabsession).html('<div class="tabContentDetail">\
                            <div id="searchGroup" class="searchGroup" ng-hide="ctrl.dragdropstatus">\
                                ' + text_option + '\
                            </div>\
                            <div id="EditGroup" class="EditGroup">\
                                <kendo-button ng-click="ctrl.add_table()" ng-hide="ctrl.dragdropstatus" class="roundBtn btnYellow"><i class="k-icon k-i-k-icon k-i-plus-sm "></i></kendo-button>\
                                <kendo-button ng-click="ctrl.sort_table()" ng-hide="ctrl.dragdropstatus" class="roundBtn btnYellow"><i class="k-icon k-i-k-icon k-i-arrows-kpi k-i-kpi"></i></kendo-button>\
                            </div>\
                            <kendo-sortable options="ctrl.sortableOptions" class="tableSizeMasterDetail">\
                            <kendo-grid k-scope-field="areaGrid" options="ctrl.exampleGridOptions">\
                            </kendo-grid>\
                             </kendo-sortable>\
                            </div>\
                            <div class="footerBtnCenter">\
                                 <kendo-button ng-click="ctrl.submit_sort_table()" ng-show="ctrl.dragdropstatus" class="btn btnYellow"><i class="far fa-save"></i> บันทึก</kendo-button>\
                                <kendo-button ng-click="ctrl.cancel_sort_table()" ng-show="ctrl.dragdropstatus" class="btn btnGray"><i class="fas fa-times"></i> ยกเลิก</kendo-button>\
                            </div>');
                    _this.textheader = result['mastersession'][_this.mastersession_id_index]['MASTERSESSION_NAME'];
                    $('#header_text').text(_this.textheader);
                    _this.testData = new kendo.data.DataSource({
                        data: data_table,
                        pageSize: 10,
                        serverPaging: false,
                        serverSorting: false
                    });
                    var column_grid = [];
                    column_grid.push({
                        template: "<kendo-button class='roundBtn btnYellow' icon=\"'k-icon k-i-edit'\" ng-click=\"ctrl.editItem('#:id[0]#');\"></kendo-button>",
                        field: "id[0]",
                        title: "จัดการ",
                        width: "70px"
                    }, {
                        template: "{{ctrl.setIndex(dataItem)}}",
                        title: "ลำดับ",
                        width: "70px"
                    });
                    if (_this.mastersession_id_index > 0) {
                        for (var i = 1; i <= _this.mastersession_id_index; i++) {
                            column_grid.push({
                                field: "Name[" + i + "]",
                                title: result['mastersession'][i - 1]['MASTERSESSION_NAME'],
                                width: "130px"
                            });
                        }
                    }
                    column_grid.push({
                        field: "Name[0]",
                        title: result['mastersession'][_this.mastersession_id_index]['MASTERSESSION_NAME'],
                    }, {
                        template: "<div class=\"#:isuse#\">#=isuse_text#</div>",
                        field: "isuse",
                        title: "การใช้งาน",
                        width: "100px"
                    });
                    _this.exampleGridOptions = {
                        dataSource: _this.testData,
                        sortable: true,
                        pageable: true,
                        resizable: true,
                        dataBinding: function () {
                            tableRowNo = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                        },
                        noRecords: {
                            template: "No data"
                        },
                        columns: column_grid
                    };
                    _this.$compile($('#tab_master_detail_' + id_tabsession))(_this.$scope);
                }).catch(function (err) {
                    console.log('error=>', err);
                }).finally(function () {
                });
                _this._masterDataService.getMastertopic(id_tabsession).then(function (result) {
                    console.log('getMastertopic =>', result);
                    _this.list_master_topic_id = [];
                    _this.data_topic_size = 0;
                    $('#data_topic_text_div').html('');
                    for (var i = 0; i < result.mastertopic.length; i++) {
                        $('#data_topic_text_div').append('<div class="form-group"><label>' + result.mastertopic[i]['MASTERTOPIC_NAME'] + '</label><input type="text" ng-readonly="doc_transfer_id" ng-model="data_topic_text_' + _this.data_topic_size + '" required></div>');
                        _this.list_master_topic_id.push(result.mastertopic[i]['MASTERTOPIC_ID']);
                        _this.data_topic_size++;
                    }
                    _this.$compile($('#data_topic_text_div'))(_this.$scope);
                }).catch(function (err) {
                    console.log('error=>', err);
                }).finally(function () {
                });
            },
        };
        //refresh_table
        this.refresh_table = function () {
            var _this = this;
            this._masterDataService.getDataIntab(this.selectMenuId, this.mastersession_id).then(function (result) {
                var tableRowNo;
                var data_table = [];
                var count_index = 1;
                for (var i = 0; i < result['list_table'].length; i++) {
                    if (result['list_table'][i].MASTER_TOPIC_ID == result['mastertopic'][0].ID) {
                        var name_list = [];
                        var id_list = [];
                        name_list.push(result['list_table'][i]['DATA_TOPIC_TEXT']);
                        id_list.push(result['list_table'][i]['DATA_TOPIC_ID']);
                        var string_isuse = 'active';
                        var string_isuse_text = '<i class="k-icon k-i-k-icon k-i-check-circle k-i-checkmark-circle"></i>';
                        if (result['list_table'][i]['ISUSE'] != "T") {
                            string_isuse = '';
                            string_isuse_text = '<i class="k-icon k-i-k-icon k-i-close-circle k-i-x-circle"></i>';
                        }
                        if (result['list_table'][i]['DATA_TOPIC_PARENT'] != null) {
                            var limit_loop = 0;
                            var check_parent = result['list_table'][i]['DATA_TOPIC_PARENT'];
                            do {
                                for (var j = 0; j < result['list_table'].length; j++) {
                                    if (result['list_table'][j]['DATA_TOPIC_ID'] == check_parent) {
                                        name_list.splice(1, 0, result['list_table'][j]['DATA_TOPIC_TEXT']);
                                        id_list.splice(1, 0, result['list_table'][j]['DATA_TOPIC_ID']);
                                        check_parent = result['list_table'][j]['DATA_TOPIC_PARENT'];
                                    }
                                }
                                limit_loop++;
                            } while (check_parent != null && limit_loop < 10);
                        }
                        data_table.push({
                            index: count_index,
                            Name: name_list,
                            id: id_list,
                            isuse: string_isuse,
                            isuse_text: string_isuse_text
                        });
                        count_index++;
                    }
                }
                _this.mastersession_id_index = 0;
                var text_option = '';
                for (var i = 0; i < result['mastersession'].length; i++) {
                    if (result['mastersession'][i]['MASTERSESSION_ID'] == _this.mastersession_id) {
                        _this.mastersession_id_index = i;
                        break;
                    }
                    else
                        _this.$scope['combo_select_data_' + i] = [{ id: 0, name: 'ทั้งหมด' }];
                    _this.$scope['modal_combo_select_data_' + i] = [{ id: 0, name: 'ทั้งหมด' }];
                    _this.$scope['combo_select_' + i] = _this.$scope['combo_select_data_' + i][0];
                    _this.$scope['modal_combo_select_' + i] = _this.$scope['modal_combo_select_data_' + i][0];
                    if (i == 0) {
                        for (var j = 0; j < result['list_table'].length; j++) {
                            if (result['list_table'][j]['MASTER_TOPIC_ID'] == result['mastersession'][i]['MASTERTOPIP_ID']) {
                                _this.$scope['combo_select_data_' + i].push({
                                    name: result['list_table'][j]['DATA_TOPIC_TEXT'],
                                    id: result['list_table'][j]['DATA_TOPIC_ID'],
                                });
                                _this.$scope['modal_combo_select_data_' + i].push({
                                    name: result['list_table'][j]['DATA_TOPIC_TEXT'],
                                    id: result['list_table'][j]['DATA_TOPIC_ID'],
                                });
                            }
                        }
                    }
                    text_option += '<div class="col-xs-12 col-sm-12 col-md-6"><div class="input-group comboBoxStyle"><label>' + result['mastersession'][i]['MASTERSESSION_NAME'] + '</label> \
                                <select kendo-combo-box k-ng-model="combo_select_' + i + '" \
                                k-placeholder="\'เลือก' + result['mastersession'][i]['MASTERSESSION_NAME'] + '\'"\
                                k-data-source="combo_select_data_' + i + '" \
                                k-on-change="ctrl.change_search_select(' + i + ')" \
                                k-data-text-field="\'name\'" \
                                k-data-value-field="\'id\'" \
                                k-filter="\'contains\'" \
                                k-auto-bind="false" \
                                k-min-length="1" \
                                ></select></div></div>';
                }
                var col_size = 6;
                if (_this.mastersession_id_index == 0 || _this.mastersession_id_index % 2 == 0)
                    col_size = 12;
                text_option += '<div class="col-xs-12 col-sm-12 col-md-' + col_size + '"><div class="input-group"><label>ค้นหา</label>\
                        <input type="text" ng-model="ctrl.searchText" placeholder="ค้นหา">\
                                    <span class="input-group-btn">\
                                     <kendo-button ng-click="ctrl.search_table()" class="btn btnYellow"><i class="fas fa-search"></i> ค้นหา</kendo-button>\
                                  </span>\
                                   <span class="input-group-btn">\
                                    <kendo-button ng-click="ctrl.re_search_table()" class="btn btnGray"><i class="fas fa-redo-alt"></i> ค่าเริ่มต้น</kendo-button>\
                                  </span>\
                                </div></div>';
                $('#tab_master_detail_' + _this.mastersession_id).html('<div class="tabContentDetail">\
                            <div id="searchGroup" class="searchGroup" ng-hide="ctrl.dragdropstatus">\
                                ' + text_option + '\
                            </div>\
                            <div id="EditGroup" class="EditGroup">\
                                <kendo-button ng-click="ctrl.add_table()" ng-hide="ctrl.dragdropstatus" class="roundBtn btnYellow"><i class="k-icon k-i-k-icon k-i-plus-sm "></i></kendo-button>\
                                <kendo-button ng-click="ctrl.sort_table()" ng-hide="ctrl.dragdropstatus" class="roundBtn btnYellow"><i class="k-icon k-i-k-icon k-i-arrows-kpi k-i-kpi"></i></kendo-button>\
                            </div>\
                            <kendo-sortable options="ctrl.sortableOptions" class="tableSizeMasterDetail">\
                            <kendo-grid k-scope-field="areaGrid" options="ctrl.exampleGridOptions">\
                            </kendo-grid="areaGrid">\
                             </kendo-sortable>\
                            </div>\
                            <div class="footerBtnCenter">\
                                 <kendo-button ng-click="ctrl.submit_sort_table()" ng-show="ctrl.dragdropstatus" class="btn btnYellow"><i class="far fa-save"></i> บันทึก</kendo-button>\
                                <kendo-button ng-click="ctrl.cancel_sort_table()" ng-show="ctrl.dragdropstatus" class="btn btnGray"><i class="fas fa-times"></i> ยกเลิก</kendo-button>\
                            </div>');
                _this.textheader = result['mastersession'][_this.mastersession_id_index]['MASTERSESSION_NAME'];
                $('#header_text').text(_this.textheader);
                _this.testData = new kendo.data.DataSource({
                    data: data_table,
                    pageSize: 10,
                    serverPaging: false,
                    serverSorting: false
                });
                var column_grid = [];
                column_grid.push({
                    template: "<kendo-button class='roundBtn btnYellow' icon=\"'k-icon k-i-edit'\" ng-click=\"ctrl.editItem('#:id[0]#');\"></kendo-button>",
                    field: "id[0]",
                    title: "จัดการ",
                    width: "70px"
                }, {
                    template: "{{ctrl.setIndex(dataItem)}}",
                    title: "ลำดับ",
                    width: "70px"
                });
                if (_this.mastersession_id_index > 0) {
                    for (var i = 1; i <= _this.mastersession_id_index; i++) {
                        column_grid.push({
                            field: "Name[" + i + "]",
                            title: result['mastersession'][i - 1]['MASTERSESSION_NAME'],
                            width: "130px"
                        });
                    }
                }
                column_grid.push({
                    field: "Name[0]",
                    title: result['mastersession'][_this.mastersession_id_index]['MASTERSESSION_NAME'],
                }, {
                    template: "<div class=\"#:isuse#\">#=isuse_text#</div>",
                    field: "isuse",
                    title: "การใช้งาน",
                    width: "100px"
                });
                _this.exampleGridOptions = {
                    dataSource: _this.testData,
                    sortable: true,
                    pageable: true,
                    resizable: true,
                    dataBinding: function () {
                        tableRowNo = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    },
                    noRecords: {
                        template: "No data"
                    },
                    columns: column_grid
                };
                _this.$compile($('#tab_master_detail_' + _this.mastersession_id))(_this.$scope);
            }).catch(function (err) {
                console.log('error=>', err);
            }).finally(function () {
            });
        };
        //search 
        this.search_table = function () {
            var filter_array = [];
            filter_array.push({ field: "Name[0]", operator: "contains", value: this.searchText });
            if (this.mastersession_id_index > 0) {
                for (var i = 0; i < this.mastersession_id_index; i++) {
                    if (this.$scope['combo_select_' + i] && this.$scope['combo_select_' + i]['id'] != 0) {
                        filter_array.push({ field: "id[" + (i + 1) + "]", operator: "eq", value: this.$scope['combo_select_' + i]['id'] });
                    }
                }
            }
            this.testData.filter(filter_array);
        };
        //clear search
        this.re_search_table = function () {
            this.searchText = '';
            if (this.mastersession_id_index != 0)
                this.$scope['combo_select_0'] = this.$scope['combo_select_data_0'][0];
            for (var i = 1; i <= this.mastersession_id_index; i++) {
                this.$scope['combo_select_data_' + i] = [{ id: 0, name: 'ทั้งหมด' }];
                this.$scope['combo_select_' + i] = this.$scope['combo_select_data_' + i][0];
            }
            this.search_table();
        };
        //change dropdown seach 
        this.change_search_select = function (index) {
            if (index + 2 <= this.mastersession_id_index) {
                this.$scope['combo_select_data_' + (index + 1)] = [{ id: 0, name: 'ทั้งหมด' }];
                for (var j = 0; j < this.data_table.length; j++) {
                    if (this.data_table[j]['data_topic_parent'] == this.$scope['combo_select_' + index]['id']) {
                        this.$scope['combo_select_data_' + (index + 1)].push({
                            name: this.data_table[j]['data_topic_text'],
                            id: this.data_table[j]['data_topic_id'],
                        });
                    }
                }
                this.$scope['combo_select_' + (index + 1)] = this.$scope['combo_select_data_' + (index + 1)][0];
                for (var i = index + 2; i <= this.mastersession_id_index; i++) {
                    this.$scope['combo_select_data_' + i] = [{ id: 0, name: 'ทั้งหมด' }];
                    this.$scope['combo_select_' + i] = this.$scope['combo_select_data_' + i][0];
                }
            }
        };
        //change dropdown form input 
        this.modal_change_search_select = function (index) {
            if (index + 2 <= this.mastersession_id_index) {
                this.$scope['modal_combo_select_data_' + (index + 1)] = [];
                for (var j = 0; j < this.data_table.length; j++) {
                    if (this.data_table[j]['data_topic_parent'] == this.$scope['modal_combo_select_' + index]['id']) {
                        this.$scope['modal_combo_select_data_' + (index + 1)].push({
                            name: this.$scope.data_table[j]['data_topic_text'],
                            id: this.$scope.data_table[j]['data_topic_id'],
                        });
                    }
                }
                this.$scope['modal_combo_select_' + (index + 1)] = null;
                //$scope['modal_combo_select_' + (index + 1)] = $scope['modal_combo_select_data_' + (index + 1)][0];
                for (var i = index + 2; i <= this.mastersession_id_index; i++) {
                    this.$scope['modal_combo_select_data_' + i] = [];
                    this.$scope['modal_combo_select_' + i] = null;
                    //$scope['modal_combo_select_' + i] = $scope['modal_combo_select_data_' + i][0];
                }
            }
        };
        //add mode
        this.add_table = function () {
            $('#masterpopupdiv *').removeClass('k-invalid');
            this.$scope.data_topic_doc_id = "";
            this.data_topic_check = true;
            this.$scope.doc_transfer_id = false;
            this.$scope['modal_combo_select_0'] = null;
            for (var i = 1; i <= this.mastersession_id_index; i++) {
                this.$scope['modal_combo_select_' + i] = null;
                this.$scope['modal_combo_select_data_' + i] = [];
            }
            for (i = 0; i < this.data_topic_size; i++) {
                this.$scope['data_topic_text_' + i] = '';
            }
            this.win_save.title('เพิ่มข้อมูล');
            this.win_save.center().open();
        };
        //edit mode
        this.editItem = function (id) {
            var _this = this;
            if (this.dragdropstatus)
                return;
            this._dialogService.showBusyBottomSheet();
            this._masterDataService.getDatatopic(id, this.selectMenuId).then(function (result) {
                console.log("getDatatopic", result);
                _this.$scope.data_topic_doc_id = result.document[0]['DOC_ID'];
                if (result.document[0]['TRANSFER_ID'] == '' || result.document[0]['TRANSFER_ID'] == null)
                    _this.$scope.doc_transfer_id = false;
                else
                    _this.$scope.doc_transfer_id = true;
                for (var i = 0; i < _this.data_topic_size; i++) {
                    _this.$scope['data_topic_text_' + i] = '';
                    for (var j = 0; j < result.mastertopic.length; j++) {
                        if (_this.list_master_topic_id[i] == result.mastertopic[j]['MASTER_TOPIC_ID']) {
                            _this.$scope['data_topic_text_' + i] = result.mastertopic[j]['DATA_TOPIC'];
                        }
                    }
                }
                _this.data_topic_check = result.document[0]['ISUSE'] == "T" ? true : false;
                if (result.document[0]['DOCUMENT_PARENT_ID'] != null) {
                    var parent_now = result.document[0]['DOCUMENT_PARENT_ID'];
                    var limit_loop = 0;
                    var array_combo = [];
                    do {
                        for (var i = 0; i < _this.data_table.length; i++) {
                            if (_this.data_table[i]['DATA_TOPIC_ID'] == parent_now) {
                                array_combo.unshift(_this.data_table[i]['DATA_TOPIC_ID']);
                                parent_now = _this.data_table[i]['DATA_TOPIC_PARENT'];
                            }
                        }
                        limit_loop++;
                    } while (parent_now != null && limit_loop < 10);
                    for (var i = 0; i < array_combo.length; i++) {
                        for (var j = 0; j < _this.$scope['modal_combo_select_data_' + i].length; j++) {
                            if (_this.$scope['modal_combo_select_data_' + i][j]['id'] == array_combo[i]) {
                                _this.$scope['modal_combo_select_' + i] = _this.$scope['modal_combo_select_data_' + i][j];
                            }
                        }
                        if (i + 1 < _this.$scope.mastersession_id_index) {
                            _this.$scope['combo_select_data_' + (i + 1)] = [{ id: 0, name: 'ทั้งหมด' }];
                            for (var j = 0; j < _this.data_table.length; j++) {
                                if (_this.data_table[j]['DATA_TOPIC_PARENT'] == _this.$scope['modal_combo_select_' + i]['id']) {
                                    _this.$scope['modal_combo_select_data_' + (i + 1)].push({
                                        name: _this.data_table[j]['DATA_TOPIC_TEXT'],
                                        id: _this.data_table[j]['DATA_TOPIC_ID'],
                                    });
                                }
                            }
                        }
                    }
                }
                _this.win_save.title('แก้ไขข้อมูล');
                _this.win_save.center().open();
            }).catch(function (err) {
                console.log('error=>', err);
            }).finally(function () {
                _this._dialogService.hideBusyBottomSheet();
            });
        };
        //close add/edit mode
        this.close_document = function () {
            this.win_save.close();
        };
        //sort
        this.sortableOptions = {
            filter: ".k-grid table.dragable tr[data-uid]",
            hint: function (element) {
                return element.clone().addClass("hint");
            },
            cursor: "move",
            ignore: "TD, input",
            placeholder: function (element) {
                var tmp = element.clone();
                tmp.remove("tr:first");
                return tmp
                    .removeAttr("uid")
                    .addClass("k-state-hover")
                    .css("opacity", 0.65);
                //return $('<tr colspan="4" class="placeholder"></tr>');
                //return element.clone().addClass("placeholder");
            },
            container: ".k-grid tbody",
            change: function (e) {
                var grid = _this.$scope.areaGrid, dataItem = grid.dataSource.getByUid(e.item.data("uid"));
                grid.dataSource.remove(dataItem);
                grid.dataSource.insert(e.newIndex, dataItem);
            },
        };
        this.sort_table = function () {
            this.re_search_table();
            this.dragdropstatus = !this.dragdropstatus;
            $('#header_text').text('เรียงลำดับ' + this.textheader);
            if (this.dragdropstatus) {
                this.$scope.areaGrid.table.addClass("dragable");
                this.$scope.areaGrid.dataSource.pageSize(100000);
            }
            else {
                this.$scope.areaGrid.table.removeClass("dragable");
                this.$scope.areaGrid.dataSource.pageSize(10);
            }
        };
        this.cancel_sort_table = function () {
            this.refresh_table();
            this.dragdropstatus = !this.dragdropstatus;
            if (this.dragdropstatus) {
                this.$scope.areaGrid.table.addClass("dragable");
                this.$scope.areaGrid.dataSource.pageSize(100000);
            }
            else {
                this.$scope.areaGrid.table.removeClass("dragable");
                this.$scope.areaGrid.dataSource.pageSize(10);
            }
        };
        //save sort
        this.submit_sort_table = function () {
            var _this = this;
            var list_doc_id_data = [];
            var sort = 1;
            for (var i = 0; i < this.testData._view.length; i++) {
                list_doc_id_data.push({ docId: this.testData._view[i]['id'][0], sort: sort });
                sort++;
            }
            console.log('after sort =>', list_doc_id_data);
            this._dialogService.showBusyBottomSheet();
            this._masterDataService.Sortdata(list_doc_id_data).then(function (result2) {
                console.log(result2);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, "", "");
                _this.dragdropstatus = !_this.dragdropstatus;
                _this.refresh_table();
                if (_this.dragdropstatus) {
                    _this.$scope.areaGrid.table.addClass("dragable");
                    _this.$scope.areaGrid.dataSource.pageSize(100000);
                }
                else {
                    _this.$scope.areaGrid.table.removeClass("dragable");
                    _this.$scope.areaGrid.dataSource.pageSize(10);
                }
            }).catch(function (err) {
                console.log('error=>', err);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
            }).finally(function () {
                _this._dialogService.hideBusyBottomSheet();
            });
            ;
        };
        //save doc
        this.save_document = function () {
            var _this = this;
            if (!this.$scope.masterpopup.validate()) {
                this.win_save.close();
                this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.pleaseEnterAllInformation, this.callBackshowDialog, this.callBackshowDialog);
                return;
            }
            var parent;
            if (this.mastersession_id_index > 0) {
                if (!this.$scope['modal_combo_select_' + (this.mastersession_id_index - 1)] || this.$scope['modal_combo_select_' + (this.mastersession_id_index - 1)]['id'] == 0) {
                    this.win_save.close();
                    this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.pleaseEnterAllInformation, this.callBackshowDialog(), this.callBackshowDialog());
                    return;
                }
                parent = this.$scope['modal_combo_select_' + (this.mastersession_id_index - 1)]['id'];
            }
            else
                parent = null;
            var list_master_topic_id_temp = [];
            var list_master_topic_text_temp = [];
            var topiclistData = [];
            for (var i = 0; i < this.data_topic_size; i++) {
                if (this.$scope['data_topic_text_' + i] == '') {
                    this.win_save.close();
                    this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.pleaseEnterAllInformation, this.callBackshowDialog(), this.callBackshowDialog());
                    return;
                }
                topiclistData.push({
                    topicId: this.list_master_topic_id[i],
                    asnwerData: this.$scope['data_topic_text_' + i]
                });
            }
            var data_save = {
                doc_id: this.$scope.data_topic_doc_id,
                user_id: " ",
                menu_id: this.selectMenuId,
                doc_isuse: this.data_topic_check,
                master_session_id: this.mastersession_id,
                doc_id_parent: parent,
                topic_list_Data: topiclistData,
            };
            this._dialogService.showBusyBottomSheet();
            this._masterDataService.Mastersave(data_save).then(function (result2) {
                console.log('result2', result2);
                _this.refresh_table();
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, "", "");
            }).catch(function (err) {
                console.log('error=>', err);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
            }).finally(function () {
                _this.win_save.close();
                _this._dialogService.hideBusyBottomSheet();
            });
        };
        this.callBackshowDialog = function () {
            _this.win_save.center().open();
        };
    }
    masterDataManagementController.prototype.$onInit = function () {
        this.selectMenuId = this._routeParams.id;
        this.getSectionOfMenu(this.selectMenuId);
    };
    masterDataManagementController.prototype.getSectionOfMenu = function (id) {
        var _this = this;
        this._dialogService.showBusyBottomSheet();
        this._masterDataService.getSectionOfMenu(id).then(function (result) {
            console.log('res=>', result);
            for (var i = 0; i < result.Table.length; i++) {
                _this.tabManager.append({
                    text: '<div id="tab_master_' + result.Table[i]['MASTERSESSION_ID'] + '">' + result.Table[i]['MASTERSESSION_NAME'] + '</div>',
                    encoded: false,
                    content: '<div id="tab_master_detail_' + result.Table[i]['MASTERSESSION_ID'] + '">' + result.Table[i]['MASTERSESSION_NAME'] + '</div>'
                });
            }
            _this.$compile(_this.tabManager.tabGroup.children("li"))(_this.$scope);
            _this.tabManager.select(0);
        }).catch(function (err) {
            console.log('error=>', err);
        }).finally(function () {
            _this._dialogService.hideBusyBottomSheet();
        });
    };
    //show order 
    masterDataManagementController.prototype.setIndex = function (item) {
        var index = this.exampleGridOptions.dataSource.indexOf(item) + 1;
        return index;
    };
    return masterDataManagementController;
}());
//# sourceMappingURL=master-data-management.component.js.map