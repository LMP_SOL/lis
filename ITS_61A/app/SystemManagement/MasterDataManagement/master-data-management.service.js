var MasterDataService = (function () {
    function MasterDataService($http, _webConfigService, $q) {
        this.$http = $http;
        this._webConfigService = _webConfigService;
        this.$q = $q;
    }
    MasterDataService.prototype.getSectionOfMenu = function (menuId) {
        return this.$http.get(this._webConfigService.getBaseUrl() + ("api/MasterData/getSectionOfMasterMenu?menuId=" + menuId))
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    MasterDataService.prototype.getDataIntab = function (menuId, sessionId) {
        return this.$http.get(this._webConfigService.getBaseUrl() + ("api/MasterData/getDataIntab?menuId=" + menuId + "&sessionId=" + sessionId))
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    MasterDataService.prototype.getDatatopic = function (docID, menuId) {
        return this.$http.get(this._webConfigService.getBaseUrl() + ("api/MasterData/Datatopic?docId=" + docID + "&menuId=" + menuId))
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    MasterDataService.prototype.getMastertopic = function (sessionId) {
        return this.$http.get(this._webConfigService.getBaseUrl() + ("api/MasterData/Mastertopic?sessionId=" + sessionId))
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    MasterDataService.prototype.Sortdata = function (data) {
        return this.$http.post(this._webConfigService.getBaseUrl() + "api/MasterData/Sortdata", data)
            .then(function (res) {
            return res;
        });
    };
    MasterDataService.prototype.Mastersave = function (data) {
        return this.$http.post(this._webConfigService.getBaseUrl() + "api/MasterData/Mastersave", data)
            .then(function (res) {
            return res;
        });
    };
    MasterDataService.prototype.getDataSubdistrictOffset = function (limit, offset, keyword, district) {
        return this.$http.get(this._webConfigService.getBaseUrl() + ("api/MasterData/getDataSubdistrictOffset?limit=" + limit + "&offset=" + offset + "&keyword=" + keyword + "&district=" + district))
            .then(function (res) {
            return res;
        });
    };
    MasterDataService.prototype.getDataDistrictAndProvince = function () {
        var _this = this;
        return this.$http.get(this._webConfigService.getBaseUrl() + "api/MasterData/getDataDistrictAndProvince")
            .then(function (res) {
            _this.DistrictAndProvinceData = res.data;
            return res;
        });
    };
    MasterDataService.prototype.getDepartment = function () {
        return this.$http.get(this._webConfigService.getBaseUrl() + "api/MasterData/GetDataDepartment")
            .then(function (res) {
            return res;
        });
    };
    MasterDataService.prototype.getMasterData = function (session_id, topic_id) {
        return this.$http.get(this._webConfigService.getBaseUrl() + ("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id)).then(function (res) {
            return res;
        });
    };
    return MasterDataService;
}());
//# sourceMappingURL=master-data-management.service.js.map