﻿class InsertEmployeeController implements ng.IController {


    public model: workerModel = new workerModel();
    public mode: any
    public identityTypeList: any
    public modelList: any
    public modelListEdit: any
    public modelSourceEmp: any
    public privilegeList: any
    public select: any
    public prefixtList: any
    public positionlist: any
    public departmentlist: any
    public grouplist: any
    public toTS: string;
    public setTime: any; 
    public managementPositionList: any;
    private object;
    public textSearchDepartment;

    public ID_employee: string; // change to get from user management

    public session_id_worker: string = "0EB8E806125242A4B46709245A3F490D";
    public topic_id_worker: string = "CC23F9657CB14AE880ED7D44D3856441";

    public session_id_prefix: string = "44CB0A25CB8A435CB48477DBFEF153F1";
    public topic_id_prefix: string = "A0F1527332E14B20AD00308E0A749F81";

    public session_id_position: string = "55177F5F94294CD591D415677AE435FC";
    public topic_id_position: string = "A501CAD099794342ADE98B2A8F6ED38C";

    public session_id_department: string = "6A469DE811EC44A5A6826D8D7AFC4EBB";
    public topic_id_department: string = "7C8FE4A791124CF595EFDE0F4F08F2D5";

    public session_id_managementPositionId: string = "55177F5F94294CD591D415677AE435FC";
    public topic_id_managementPositionId: string = "B1C80A355D7048CFA159174583A700FC";
   
    constructor(private _masterDataService: MasterDataService,
        private _systemManagementService: SystemManagementService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
        private $element: ng.IRootElementService,
        private _dialogService: DialogService,

    ) {

    }
    public editMode: boolean;
    public viewMode: boolean;
    public viewModeSession: boolean;
    //public datefrom: any
    //public dateto: any
    public endDateFrom: any;
    public endDateTo: any;

    //this.startDateFrom = this.model.WORK_START_DATE;
    //this.startDateTo = this.model.WORK_END_DATE;

    //public startDateFrom = this.model.WORK_START_DATE;
    //public startDateTo = this.model.WORK_END_DATE;

    public startDateFrom: any
    public startDateTo: any

    $onInit() {

        console.log(this.object);
        console.log('Mode', this.model);
        this.model.IDENTITY_USER_TYPE_ID = "576CBA8240874715AD3AEFF3A3DFB59D";

        this.ID_employee = this.object[0].userid;


        this.viewMode = false
        this.viewModeSession = true
        this.editMode = this.object[0].Mode // false = Insert || true = Update
        console.log(this.ID_employee);
        // Edit mode load use empoyee by ID
        if (this.editMode)
            this.getdataInsertWorker(this.ID_employee);
        this.getdataFromMasterInsertWorker(this.session_id_worker, this.topic_id_worker);
        this.getdataFromMasterInsertWorkerPrefix(this.session_id_prefix, this.topic_id_prefix);
        this.getdataFromMasterInsertWorkerPosition(this.session_id_position, this.topic_id_position);
        this.getdataFromMasterInsertWorkerDepartment();
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_managementPositionId, this.topic_id_managementPositionId);
        this.getdataIdentityGroup();
    }


    public tabManagerOptions = {
        select: (e) => {
            console.log("open tab");

        }
    }

    /// call DB
    public getdataFromMasterInsertWorker(session_id, topic_id) {
        this._masterDataService.getMasterData(session_id, topic_id).then((res: any) => {
            console.log('res2-->', res)
            this.identityTypeList = res.data.Table;
        })
    }

    public getdataFromMasterInsertWorkerPrefix(session_id, topic_id) {
        this._masterDataService.getMasterData(session_id, topic_id).then((res: any) => {
            console.log('res3-->', res)
            this.prefixtList = res.data.Table;
        })
    }

    public getdataFromMasterInsertWorkerPosition(session_id, topic_id) {
        this._masterDataService.getMasterData(session_id, topic_id).then((res: any) => {
            console.log('res4-->', res)
            this.positionlist = res.data.Table;
        })
    }

    public getdataFromMasterInsertWorkerDepartment() {
        this._masterDataService.getDepartment().then((res: any) => {
            console.log('res5-->', res)
            this.departmentlist = res.data.DATA_SELECT;
        }).finally(() => {

            this.$element.find('input').on('keydown', (ev) =>{
                ev.stopPropagation();
            });
        })
    }

    public getdataIdentityGroup() {
        this._systemManagementService.getdataIdentityGroup().then((res: any) => {
            console.log('res6-->', res)
            this.grouplist = res.Table;
        })
    }

    public getdataFromMasterInsertWorkerManagementPosition(session_id, topic_id) {
        this.$http.get(`api/SystemManagement/GetdataFromMasterInsertWorker?session_id=${session_id}&topic_id=${topic_id}`).then((res: any) => {
            console.log('res7-->', res)
            this.managementPositionList = res.data.Table;
        })
    }

    public getdataInsertWorker(id) {

        this._systemManagementService.getdataEmployeeOrWorker(this.ID_employee).then((res: any) => {
            console.log('res Emp -->', res)
            this.modelSourceEmp = res.data.Table[0];
            this.privilegeList = res.data.Table1;
            this.model = this.modelSourceEmp;
      
            this.startDateFrom = this.model.WORK_START_DATE;
            this.startDateTo = this.model.WORK_END_DATE;
        })

    }

    public setData(emp) {

        this.model.EMPLOYEE_CODE = emp.UnitCodeID;

        let prefix = this.prefixtList.find((x) => { return x.DATA_TOPIC == emp.prefixname });     
        if (prefix) 
            this.model.PREFIX_ID = prefix.ID;

        this.model.NAME_TH = emp.firstenname;

        this.model.LASTNAME_TH = emp.lastenname;

        let positionFullName = this.positionlist.find((x) => { return x.DATA_TOPIC == emp.PositionWorkline });
        if (positionFullName)
            this.model.POSITION_ID = positionFullName.ID;

        let positionName = this.managementPositionList.find((x) => { return x.DATA_TOPIC == emp.PositionWorklineShortName });
        if (positionName)
            this.model.MANAGEMENT_POSITION_ID = positionName.ID;

        this.model.LEVEL_USER = emp.Level;

        let department = this.departmentlist.find((x) => { return x.HR_UNITCODECODE == emp.UnitCodeID });
        if (department)
            this.model.DEPARTMENT_ID = department.ORG_CODE;
        this.model.EMAIL = emp.email;

        
    }
    public officeClearSearchTerm() {
        this.textSearchDepartment = "";
    }


    public openDialogSearchPerson() {

        let template = '<insert-employee-popup model="dlgCtrl.data" save-select-employee="dlgCtrl.onClickOK(DataSave)" cancel-select-employee="dlgCtrl.onClickCancel()" ></insert-employee-popup>';
        this._dialogService.selectEmployeeDataContentDialog('การเลือกข้อมูล', Constants.messageDialog.save, Constants.messageDialog.cancel, template, this.model, this.callbackOK, this.callbackCancel);
    }


    public callbackOK = (res) => {
        //console.log('this.topicVm master T2 =>', this.topicVm); 
        console.log('ok', res);
        this.setData(res);
        return res
    }

    public callbackCancel = (res) => {
        console.log('No', res);
        return res
    }

// for Insert employee
    public insertWorkerSave(data_save: any): ng.IPromise<any> {
        debugger
        return this.$http.post(`api/SystemManagement/InsertWorkerSave`, data_save).then((res: any) => {
            return res;
        });
    }

    public insertEmployee() {
        this.model.WORK_START_DATE = this.getFormatDateEng(this.startDateFrom);
        this.model.WORK_END_DATE = this.getFormatDateEng(this.startDateTo);
        

        var data_save = {

            IDENTITY_USER_TYPE_ID: this.model.IDENTITY_USER_TYPE_ID,        //have
            EMPLOYEE_CODE: this.model.EMPLOYEE_CODE,                        //have
            ID_CARD: this.model.ID_CARD,                                    //null number
            PREFIX_ID: this.model.PREFIX_ID,                                //have
            NAME_TH: this.model.NAME_TH,                                    //have
            LASTNAME_TH: this.model.LASTNAME_TH,                            //have
            PASSWORD_USER: this.model.PASSWORD_USER,                        //have
            PASSWORD_USER_DATE: "2018-12-31",                //this.setTime,//this.model.PASSWORD_USER_DATE,      // timestamp
            IS_GENERATE_PASSWORD: this.model.IS_GENERATE_PASSWORD,          //null
            USER_CODE: "user_code",//this.model.USER_CODE, //รอ
            POSITION_ID: this.model.POSITION_ID,                            //null //have
            DEPARTMENT_ID: "01020001",//this.model.DEPARTMENT_ID,
            MANAGEMENT_POSITION_ID: this.model.MANAGEMENT_POSITION_ID,      //null
            LEVEL_USER: this.model.LEVEL_USER,//this.model.LEVEL_USER,
            EMAIL: "mail@email.com",//this.model.EMAIL,
            WORK_START_DATE: this.model.WORK_START_DATE,//this.model.WORK_START_DATE,            // date
            WORK_END_DATE: this.model.WORK_END_DATE,//this.model.WORK_END_DATE,                // date
            COUNT_NUM_LOCK: this.model.COUNT_NUM_LOCK,                      //null number
            NUM_LOCK_DATE: this.model.NUM_LOCK_DATE,                        //null
            OTP_NUMBER: this.model.OTP_NUMBER,                              //null
            OTP_DATE: this.model.OTP_DATE,                                  //null
            CREATE_BY: "AA18BA7B99FF403FAFD8529B35F94370"//this.model.CREATE_BY, 
            //CREATE_DATE: this.setTime//this.model.CREATE_DATE                     // timestamp	

        }


        //.sendDataService = data_save

        console.log(data_save);

        this.insertWorkerSave(data_save);

    }

    public updateInsertEmployee() {
        console.log("this.model", this.model)
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/updateWorkerDataModel', this.model).then((res: any) => {
            if (res.data) {
                console.log('res-->', res.data)
                this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                    Constants.messageDialog.enter,
                    Constants.messageDialog.completeSave,
                    this.getdataIdentityGroup(),
                    "");
            }
        }).catch((err) => {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                Constants.messageDialog.enter,
                Constants.messageDialog.uncompleteSave,
                "", "");
        }).finally(() => {
            this._dialogService.hideBusyBottomSheet();

        })

    }

    //for cancel 
    public cancelInsertEmployee()
    {
        this.mode = 0;
    }

    //for update
    public updateEmployee()
    {

    }

    private getFormatDateEng(date) {
        let shortMonths = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        //let day = _date.getDate();
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear();
        let Year = year.toString();
        let subYear = Year.substring(2)
        let result = `${subYear}-${month}-${day}`;

        return result;
    }
}

