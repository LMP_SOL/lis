﻿class InsertEmployeePopupController implements ng.IController {

    public model: any;
    public modelHRIS: any;
    public HRISList: HRISPersonDataModel[];
    private saveSelectEmployee: any;
    private cancelSelectEmployee: any;

    constructor(private _dialogService: DialogService,
        private _systemManagementService: SystemManagementService,
        private _masterDataService: MasterDataService,
        private $element: ng.IRootElementService,
        private $location: ng.ILocationService,

    ) {

    }
    public searchModel: any;
    public viewMode: boolean;
    public viewModeSession: boolean;
    public sourceData: any;
    public mainGridOptions: any
    public kendoGrid: any;

    public startDateFrom: any;
    public startDateTo: any;
    public endDateFrom: any;
    public endDateTo: any;
    public departmentlist: any
    public textSearchDepartment: any;

    $onInit() {
        this.viewMode = false
        this.viewModeSession = true
        this.getPersonFromHRIS();
        this.getdataFromMasterInsertWorkerDepartment();
        //---------------------------------------------------------------------

    }
    public getdataFromMasterInsertWorkerDepartment() {
        this._masterDataService.getDepartment().then((res: any) => {
            console.log('res5-->', res)
            this.departmentlist = res.data.DATA_SELECT;
        }).finally(() => {

            this.$element.find('input').on('keydown', (ev) => {
                ev.stopPropagation();
            });
        })
    }
    public officeClearSearchTerm() {
        this.textSearchDepartment = "";
    }


    public save() {

        if (this.modelHRIS != undefined) {
            let data = { DataSave: JSON.parse(this.modelHRIS) };
            this.saveSelectEmployee(data);
        } else {
           
                this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, "กรุณาเลือกพนักงาน", "", "");
        }
        
    }

    public cancel() {
        this.cancelSelectEmployee();
    }
     
    public getPersonFromHRIS() {
        this._dialogService.showBusyBottomSheet();
        this._systemManagementService.getdataPersonalHRIS().then((res) => {
            console.log('HRIS', res);
            this.HRISList = res.Table;
           
            this.selectEmplyee(this.HRISList);
        }).catch((err) => {

        }).finally(() => {
            this._dialogService.hideBusyBottomSheet();
        });
    }

    public selectEmplyee(item) {

        this.sourceData = new kendo.data.DataSource({
            data: item,
            pageSize: 10,

            serverPaging: false,
            serverSorting: false
        });

        this.mainGridOptions = {
            dataSource: this.sourceData,
            sortable: true,
            pageable: true,

            columns: [{

                title: "",
                width: "50px",
                template: "<input type='radio' name='gender' ng-model=\"ctrl.modelHRIS\" value=\"{{dataItem}}\"> <br>"
            }, {
                template: "{{ctrl.setIndex(dataItem)}}",
                title: "ลำดับ",
                width: "90px"
            }, {
                field: "emcode",
                title: "รหัสพนักงาน",
                width: "120px"
            }, {
                template: "{{dataItem.prefixname}}{{dataItem.firstenname}} {{dataItem.lastenname}}",
                title: "ชื่อ-สกุล",

            }, {
                field: "UnitCodeName",
                title: "หน่วยงาน",

            }]
        };
        console.log('mainGridOptions', this.mainGridOptions)

    }

    //search 
    public search_table = function () {

        var filter_array = [];
        if (this.searchModel.name != undefined) {
            filter_array.push({ field: "name_lastname", operator: "contains", value: this.searchModel.name.replace(" ", "") });
           
        }
        if (this.searchModel.employeeCode != undefined) {
            filter_array.push({ field: "emcode", operator: "contains", value: this.searchModel.employeeCode });
        }
        if (this.searchModel.delpartment != undefined) {
            filter_array.push({ field: "unitcodecode", operator: "contains", value: this.searchModel.delpartment });
        }
       

        this.sourceData.filter(filter_array);

    }
    //clear search
    public re_search_table = function () {
        this.searchModel.employeeCode = undefined;
        this.searchModel.name = undefined;
        this.searchModel.delpartment = undefined;

        this.search_table();
    }

    private setIndex(item) {
        let index = this.mainGridOptions.dataSource.indexOf(item) + 1;
        return index;
    }
}

