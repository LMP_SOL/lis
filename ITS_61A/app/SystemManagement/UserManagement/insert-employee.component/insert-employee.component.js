var InsertEmployeeController = (function () {
    function InsertEmployeeController(_masterDataService, _systemManagementService, $location, $http, $element, _dialogService) {
        var _this = this;
        this._masterDataService = _masterDataService;
        this._systemManagementService = _systemManagementService;
        this.$location = $location;
        this.$http = $http;
        this.$element = $element;
        this._dialogService = _dialogService;
        this.model = new workerModel();
        this.session_id_worker = "0EB8E806125242A4B46709245A3F490D";
        this.topic_id_worker = "CC23F9657CB14AE880ED7D44D3856441";
        this.session_id_prefix = "44CB0A25CB8A435CB48477DBFEF153F1";
        this.topic_id_prefix = "A0F1527332E14B20AD00308E0A749F81";
        this.session_id_position = "55177F5F94294CD591D415677AE435FC";
        this.topic_id_position = "A501CAD099794342ADE98B2A8F6ED38C";
        this.session_id_department = "6A469DE811EC44A5A6826D8D7AFC4EBB";
        this.topic_id_department = "7C8FE4A791124CF595EFDE0F4F08F2D5";
        this.session_id_managementPositionId = "55177F5F94294CD591D415677AE435FC";
        this.topic_id_managementPositionId = "B1C80A355D7048CFA159174583A700FC";
        this.tabManagerOptions = {
            select: function (e) {
                console.log("open tab");
            }
        };
        this.callbackOK = function (res) {
            //console.log('this.topicVm master T2 =>', this.topicVm); 
            console.log('ok', res);
            _this.setData(res);
            return res;
        };
        this.callbackCancel = function (res) {
            console.log('No', res);
            return res;
        };
    }
    InsertEmployeeController.prototype.$onInit = function () {
        console.log(this.object);
        console.log('Mode', this.model);
        this.model.IDENTITY_USER_TYPE_ID = "576CBA8240874715AD3AEFF3A3DFB59D";
        this.ID_employee = this.object[0].userid;
        this.viewMode = false;
        this.viewModeSession = true;
        this.editMode = this.object[0].Mode; // false = Insert || true = Update
        console.log(this.ID_employee);
        // Edit mode load use empoyee by ID
        if (this.editMode)
            this.getdataInsertWorker(this.ID_employee);
        this.getdataFromMasterInsertWorker(this.session_id_worker, this.topic_id_worker);
        this.getdataFromMasterInsertWorkerPrefix(this.session_id_prefix, this.topic_id_prefix);
        this.getdataFromMasterInsertWorkerPosition(this.session_id_position, this.topic_id_position);
        this.getdataFromMasterInsertWorkerDepartment();
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_managementPositionId, this.topic_id_managementPositionId);
        this.getdataIdentityGroup();
    };
    /// call DB
    InsertEmployeeController.prototype.getdataFromMasterInsertWorker = function (session_id, topic_id) {
        var _this = this;
        this._masterDataService.getMasterData(session_id, topic_id).then(function (res) {
            console.log('res2-->', res);
            _this.identityTypeList = res.data.Table;
        });
    };
    InsertEmployeeController.prototype.getdataFromMasterInsertWorkerPrefix = function (session_id, topic_id) {
        var _this = this;
        this._masterDataService.getMasterData(session_id, topic_id).then(function (res) {
            console.log('res3-->', res);
            _this.prefixtList = res.data.Table;
        });
    };
    InsertEmployeeController.prototype.getdataFromMasterInsertWorkerPosition = function (session_id, topic_id) {
        var _this = this;
        this._masterDataService.getMasterData(session_id, topic_id).then(function (res) {
            console.log('res4-->', res);
            _this.positionlist = res.data.Table;
        });
    };
    InsertEmployeeController.prototype.getdataFromMasterInsertWorkerDepartment = function () {
        var _this = this;
        this._masterDataService.getDepartment().then(function (res) {
            console.log('res5-->', res);
            _this.departmentlist = res.data.DATA_SELECT;
        }).finally(function () {
            _this.$element.find('input').on('keydown', function (ev) {
                ev.stopPropagation();
            });
        });
    };
    InsertEmployeeController.prototype.getdataIdentityGroup = function () {
        var _this = this;
        this._systemManagementService.getdataIdentityGroup().then(function (res) {
            console.log('res6-->', res);
            _this.grouplist = res.Table;
        });
    };
    InsertEmployeeController.prototype.getdataFromMasterInsertWorkerManagementPosition = function (session_id, topic_id) {
        var _this = this;
        this.$http.get("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id).then(function (res) {
            console.log('res7-->', res);
            _this.managementPositionList = res.data.Table;
        });
    };
    InsertEmployeeController.prototype.getdataInsertWorker = function (id) {
        var _this = this;
        this._systemManagementService.getdataEmployeeOrWorker(this.ID_employee).then(function (res) {
            console.log('res Emp -->', res);
            _this.modelSourceEmp = res.data.Table[0];
            _this.privilegeList = res.data.Table1;
            _this.model = _this.modelSourceEmp;
            _this.startDateFrom = _this.model.WORK_START_DATE;
            _this.startDateTo = _this.model.WORK_END_DATE;
        });
    };
    InsertEmployeeController.prototype.setData = function (emp) {
        this.model.EMPLOYEE_CODE = emp.UnitCodeID;
        var prefix = this.prefixtList.find(function (x) { return x.DATA_TOPIC == emp.prefixname; });
        if (prefix)
            this.model.PREFIX_ID = prefix.ID;
        this.model.NAME_TH = emp.firstenname;
        this.model.LASTNAME_TH = emp.lastenname;
        var positionFullName = this.positionlist.find(function (x) { return x.DATA_TOPIC == emp.PositionWorkline; });
        if (positionFullName)
            this.model.POSITION_ID = positionFullName.ID;
        var positionName = this.managementPositionList.find(function (x) { return x.DATA_TOPIC == emp.PositionWorklineShortName; });
        if (positionName)
            this.model.MANAGEMENT_POSITION_ID = positionName.ID;
        this.model.LEVEL_USER = emp.Level;
        var department = this.departmentlist.find(function (x) { return x.HR_UNITCODECODE == emp.UnitCodeID; });
        if (department)
            this.model.DEPARTMENT_ID = department.ORG_CODE;
        this.model.EMAIL = emp.email;
    };
    InsertEmployeeController.prototype.officeClearSearchTerm = function () {
        this.textSearchDepartment = "";
    };
    InsertEmployeeController.prototype.openDialogSearchPerson = function () {
        var template = '<insert-employee-popup model="dlgCtrl.data" save-select-employee="dlgCtrl.onClickOK(DataSave)" cancel-select-employee="dlgCtrl.onClickCancel()" ></insert-employee-popup>';
        this._dialogService.selectEmployeeDataContentDialog('การเลือกข้อมูล', Constants.messageDialog.save, Constants.messageDialog.cancel, template, this.model, this.callbackOK, this.callbackCancel);
    };
    // for Insert employee
    InsertEmployeeController.prototype.insertWorkerSave = function (data_save) {
        debugger;
        return this.$http.post("api/SystemManagement/InsertWorkerSave", data_save).then(function (res) {
            return res;
        });
    };
    InsertEmployeeController.prototype.insertEmployee = function () {
        this.model.WORK_START_DATE = this.getFormatDateEng(this.startDateFrom);
        this.model.WORK_END_DATE = this.getFormatDateEng(this.startDateTo);
        var data_save = {
            IDENTITY_USER_TYPE_ID: this.model.IDENTITY_USER_TYPE_ID,
            EMPLOYEE_CODE: this.model.EMPLOYEE_CODE,
            ID_CARD: this.model.ID_CARD,
            PREFIX_ID: this.model.PREFIX_ID,
            NAME_TH: this.model.NAME_TH,
            LASTNAME_TH: this.model.LASTNAME_TH,
            PASSWORD_USER: this.model.PASSWORD_USER,
            PASSWORD_USER_DATE: "2018-12-31",
            IS_GENERATE_PASSWORD: this.model.IS_GENERATE_PASSWORD,
            USER_CODE: "user_code",
            POSITION_ID: this.model.POSITION_ID,
            DEPARTMENT_ID: "01020001",
            MANAGEMENT_POSITION_ID: this.model.MANAGEMENT_POSITION_ID,
            LEVEL_USER: this.model.LEVEL_USER,
            EMAIL: "mail@email.com",
            WORK_START_DATE: this.model.WORK_START_DATE,
            WORK_END_DATE: this.model.WORK_END_DATE,
            COUNT_NUM_LOCK: this.model.COUNT_NUM_LOCK,
            NUM_LOCK_DATE: this.model.NUM_LOCK_DATE,
            OTP_NUMBER: this.model.OTP_NUMBER,
            OTP_DATE: this.model.OTP_DATE,
            CREATE_BY: "AA18BA7B99FF403FAFD8529B35F94370" //this.model.CREATE_BY, 
            //CREATE_DATE: this.setTime//this.model.CREATE_DATE                     // timestamp	
        };
        //.sendDataService = data_save
        console.log(data_save);
        this.insertWorkerSave(data_save);
    };
    InsertEmployeeController.prototype.updateInsertEmployee = function () {
        var _this = this;
        console.log("this.model", this.model);
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/updateWorkerDataModel', this.model).then(function (res) {
            if (res.data) {
                console.log('res-->', res.data);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, _this.getdataIdentityGroup(), "");
            }
        }).catch(function (err) {
            _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
        }).finally(function () {
            _this._dialogService.hideBusyBottomSheet();
        });
    };
    //for cancel 
    InsertEmployeeController.prototype.cancelInsertEmployee = function () {
        this.mode = 0;
    };
    //for update
    InsertEmployeeController.prototype.updateEmployee = function () {
    };
    InsertEmployeeController.prototype.getFormatDateEng = function (date) {
        var shortMonths = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        //let day = _date.getDate();
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear();
        var Year = year.toString();
        var subYear = Year.substring(2);
        var result = subYear + "-" + month + "-" + day;
        return result;
    };
    return InsertEmployeeController;
}());
//# sourceMappingURL=insert-employee.component.js.map