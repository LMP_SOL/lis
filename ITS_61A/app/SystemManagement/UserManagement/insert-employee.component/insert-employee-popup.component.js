var InsertEmployeePopupController = (function () {
    function InsertEmployeePopupController(_dialogService, _systemManagementService, _masterDataService, $element, $location) {
        this._dialogService = _dialogService;
        this._systemManagementService = _systemManagementService;
        this._masterDataService = _masterDataService;
        this.$element = $element;
        this.$location = $location;
        //search 
        this.search_table = function () {
            var filter_array = [];
            if (this.searchModel.name != undefined) {
                filter_array.push({ field: "name_lastname", operator: "contains", value: this.searchModel.name.replace(" ", "") });
            }
            if (this.searchModel.employeeCode != undefined) {
                filter_array.push({ field: "emcode", operator: "contains", value: this.searchModel.employeeCode });
            }
            if (this.searchModel.delpartment != undefined) {
                filter_array.push({ field: "unitcodecode", operator: "contains", value: this.searchModel.delpartment });
            }
            this.sourceData.filter(filter_array);
        };
        //clear search
        this.re_search_table = function () {
            this.searchModel.employeeCode = undefined;
            this.searchModel.name = undefined;
            this.searchModel.delpartment = undefined;
            this.search_table();
        };
    }
    InsertEmployeePopupController.prototype.$onInit = function () {
        this.viewMode = false;
        this.viewModeSession = true;
        this.getPersonFromHRIS();
        this.getdataFromMasterInsertWorkerDepartment();
        //---------------------------------------------------------------------
    };
    InsertEmployeePopupController.prototype.getdataFromMasterInsertWorkerDepartment = function () {
        var _this = this;
        this._masterDataService.getDepartment().then(function (res) {
            console.log('res5-->', res);
            _this.departmentlist = res.data.DATA_SELECT;
        }).finally(function () {
            _this.$element.find('input').on('keydown', function (ev) {
                ev.stopPropagation();
            });
        });
    };
    InsertEmployeePopupController.prototype.officeClearSearchTerm = function () {
        this.textSearchDepartment = "";
    };
    InsertEmployeePopupController.prototype.save = function () {
        if (this.modelHRIS != undefined) {
            var data = { DataSave: JSON.parse(this.modelHRIS) };
            this.saveSelectEmployee(data);
        }
        else {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, "กรุณาเลือกพนักงาน", "", "");
        }
    };
    InsertEmployeePopupController.prototype.cancel = function () {
        this.cancelSelectEmployee();
    };
    InsertEmployeePopupController.prototype.getPersonFromHRIS = function () {
        var _this = this;
        this._dialogService.showBusyBottomSheet();
        this._systemManagementService.getdataPersonalHRIS().then(function (res) {
            console.log('HRIS', res);
            _this.HRISList = res.Table;
            _this.selectEmplyee(_this.HRISList);
        }).catch(function (err) {
        }).finally(function () {
            _this._dialogService.hideBusyBottomSheet();
        });
    };
    InsertEmployeePopupController.prototype.selectEmplyee = function (item) {
        this.sourceData = new kendo.data.DataSource({
            data: item,
            pageSize: 10,
            serverPaging: false,
            serverSorting: false
        });
        this.mainGridOptions = {
            dataSource: this.sourceData,
            sortable: true,
            pageable: true,
            columns: [{
                    title: "",
                    width: "50px",
                    template: "<input type='radio' name='gender' ng-model=\"ctrl.modelHRIS\" value=\"{{dataItem}}\"> <br>"
                }, {
                    template: "{{ctrl.setIndex(dataItem)}}",
                    title: "ลำดับ",
                    width: "90px"
                }, {
                    field: "emcode",
                    title: "รหัสพนักงาน",
                    width: "120px"
                }, {
                    template: "{{dataItem.prefixname}}{{dataItem.firstenname}} {{dataItem.lastenname}}",
                    title: "ชื่อ-สกุล",
                }, {
                    field: "UnitCodeName",
                    title: "หน่วยงาน",
                }]
        };
        console.log('mainGridOptions', this.mainGridOptions);
    };
    InsertEmployeePopupController.prototype.setIndex = function (item) {
        var index = this.mainGridOptions.dataSource.indexOf(item) + 1;
        return index;
    };
    return InsertEmployeePopupController;
}());
//# sourceMappingURL=insert-employee-popup.component.js.map