var InsertWorkerController = (function () {
    function InsertWorkerController(_masterDataService, _systemManagementService, $location, $http, $element, _dialogService) {
        this._masterDataService = _masterDataService;
        this._systemManagementService = _systemManagementService;
        this.$location = $location;
        this.$http = $http;
        this.$element = $element;
        this._dialogService = _dialogService;
        /// 
        this.model = new workerModel();
        this.M_model = new workerModel();
        this.ID_worker = "AA18BA7B99FF403FAFD8529B35F94370"; ///in put from user management
        /// dont change
        this.session_id_worker = "0EB8E806125242A4B46709245A3F490D";
        this.topic_id_worker = "CC23F9657CB14AE880ED7D44D3856441";
        this.session_id_prefix = "44CB0A25CB8A435CB48477DBFEF153F1";
        this.topic_id_prefix = "A0F1527332E14B20AD00308E0A749F81";
        this.session_id_position = "55177F5F94294CD591D415677AE435FC";
        this.topic_id_position = "A501CAD099794342ADE98B2A8F6ED38C";
        this.session_id_department = "6A469DE811EC44A5A6826D8D7AFC4EBB";
        this.topic_id_department = "7C8FE4A791124CF595EFDE0F4F08F2D5";
    }
    /// Oninit
    InsertWorkerController.prototype.$onInit = function () {
        console.log("XXXXXXXXX", this.object);
        this.id = this.object[0].userid;
        this.viewMode = false;
        this.viewModeSession = true;
        this.editMode = this.object[0].Mode; // check mode Add or Edit
        this.getdataFromMasterInsertWorker(this.session_id_worker, this.topic_id_worker);
        this.getdataFromMasterInsertWorkerPrefix(this.session_id_prefix, this.topic_id_prefix);
        this.getdataFromMasterInsertWorkerPosition(this.session_id_position, this.topic_id_position);
        this.getdataFromMasterInsertWorkerDepartment();
        this.getdataIdentityGroup();
        if (this.editMode == false) {
            this.setTime = "2018-08-30 11:04:01.629952900";
            return false;
        }
        else {
            this.getdataInsertWorker(this.id);
        }
    };
    /// call DB
    InsertWorkerController.prototype.getdataFromMasterInsertWorker = function (session_id, topic_id) {
        var _this = this;
        this.$http.get("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id).then(function (res) {
            console.log('res2-->', res);
            _this.identityTypeList = res.data.Table;
        });
    };
    InsertWorkerController.prototype.getdataFromMasterInsertWorkerPrefix = function (session_id, topic_id) {
        var _this = this;
        this.$http.get("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id).then(function (res) {
            console.log('res3-->', res);
            _this.prefixtList = res.data.Table;
        });
    };
    InsertWorkerController.prototype.getdataFromMasterInsertWorkerPosition = function (session_id, topic_id) {
        var _this = this;
        this.$http.get("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id).then(function (res) {
            _this.positionlist = res.data.Table;
            console.log('res4-->', _this.positionlist);
        });
    };
    InsertWorkerController.prototype.getdataFromMasterInsertWorkerDepartment = function () {
        var _this = this;
        this._masterDataService.getDepartment().then(function (res) {
            console.log('res5-->', res);
            _this.departmentlist = res.data.DATA_SELECT;
        }).finally(function () {
            _this.$element.find('input').on('keydown', function (ev) {
                ev.stopPropagation();
            });
        });
    };
    InsertWorkerController.prototype.getdataIdentityGroup = function () {
        var _this = this;
        this.$http.get("api/SystemManagement/GetdataIdentityGroup").then(function (res) {
            console.log('res6-->', res);
            _this.grouplist = res.data.Table;
        });
    };
    InsertWorkerController.prototype.getdataInsertWorker = function (id) {
        var _this = this;
        var Id_Work = this.id;
        this.$http.get("api/SystemManagement/GetdataInsertWorker?id=" + Id_Work).then(function (res) {
            console.log('res-->', res);
            _this.modelList = res.data.Table;
            _this.privilegeList = res.data.Table1;
            _this.setData();
        }).catch(function (err) {
        }).finally(function () {
        });
    };
    // for add worker
    InsertWorkerController.prototype.insertWorkerSave = function (data_save) {
        return this.$http.post("api/SystemManagement/InsertWorkerSave", data_save).then(function (res) {
            return res;
        });
    };
    InsertWorkerController.prototype.setData = function () {
        var _this = this;
        var testFilter = this.modelList.filter(function (dep) { return dep.ID.toLowerCase() == _this.id.toLowerCase(); });
        console.log('testFilter', testFilter);
        this.model.IDENTITY_USER_TYPE_ID = testFilter["0"].IDENTITY_USER_TYPE_ID;
        this.model.EMPLOYEE_CODE = testFilter["0"].EMPLOYEE_CODE;
        this.model.ID_CARD = testFilter["0"].ID_CARD;
        this.model.NAME_TH = testFilter["0"].NAME_TH;
        this.model.LASTNAME_TH = testFilter["0"].LASTNAME_TH;
        this.model.PASSWORD_USER = testFilter["0"].PASSWORD_USER;
        this.model.POSITION_NAME = testFilter["0"].POSITION_NAME;
        this.model.MANAGEMENT_POSITION_ID = testFilter["0"].MANAGEMENT_POSITION_ID;
        this.model.DEPARTMENT = testFilter["0"].DEPARTMENT;
        this.model.DEPARTMENT_ID = testFilter["0"].DEPARTMENT_ID;
        this.model.GROUP_NAME = testFilter["0"].GROUP_NAME;
        this.model.IDENTITY_USER_TYPE_ID = testFilter["0"].IDENTITY_USER_TYPE_ID;
        this.model.PREFIX_ID = testFilter["0"].PREFIX_ID; //'895A49B59DCD40908E67FA0CD20992BE';//testFilter["0"].PREFIX_ID;  //
        this.model.POSITION_ID = testFilter["0"].POSITION_ID;
        this.model.USER_CODE = testFilter["0"].USER_CODE;
        this.model.WORK_START_DATE = testFilter["0"].WORK_START_DATE;
        this.model.WORK_END_DATE = testFilter["0"].WORK_END_DATE;
        this.model.EMAIL = testFilter["0"].EMAIL;
        this.model.LEVEL_USER = testFilter["0"].LEVEL_USER;
        this.startDateFrom = this.model.WORK_START_DATE;
        this.startDateTo = this.model.WORK_END_DATE;
    };
    InsertWorkerController.prototype.clearData = function () {
        this.mode = 0;
    };
    InsertWorkerController.prototype.saveOrUpdate = function () {
        if (this.editMode == false) {
            this.sentData();
        }
        else if (this.editMode == true) {
            this.sentData();
        }
    };
    InsertWorkerController.prototype.updateInsertWorker = function (data_save) {
        var _this = this;
        console.log("this.model", this.model);
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/updateWorkerDataModel', data_save).then(function (res) {
            if (res.data) {
                console.log('res-->', res.data);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, _this.getdataIdentityGroup(), "");
            }
        }).catch(function (err) {
            _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
        }).finally(function () {
            _this._dialogService.hideBusyBottomSheet();
        });
    };
    InsertWorkerController.prototype.sentData = function () {
        this.model.WORK_START_DATE = this.getFormatDateEng(this.startDateFrom);
        this.model.WORK_END_DATE = this.getFormatDateEng(this.startDateTo);
        var data_save = {
            ID: this.object["0"].userid,
            IDENTITY_USER_TYPE_ID: this.model.IDENTITY_USER_TYPE_ID,
            EMPLOYEE_CODE: this.model.EMPLOYEE_CODE,
            ID_CARD: this.model.ID_CARD,
            PREFIX_ID: this.model.PREFIX_ID,
            NAME_TH: this.model.NAME_TH,
            LASTNAME_TH: this.model.LASTNAME_TH,
            PASSWORD_USER: this.model.PASSWORD_USER,
            PASSWORD_USER_DATE: "2018-12-31",
            IS_GENERATE_PASSWORD: this.model.IS_GENERATE_PASSWORD,
            USER_CODE: "user_code",
            POSITION_ID: this.model.POSITION_ID,
            DEPARTMENT_ID: "01020001",
            MANAGEMENT_POSITION_ID: this.model.MANAGEMENT_POSITION_ID,
            LEVEL_USER: "lv_user",
            EMAIL: "mail@email.com",
            WORK_START_DATE: this.model.WORK_START_DATE,
            WORK_END_DATE: this.model.WORK_END_DATE,
            COUNT_NUM_LOCK: this.model.COUNT_NUM_LOCK,
            NUM_LOCK_DATE: this.model.NUM_LOCK_DATE,
            OTP_NUMBER: this.model.OTP_NUMBER,
            OTP_DATE: this.model.OTP_DATE,
            CREATE_BY: "AA18BA7B99FF403FAFD8529B35F94370" //this.model.CREATE_BY, 
            //CREATE_DATE: this.setTime//this.model.CREATE_DATE                     // timestamp	
        };
        //.sendDataService = data_save
        console.log(data_save);
        if (this.editMode == false) {
            this.insertWorkerSave(data_save);
        }
        else if (this.editMode == true) {
            this.updateInsertWorker(data_save);
        }
    };
    InsertWorkerController.prototype.getFormatDateEng = function (date) {
        var shortMonths = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        var _date = new Date(date);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        //let day = _date.getDate();
        var month = shortMonths[_date.getMonth()];
        var year = _date.getFullYear();
        var Year = year.toString();
        var subYear = Year.substring(2);
        var result = subYear + "-" + month + "-" + day;
        return result;
    };
    return InsertWorkerController;
}());
//# sourceMappingURL=insert-worker.component.js.map