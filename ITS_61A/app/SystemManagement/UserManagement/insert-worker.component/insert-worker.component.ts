﻿class InsertWorkerController implements ng.IController {

    /// 
    public model: workerModel = new workerModel();
    public M_model: workerModel = new workerModel();
    
    public identityTypeList: any
    public modelListEdit: any
    public modelList: any
    public privilegeList: any
    public select: any
    public prefixtList: any
    public positionlist: any
    public departmentlist: any
    public grouplist: any
    public id: any
    public toTS: string;
    public mode;
    public ID_worker: string = "AA18BA7B99FF403FAFD8529B35F94370";  ///in put from user management

    /// dont change
    public session_id_worker: string = "0EB8E806125242A4B46709245A3F490D";
    public topic_id_worker: string = "CC23F9657CB14AE880ED7D44D3856441";

    public session_id_prefix: string = "44CB0A25CB8A435CB48477DBFEF153F1";
    public topic_id_prefix: string = "A0F1527332E14B20AD00308E0A749F81";

    public session_id_position: string = "55177F5F94294CD591D415677AE435FC";
    public topic_id_position: string = "A501CAD099794342ADE98B2A8F6ED38C";

    public session_id_department: string = "6A469DE811EC44A5A6826D8D7AFC4EBB";
    public topic_id_department: string = "7C8FE4A791124CF595EFDE0F4F08F2D5";


    constructor(private _masterDataService: MasterDataService,
        private _systemManagementService: SystemManagementService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
        private $element: ng.IRootElementService,
        private _dialogService: DialogService,
    ) {

    }

    public editMode: boolean;
    public viewMode: boolean;
    public viewModeSession: boolean;
    public startDateFrom: any;
    public startDateTo: any;
    public endDateFrom: any;
    public endDateTo: any;
    public setTime: any;
    private object;
    
    /// Oninit
    $onInit() {
        console.log("XXXXXXXXX", this.object);
        this.id = this.object[0].userid;
        
        this.viewMode = false
        this.viewModeSession = true
        this.editMode = this.object[0].Mode // check mode Add or Edit



        
        this.getdataFromMasterInsertWorker(this.session_id_worker, this.topic_id_worker);
        this.getdataFromMasterInsertWorkerPrefix(this.session_id_prefix, this.topic_id_prefix);
        this.getdataFromMasterInsertWorkerPosition(this.session_id_position, this.topic_id_position);
        this.getdataFromMasterInsertWorkerDepartment();
        this.getdataIdentityGroup();

        if (this.editMode == false) {
            this.setTime = "2018-08-30 11:04:01.629952900";
            return false
        }
        else {
            this.getdataInsertWorker(this.id)
        }
    }

    /// call DB
    public getdataFromMasterInsertWorker(session_id, topic_id) {
        this.$http.get(`api/SystemManagement/GetdataFromMasterInsertWorker?session_id=${session_id}&topic_id=${topic_id}`).then((res: any) => {
            console.log('res2-->', res)
            this.identityTypeList = res.data.Table;

        })
    }

    public getdataFromMasterInsertWorkerPrefix(session_id, topic_id) {
        this.$http.get(`api/SystemManagement/GetdataFromMasterInsertWorker?session_id=${session_id}&topic_id=${topic_id}`).then((res: any) => {
            console.log('res3-->', res)
            this.prefixtList = res.data.Table;
        })
    }

    public getdataFromMasterInsertWorkerPosition(session_id, topic_id) {
        this.$http.get(`api/SystemManagement/GetdataFromMasterInsertWorker?session_id=${session_id}&topic_id=${topic_id}`).then((res: any) => {
           
            this.positionlist = res.data.Table;
            console.log('res4-->', this.positionlist)
        })
    }

    public getdataFromMasterInsertWorkerDepartment() {
        this._masterDataService.getDepartment().then((res: any) => {
            console.log('res5-->', res)
            this.departmentlist = res.data.DATA_SELECT;
        }).finally(() => {

            this.$element.find('input').on('keydown', (ev) => {
                ev.stopPropagation();
            });
        })
    }

    public getdataIdentityGroup() {
        this.$http.get(`api/SystemManagement/GetdataIdentityGroup`).then((res: any) => {
            console.log('res6-->', res)
            this.grouplist = res.data.Table;
        })
    }

    public getdataInsertWorker(id) {
        let Id_Work = this.id;
        this.$http.get(`api/SystemManagement/GetdataInsertWorker?id=${Id_Work}`).then((res: any) => {
            console.log('res-->', res)
            this.modelList = res.data.Table;
            this.privilegeList = res.data.Table1;
            this.setData()
        }).catch((err) => {

        }).finally(() => {
        })

    }
    // for add worker
    public insertWorkerSave(data_save: any): ng.IPromise<any> {
        return this.$http.post(`api/SystemManagement/InsertWorkerSave`, data_save).then((res: any) => {
            return res;
        });
    }
    
    public setData() {
        let testFilter = this.modelList.filter((dep) => dep.ID.toLowerCase() == this.id.toLowerCase());
        console.log('testFilter', testFilter);

        this.model.IDENTITY_USER_TYPE_ID = testFilter["0"].IDENTITY_USER_TYPE_ID;
        this.model.EMPLOYEE_CODE = testFilter["0"].EMPLOYEE_CODE;
        this.model.ID_CARD = testFilter["0"].ID_CARD;
        this.model.NAME_TH = testFilter["0"].NAME_TH;
        this.model.LASTNAME_TH = testFilter["0"].LASTNAME_TH;
        this.model.PASSWORD_USER = testFilter["0"].PASSWORD_USER;
        this.model.POSITION_NAME = testFilter["0"].POSITION_NAME;
        this.model.MANAGEMENT_POSITION_ID = testFilter["0"].MANAGEMENT_POSITION_ID;
        this.model.DEPARTMENT = testFilter["0"].DEPARTMENT;
        this.model.DEPARTMENT_ID = testFilter["0"].DEPARTMENT_ID;
        this.model.GROUP_NAME = testFilter["0"].GROUP_NAME;
        this.model.IDENTITY_USER_TYPE_ID = testFilter["0"].IDENTITY_USER_TYPE_ID;
        this.model.PREFIX_ID = testFilter["0"].PREFIX_ID;//'895A49B59DCD40908E67FA0CD20992BE';//testFilter["0"].PREFIX_ID;  //
        this.model.POSITION_ID = testFilter["0"].POSITION_ID;
        this.model.USER_CODE = testFilter["0"].USER_CODE;
        this.model.WORK_START_DATE = testFilter["0"].WORK_START_DATE;
        this.model.WORK_END_DATE = testFilter["0"].WORK_END_DATE;
        this.model.EMAIL = testFilter["0"].EMAIL;
        this.model.LEVEL_USER = testFilter["0"].LEVEL_USER;
        this.startDateFrom = this.model.WORK_START_DATE;
        this.startDateTo = this.model.WORK_END_DATE;
    }

    public clearData() {
        this.mode = 0;
        
    }

    public saveOrUpdate() {
        if (this.editMode == false) {
            this.sentData()
         
        } else if (this.editMode == true){
            
            this.sentData()
        }
    }

    public updateInsertWorker(data_save: any) {
        console.log("this.model", this.model)
        this._dialogService.showBusyBottomSheet();
        this.$http.post('api/SystemManagement/updateWorkerDataModel', data_save).then((res: any) => {
            if (res.data) {
                console.log('res-->', res.data)
                this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                    Constants.messageDialog.enter,
                    Constants.messageDialog.completeSave,
                    this.getdataIdentityGroup(),
                    "");
            }
        }).catch((err) => {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert,
                Constants.messageDialog.enter,
                Constants.messageDialog.uncompleteSave,
                "", "");
        }).finally(() => {
            this._dialogService.hideBusyBottomSheet();

        })

    }
    public sentData() {
        this.model.WORK_START_DATE = this.getFormatDateEng(this.startDateFrom);
        this.model.WORK_END_DATE = this.getFormatDateEng(this.startDateTo);


        var data_save = {
            ID: this.object["0"].userid,
            IDENTITY_USER_TYPE_ID: this.model.IDENTITY_USER_TYPE_ID,        //have
            EMPLOYEE_CODE: this.model.EMPLOYEE_CODE,                        //have
            ID_CARD: this.model.ID_CARD,                                    //null number
            PREFIX_ID: this.model.PREFIX_ID,                                //have
            NAME_TH: this.model.NAME_TH,                                    //have
            LASTNAME_TH: this.model.LASTNAME_TH,                            //have
            PASSWORD_USER: this.model.PASSWORD_USER,                        //have
            PASSWORD_USER_DATE: "2018-12-31",                                       //this.model.PASSWORD_USER_DATE,      // timestamp
            IS_GENERATE_PASSWORD: this.model.IS_GENERATE_PASSWORD,          //null
            USER_CODE: "user_code",//this.model.USER_CODE, //รอ
            POSITION_ID: this.model.POSITION_ID,                            //null //have
            DEPARTMENT_ID: "01020001",//this.model.DEPARTMENT_ID,
            MANAGEMENT_POSITION_ID: this.model.MANAGEMENT_POSITION_ID,      //null
            LEVEL_USER: "lv_user",//this.model.LEVEL_USER,
            EMAIL: "mail@email.com",//this.model.EMAIL,
            WORK_START_DATE: this.model.WORK_START_DATE ,//this.model.WORK_START_DATE,            // date
            WORK_END_DATE: this.model.WORK_END_DATE,//this.model.WORK_END_DATE,                // date
            COUNT_NUM_LOCK: this.model.COUNT_NUM_LOCK,                      //null number
            NUM_LOCK_DATE: this.model.NUM_LOCK_DATE,                        //null
            OTP_NUMBER: this.model.OTP_NUMBER,                              //null
            OTP_DATE: this.model.OTP_DATE,                                  //null
            CREATE_BY: "AA18BA7B99FF403FAFD8529B35F94370"//this.model.CREATE_BY, 
            //CREATE_DATE: this.setTime//this.model.CREATE_DATE                     // timestamp	
            
        }


        //.sendDataService = data_save

        console.log(data_save);

        
        

        if (this.editMode == false) {
            this.insertWorkerSave(data_save);

        } else if (this.editMode == true) {
          this.updateInsertWorker(data_save);  
        }

    }
    private getFormatDateEng(date) {
        let shortMonths = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        let _date = new Date(date);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        //let day = _date.getDate();
        let month = shortMonths[_date.getMonth()];
        let year = _date.getFullYear();
        let Year = year.toString();
        let subYear = Year.substring(2)
        let result = `${subYear}-${month}-${day}`;

        return result;
    }


    
    
}