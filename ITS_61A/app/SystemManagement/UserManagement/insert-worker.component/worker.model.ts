﻿class workerModel {
    public ID: string;
    public IDENTITY_TYPE: string;
   public IDENTITY_USER_TYPE_ID: string;
   public EMPLOYEE_CODE: string;
   public ID_CARD: string;
   public PREFIX_ID: string;
   public NAME_TH: string;
   public LASTNAME_TH: string;
   public PASSWORD_USER: string;
   public PASSWORD_USER_DATE: string;
   public IS_GENERATE_PASSWORD: string;
   public USER_CODE: string;
   public POSITION_ID: string;
   public POSITION_NAME: string;
   public DEPARTMENT_ID: string;
   public DEPARTMENT: string;
   public MANAGEMENT_POSITION_ID: string;
   public LEVEL_USER: string;
   public EMAIL: string;
   public WORK_START_DATE: string;
   public WORK_END_DATE: string;
   public COUNT_NUM_LOCK: number;
   public NUM_LOCK_DATE: string;
   public OTP_NUMBER: string;
   public OTP_DATE: string;
   public CREATE_BY: string;
   public CREATE_DATE: string;
   public GROUP_NAME: string;
   
}

