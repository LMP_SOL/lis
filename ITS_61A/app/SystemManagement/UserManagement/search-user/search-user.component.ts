﻿class SearchUserController implements ng.IController {
 
    constructor(private authorizationService: AuthorizationService,
        private systemManagementService : SystemManagementService,     
        private dialogService: DialogService,
    ) {

    }

    private gridOptions: any;
    private page: any;
    private OfficeList: any;
    private StatusList: any;
    private PositionList: any;   
    private grid: any;
    private PersonCode = "";
    private PersonName = "";
    private Username = "";
    private Office = "";
    private Status = null;
    private Position = "";
    private Mode = 0;
    private dataitem = [];
    private paramObj: any;
    /// Oninit
    $onInit() { 
        this.gridOptions = this.createGridOptions(null);
        this.OfficeList = [];
        this.StatusList = [];
        this.PositionList = [];

       
        console.log(this.PositionList);
        this.InitCriteria();
     
                           
    }

    private InitCriteria() {
        this.systemManagementService.getCriteriaforsearcUser().then((res) => {
            console.log('rexxxx', res);
            this.OfficeList = res.Table;

            this.StatusList = [{ ID: 1, Name: "ไม่ใช้งาน" }, { ID: 2, Name: "ใช้งาน" }];

            this.PositionList = res.Table1;
        })
    }

    private addEmployee() {
        this.paramObj = [];
        this.Mode = 1
        this.paramObj.push({          
            Mode: false

        })
    }


    private addworker() {
        this.paramObj = [];
        this.Mode = 2
        this.paramObj.push({          
            Mode: false

        })
    }
  
    private onEdit(dataItem) {
        console.log("XXX", dataItem);
        this.paramObj = [];
        if (dataItem.TYPE_ID == '576CBA8240874715AD3AEFF3A3DFB59D') {
            this.Mode = 1
            this.paramObj.push({
                userid: dataItem.USERID,
                Mode : true

            })

        } else 
        {
            this.Mode = 2
            this.paramObj.push({
                userid: dataItem.USERID,
                Mode: true

            })
        }
       
    }

    private onChangePass(dataItem) {
        this.dialogService.showCustomConfirm("ข้อความจากระบบ", "ต้องการเปลี่ยนรหัสผ่านหรือไม่", dataItem, this.ConfilmChangepassword ,undefined)
    }

    

    private ConfilmChangepassword = (item) => {
        let email = item.EMAIL;
        console.log('email', email);
        this.systemManagementService.SendEmailforChangepassword(email).then((data) => {
            if (data) {
                this.dialogService.showCustomAlert("ข้อความจากระบบ", "ตกลง", "เปลี่ยนรหัสผ่านสำเร็จ รหัสผ่านถูกไปที่ EMAIL", undefined, undefined);
            } else {
                this.dialogService.showCustomAlert("ข้อความจากระบบ", "ตกลง", "เปลี่ยนรหัสผ่านไม่สำเร็จ", undefined, undefined);
            }
            console.log("dataXXX",data)
        })
        
        return true;
    }


    private createGridOptions(dataSource: any = null) {
        let template1 = `<div  layout="row" layout-wrap>
                        <div ng-click="ctrl.onEdit(dataItem)"><img ng-src="Content/btn/edit_table_btn.png"></div>
                        <div ng-click="ctrl.onChangePass(dataItem)"><img ng-src="Content/btn/change_password_btn.png"></div>
                        </div>`;

        return {         
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                { field: "management", title: "การจัดการ", template: template1, attributes: { class: "text-center", "data-title": "การจัดการ" } },
                { field: "rowNumber", title: "ลำดับ", template: '<span class="row-number"></span>', attributes: { class: "text-center", "data-title": "ลำดับ" } },              
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    attributes: { class: "text-center"},                
                   
                },
                {
                    field: "USER_CODE",
                    title: "รหัสผู้ใช้",
                    attributes: { class: "text-center" }
                },
                {
                    field: "DATA_TOPIC",
                    title: "ตำแหน่ง",
                    attributes: { class: "text-center" },
                },
                {
                    field: "ORG_NAME",
                    title: "หน่วยงาน",
                    attributes: { class: "text-center"},                   
                },
                {
                    template: '<div layout="row" layout-align="center center" class="w100-percen">' +
                    '#if(STATUS_ID == true){# <img class="img-responsive icon-btn" ng-src="Content/btn/icon-use.png"> #} else{# <img class="img-responsive icon-btn" ng-src="Content/btn/icon-unuse.png.png"> #}#'
                    + '</div>'
                    , title: "การใช้งาน"
                    , attributes: { class: "text-center", "data-title": "สถานะ" }
                },              

            ],                   
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;
                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }

   
    private Search() {
        if (this.Status == null) {
            this.Status = 3;
        }

        this.systemManagementService.getdataForsearchUser(this.PersonCode, this.PersonName, this.Username, this.Office, this.Status, this.Position).then((res) => {
            console.log('VVVVVV', res);
            this.dataitem = res.Table

            let dataSource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
            this.grid.setDataSource(dataSource)
        })
       

    }

    private Clear() {

        this.PersonCode = "";
        this.PersonName = "";
        this.Username = "";
        this.Office = "";
        this.Status = null;
        this.Position = "";
        this.Status = 3;

        this.dataitem = [];
        console.log("xxxx")
        let dataSource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
        this.grid.setDataSource(dataSource)
        
    }

    private Export() {

        console.log(this.dataitem);

        if (this.dataitem.length > 0) {
            let cols = [];
            let sheets = [];
            let rows = [];
            let dataSource = [];

            let Hearder = {
                cells: [
                    { value: "ลำดับ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "รหัสผู้ใช้งาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "ชื่อ-สกุล", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "หน่วยงาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "ตำแหน่ง", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }},
                    { value: "ระดับ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "ประเภทผู้ใช้งาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },                
                    { value: "อีเมล์", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }},
                    { value: "วันที่เริ่มต้น", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }},
                    { value: "วันที่สิ้นสุด", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }},
                    { value: "กลุ่มสิทธิ์การเข้าใช้งาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 }}
                ]
            };

            dataSource.push(Hearder);

            this.dataitem.map((x) => {
                dataSource.push(
                    {
                      cells: [
                          { value: x.ROWN, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: x.USER_CODE, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: x.FULLNAME, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: x.ORG_NAME, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: x.DATA_TOPIC, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: x.LEVEL_USER, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: x.USERTYPETEXT, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: x.EMAIL, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: this.getFormatDate(x.WORK_START_DATE), borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: this.getFormatDate(x.WORK_END_DATE), borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                          { value: x.GROUP_NAME, borderBottom: { color: "#000000", size: 1 }}
                        ]
                    },
                )
            })

            rows = (dataSource)

            rows.map((item) => {
                cols.push({ autoWidth: true });
            });

            sheets.push(
                {
                    columns: cols,
                    title: "ข้อมูลผู้ใช้",
                    rows: rows
                }
            );

            var workbook = new kendo.ooxml.Workbook({
                sheets: sheets
            });
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: `${"ข้อมูลผู้ใช้งาน"}_${this.getDate()}.xlsx` });

        }

    }

    private getDate() {
        let today = new Date();
        let dd = today.getDate().toString();
        if (Number(dd) < 10) dd = '0' + dd;
        let mm = (today.getMonth() + 1).toString();
        if (Number(mm) < 10) mm = '0' + mm;
        //let yyyy = today.getFullYear() + 543;
        let yyyy = today.getFullYear();
        let result = yyyy + mm + dd;
        return result;
    }

    public getFormatDate(date) {
        //let res = date.substring(0, 10);
        let _date = new Date(date);
        //console.log(res);
        let day = _date.getDate().toString();
        if (Number(day) < 10) day = '0' + day;
        let month = (_date.getMonth() + 1).toString();
        if (Number(month) < 10) month = '0' + month;
        let year = _date.getFullYear();
        //let hour = res.getHours();
        //let min = res.getMinutes();
        //let sec = res.getSeconds();
        //let millisec = res.getMilliseconds();
        //let result = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec + "." + millisec;

        let result = day + "/" + month + "/" + year;
        return result;
    }

}