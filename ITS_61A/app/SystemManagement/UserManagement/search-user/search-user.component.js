var SearchUserController = (function () {
    function SearchUserController(authorizationService, systemManagementService, dialogService) {
        var _this = this;
        this.authorizationService = authorizationService;
        this.systemManagementService = systemManagementService;
        this.dialogService = dialogService;
        this.PersonCode = "";
        this.PersonName = "";
        this.Username = "";
        this.Office = "";
        this.Status = null;
        this.Position = "";
        this.Mode = 0;
        this.dataitem = [];
        this.ConfilmChangepassword = function (item) {
            var email = item.EMAIL;
            console.log('email', email);
            _this.systemManagementService.SendEmailforChangepassword(email).then(function (data) {
                if (data) {
                    _this.dialogService.showCustomAlert("ข้อความจากระบบ", "ตกลง", "เปลี่ยนรหัสผ่านสำเร็จ รหัสผ่านถูกไปที่ EMAIL", undefined, undefined);
                }
                else {
                    _this.dialogService.showCustomAlert("ข้อความจากระบบ", "ตกลง", "เปลี่ยนรหัสผ่านไม่สำเร็จ", undefined, undefined);
                }
                console.log("dataXXX", data);
            });
            return true;
        };
    }
    /// Oninit
    SearchUserController.prototype.$onInit = function () {
        this.gridOptions = this.createGridOptions(null);
        this.OfficeList = [];
        this.StatusList = [];
        this.PositionList = [];
        console.log(this.PositionList);
        this.InitCriteria();
    };
    SearchUserController.prototype.InitCriteria = function () {
        var _this = this;
        this.systemManagementService.getCriteriaforsearcUser().then(function (res) {
            console.log('rexxxx', res);
            _this.OfficeList = res.Table;
            _this.StatusList = [{ ID: 1, Name: "ไม่ใช้งาน" }, { ID: 2, Name: "ใช้งาน" }];
            _this.PositionList = res.Table1;
        });
    };
    SearchUserController.prototype.addEmployee = function () {
        this.paramObj = [];
        this.Mode = 1;
        this.paramObj.push({
            Mode: false
        });
    };
    SearchUserController.prototype.addworker = function () {
        this.paramObj = [];
        this.Mode = 2;
        this.paramObj.push({
            Mode: false
        });
    };
    SearchUserController.prototype.onEdit = function (dataItem) {
        console.log("XXX", dataItem);
        this.paramObj = [];
        if (dataItem.TYPE_ID == '576CBA8240874715AD3AEFF3A3DFB59D') {
            this.Mode = 1;
            this.paramObj.push({
                userid: dataItem.USERID,
                Mode: true
            });
        }
        else {
            this.Mode = 2;
            this.paramObj.push({
                userid: dataItem.USERID,
                Mode: true
            });
        }
    };
    SearchUserController.prototype.onChangePass = function (dataItem) {
        this.dialogService.showCustomConfirm("ข้อความจากระบบ", "ต้องการเปลี่ยนรหัสผ่านหรือไม่", dataItem, this.ConfilmChangepassword, undefined);
    };
    SearchUserController.prototype.createGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template1 = "<div  layout=\"row\" layout-wrap>\n                        <div ng-click=\"ctrl.onEdit(dataItem)\"><img ng-src=\"Content/btn/edit_table_btn.png\"></div>\n                        <div ng-click=\"ctrl.onChangePass(dataItem)\"><img ng-src=\"Content/btn/change_password_btn.png\"></div>\n                        </div>";
        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                { field: "management", title: "การจัดการ", template: template1, attributes: { class: "text-center", "data-title": "การจัดการ" } },
                { field: "rowNumber", title: "ลำดับ", template: '<span class="row-number"></span>', attributes: { class: "text-center", "data-title": "ลำดับ" } },
                {
                    field: "FULLNAME",
                    title: "ชื่อ-สกุล",
                    attributes: { class: "text-center" },
                },
                {
                    field: "USER_CODE",
                    title: "รหัสผู้ใช้",
                    attributes: { class: "text-center" }
                },
                {
                    field: "DATA_TOPIC",
                    title: "ตำแหน่ง",
                    attributes: { class: "text-center" },
                },
                {
                    field: "ORG_NAME",
                    title: "หน่วยงาน",
                    attributes: { class: "text-center" },
                },
                {
                    template: '<div layout="row" layout-align="center center" class="w100-percen">' +
                        '#if(STATUS_ID == true){# <img class="img-responsive icon-btn" ng-src="Content/btn/icon-use.png"> #} else{# <img class="img-responsive icon-btn" ng-src="Content/btn/icon-unuse.png.png"> #}#'
                        + '</div>',
                    title: "การใช้งาน",
                    attributes: { class: "text-center", "data-title": "สถานะ" }
                },
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    SearchUserController.prototype.Search = function () {
        var _this = this;
        if (this.Status == null) {
            this.Status = 3;
        }
        this.systemManagementService.getdataForsearchUser(this.PersonCode, this.PersonName, this.Username, this.Office, this.Status, this.Position).then(function (res) {
            console.log('VVVVVV', res);
            _this.dataitem = res.Table;
            var dataSource = new kendo.data.DataSource({ data: _this.dataitem, pageSize: 10 });
            _this.grid.setDataSource(dataSource);
        });
    };
    SearchUserController.prototype.Clear = function () {
        this.PersonCode = "";
        this.PersonName = "";
        this.Username = "";
        this.Office = "";
        this.Status = null;
        this.Position = "";
        this.Status = 3;
        this.dataitem = [];
        console.log("xxxx");
        var dataSource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
        this.grid.setDataSource(dataSource);
    };
    SearchUserController.prototype.Export = function () {
        var _this = this;
        console.log(this.dataitem);
        if (this.dataitem.length > 0) {
            var cols_1 = [];
            var sheets = [];
            var rows = [];
            var dataSource_1 = [];
            var Hearder = {
                cells: [
                    { value: "ลำดับ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "รหัสผู้ใช้งาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "ชื่อ-สกุล", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "หน่วยงาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "ตำแหน่ง", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "ระดับ", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "ประเภทผู้ใช้งาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "อีเมล์", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "วันที่เริ่มต้น", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "วันที่สิ้นสุด", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                    { value: "กลุ่มสิทธิ์การเข้าใช้งาน", hAlign: "left", borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } }
                ]
            };
            dataSource_1.push(Hearder);
            this.dataitem.map(function (x) {
                dataSource_1.push({
                    cells: [
                        { value: x.ROWN, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.USER_CODE, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.FULLNAME, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.ORG_NAME, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.DATA_TOPIC, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.LEVEL_USER, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.USERTYPETEXT, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.EMAIL, borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: _this.getFormatDate(x.WORK_START_DATE), borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: _this.getFormatDate(x.WORK_END_DATE), borderBottom: { color: "#000000", size: 1 }, borderRight: { color: "#000000", size: 1 } },
                        { value: x.GROUP_NAME, borderBottom: { color: "#000000", size: 1 } }
                    ]
                });
            });
            rows = (dataSource_1);
            rows.map(function (item) {
                cols_1.push({ autoWidth: true });
            });
            sheets.push({
                columns: cols_1,
                title: "ข้อมูลผู้ใช้",
                rows: rows
            });
            var workbook = new kendo.ooxml.Workbook({
                sheets: sheets
            });
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: "ข้อมูลผู้ใช้งาน" + "_" + this.getDate() + ".xlsx" });
        }
    };
    SearchUserController.prototype.getDate = function () {
        var today = new Date();
        var dd = today.getDate().toString();
        if (Number(dd) < 10)
            dd = '0' + dd;
        var mm = (today.getMonth() + 1).toString();
        if (Number(mm) < 10)
            mm = '0' + mm;
        //let yyyy = today.getFullYear() + 543;
        var yyyy = today.getFullYear();
        var result = yyyy + mm + dd;
        return result;
    };
    SearchUserController.prototype.getFormatDate = function (date) {
        //let res = date.substring(0, 10);
        var _date = new Date(date);
        //console.log(res);
        var day = _date.getDate().toString();
        if (Number(day) < 10)
            day = '0' + day;
        var month = (_date.getMonth() + 1).toString();
        if (Number(month) < 10)
            month = '0' + month;
        var year = _date.getFullYear();
        //let hour = res.getHours();
        //let min = res.getMinutes();
        //let sec = res.getSeconds();
        //let millisec = res.getMilliseconds();
        //let result = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec + "." + millisec;
        var result = day + "/" + month + "/" + year;
        return result;
    };
    return SearchUserController;
}());
//# sourceMappingURL=search-user.component.js.map