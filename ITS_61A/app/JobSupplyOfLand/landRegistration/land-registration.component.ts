﻿class LandRegistrationController implements ng.IController {

    public mode: number;// o = manage ,1 = add, 2 = edit, 3 = view  
    public announcementSelectedId: string;
    public menuName: string;
    public AnnouncementLandList: any[];
    public provinceList: any[];
    public listData: any[];
    public isshowSearch: boolean = true;
    public obj: any;
    public data: any;
    public operation: string = "add";
    public operationTH: string = "เพิ่ม";
    public imgFile = {
        result: "",
        imageFiles: {
            files: []
        }
    };
    public docFile = {
        result: "",
        documentFiles: {
            files: []
        }
    };


    public modelSearch: any = {
        ANNOUNCEMENTID: undefined,
        PROJECT_NAME: undefined,
        DEED_NO_FROM: undefined,
        DEED_NO_TO: undefined,
    };
    public modelSave: any = {
        NOTICE_DATE: undefined,
        TRANSFER_DATE: undefined
    }
    constructor(private _dialogService: DialogService,
        private _jobSupplyOfLandService: JobSupplyOfLandService,
        private _masterDataService: MasterDataService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
        private $element: ng.IRootElementService,
        private _common: CommonService,
        private webConfigService: WebConfigService,

    ) { }




    private gridOptions = {
        gridID: 'gridRegistration',
        selectable: 'row',
        dataSource: this.listDataGrid(),
        scrollable: false,
        sortable: false,
        pageable: true,
        noRecords: true,
        messages: { noRecords: "ไม่พบข้อมูล" },
        dataBound: function () {
        },
        column: [
            {
                field: "PROJECT",
                title: "รหัสโครงการ,ชื่อโครงการ",
                attributes: {
                    "data-title": "รหัสโครงการ,ชื่อโครงการ",
                    "class": "text-center"
                }
            }, {
                field: "DEED_NO",
                title: "ฉโนดเลขที่",
                attributes: {
                    "data-title": "ฉโนดเลขที่",
                    "class": "text-center"
                }
            }
            , {
                field: "PARCEL_NO",
                title: "เลขที่ดิน",
                attributes: {
                    "data-title": "เลขที่ดิน",
                    "class": "text-center"
                }
            }
            , {
                field: "DEALING_FILE_NO",
                title: "หน้าสำรวจ",
                attributes: {
                    "data-title": "หน้าสำรวจ",
                    "class": "text-center"
                }
            }
            , {
                field: "SIZE_IN_DEED_RAI_NGAN_WA",
                title: "เนื้อที่ตามฉโนด (ไร่-งาน-วา)",
                attributes: {
                    "data-title": "เนื้อที่ตามฉโนด (ไร่-งาน-วา)",
                    "class": "text-center"
                }
            }
            , {
                field: "TRANSFER_DATE",
                title: "วันที่โอนให้ กคช.",
                attributes: {
                    "data-title": "วันที่โอนให้ กคช.",
                    "class": "text-center"
                }
            }
            , {
                field: "TRANSFEROR_NAME",
                title: "ชื่อเจ้าของกรรมสิทธุ์เดิม",
                attributes: {
                    "data-title": "ชื่อเจ้าของกรรมสิทธฺ์เดิม",
                    "class": "text-center"
                }
            }
        ],
        management: true,
        operation: {
            view: true,
            del: true,
            edit: true
        },
        showIndex: true,
        showStatus: false,

    };

    private listDataGrid() {
        this.listData = [];
        //for (let i = 0; i < 20; i++) {
        //    this.listData.push({
        //        ID: i + 1, PROJECT: "โครงการ" + i, NO: "ฉโนดเลขที่" + i, NO_M: "เลขที่ดิน" + i});
        //}
        return new kendo.data.DataSource({ data: this.listData, pageSize: 10 });
    }

    public configProvinceAutoComplete = {
        isDisabled: false,
        noCache: true,
        selectedItem: undefined,
        searchText: undefined,
    };


    $onInit() {
        //this.operation = (this.mode = 0) ? 'add' : 'view';
        this.mode = 0;
        this.menuName = "ข้อมูลทะเบียนประวัติที่ดิน (ฉโนด)";
        this.getDataAnnouncementLand();
        this.getDataProvince();
        this.initDataSearch();
    }

    public getDataAnnouncementLand() {
        this._jobSupplyOfLandService.GetDataForAnnouncementLand().then((res) => {
            console.log('GetDataForAnnouncementLand-->', res)
            this.AnnouncementLandList = res.Table;
        })
    }

    public getDataProvince() {
        this._masterDataService.getDataDistrictAndProvince().then((res) => {
            console.log('GetDataProvince-->', res)
            this.provinceList = res.data.province;
        })
    }

    ///***** Province
    public searchTextChange(item: string) {
        console.log('itemmmmmmmmmmmmmm', item)

    }

    public selectedItemChangeProvince(item) {
        // console.log('selectedItemChange ', item);
        if (item) {
            this.configProvinceAutoComplete.selectedItem = item;
        }
    }
    public querySearchProvince(query) {
        var results = query ? this.provinceList.filter(this.createFilterForProvince(query)) : this.provinceList;
        return results;

    }
    public createFilterForProvince(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(item) {
            return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    }
    ///******* end autocomplete *****////

    public initDataSearch() {
        this._dialogService.showBusyBottomSheet();
        let tmpProvince = (this.configProvinceAutoComplete.selectedItem) ? this.configProvinceAutoComplete.selectedItem.ID : "";
        this._jobSupplyOfLandService.GetDataForLandRegistration(this.modelSearch.ANNOUNCEMENTID, this.modelSearch.PROJECT_NAME, this.modelSearch.DEED_NO_FROM, this.modelSearch.DEED_NO_TO, tmpProvince).then((res) => {
            res.Table.forEach((item) => {
                item.PROJECT = item.PROJECT_CODE + "," + item.PROJECT_NAME
                item.SIZE_IN_DEED_RAI_NGAN_WA = item.SIZE_IN_DEED_RAI + "-" + item.SIZE_IN_DEED_NGAN + "-" + item.SIZE_IN_DEED_WA
                item.TRANSFER_DATE = this._common.formatDateThai(this._common.formatDate(item.TRANSFER_DATE,"YYYY-MM-DD"), "dd MMMM yyyy")
            });
            this.gridOptions.dataSource.data(res.Table);       
        }).finally(() => {
            this._dialogService.hideBusyBottomSheet()
        });
    }

    public add() {
        this.isshowSearch = false;
    }
    public search() {
        this.initDataSearch();
    }
    public clear() {
        this.modelSearch = {};
        this.configProvinceAutoComplete.selectedItem = undefined;
        this.initDataSearch();
    }
    public goBack() {
        this.isshowSearch = true;
    }

    private gridCallbackView = (res) => {
        console.log('res', res);
    }

    private gridCallbackEdit = (res) => {
        console.log('res', res);
        this.isshowSearch = false;
        this.operationTH = "แก้ไข";
    }

    private gridCallbackDel = (res) => {
        this._dialogService.showCustomConfirm(Constants.messageDialog.alert, Constants.messageDialog.confrim_delete_info,res, this.DeleteCallback, this.cancelCallback)
    }

    private DeleteCallback = (res) => {
        console.log('res', res);
        //this._dialogService.showBusyBottomSheet()
        this._jobSupplyOfLandService.DeleteDDeed(res.DID)
            .then((data) => {
                this._dialogService.showCustomAlert("ข้อความจากระบบ", "ตกลง", Constants.messageDialog.completeDelete, "", "");
                this.search();        
            }).finally(() => {
                //this._dialogService.hideBusyBottomSheet()
            });
        return true;
    }
    private cancelCallback = (res) => {
        return true;
    }

}