var LandRegistrationController = (function () {
    function LandRegistrationController(_dialogService, _jobSupplyOfLandService, _masterDataService, $location, $http, $element, _common, webConfigService) {
        var _this = this;
        this._dialogService = _dialogService;
        this._jobSupplyOfLandService = _jobSupplyOfLandService;
        this._masterDataService = _masterDataService;
        this.$location = $location;
        this.$http = $http;
        this.$element = $element;
        this._common = _common;
        this.webConfigService = webConfigService;
        this.isshowSearch = true;
        this.operation = "add";
        this.operationTH = "เพิ่ม";
        this.imgFile = {
            result: "",
            imageFiles: {
                files: []
            }
        };
        this.docFile = {
            result: "",
            documentFiles: {
                files: []
            }
        };
        this.modelSearch = {
            ANNOUNCEMENTID: undefined,
            PROJECT_NAME: undefined,
            DEED_NO_FROM: undefined,
            DEED_NO_TO: undefined,
        };
        this.modelSave = {
            NOTICE_DATE: undefined,
            TRANSFER_DATE: undefined
        };
        this.gridOptions = {
            gridID: 'gridRegistration',
            selectable: 'row',
            dataSource: this.listDataGrid(),
            scrollable: false,
            sortable: false,
            pageable: true,
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
            },
            column: [
                {
                    field: "PROJECT",
                    title: "รหัสโครงการ,ชื่อโครงการ",
                    attributes: {
                        "data-title": "รหัสโครงการ,ชื่อโครงการ",
                        "class": "text-center"
                    }
                }, {
                    field: "DEED_NO",
                    title: "ฉโนดเลขที่",
                    attributes: {
                        "data-title": "ฉโนดเลขที่",
                        "class": "text-center"
                    }
                },
                {
                    field: "PARCEL_NO",
                    title: "เลขที่ดิน",
                    attributes: {
                        "data-title": "เลขที่ดิน",
                        "class": "text-center"
                    }
                },
                {
                    field: "DEALING_FILE_NO",
                    title: "หน้าสำรวจ",
                    attributes: {
                        "data-title": "หน้าสำรวจ",
                        "class": "text-center"
                    }
                },
                {
                    field: "SIZE_IN_DEED_RAI_NGAN_WA",
                    title: "เนื้อที่ตามฉโนด (ไร่-งาน-วา)",
                    attributes: {
                        "data-title": "เนื้อที่ตามฉโนด (ไร่-งาน-วา)",
                        "class": "text-center"
                    }
                },
                {
                    field: "TRANSFER_DATE",
                    title: "วันที่โอนให้ กคช.",
                    attributes: {
                        "data-title": "วันที่โอนให้ กคช.",
                        "class": "text-center"
                    }
                },
                {
                    field: "TRANSFEROR_NAME",
                    title: "ชื่อเจ้าของกรรมสิทธุ์เดิม",
                    attributes: {
                        "data-title": "ชื่อเจ้าของกรรมสิทธฺ์เดิม",
                        "class": "text-center"
                    }
                }
            ],
            management: true,
            operation: {
                view: true,
                del: true,
                edit: true
            },
            showIndex: true,
            showStatus: false,
        };
        this.configProvinceAutoComplete = {
            isDisabled: false,
            noCache: true,
            selectedItem: undefined,
            searchText: undefined,
        };
        this.gridCallbackView = function (res) {
            console.log('res', res);
        };
        this.gridCallbackEdit = function (res) {
            console.log('res', res);
            _this.isshowSearch = false;
            _this.operationTH = "แก้ไข";
        };
        this.gridCallbackDel = function (res) {
            _this._dialogService.showCustomConfirm(Constants.messageDialog.alert, Constants.messageDialog.confrim_delete_info, res, _this.DeleteCallback, _this.cancelCallback);
        };
        this.DeleteCallback = function (res) {
            console.log('res', res);
            //this._dialogService.showBusyBottomSheet()
            _this._jobSupplyOfLandService.DeleteDDeed(res.DID)
                .then(function (data) {
                _this._dialogService.showCustomAlert("ข้อความจากระบบ", "ตกลง", Constants.messageDialog.completeDelete, "", "");
                _this.search();
            }).finally(function () {
                //this._dialogService.hideBusyBottomSheet()
            });
            return true;
        };
        this.cancelCallback = function (res) {
            return true;
        };
    }
    LandRegistrationController.prototype.listDataGrid = function () {
        this.listData = [];
        //for (let i = 0; i < 20; i++) {
        //    this.listData.push({
        //        ID: i + 1, PROJECT: "โครงการ" + i, NO: "ฉโนดเลขที่" + i, NO_M: "เลขที่ดิน" + i});
        //}
        return new kendo.data.DataSource({ data: this.listData, pageSize: 10 });
    };
    LandRegistrationController.prototype.$onInit = function () {
        //this.operation = (this.mode = 0) ? 'add' : 'view';
        this.mode = 0;
        this.menuName = "ข้อมูลทะเบียนประวัติที่ดิน (ฉโนด)";
        this.getDataAnnouncementLand();
        this.getDataProvince();
        this.initDataSearch();
    };
    LandRegistrationController.prototype.getDataAnnouncementLand = function () {
        var _this = this;
        this._jobSupplyOfLandService.GetDataForAnnouncementLand().then(function (res) {
            console.log('GetDataForAnnouncementLand-->', res);
            _this.AnnouncementLandList = res.Table;
        });
    };
    LandRegistrationController.prototype.getDataProvince = function () {
        var _this = this;
        this._masterDataService.getDataDistrictAndProvince().then(function (res) {
            console.log('GetDataProvince-->', res);
            _this.provinceList = res.data.province;
        });
    };
    ///***** Province
    LandRegistrationController.prototype.searchTextChange = function (item) {
        console.log('itemmmmmmmmmmmmmm', item);
    };
    LandRegistrationController.prototype.selectedItemChangeProvince = function (item) {
        // console.log('selectedItemChange ', item);
        if (item) {
            this.configProvinceAutoComplete.selectedItem = item;
        }
    };
    LandRegistrationController.prototype.querySearchProvince = function (query) {
        var results = query ? this.provinceList.filter(this.createFilterForProvince(query)) : this.provinceList;
        return results;
    };
    LandRegistrationController.prototype.createFilterForProvince = function (query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    };
    ///******* end autocomplete *****////
    LandRegistrationController.prototype.initDataSearch = function () {
        var _this = this;
        this._dialogService.showBusyBottomSheet();
        var tmpProvince = (this.configProvinceAutoComplete.selectedItem) ? this.configProvinceAutoComplete.selectedItem.ID : "";
        this._jobSupplyOfLandService.GetDataForLandRegistration(this.modelSearch.ANNOUNCEMENTID, this.modelSearch.PROJECT_NAME, this.modelSearch.DEED_NO_FROM, this.modelSearch.DEED_NO_TO, tmpProvince).then(function (res) {
            res.Table.forEach(function (item) {
                item.PROJECT = item.PROJECT_CODE + "," + item.PROJECT_NAME;
                item.SIZE_IN_DEED_RAI_NGAN_WA = item.SIZE_IN_DEED_RAI + "-" + item.SIZE_IN_DEED_NGAN + "-" + item.SIZE_IN_DEED_WA;
                item.TRANSFER_DATE = _this._common.formatDateThai(_this._common.formatDate(item.TRANSFER_DATE, "YYYY-MM-DD"), "dd MMMM yyyy");
            });
            _this.gridOptions.dataSource.data(res.Table);
        }).finally(function () {
            _this._dialogService.hideBusyBottomSheet();
        });
    };
    LandRegistrationController.prototype.add = function () {
        this.isshowSearch = false;
    };
    LandRegistrationController.prototype.search = function () {
        this.initDataSearch();
    };
    LandRegistrationController.prototype.clear = function () {
        this.modelSearch = {};
        this.configProvinceAutoComplete.selectedItem = undefined;
        this.initDataSearch();
    };
    LandRegistrationController.prototype.goBack = function () {
        this.isshowSearch = true;
    };
    return LandRegistrationController;
}());
//# sourceMappingURL=land-registration.component.js.map