﻿class payTaxLandController implements ng.IController {
    constructor(
        private _dialogService: DialogService,
        private _JobSupplyOfLandService: JobSupplyOfLandService,
        private dialog: DialogService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService, ) {
    }

    private gridOptions: any;
    private dataitem = [];
    private kendoGrid: any;
    private Tax_Year: any;
    private ProvinceList: any;

    private Year: any;
    private Project_Id: any;
    private Deed_Id_From: any;
    private Deed_Id_To: any;
    private Province: any;
    private Department_pay_tax: any;
    private POSITION_LEVEL_LIST: any;
    private Mode: any;

    public configProvinceAutoComplete = {
        isDisabled: false,
        noCache: true,
        selectedItem: undefined,
        searchText: undefined,
    };

    public configMultiSelect_POSITION_LEVEL = {
        POSITION_LEVEL: [],
        searchText: '',
    };

    ///***** Province
    public searchTextChange(item: string) {
        //console.log('itemmmmmmmmmmmmmm', item)
    }

    public selectedItemChangeProvince(item) {
        // console.log('selectedItemChange ', item);
        if (item) {
            this.configProvinceAutoComplete.selectedItem = item;
        }
    }
    public querySearchProvince(query) {
        var results = query ? this.ProvinceList.filter(this.createFilterForProvince(query)) : this.ProvinceList;
        return results;

    }
    public createFilterForProvince(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(item) {
            return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    }
    ///******* end autocomplete *****////

    $onInit() {
        this.Mode = 1;
        this.gridOptions = this.createGridOptions(null);
        this.Tax_Year = [];
        this.ProvinceList = [];
        //this.getLevelData();
        this.InitCriteria();

        angular.element('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });
    }

    private getLevelData() {
        let arr = [];
        for (let i = 0; i < 10; i++) {
            arr.push({ ID: i + 1, NAME: "TEST" + (i + 1) });
        }

        //this.POSITION_LEVEL_LIST = arr;
        //this.$http.get(this.webConfigService.getBaseUrl() + `api/report/GetLevel`)
        //.then((res: any) => {
        //this.levelDataList = res.data.Table;
        //let firstItem = { ID: "", ANSWER_DATA: "ทั้งหมด" };
        //this.levelDataList.splice(0, 0, firstItem);
        //});
    }

    private createGridOptions(dataSource: any = null) {
        let template1 = `<div  layout="row" layout-wrap>
                        <div ng-click="ctrl.onView(dataItem)"><img ng-src="Content/btn/view_table_btn.png"></div>
                        <div ng-click="ctrl.onEdit(dataItem)"><img ng-src="Content/btn/edit_table_btn.png"></div>
                        <div ng-click="ctrl.onDelete(dataItem)"><img ng-src="Content/btn/delete_table_btn.png"></div>
                        </div>`;

        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "จัดการ", template: template1,
                    attributes: { class: "text-center", "data-title": "จัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "PROJECT_NAME",
                    title: "รหัสโครงการ,ชื่อโครงการ",
                    width: "auto",
                    attributes: { class: "text-center" },

                },
                {
                    field: "DEED_NO",
                    title: "โฉนดเลขที่",
                    width: "auto",
                    attributes: { class: "text-center" }
                },
                {
                    field: "ANNUM_YEAR",
                    title: "ปีภาษี",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "PROVINCE_NAME_TH",
                    title: "จังหวัด",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "department",
                    title: "หน่วยงานที่รับชำระภาษี",
                    width: "auto",
                    attributes: { class: "text-center" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;
                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }

    private Default() {
        console.log('default===>');
        let data = [];
        let datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource)
    }

    private InitCriteria() {
        console.log('InitCriteria===>');
        this._JobSupplyOfLandService.GetCriteriaForSearchPayTaxLand().then((res) => {
            console.log('resxxx===>', res);
            this.Tax_Year = res.Table;

            this.ProvinceList = res.Table1;
            
            this.POSITION_LEVEL_LIST = res.Table2;
        })
    }

    private Search() {
        if (this.Year == undefined){
            this.Year = "";
        }
        if (this.Project_Id == undefined) {
            this.Project_Id = "";
        }
        if (this.Deed_Id_From == undefined) {
            this.Deed_Id_From = "";
        }
        if (this.Deed_Id_To == undefined) {
            this.Deed_Id_To = "";
        }
        if (this.Province == undefined) {
            this.Province = "";
        }
        
        this._JobSupplyOfLandService.GetDataForPayTaxLand(this.Year, this.Project_Id, this.Deed_Id_From, this.Deed_Id_To, this.configProvinceAutoComplete.selectedItem.ID, "").then((res) => {
            console.log('Search==>', res);
            this.dataitem = res.Table

            let dataSource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
            this.kendoGrid.setDataSource(dataSource)
        })
    }

    private Add() {
        this.paramObj = [];
        this.Mode = 2;
        this.paramObj.push({
            item: {},
            mode: "add"
        });
        console.log('Add===>');
    }

    private onDelete(dataItem) {
        this.dialog.showCustomConfirm("ยืนยันการลบข้อมูล", "ต้องการลบข้อมูลหรือไม่", dataItem, this.Delete, undefined);
        console.log("onDelete===>", dataItem);
    }

    private Delete = (dataItem) => {
        debugger
        this.$http.get(`api/JobSupplyOfLand/DeleteDataTaxLand?id_p_tax_land=${dataItem["ID_P_TAX_LAND"]}`).then((res: any) => {
            console.log('dataItem["ID_P_TAX_LAND"]===>',dataItem["ID_P_TAX_LAND"]);
            if (res.status == 200) {
                this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ลบข้อมูลเรียบร้อย", undefined, undefined);
                this.Search();
            }
            else {
                this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ไม่สามารถลบข้อมูลได้", undefined, undefined);
            }
        })
        console.log("Delete", dataItem);
        return true;
    }

    private paramObj: any;

    private onView(dataItem) {
        this.paramObj = [];
        this.Mode = 2;
        console.log("onView--->", dataItem);
        this.paramObj.push({
            item: dataItem,
            mode: "view"
        });
    }

    private onEdit(dataItem) {
        this.paramObj = [];
        this.Mode = 2;
        console.log("onEdit--->", dataItem);
        this.paramObj.push({
            item: dataItem,
            mode: "edit"
        });
    }
}