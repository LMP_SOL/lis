﻿class InsertDataPayTaxLandController implements ng.IController {
    constructor(
        private _dialogService: DialogService,
        private _JobSupplyOfLandService: JobSupplyOfLandService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService, ) {
    }
    private object;
    private test;
    private gridOptions: any;


    $onInit() {
        this.gridOptions = this.createGridOptions(null);
        console.log('object===>', this.object);
        this.test = this.object[0].mode;
    }

    private createGridOptions(dataSource: any = null) {
        let template1 = `<div  layout="row" layout-wrap>
                        <div ng-click="ctrl.onDelete(dataItem)"><img ng-src="Content/btn/delete_table_btn.png"></div>
                        </div>`;

        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "จัดการ", template: template1,
                    attributes: { class: "text-center", "data-title": "จัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "PROJECT_NAME",
                    title: "เลขที่ดิน",
                    attributes: { class: "text-center" },

                },
                {
                    field: "DEED_ID",
                    title: "โฉนดเลขที่",
                    attributes: { class: "text-center" }
                },
                {
                    field: "ANNUM_YEAR",
                    title: "หน้าสำรวจ",
                    attributes: { class: "text-center" },
                },
                {
                    field: "PROVINCE_NAME_TH",
                    title: "เลขระวาง",
                    attributes: { class: "text-center" },
                },
                {
                    field: "department",
                    title: "ขนาดที่ดิน(ไร่ งาน วา)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "department",
                    title: "ราคาประเมินที่ดิน(บาท/ตร.ว.)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "department",
                    title: "มูลค่าที่ดิน (บาท)",
                    attributes: { class: "text-center" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;
                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }

}