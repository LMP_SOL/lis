var payTaxLandController = (function () {
    function payTaxLandController(_dialogService, _JobSupplyOfLandService, dialog, $location, $http) {
        var _this = this;
        this._dialogService = _dialogService;
        this._JobSupplyOfLandService = _JobSupplyOfLandService;
        this.dialog = dialog;
        this.$location = $location;
        this.$http = $http;
        this.dataitem = [];
        this.configProvinceAutoComplete = {
            isDisabled: false,
            noCache: true,
            selectedItem: undefined,
            searchText: undefined,
        };
        this.configMultiSelect_POSITION_LEVEL = {
            POSITION_LEVEL: [],
            searchText: '',
        };
        this.Delete = function (dataItem) {
            debugger;
            _this.$http.get("api/JobSupplyOfLand/DeleteDataTaxLand?id_p_tax_land=" + dataItem["ID_P_TAX_LAND"]).then(function (res) {
                console.log('dataItem["ID_P_TAX_LAND"]===>', dataItem["ID_P_TAX_LAND"]);
                if (res.status == 200) {
                    _this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ลบข้อมูลเรียบร้อย", undefined, undefined);
                    _this.Search();
                }
                else {
                    _this.dialog.showCustomAlert("ข้อความจากระบบ", "OK", "ไม่สามารถลบข้อมูลได้", undefined, undefined);
                }
            });
            console.log("Delete", dataItem);
            return true;
        };
    }
    ///***** Province
    payTaxLandController.prototype.searchTextChange = function (item) {
        //console.log('itemmmmmmmmmmmmmm', item)
    };
    payTaxLandController.prototype.selectedItemChangeProvince = function (item) {
        // console.log('selectedItemChange ', item);
        if (item) {
            this.configProvinceAutoComplete.selectedItem = item;
        }
    };
    payTaxLandController.prototype.querySearchProvince = function (query) {
        var results = query ? this.ProvinceList.filter(this.createFilterForProvince(query)) : this.ProvinceList;
        return results;
    };
    payTaxLandController.prototype.createFilterForProvince = function (query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    };
    ///******* end autocomplete *****////
    payTaxLandController.prototype.$onInit = function () {
        this.Mode = 1;
        this.gridOptions = this.createGridOptions(null);
        this.Tax_Year = [];
        this.ProvinceList = [];
        //this.getLevelData();
        this.InitCriteria();
        angular.element('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });
    };
    payTaxLandController.prototype.getLevelData = function () {
        var arr = [];
        for (var i = 0; i < 10; i++) {
            arr.push({ ID: i + 1, NAME: "TEST" + (i + 1) });
        }
        //this.POSITION_LEVEL_LIST = arr;
        //this.$http.get(this.webConfigService.getBaseUrl() + `api/report/GetLevel`)
        //.then((res: any) => {
        //this.levelDataList = res.data.Table;
        //let firstItem = { ID: "", ANSWER_DATA: "ทั้งหมด" };
        //this.levelDataList.splice(0, 0, firstItem);
        //});
    };
    payTaxLandController.prototype.createGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template1 = "<div  layout=\"row\" layout-wrap>\n                        <div ng-click=\"ctrl.onView(dataItem)\"><img ng-src=\"Content/btn/view_table_btn.png\"></div>\n                        <div ng-click=\"ctrl.onEdit(dataItem)\"><img ng-src=\"Content/btn/edit_table_btn.png\"></div>\n                        <div ng-click=\"ctrl.onDelete(dataItem)\"><img ng-src=\"Content/btn/delete_table_btn.png\"></div>\n                        </div>";
        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "จัดการ", template: template1,
                    attributes: { class: "text-center", "data-title": "จัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "PROJECT_NAME",
                    title: "รหัสโครงการ,ชื่อโครงการ",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "DEED_NO",
                    title: "โฉนดเลขที่",
                    width: "auto",
                    attributes: { class: "text-center" }
                },
                {
                    field: "ANNUM_YEAR",
                    title: "ปีภาษี",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "PROVINCE_NAME_TH",
                    title: "จังหวัด",
                    width: "auto",
                    attributes: { class: "text-center" },
                },
                {
                    field: "department",
                    title: "หน่วยงานที่รับชำระภาษี",
                    width: "auto",
                    attributes: { class: "text-center" },
                }
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    payTaxLandController.prototype.Default = function () {
        console.log('default===>');
        var data = [];
        var datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource);
    };
    payTaxLandController.prototype.InitCriteria = function () {
        var _this = this;
        console.log('InitCriteria===>');
        this._JobSupplyOfLandService.GetCriteriaForSearchPayTaxLand().then(function (res) {
            console.log('resxxx===>', res);
            _this.Tax_Year = res.Table;
            _this.ProvinceList = res.Table1;
            _this.POSITION_LEVEL_LIST = res.Table2;
        });
    };
    payTaxLandController.prototype.Search = function () {
        var _this = this;
        if (this.Year == undefined) {
            this.Year = "";
        }
        if (this.Project_Id == undefined) {
            this.Project_Id = "";
        }
        if (this.Deed_Id_From == undefined) {
            this.Deed_Id_From = "";
        }
        if (this.Deed_Id_To == undefined) {
            this.Deed_Id_To = "";
        }
        if (this.Province == undefined) {
            this.Province = "";
        }
        this._JobSupplyOfLandService.GetDataForPayTaxLand(this.Year, this.Project_Id, this.Deed_Id_From, this.Deed_Id_To, this.configProvinceAutoComplete.selectedItem.ID, "").then(function (res) {
            console.log('Search==>', res);
            _this.dataitem = res.Table;
            var dataSource = new kendo.data.DataSource({ data: _this.dataitem, pageSize: 10 });
            _this.kendoGrid.setDataSource(dataSource);
        });
    };
    payTaxLandController.prototype.Add = function () {
        this.paramObj = [];
        this.Mode = 2;
        this.paramObj.push({
            item: {},
            mode: "add"
        });
        console.log('Add===>');
    };
    payTaxLandController.prototype.onDelete = function (dataItem) {
        this.dialog.showCustomConfirm("ยืนยันการลบข้อมูล", "ต้องการลบข้อมูลหรือไม่", dataItem, this.Delete, undefined);
        console.log("onDelete===>", dataItem);
    };
    payTaxLandController.prototype.onView = function (dataItem) {
        this.paramObj = [];
        this.Mode = 2;
        console.log("onView--->", dataItem);
        this.paramObj.push({
            item: dataItem,
            mode: "view"
        });
    };
    payTaxLandController.prototype.onEdit = function (dataItem) {
        this.paramObj = [];
        this.Mode = 2;
        console.log("onEdit--->", dataItem);
        this.paramObj.push({
            item: dataItem,
            mode: "edit"
        });
    };
    return payTaxLandController;
}());
//# sourceMappingURL=pay-tax-land.component.js.map