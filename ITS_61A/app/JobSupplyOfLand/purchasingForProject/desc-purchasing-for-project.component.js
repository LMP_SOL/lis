var DescPurchasingForProjectController = (function () {
    function DescPurchasingForProjectController(_dialogService, _JobSupplyOfLandService, $location, $http) {
        this._dialogService = _dialogService;
        this._JobSupplyOfLandService = _JobSupplyOfLandService;
        this.$location = $location;
        this.$http = $http;
    }
    DescPurchasingForProjectController.prototype.$onInit = function () {
        this.gridOptions = this.createGridOptions(null);
    };
    DescPurchasingForProjectController.prototype.createGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template1 = "<div  layout=\"row\" layout-wrap>\n                        <div ng-click=\"ctrl.onEdit(dataItem)\"><img ng-src=\"Content/btn/edit_table_btn.png\"></div>\n                        <div ng-click=\"ctrl.onChangePass(dataItem)\"><img ng-src=\"Content/btn/change_password_btn.png\"></div>\n                        </div>";
        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "Deed_No",
                    title: "โฉนดเลขที่",
                    attributes: { class: "text-center" },
                },
                {
                    field: "Pacel_No",
                    title: "เลขที่ดิน",
                    attributes: { class: "text-center" }
                },
                {
                    field: "Dealing_File_No",
                    title: "หน้าสำรวจ",
                    attributes: { class: "text-center" },
                },
                {
                    field: "Area(Rai_Ngan_Wa)",
                    title: "เนื่อที่(ไร่-งาน-วา)",
                    attributes: { class: "text-center" },
                },
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    DescPurchasingForProjectController.prototype.Back = function () {
    };
    return DescPurchasingForProjectController;
}());
//# sourceMappingURL=desc-purchasing-for-project.component.js.map