﻿class p_land_project {
    public ID: any
    public ANNOUNCEMENT_LAND_ID: any
    public AGENCY_RESPONSIBLE_ID: any
    public AGENCY_PURCHASE_ID: any
    public CONSIDER_DATE: any
    public BOARD_DATE: any
    public PO_NO: any
    public WBS_ID : any
    public PRICE_PER_RAI: any
    public LAND_VALUE: any
    public CONSTRUCTION_PERIOD_MONTH: any
    public UNIT_VALUE: any
    public UNIT_PRICE_VALUE: any
    public LAND_VALUE_1_AMOUNT: any
    public LAND_VALUE_1_PERCENT: any
    public LAND_VALUE_2_AMOUNT: any
    public LAND_VALUE_2_PERCENT: any
    public LAND_VALUE_2_PAYMENT: any
    public LAND_VALUE_2_PAYMENT_PERCENT: any
    public LAND_VALUE_2_LAST: any
    public LAND_VALUE_2_LAST_PERCENT: any
    public LAND_VALUE_2_REST: any
    public LAND_VALUE_2_REST_PERCENT: any
    public TOTAL_LAND: any
    public GUARANTEE_DOCUMENT: any
    public GUARANTEE_NO: any
    public GUARANTEE_DATE: any
    public GUARANTEE_AMOUNT: any
    public IS_GUARANTEE_ROLLBACK: any
    public GUARANTEE_ROLLBACK_DATE: any
    public TR_OF_OWNERSHIP_AREA_RAI: any
    public TR_OF_OWNERSHIP_AREA_NGAN: any
    public TR_OF_OWNERSHIP_AREA_WA: any
    public REMARK: any
    public MEASURE_AREA_ID: any
    public OVER_LAND_RAI: any
    public OVER_LAND_NGAN: any
    public OVER_LAND_WA: any
    public IS_PROJECT_CANCEL: any
    public PROJECT_CANCEL_DATE: any
    public DESC_PROJECT_CANCEL: any
    public CREATE_BY: any
    public CREATE_DATE: any
    public UPDATE_BY: any
    public UPDATE_DATE: any
    public ISUSE: any
    public BOARD_NO: any
}

class d_deed {

    PROJECT_ID
    DEED_NO
    DEALING_FILE_NO
    PARCEL_NO
    HOUSE_NO
    HOUSE_NO2
    LOCATION_IN_DEED
    SUBDISTRICT_ID
    DISTRICT_ID
    PROVINCE_ID
    MAPSHEET_OLD
    MAPSHEET_NEW
    SIZE_IN_DEED_RAI
    SIZE_IN_DEED_NGAN
    SIZE_IN_DEED_WA
    USED_SIZE_RAI
    USED_SIZE_NGAN
    USED_SIZE_WA
    LAND_DESC
    FUNCTION_ID
    CONTRACTOR_TYPE_ID
    PERFIX_ID
    NAME_TH
    LASTNAME_TH
    ID_CARD
    OFFICE_NAME
    ATTORNEY
    ATTORNEY_ID_CARD
    TRANSFER_STATUS_ID
    TRANSFER_DATE
    EDITOR_NAME
    TRANSFEROR_ID
    TRANSFEROR_NAME
    LAND_USE_TYPE_ID
    PRICE_PER_WA
    RENTAL_PRICE
    CONTRACT_DATE
    CONTRACT_END_DATE
    DURATION
    OBJECTIVE_DETAIL
    IS_SERVIENT_PROPERTY
    SERVIENT_PROPERTY_DESC
    IS_DOMINANT_PROPERTY
    DOMINANT_PROPERTY_DESC
    IS_OBLIGATION
    OBLIGATION_DESC
    IS_ETC
    ETC_DESC
    IS_REGISTER_RENTAL
    REGISTER_RENTAL_DESC
    REMARK
    CREATE_BY
    CREATE_DATE
    UPDATE_BY
    UPDATE_DATE
    PARENT_DEED_ID
   

}