﻿class DescPurchasingForProjectController implements ng.IController {
    constructor(

        private _dialogService: DialogService,
        private _JobSupplyOfLandService: JobSupplyOfLandService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService, ) {

    }

    private gridOptions: any;
    private kendoGrid: any;
    private grid: any;
    private mode: any;

    $onInit() {
        this.gridOptions = this.createGridOptions(null);
    }

    private createGridOptions(dataSource: any = null) {
        let template1 = `<div  layout="row" layout-wrap>
                        <div ng-click="ctrl.onEdit(dataItem)"><img ng-src="Content/btn/edit_table_btn.png"></div>
                        <div ng-click="ctrl.onChangePass(dataItem)"><img ng-src="Content/btn/change_password_btn.png"></div>
                        </div>`;

        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "Deed_No",
                    title: "โฉนดเลขที่",
                    attributes: { class: "text-center" },

                },
                {
                    field: "Pacel_No",
                    title: "เลขที่ดิน",
                    attributes: { class: "text-center" }
                },
                {
                    field: "Dealing_File_No",
                    title: "หน้าสำรวจ",
                    attributes: { class: "text-center" },
                },
                {
                    field: "Area(Rai_Ngan_Wa)",
                    title: "เนื่อที่(ไร่-งาน-วา)",
                    attributes: { class: "text-center" },
                },
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;
                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }
    private Back() {
       
    }
}