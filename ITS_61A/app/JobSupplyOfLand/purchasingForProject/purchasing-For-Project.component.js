var PurchasingForProjectController = (function () {
    function PurchasingForProjectController(_dialogService, _JobSupplyOfLandService, $location, $http) {
        this._dialogService = _dialogService;
        this._JobSupplyOfLandService = _JobSupplyOfLandService;
        this.$location = $location;
        this.$http = $http;
        this.Status = null;
        this.dataitem = [];
        this.Mode = 0;
        this.Delete = function (dataItem) {
            //this.$http.get(`api/_JobSupplyOfLandService/DeleteDataGroupPrivilege?group_id=${dataItem["RAWTOHEX(ID)"]}&group_update_by=${dataItem["RAWTOHEX(CREATE_BY)"]}`).then((res: any) => {
            //    console.log(res);
            //    if (res.status == 200) {
            //        this._dialogService.showCustomAlert("ข้อความจากระบบ", "OK", "ลบข้อมูลเรียบร้อย", undefined, undefined);
            //        this.Search();
            //    }
            //    else {
            //        this._dialogService.showCustomAlert("ข้อความจากระบบ", "OK", "ไม่สามารถลบข้อมูลได้", undefined, undefined);
            //    }
            //})
            //console.log("Delete", dataItem);
            //return true;
        };
    }
    PurchasingForProjectController.prototype.$onInit = function () {
        this.Mode = 0;
        this.gridOptions = this.createGridOptions(null);
        this.ProvinceList = [];
        this.AnnouncementsList = [];
    };
    PurchasingForProjectController.prototype.InitCriteria = function () {
        var _this = this;
        this._JobSupplyOfLandService.getCriteriaforsearcPurchasingProject().then(function (res) {
            console.log('rexxxx', res);
            _this.ProvinceList = res.Table;
            _this.AnnouncementsList = res.Table1;
        });
    };
    PurchasingForProjectController.prototype.createGridOptions = function (dataSource) {
        if (dataSource === void 0) { dataSource = null; }
        var template1 = "<div  layout=\"row\" layout-wrap>\n                        <div ng-click=\"ctrl.onView(dataItem)\"><img ng-src=\"Content/btn/view_table_btn.png\"></div>\n                        <div ng-click=\"ctrl.onEdit(dataItem)\"><img ng-src=\"Content/btn/edit_table_btn.png\"></div>\n                        <div ng-click=\"ctrl.onDelete(dataItem)\"><img ng-src=\"Content/btn/delete_table_btn.png\"></div>\n                        </div>";
        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "จัดการ", template: template1,
                    attributes: { class: "text-center", "data-title": "จัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "ANNOUNCEMENT_NO",
                    title: "ประกาศฉบับบที่",
                    attributes: { class: "text-center" },
                },
                {
                    field: "PROJECT_ID",
                    title: "รหัสโครงการ,ชื่อโครงการ",
                    attributes: { class: "text-center" }
                },
                {
                    field: "PROVINCE_NAME",
                    title: "จังหวัด",
                    attributes: { class: "text-center" },
                },
                {
                    field: "SIZE_IN_DEED",
                    title: "ขนาดพื้นที่(ไร่-งาน-วา)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "PRICE_PER_RAI",
                    title: "ราคาที่ซื้อ(บาท/ไร่)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "PRICE_SUM_PROJECT",
                    title: "ค่าที่ดินทั้งโครงการ(บาท)",
                    attributes: { class: "text-center" },
                },
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                var isPageable = this.options.pageable;
                var currentPage = 0;
                var itemPerPage = 0;
                var lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                var rows = this.items();
                for (var i = 0; i < rows.length; i++) {
                    var row = angular.element(rows[i]);
                    var currentIndex = i + 1;
                    var rowNum = void 0;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    var span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        };
    };
    PurchasingForProjectController.prototype.Search = function () {
        var _this = this;
        this._JobSupplyOfLandService.GetDataForSearchPurchasingProject(this.PROJECT_ID, this.PROVINCE_ID, this.ANNOUNCEMENTS_NO, this.DEED_NO_FROM, this.DEED_NO_TO, this.PARCEL_NO, this.DEALING_FILE_NO).then(function (res) {
            console.log('Search==>', res);
            _this.dataitem = res.Table;
            var dataSource = new kendo.data.DataSource({ data: _this.dataitem, pageSize: 10 });
            _this.kendoGrid.setDataSource(dataSource);
        });
    };
    PurchasingForProjectController.prototype.Default = function () {
        this.PROJECT_ID = "";
        this.PROVINCE_ID = "";
        this.ANNOUNCEMENTS_NO = "";
        this.DEED_NO_FROM = "";
        this.DEED_NO_TO = "";
        this.PARCEL_NO = "";
        this.DEALING_FILE_NO = "";
        var data = [];
        var datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource);
    };
    PurchasingForProjectController.prototype.InsertData = function () {
    };
    PurchasingForProjectController.prototype.onEdit = function (dataItem) {
        console.log("EditMode");
    };
    PurchasingForProjectController.prototype.onView = function (dataItem) {
        console.log("ViewMode");
    };
    PurchasingForProjectController.prototype.onDelete = function (dataItem) {
        console.log("onDelete===>", dataItem);
        this._dialogService.showCustomConfirm("ยืนยันการลบข้อมูล", "ต้องการลบข้อมูลหรือไม่", dataItem, this.Delete, undefined);
    };
    return PurchasingForProjectController;
}());
//# sourceMappingURL=purchasing-For-Project.component.js.map