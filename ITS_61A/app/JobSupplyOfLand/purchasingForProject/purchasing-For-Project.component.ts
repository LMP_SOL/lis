﻿class PurchasingForProjectController implements ng.IController {
    constructor(

        private _dialogService: DialogService,
        private _JobSupplyOfLandService: JobSupplyOfLandService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService, ) {

    }

    private gridOptions: any;
    private kendoGrid: any;
    private grid: any;
    private Status = null;
    private dataitem = [];
    private Mode = 0;
    private PROJECT_ID: any;
    private PROVINCE_ID: any;
    private ANNOUNCEMENTS_NO: any;
    private DEED_NO_FROM: any;
    private DEED_NO_TO: any;
    private PARCEL_NO: any;
    private DEALING_FILE_NO: any;

    private ProvinceList: any;
    private AnnouncementsList: any;


    

    $onInit() {
        this.Mode = 0;
        this.gridOptions = this.createGridOptions(null);
        this.ProvinceList = [];
        this.AnnouncementsList = [];

    }

    private InitCriteria() {
        this._JobSupplyOfLandService.getCriteriaforsearcPurchasingProject().then((res) => {
            console.log('rexxxx', res);

            this.ProvinceList = res.Table;

            this.AnnouncementsList = res.Table1;
        })
    }

    private createGridOptions(dataSource: any = null) {
        let template1 = `<div  layout="row" layout-wrap>
                        <div ng-click="ctrl.onView(dataItem)"><img ng-src="Content/btn/view_table_btn.png"></div>
                        <div ng-click="ctrl.onEdit(dataItem)"><img ng-src="Content/btn/edit_table_btn.png"></div>
                        <div ng-click="ctrl.onDelete(dataItem)"><img ng-src="Content/btn/delete_table_btn.png"></div>
                        </div>`;
    
        return {
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "management",
                    title: "จัดการ", template: template1,
                    attributes: { class: "text-center", "data-title": "จัดการ" }
                },
                {
                    field: "rowNumber",
                    title: "ลำดับ", template: '<span class="row-number"></span>',
                    attributes: { class: "text-center", "data-title": "ลำดับ" }
                },
                {
                    field: "ANNOUNCEMENT_NO",
                    title: "ประกาศฉบับบที่",
                    attributes: { class: "text-center" },

                },
                {
                    field: "PROJECT_ID",
                    title: "รหัสโครงการ,ชื่อโครงการ",
                    attributes: { class: "text-center" }
                },
                {
                    field: "PROVINCE_NAME",
                    title: "จังหวัด",
                    attributes: { class: "text-center" },
                },
                {
                    field: "SIZE_IN_DEED",
                    title: "ขนาดพื้นที่(ไร่-งาน-วา)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "PRICE_PER_RAI",
                    title: "ราคาที่ซื้อ(บาท/ไร่)",
                    attributes: { class: "text-center" },
                },
                {
                    field: "PRICE_SUM_PROJECT",
                    title: "ค่าที่ดินทั้งโครงการ(บาท)",
                    attributes: { class: "text-center" },
                },
            ],
            noRecords: true,
            messages: { noRecords: "ไม่พบข้อมูล" },
            dataBound: function () {
                let isPageable = this.options.pageable;
                let currentPage = 0;
                let itemPerPage = 0;
                let lastIndex = 0;
                if (isPageable) {
                    currentPage = this.dataSource.page();
                    itemPerPage = this.dataSource.pageSize();
                    lastIndex = (currentPage - 1) * itemPerPage;
                }
                let rows = this.items();
                for (let i = 0; i < rows.length; i++) {
                    let row = angular.element(rows[i]);
                    let currentIndex = i + 1;
                    let rowNum;
                    if (isPageable) {
                        rowNum = lastIndex + currentIndex;
                    }
                    else {
                        rowNum = currentIndex;
                    }
                    let span = row.find(".row-number");
                    span.html(rowNum.toString());
                }
            }
        }
    }


    private Search() {
        this._JobSupplyOfLandService.GetDataForSearchPurchasingProject(this.PROJECT_ID, this.PROVINCE_ID, this.ANNOUNCEMENTS_NO, this.DEED_NO_FROM, this.DEED_NO_TO, this.PARCEL_NO, this.DEALING_FILE_NO).then((res) => {
            console.log('Search==>', res);
            this.dataitem = res.Table

            let dataSource = new kendo.data.DataSource({ data: this.dataitem, pageSize: 10 });
            this.kendoGrid.setDataSource(dataSource)
            
        })
    }

    private Default() {
        this.PROJECT_ID = "";
        this.PROVINCE_ID = "";
        this.ANNOUNCEMENTS_NO = "";
        this.DEED_NO_FROM = "";
        this.DEED_NO_TO = "";
        this.PARCEL_NO = "";
        this.DEALING_FILE_NO = "";
        let data = []
        let datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
        this.kendoGrid.setDataSource(datasource)

    }

    private InsertData(){

    }

    private onEdit(dataItem) {
        console.log("EditMode");

    }

    private onView(dataItem) {
        console.log("ViewMode");

    }

    private onDelete(dataItem) {
        console.log("onDelete===>", dataItem);
        this._dialogService.showCustomConfirm("ยืนยันการลบข้อมูล", "ต้องการลบข้อมูลหรือไม่", dataItem, this.Delete, undefined);
    }

    private Delete = (dataItem) => {
        //this.$http.get(`api/_JobSupplyOfLandService/DeleteDataGroupPrivilege?group_id=${dataItem["RAWTOHEX(ID)"]}&group_update_by=${dataItem["RAWTOHEX(CREATE_BY)"]}`).then((res: any) => {
        //    console.log(res);
        //    if (res.status == 200) {
        //        this._dialogService.showCustomAlert("ข้อความจากระบบ", "OK", "ลบข้อมูลเรียบร้อย", undefined, undefined);
        //        this.Search();
        //    }
        //    else {
        //        this._dialogService.showCustomAlert("ข้อความจากระบบ", "OK", "ไม่สามารถลบข้อมูลได้", undefined, undefined);
        //    }
        //})
        //console.log("Delete", dataItem);
        //return true;
    }

    
}