﻿class AnnouncementManagementController implements ng.IController {

    public mode: number;// o = manage ,1 = add, 2 = edit, 3 = view  
    public announcementSelectedId: string;
    public modelSearch: announcementSearchModel;
    public formSearch: any;
    public clearSelectionArea: any;
    public listData: any;

    constructor(private _dialogService: DialogService,
        private _jobSupplyOfLandService: JobSupplyOfLandService,
        private _masterDataService: MasterDataService,
        private _commonService: CommonService,
        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
        private $element: ng.IRootElementService,


    ) {

    }

    $onInit() {
        this.modelSearch = new announcementSearchModel();
        this.mode = 0;
        this.initDataSearch_open();

    }

    public addAnnouncement() {
        this.mode = 1;
    }
    public editAnnouncement() {
        this.mode = 2;
    }
    public viewAnnouncement() {
        this.mode = 3;
    }



    public initDataSearch = () => {

        console.log(' this.formSearch', this.formSearch);

        if (this.formSearch.$valid) {
            this.modelSearch.NOTICE_DATE = this.modelSearch.NOTICE_DATE ? this._commonService.formatDate(this.modelSearch.NOTICE_DATE, "") : null;
            this.modelSearch.NOTICE_DATE_TO = this.modelSearch.NOTICE_DATE_TO ? this._commonService.formatDate(this.modelSearch.NOTICE_DATE_TO, "") : null;
            this.modelSearch.OFFER_SALE_DATE = this.modelSearch.OFFER_SALE_DATE ? this._commonService.formatDate(this.modelSearch.OFFER_SALE_DATE, "") : null;
            this.modelSearch.OFFER_SALE_DATE_TO = this.modelSearch.OFFER_SALE_DATE_TO ? this._commonService.formatDate(this.modelSearch.OFFER_SALE_DATE_TO, "") : null;

            console.log(' this.modelSearch', this.modelSearch);
            this._jobSupplyOfLandService.GetDataSearchAnnouncementLand(this.modelSearch).then((res) => {
                console.log(' result search', res);
                this.listData = res.announcements;

                this.gridOptions.dataSource.data(this.listData);

            }).catch((err) => {

            }).finally(() => {

            })
        } else {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert, "ตกลง", "กรุณาเลือกข้อมูลค้นหาให้ครบ!", "", "");
        }


    }
    public initDataSearch_open = () => {

       
            this.modelSearch.NOTICE_DATE = this.modelSearch.NOTICE_DATE ? this._commonService.formatDate(this.modelSearch.NOTICE_DATE, "") : null;
            this.modelSearch.NOTICE_DATE_TO = this.modelSearch.NOTICE_DATE_TO ? this._commonService.formatDate(this.modelSearch.NOTICE_DATE_TO, "") : null;
            this.modelSearch.OFFER_SALE_DATE = this.modelSearch.OFFER_SALE_DATE ? this._commonService.formatDate(this.modelSearch.OFFER_SALE_DATE, "") : null;
            this.modelSearch.OFFER_SALE_DATE_TO = this.modelSearch.OFFER_SALE_DATE_TO ? this._commonService.formatDate(this.modelSearch.OFFER_SALE_DATE_TO, "") : null;

            console.log(' this.modelSearch', this.modelSearch);
            this._jobSupplyOfLandService.GetDataSearchAnnouncementLand(this.modelSearch).then((res) => {
                console.log(' result search', res);
                this.listData = res.announcements;

                this.gridOptions.dataSource.data(this.listData);

            }).catch((err) => {

            }).finally(() => {

            })
    
    }

    public Search() {
        this.initDataSearch();
    }
    public Clear() {
        this.clearSelectionArea.clear();
        this.modelSearch = new announcementSearchModel();
        this.initDataSearch();

    }

    private gridOptions = {
        gridID: 'gridAnnouncementManage',
        selectable: 'row',
        dataSource: this.listDataGrid(),
        scrollable: false,
        sortable: false,
        pageable: true,
        dataBound: function () {
        },
        column: [
            {
                field: "ANNOUNCEMENT_NO",
                title: "ประกาศฉบับที่",
                attributes: {
                    "data-title": "ประกาศฉบับที่",
                    "class": "text-center"
                }
            }
            , {

                template: "<div>{{gmCtrl.setShowDateThai(dataItem.NOTICE_DATE)}}</div>",
                title: "วันที่ประกาศ",
                attributes: {
                    "data-title": "วันที่ประกาศ",
                    "class": "text-center"
                }
            }
            , {
                field: "NOTICE_SUBJECT",
                title: "เรื่อง",
                attributes: {
                    "data-title": "เรื่อง",
                    "class": "text-center"
                }
            }
            , {
                field: "SIZE_IN_DEED",
                title: "ขนาดเนื้อที่ประกาศ (ไร่-งาน-วา)",
                attributes: {
                    "data-title": "ขนาดเนื้อที่ประกาศ (ไร่-งาน-วา)",
                    "class": "text-center"
                }
            }

        ],
        management: true,
        operation: {
            view: true,
            del: true,
            edit: true
        },
        showIndex: true,
        showStatus: false,

    };

    private listDataGrid() {
        this.listData = [];

        return new kendo.data.DataSource({ data: this.listData, pageSize: 10 });
    }

    public gridCallbackEdit = (res) => {
        console.log(' this.gridCallbackEdit', res);
    }
    public gridCallbackDel = (res) => {

        this._dialogService.showCustomConfirm(Constants.messageDialog.confrim_delete_info, "ต้องการลบข้อมูลประกาศฉบับที่ " + res.ANNOUNCEMENT_NO + " หรือไม่?", res, this.CallbackDelOk, this.CallbackDelCancel)
    }
    public gridCallbackView = (res) => {
        console.log(' this.gridCallbackView', res);
    }

    public CallbackDelOk = (res) => {
        console.log(' this.CallbackDelOk', res);
        this._jobSupplyOfLandService.DeleteAnnouncementLand(res.ID).then((del_res) => {
            this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, "", "");

        }).catch((err) => {
            console.log('del_res_err ', err);
            this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
        }).finally(() => {
            this.initDataSearch();
        })
        return true;
    }
    public CallbackDelCancel = (res) => {

        return true;
    }

}