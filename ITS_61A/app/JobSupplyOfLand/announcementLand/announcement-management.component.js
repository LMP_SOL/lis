var AnnouncementManagementController = (function () {
    function AnnouncementManagementController(_dialogService, _jobSupplyOfLandService, _masterDataService, _commonService, $location, $http, $element) {
        var _this = this;
        this._dialogService = _dialogService;
        this._jobSupplyOfLandService = _jobSupplyOfLandService;
        this._masterDataService = _masterDataService;
        this._commonService = _commonService;
        this.$location = $location;
        this.$http = $http;
        this.$element = $element;
        this.initDataSearch = function () {
            console.log(' this.formSearch', _this.formSearch);
            if (_this.formSearch.$valid) {
                _this.modelSearch.NOTICE_DATE = _this.modelSearch.NOTICE_DATE ? _this._commonService.formatDate(_this.modelSearch.NOTICE_DATE, "") : null;
                _this.modelSearch.NOTICE_DATE_TO = _this.modelSearch.NOTICE_DATE_TO ? _this._commonService.formatDate(_this.modelSearch.NOTICE_DATE_TO, "") : null;
                _this.modelSearch.OFFER_SALE_DATE = _this.modelSearch.OFFER_SALE_DATE ? _this._commonService.formatDate(_this.modelSearch.OFFER_SALE_DATE, "") : null;
                _this.modelSearch.OFFER_SALE_DATE_TO = _this.modelSearch.OFFER_SALE_DATE_TO ? _this._commonService.formatDate(_this.modelSearch.OFFER_SALE_DATE_TO, "") : null;
                console.log(' this.modelSearch', _this.modelSearch);
                _this._jobSupplyOfLandService.GetDataSearchAnnouncementLand(_this.modelSearch).then(function (res) {
                    console.log(' result search', res);
                    _this.listData = res.announcements;
                    _this.gridOptions.dataSource.data(_this.listData);
                }).catch(function (err) {
                }).finally(function () {
                });
            }
            else {
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, "ตกลง", "กรุณาเลือกข้อมูลค้นหาให้ครบ!", "", "");
            }
        };
        this.initDataSearch_open = function () {
            _this.modelSearch.NOTICE_DATE = _this.modelSearch.NOTICE_DATE ? _this._commonService.formatDate(_this.modelSearch.NOTICE_DATE, "") : null;
            _this.modelSearch.NOTICE_DATE_TO = _this.modelSearch.NOTICE_DATE_TO ? _this._commonService.formatDate(_this.modelSearch.NOTICE_DATE_TO, "") : null;
            _this.modelSearch.OFFER_SALE_DATE = _this.modelSearch.OFFER_SALE_DATE ? _this._commonService.formatDate(_this.modelSearch.OFFER_SALE_DATE, "") : null;
            _this.modelSearch.OFFER_SALE_DATE_TO = _this.modelSearch.OFFER_SALE_DATE_TO ? _this._commonService.formatDate(_this.modelSearch.OFFER_SALE_DATE_TO, "") : null;
            console.log(' this.modelSearch', _this.modelSearch);
            _this._jobSupplyOfLandService.GetDataSearchAnnouncementLand(_this.modelSearch).then(function (res) {
                console.log(' result search', res);
                _this.listData = res.announcements;
                _this.gridOptions.dataSource.data(_this.listData);
            }).catch(function (err) {
            }).finally(function () {
            });
        };
        this.gridOptions = {
            gridID: 'gridAnnouncementManage',
            selectable: 'row',
            dataSource: this.listDataGrid(),
            scrollable: false,
            sortable: false,
            pageable: true,
            dataBound: function () {
            },
            column: [
                {
                    field: "ANNOUNCEMENT_NO",
                    title: "ประกาศฉบับที่",
                    attributes: {
                        "data-title": "ประกาศฉบับที่",
                        "class": "text-center"
                    }
                },
                {
                    template: "<div>{{gmCtrl.setShowDateThai(dataItem.NOTICE_DATE)}}</div>",
                    title: "วันที่ประกาศ",
                    attributes: {
                        "data-title": "วันที่ประกาศ",
                        "class": "text-center"
                    }
                },
                {
                    field: "NOTICE_SUBJECT",
                    title: "เรื่อง",
                    attributes: {
                        "data-title": "เรื่อง",
                        "class": "text-center"
                    }
                },
                {
                    field: "SIZE_IN_DEED",
                    title: "ขนาดเนื้อที่ประกาศ (ไร่-งาน-วา)",
                    attributes: {
                        "data-title": "ขนาดเนื้อที่ประกาศ (ไร่-งาน-วา)",
                        "class": "text-center"
                    }
                }
            ],
            management: true,
            operation: {
                view: true,
                del: true,
                edit: true
            },
            showIndex: true,
            showStatus: false,
        };
        this.gridCallbackEdit = function (res) {
            console.log(' this.gridCallbackEdit', res);
        };
        this.gridCallbackDel = function (res) {
            _this._dialogService.showCustomConfirm(Constants.messageDialog.confrim_delete_info, "ต้องการลบข้อมูลประกาศฉบับที่ " + res.ANNOUNCEMENT_NO + " หรือไม่?", res, _this.CallbackDelOk, _this.CallbackDelCancel);
        };
        this.gridCallbackView = function (res) {
            console.log(' this.gridCallbackView', res);
        };
        this.CallbackDelOk = function (res) {
            console.log(' this.CallbackDelOk', res);
            _this._jobSupplyOfLandService.DeleteAnnouncementLand(res.ID).then(function (del_res) {
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.completeSave, "", "");
            }).catch(function (err) {
                console.log('del_res_err ', err);
                _this._dialogService.showCustomAlert(Constants.messageDialog.alert, Constants.messageDialog.enter, Constants.messageDialog.uncompleteSave, "", "");
            }).finally(function () {
                _this.initDataSearch();
            });
            return true;
        };
        this.CallbackDelCancel = function (res) {
            return true;
        };
    }
    AnnouncementManagementController.prototype.$onInit = function () {
        this.modelSearch = new announcementSearchModel();
        this.mode = 0;
        this.initDataSearch_open();
    };
    AnnouncementManagementController.prototype.addAnnouncement = function () {
        this.mode = 1;
    };
    AnnouncementManagementController.prototype.editAnnouncement = function () {
        this.mode = 2;
    };
    AnnouncementManagementController.prototype.viewAnnouncement = function () {
        this.mode = 3;
    };
    AnnouncementManagementController.prototype.Search = function () {
        this.initDataSearch();
    };
    AnnouncementManagementController.prototype.Clear = function () {
        this.clearSelectionArea.clear();
        this.modelSearch = new announcementSearchModel();
        this.initDataSearch();
    };
    AnnouncementManagementController.prototype.listDataGrid = function () {
        this.listData = [];
        return new kendo.data.DataSource({ data: this.listData, pageSize: 10 });
    };
    return AnnouncementManagementController;
}());
//# sourceMappingURL=announcement-management.component.js.map