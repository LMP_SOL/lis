var announcementLandTabController = (function () {
    function announcementLandTabController(_dialogService, _jobSupplyOfLandService, $location, $http, $element) {
        this._dialogService = _dialogService;
        this._jobSupplyOfLandService = _jobSupplyOfLandService;
        this.$location = $location;
        this.$http = $http;
        this.$element = $element;
    }
    announcementLandTabController.prototype.$onInit = function () {
        console.log("mode", this.mode);
        this.$element.find('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });
    };
    return announcementLandTabController;
}());
//# sourceMappingURL=announcement-land-tab.component.js.map