﻿class announcementLandTabController implements ng.IController {
    public mode: any;
    public form: any;
    public modelSave: any;


    constructor(private _dialogService: DialogService,
        private _jobSupplyOfLandService: JobSupplyOfLandService,

        private $location: ng.ILocationService,
        private $http: ng.IHttpService,
        private $element: ng.IRootElementService,

    ) {

    }

    $onInit() {
        console.log("mode", this.mode);

        this.$element.find('input').on('keydown', (ev) => {
            ev.stopPropagation();
        });
    }
}