var announcementApproveTabController = (function () {
    function announcementApproveTabController(_dialogService, _jobSupplyOfLandService, $location, $http, $element) {
        this._dialogService = _dialogService;
        this._jobSupplyOfLandService = _jobSupplyOfLandService;
        this.$location = $location;
        this.$http = $http;
        this.$element = $element;
    }
    announcementApproveTabController.prototype.$onInit = function () {
        console.log("mode", this.mode);
    };
    return announcementApproveTabController;
}());
//# sourceMappingURL=announcement-approve-tab.component.js.map