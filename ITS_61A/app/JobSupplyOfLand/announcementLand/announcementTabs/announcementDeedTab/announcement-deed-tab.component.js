var announcementDeedTabController = (function () {
    function announcementDeedTabController(_dialogService, _jobSupplyOfLandService, $location, $http, $element) {
        this._dialogService = _dialogService;
        this._jobSupplyOfLandService = _jobSupplyOfLandService;
        this.$location = $location;
        this.$http = $http;
        this.$element = $element;
    }
    announcementDeedTabController.prototype.$onInit = function () {
        console.log("mode", this.mode);
    };
    return announcementDeedTabController;
}());
//# sourceMappingURL=announcement-deed-tab.component.js.map