﻿class JobSupplyOfLandService {

    constructor(private $http: ng.IHttpService,
        private $q: ng.IQService) {
    }


    //public getCriteriaforsearcUser(): ng.IPromise<any> {
    //    return this.$http.get(`api/SystemManagement/getCriteriaforsearcUser?_=${new Date().getTime().toString()}`)
    //        .then((res: any) => {
    //            if (res.data) {
    //                return res.data;
    //            }
    //        });
    //}

    public GetCriteriaForSearchPayTaxLand(): ng.IPromise<any> {
        return this.$http.get(`api/JobSupplyOfLand/GetCriteriaForSearchPayTaxLand?_=${new Date().getTime().toString()}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }

    public GetDataForPayTaxLand(taxYear: string, projectID: string, deedIdFrom: string, deedIdTo: string, province: string, taxReceiveDepartment: string): ng.IPromise<any> {    
        return this.$http.get(`api/JobSupplyOfLand/GetDataForPayTaxLand?year_tax=${taxYear}&project_id=${projectID}&deed_id_from=${deedIdFrom}&deed_id_to=${deedIdTo}&province_id=${province}&taxReceive_Department=${taxReceiveDepartment}`)
            .then((res: any) => {
                if (res.data) {
                    console.log('GetDataForSearchPayTaxLand===>', res.data)
                    return res.data;
                }
            });

        
    }
    public GetDataForAnnouncementLand(): ng.IPromise<any> {
        return this.$http.get(`api/JobSupplyOfLand/GetDataForAnnouncementLand`)
            .then((res: any) => {
                if (res.data) {
                    console.log('GetDataForGetDataForAnnouncementLand===>', res.data)
                    return res.data;
                }
            });
    }
    public GetDataForLandRegistration(ANNOUNCEMENT_LAND_ID: string, PROJECT_NAME :string , DEED_NO_FROM: string, DEED_NO_TO: string, PROVINCE_ID: string): ng.IPromise<any> {
        return this.$http.get(`api/JobSupplyOfLand/GetDataForLandRegistration?ANNOUNCEMENT_LAND_ID=${ANNOUNCEMENT_LAND_ID}&PROJECT_NAME=${PROJECT_NAME}&DEED_NO_FROM=${DEED_NO_FROM}&DEED_NO_TO=${DEED_NO_TO}&PROVINCE_ID=${PROVINCE_ID}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }

    public GetDataForSearchPurchasingProject(PROJECT_ID: string, PROVINCE_ID: string, ANNOUNCEMENTS_NO: string, DEED_NO_FROM: string, DEED_NO_TO: string, PARCEL_NO: string, DEALING_FILE_NO: string,): ng.IPromise<any> {
        return this.$http.get(`api/JobSupplyOfLand/GetDataForSearchPurchasingProject?PROJECT_ID=${PROJECT_ID}&PROVINCE_ID=${PROVINCE_ID}&ANNOUNCEMENTS_NO=${ANNOUNCEMENTS_NO}&DEED_NO_FROM=${DEED_NO_FROM}&DEED_NO_TO=${DEED_NO_TO}&PARCEL_NO=${PARCEL_NO}&DEALING_FILE_NO=${DEALING_FILE_NO}`)
            .then((res: any) => {
                if (res.data) {
                    console.log('GetDataForSearchPurchasingProject===>', res.data)
                    return res.data;
                }
            });
    }

    public getCriteriaforsearcPurchasingProject(): ng.IPromise<any> {
        return this.$http.get(`api/JobSupplyOfLand/getCriteriaforsearcPurchasingProject?_=${new Date().getTime().toString()}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }

    public GetDataSearchAnnouncementLand(model: announcementSearchModel): ng.IPromise<any> {
        return this.$http.post(`api/JobSupplyOfLand/GetDataSearchAnnouncementLand`,model)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
    public DeleteAnnouncementLand(id: string): ng.IPromise<any> {
        return this.$http.get(`api/JobSupplyOfLand/DeleteAnnouncementLand?AnnouncementLandId=${id}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
    public DeleteDDeed(id: string): ng.IPromise<any> {
        return this.$http.get(`api/JobSupplyOfLand/DeleteDDeed?DId=${id}`)
            .then((res: any) => {
                if (res.data) {
                    return res.data;
                }
            });
    }
}