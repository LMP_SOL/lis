var JobSupplyOfLandService = (function () {
    function JobSupplyOfLandService($http, $q) {
        this.$http = $http;
        this.$q = $q;
    }
    //public getCriteriaforsearcUser(): ng.IPromise<any> {
    //    return this.$http.get(`api/SystemManagement/getCriteriaforsearcUser?_=${new Date().getTime().toString()}`)
    //        .then((res: any) => {
    //            if (res.data) {
    //                return res.data;
    //            }
    //        });
    //}
    JobSupplyOfLandService.prototype.GetCriteriaForSearchPayTaxLand = function () {
        return this.$http.get("api/JobSupplyOfLand/GetCriteriaForSearchPayTaxLand?_=" + new Date().getTime().toString())
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    JobSupplyOfLandService.prototype.GetDataForPayTaxLand = function (taxYear, projectID, deedIdFrom, deedIdTo, province, taxReceiveDepartment) {
        return this.$http.get("api/JobSupplyOfLand/GetDataForPayTaxLand?year_tax=" + taxYear + "&project_id=" + projectID + "&deed_id_from=" + deedIdFrom + "&deed_id_to=" + deedIdTo + "&province_id=" + province + "&taxReceive_Department=" + taxReceiveDepartment)
            .then(function (res) {
            if (res.data) {
                console.log('GetDataForSearchPayTaxLand===>', res.data);
                return res.data;
            }
        });
    };
    JobSupplyOfLandService.prototype.GetDataForAnnouncementLand = function () {
        return this.$http.get("api/JobSupplyOfLand/GetDataForAnnouncementLand")
            .then(function (res) {
            if (res.data) {
                console.log('GetDataForGetDataForAnnouncementLand===>', res.data);
                return res.data;
            }
        });
    };
    JobSupplyOfLandService.prototype.GetDataForLandRegistration = function (ANNOUNCEMENT_LAND_ID, PROJECT_NAME, DEED_NO_FROM, DEED_NO_TO, PROVINCE_ID) {
        return this.$http.get("api/JobSupplyOfLand/GetDataForLandRegistration?ANNOUNCEMENT_LAND_ID=" + ANNOUNCEMENT_LAND_ID + "&PROJECT_NAME=" + PROJECT_NAME + "&DEED_NO_FROM=" + DEED_NO_FROM + "&DEED_NO_TO=" + DEED_NO_TO + "&PROVINCE_ID=" + PROVINCE_ID)
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    JobSupplyOfLandService.prototype.GetDataForSearchPurchasingProject = function (PROJECT_ID, PROVINCE_ID, ANNOUNCEMENTS_NO, DEED_NO_FROM, DEED_NO_TO, PARCEL_NO, DEALING_FILE_NO) {
        return this.$http.get("api/JobSupplyOfLand/GetDataForSearchPurchasingProject?PROJECT_ID=" + PROJECT_ID + "&PROVINCE_ID=" + PROVINCE_ID + "&ANNOUNCEMENTS_NO=" + ANNOUNCEMENTS_NO + "&DEED_NO_FROM=" + DEED_NO_FROM + "&DEED_NO_TO=" + DEED_NO_TO + "&PARCEL_NO=" + PARCEL_NO + "&DEALING_FILE_NO=" + DEALING_FILE_NO)
            .then(function (res) {
            if (res.data) {
                console.log('GetDataForSearchPurchasingProject===>', res.data);
                return res.data;
            }
        });
    };
    JobSupplyOfLandService.prototype.getCriteriaforsearcPurchasingProject = function () {
        return this.$http.get("api/JobSupplyOfLand/getCriteriaforsearcPurchasingProject?_=" + new Date().getTime().toString())
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    JobSupplyOfLandService.prototype.GetDataSearchAnnouncementLand = function (model) {
        return this.$http.post("api/JobSupplyOfLand/GetDataSearchAnnouncementLand", model)
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    JobSupplyOfLandService.prototype.DeleteAnnouncementLand = function (id) {
        return this.$http.get("api/JobSupplyOfLand/DeleteAnnouncementLand?AnnouncementLandId=" + id)
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    JobSupplyOfLandService.prototype.DeleteDDeed = function (id) {
        return this.$http.get("api/JobSupplyOfLand/DeleteDDeed?DId=" + id)
            .then(function (res) {
            if (res.data) {
                return res.data;
            }
        });
    };
    return JobSupplyOfLandService;
}());
//# sourceMappingURL=job-supply-of-land.service.js.map