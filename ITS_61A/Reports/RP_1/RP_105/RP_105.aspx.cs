﻿using ITS_61A.Core.Services;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Reports.ViewModel.RP_1;
using Reports.ViewModel.RP_2;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ITS_61A.Reports.RP_1
{
    public class ReportRP105CriteriaData
    {

        public string PROJECT_ID { get; set; }
        public string DEED_ID { get; set; }
        public string ANNUM { get; set; }
        public string TO_DEED_ID { get; set; }
        //public string TAX_RECEIVE_DEPARTMENT_ID { get; set; }
        public string PROVINCE_ID { get; set; }
    }
    public partial class RP_105 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var service = new ReportService();
                ReportViewer1.PageCountMode = new PageCountMode();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                var report = ReportViewer1.LocalReport;
                var assembly = typeof(_61AReport).Assembly;
                report.DataSources.Clear();

                var json = Request["JsonParameter"];
                var obj1 = string.Empty;
                var examID = string.Empty;
                var PROVINCE_ID = string.Empty;
                var TO_DEED_ID = string.Empty;
                //var PROVINTAX_RECEIVE_DEPARTMENT_IDCE_ID = string.Empty;


                var PROJECT_ID = string.Empty;
                var DEED_ID = string.Empty;
                var ANNUM = string.Empty;

                if (!string.IsNullOrEmpty(json))
                {
                    var obj = JsonConvert.DeserializeObject<ReportRP105CriteriaData>(json);
                    PROVINCE_ID = obj.PROVINCE_ID;
                    PROJECT_ID = obj.PROJECT_ID;
                    DEED_ID = obj.DEED_ID;
                    ANNUM = obj.ANNUM;
                    TO_DEED_ID = obj.TO_DEED_ID;
                    //TAX_RECEIVE_DEPARTMENT_ID


                }

                if (PROVINCE_ID != "")
                {

                    var ds = new DataSet() as DataSet;
                    ds = service.GetDataRT105(PROVINCE_ID, PROJECT_ID, DEED_ID, ANNUM, TO_DEED_ID/*, TAX_RECEIVE_DEPARTMENT_ID*/);                   

                    string path = "ITS_61A.Reports.RDLC.RP_1.RP_105.rdlc";

                    var stream = assembly.GetManifestResourceStream(path);
                    report.LoadReportDefinition(stream);

                    report.DisplayName = "RT105";

                    //var parameters = new List<ReportParameter>
                    //    {
                    //        new ReportParameter("DateStart", Startdate),
                    //        new ReportParameter("DateEnd", Enddate),
                    //        new ReportParameter("ToDay", ToDay),
                    //    };
                    //report.SetParameters(parameters);


                    List<RP_105ViewModel>ListRT105= new List<RP_105ViewModel>();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ListRT105.Add(new RP_105ViewModel
                        {

                            landLocation = row["LOCATION_IN_DEED"].ToString(),
                            landNumber = row["PARCEL_NO"].ToString(),
                            deedNumber = row["DEED_NO"].ToString(),
                            freight = row["MAPSHEET_NEW"].ToString(),
                            rai = row["SIZE_OF_LAND_RAI"].ToString(),
                            garn = row["SIZE_OF_LAND_NGAN"].ToString(),
                            wa = row["SIZE_OF_LAND_WA"].ToString(),
                            landPrice = row["ESTIMATE_LAND"].ToString(),
                            landValue = row["TOTAL_AMOUNT_LAND"].ToString(),
                            categoryLandUse = row["UTILIZATION"].ToString(),
                            LandTax = row["TAX_AMOUNT"].ToString(),
                            notation = row["REMARK"].ToString()

                        });
                    }


                    var reportDs = new ReportDataSource("RP_105DataSet", ListRT105);
                    report.DataSources.Add(reportDs);
                    report.Refresh();
                }

            }
        }
    }
}