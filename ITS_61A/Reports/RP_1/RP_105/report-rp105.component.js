var reportRP105CriteriaController = (function () {
    function reportRP105CriteriaController($location, $scope, $filter, dialogService, webConfigService, $http) {
        this.$location = $location;
        this.$scope = $scope;
        this.$filter = $filter;
        this.dialogService = dialogService;
        this.webConfigService = webConfigService;
        this.$http = $http;
        this.session_id_department = "6A469DE811EC44A5A6826D8D7AFC4EBB";
        this.topic_id_department = "7C8FE4A791124CF595EFDE0F4F08F2D5";
        this.configProvinceAutoComplete = {
            isDisabled: false,
            noCache: true,
            selectedItem: undefined,
            searchText: undefined,
        };
    }
    reportRP105CriteriaController.prototype.$onInit = function () {
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_department, this.topic_id_department);
        this.getDataProvince();
        //this.getDataProject();
        angular.element('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });
    };
    reportRP105CriteriaController.prototype.getDataProvince = function () {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + "api/Report/getDataProvince").then(function (res) {
            console.log('province-->', res);
            _this.porvinceList = res.data.Table;
        });
    };
    //public getDataProject() {
    //    this.$http.get(this.webConfigService.getBaseUrl() + `api/Report/getDataProject`).then((res: any) => {
    //        console.log('Project-->', res)
    //        this.porvinceList = res.data.Table;
    //    })
    //}
    reportRP105CriteriaController.prototype.getdataFromMasterInsertWorkerManagementPosition = function (session_id, topic_id) {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + ("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id)).then(function (res) {
            console.log('res7-->', res);
            _this.managementPositionList = res.data.Table;
        });
    };
    ///***** Province
    reportRP105CriteriaController.prototype.searchTextChange = function (item) {
        console.log('itemmmmmmmmmmmmmm', item);
    };
    reportRP105CriteriaController.prototype.selectedItemChangeProvince = function (item) {
        // console.log('selectedItemChange ', item);
        if (item) {
            this.configProvinceAutoComplete.selectedItem = item;
        }
    };
    reportRP105CriteriaController.prototype.querySearchProvince = function (query) {
        var results = query ? this.porvinceList.filter(this.createFilterForProvince(query)) : this.porvinceList;
        return results;
    };
    reportRP105CriteriaController.prototype.createFilterForProvince = function (query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    };
    ///******* end autocomplete *****////
    reportRP105CriteriaController.prototype.getReport = function () {
        var hdJson = angular.element("#JsonParameter");
        console.log("this.configProvinceAutoComplete.selectedItem", this.configProvinceAutoComplete.selectedItem);
        var paramObj = {
            obj1: "test",
            PROVINCE_ID: this.ProvinceID,
            PROJECT_ID: this.PROJECT_ID,
            DEED_ID: this.DEED_ID,
            ANNUM: this.ANNUM
        };
        hdJson.val(angular.toJson(paramObj));
        angular.element("#frmForSubmitParameter").submit();
        console.log('paramObj', paramObj);
    };
    reportRP105CriteriaController.prototype.clear = function () {
        this.ProvinceID = undefined;
        this.PROJECT_ID = undefined;
        this.DEED_ID = undefined;
        this.ANNUM = undefined;
        this.configProvinceAutoComplete.selectedItem = null;
    };
    return reportRP105CriteriaController;
}());
angular.module("reportRp105App", ['ngRoute', 'ngCookies', 'ngMaterial'])
    .service("WebConfig", ["$location", WebConfigService])
    .service("Dialog", ["$mdBottomSheet", "$mdDialog", DialogService])
    .config(function ($mdDateLocaleProvider) {
    var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
    $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
    $mdDateLocaleProvider.shortMonths = shortMonths;
    $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
    $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
    $mdDateLocaleProvider.monthHeaderFormatter = function (date) {
        return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
    };
    $mdDateLocaleProvider.formatDate = function (date) {
        return moment(date).format('DD') + "/" + moment(date).format('MM') + "/" + (moment(date).get('year') + 543);
    };
    $mdDateLocaleProvider.parseDate = function (dateString) {
        var dateArray = dateString.split("/");
        dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
        var m = moment(dateString, 'L', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
})
    .component("reportRp105", {
    templateUrl: "report-rp105.component.html",
    controllerAs: "rtReportCtrl",
    controller: ["$location", "$scope", "$filter", "Dialog", "WebConfig", "$http", reportRP105CriteriaController]
});
//# sourceMappingURL=report-rp105.component.js.map