var reportRP108CriteriaController = (function () {
    function reportRP108CriteriaController($location, $scope, $filter, dialogService, webConfigService, $http) {
        this.$location = $location;
        this.$scope = $scope;
        this.$filter = $filter;
        this.dialogService = dialogService;
        this.webConfigService = webConfigService;
        this.$http = $http;
        this.session_id_department = "6A469DE811EC44A5A6826D8D7AFC4EBB";
        this.topic_id_department = "7C8FE4A791124CF595EFDE0F4F08F2D5";
        this.configProvinceAutoComplete = {
            isDisabled: false,
            noCache: true,
            selectedItem: undefined,
            searchText: undefined,
        };
        this.configMultiSelect_POSITION_LEVEL = {
            POSITION_LEVEL: [],
            searchText: '',
        };
    }
    reportRP108CriteriaController.prototype.clearSearchPOSITION_LEVEL = function () {
        this.configMultiSelect_POSITION_LEVEL.searchText = '';
    };
    reportRP108CriteriaController.prototype.$onInit = function () {
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_department, this.topic_id_department);
        this.getDataProvince();
        this.getLevelData();
        angular.element('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });
    };
    reportRP108CriteriaController.prototype.getDataProvince = function () {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + "api/Report/getDataProvince").then(function (res) {
            console.log('province-->', res);
            _this.porvinceList = res.data.Table;
        });
    };
    reportRP108CriteriaController.prototype.getLevelData = function () {
        var arr = [];
        for (var i = 0; i < 10; i++) {
            arr.push({ ID: i + 1, NAME: "TEST" + (i + 1) });
        }
        this.POSITION_LEVEL_LIST = arr;
        //this.$http.get(this.webConfigService.getBaseUrl() + `api/report/GetLevel`)
        //.then((res: any) => {
        //this.levelDataList = res.data.Table;
        //let firstItem = { ID: "", ANSWER_DATA: "ทั้งหมด" };
        //this.levelDataList.splice(0, 0, firstItem);
        //});
    };
    reportRP108CriteriaController.prototype.getdataFromMasterInsertWorkerManagementPosition = function (session_id, topic_id) {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + ("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id)).then(function (res) {
            console.log('res7-->', res);
            _this.managementPositionList = res.data.Table;
        });
    };
    ///***** Province
    reportRP108CriteriaController.prototype.searchTextChange = function (item) {
        console.log('itemmmmmmmmmmmmmm', item);
    };
    reportRP108CriteriaController.prototype.selectedItemChangeProvince = function (item) {
        // console.log('selectedItemChange ', item);
        if (item) {
            this.configProvinceAutoComplete.selectedItem = item;
        }
    };
    reportRP108CriteriaController.prototype.querySearchProvince = function (query) {
        var results = query ? this.porvinceList.filter(this.createFilterForProvince(query)) : this.porvinceList;
        return results;
    };
    reportRP108CriteriaController.prototype.createFilterForProvince = function (query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    };
    ///******* end autocomplete *****////
    reportRP108CriteriaController.prototype.getReport = function () {
        var hdJson = angular.element("#JsonParameter");
        console.log("this.configProvinceAutoComplete.selectedItem", this.configProvinceAutoComplete.selectedItem);
        console.log('this.configMultiSelect_POSITION_LEVEL.POSITION_LEVEL', this.configMultiSelect_POSITION_LEVEL.POSITION_LEVEL);
        var paramObj = {
            obj1: "test",
            PROVINCE_ID: this.ProvinceID
        };
        hdJson.val(angular.toJson(paramObj));
        angular.element("#frmForSubmitParameter").submit();
        console.log('paramObj', paramObj);
    };
    reportRP108CriteriaController.prototype.clear = function () {
        this.ProvinceID = undefined;
        this.configProvinceAutoComplete.selectedItem = null;
        this.configMultiSelect_POSITION_LEVEL.POSITION_LEVEL = null;
    };
    return reportRP108CriteriaController;
}());
angular.module("reportRp108App", ['ngRoute', 'ngCookies', 'ngMaterial'])
    .service("WebConfig", ["$location", WebConfigService])
    .service("Dialog", ["$mdBottomSheet", "$mdDialog", DialogService])
    .config(function ($mdDateLocaleProvider) {
    var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
    $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
    $mdDateLocaleProvider.shortMonths = shortMonths;
    $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
    $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
    $mdDateLocaleProvider.monthHeaderFormatter = function (date) {
        return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
    };
    $mdDateLocaleProvider.formatDate = function (date) {
        return moment(date).format('DD') + "/" + moment(date).format('MM') + "/" + (moment(date).get('year') + 543);
    };
    $mdDateLocaleProvider.parseDate = function (dateString) {
        var dateArray = dateString.split("/");
        dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
        var m = moment(dateString, 'L', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
})
    .component("reportRp108", {
    templateUrl: "report-rp108.component.html",
    controllerAs: "rtReportCtrl",
    controller: ["$location", "$scope", "$filter", "Dialog", "WebConfig", "$http", reportRP108CriteriaController]
});
//# sourceMappingURL=report-rp108.component.js.map