﻿class reportRP108CriteriaController implements ng.IController {
    public session_id_department: string = "6A469DE811EC44A5A6826D8D7AFC4EBB";
    public topic_id_department: string = "7C8FE4A791124CF595EFDE0F4F08F2D5";
    public managementPositionList: any[];
    public managementPositionID: string;
    public porvinceList: any[];
    public ProvinceID: string;
    public POSITION_LEVEL_LIST: any[];

    public configProvinceAutoComplete = {
        isDisabled: false,
        noCache: true,
        selectedItem: undefined,
        searchText: undefined,

    };

    public configMultiSelect_POSITION_LEVEL = {
        POSITION_LEVEL: [],
        searchText: '',
    };
    public clearSearchPOSITION_LEVEL() {
        this.configMultiSelect_POSITION_LEVEL.searchText = '';
    }

    constructor(private $location: ng.ILocationService,
        private $scope: ng.IScope,
        private $filter: ng.IFilterService,
        private dialogService: DialogService,
        private webConfigService: WebConfigService,
        private $http: ng.IHttpService
    ) { }


    $onInit() {
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_department, this.topic_id_department);
        this.getDataProvince();
        this.getLevelData();

        angular.element('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });
    }

    public getDataProvince() {
        this.$http.get(this.webConfigService.getBaseUrl() + `api/Report/getDataProvince`).then((res: any) => {
            console.log('province-->', res)
            this.porvinceList = res.data.Table;
        })
    }


    private getLevelData() {
        let arr = [];
        for (let i = 0; i < 10; i++) {
            arr.push({ ID: i + 1, NAME: "TEST" + (i + 1) });
        }

        this.POSITION_LEVEL_LIST = arr;
        //this.$http.get(this.webConfigService.getBaseUrl() + `api/report/GetLevel`)
        //.then((res: any) => {
        //this.levelDataList = res.data.Table;
        //let firstItem = { ID: "", ANSWER_DATA: "ทั้งหมด" };
        //this.levelDataList.splice(0, 0, firstItem);
        //});
    }

    public getdataFromMasterInsertWorkerManagementPosition(session_id, topic_id) {
        this.$http.get(this.webConfigService.getBaseUrl() + `api/SystemManagement/GetdataFromMasterInsertWorker?session_id=${session_id}&topic_id=${topic_id}`).then((res: any) => {
            console.log('res7-->', res)
            this.managementPositionList = res.data.Table;
        })
    }

    ///***** Province
    public searchTextChange(item: string) {
        console.log('itemmmmmmmmmmmmmm', item)

    }

    public selectedItemChangeProvince(item) {
        // console.log('selectedItemChange ', item);
        if (item) {
            this.configProvinceAutoComplete.selectedItem = item;
        }
    }
    public querySearchProvince(query) {
        var results = query ? this.porvinceList.filter(this.createFilterForProvince(query)) : this.porvinceList;
        return results;

    }
    public createFilterForProvince(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(item) {
            return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    }
    ///******* end autocomplete *****////

    public getReport() {
        let hdJson = angular.element("#JsonParameter");
        console.log("this.configProvinceAutoComplete.selectedItem", this.configProvinceAutoComplete.selectedItem);
        console.log('this.configMultiSelect_POSITION_LEVEL.POSITION_LEVEL', this.configMultiSelect_POSITION_LEVEL.POSITION_LEVEL);
        let paramObj = {
            obj1: "test",
            PROVINCE_ID: this.ProvinceID

        };

        hdJson.val(angular.toJson(paramObj));
        angular.element("#frmForSubmitParameter").submit();
        console.log('paramObj', paramObj);
    }

    public clear() {
        this.ProvinceID = undefined;
        this.configProvinceAutoComplete.selectedItem = null;
        this.configMultiSelect_POSITION_LEVEL.POSITION_LEVEL = null;
    }
}



angular.module("reportRp108App", ['ngRoute', 'ngCookies', 'ngMaterial'])
    .service("WebConfig", ["$location", WebConfigService])
    .service("Dialog", ["$mdBottomSheet", "$mdDialog", DialogService])
    .config(function ($mdDateLocaleProvider) {
        var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
        $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
        $mdDateLocaleProvider.shortMonths = shortMonths;
        $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
        $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
        $mdDateLocaleProvider.monthHeaderFormatter = function (date) {
            return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
        };
        $mdDateLocaleProvider.formatDate = function (date) {
            return `${moment(date).format('DD')}/${moment(date).format('MM')}/${moment(date).get('year') + 543}`;
        };
        $mdDateLocaleProvider.parseDate = function (dateString) {
            var dateArray = dateString.split("/");
            dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
            var m = moment(dateString, 'L', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
    })
    .component("reportRp108",
    {
        templateUrl: "report-rp108.component.html",
        controllerAs: "rtReportCtrl",
        controller: ["$location", "$scope", "$filter", "Dialog", "WebConfig", "$http", reportRP108CriteriaController]
    });

