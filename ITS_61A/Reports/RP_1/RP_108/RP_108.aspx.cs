﻿using ITS_61A.Core.Services;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Reports.ViewModel.RP_1;
using Reports.ViewModel.RP_2;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ITS_61A.Reports.RP_1
{
    public class ReportRP108CriteriaData
    {
        
        public string PROJECT_ID_IN { get; set; }
        public string PROJECT_NAME_IN { get; set; }
        
        public string PROJECT_PROVINCE_ID { get; set; }
        public string D_DEED_NO1 { get; set; }
        public string D_DEED_NO2 { get; set; }
        public string CONTRACT_NO_IN { get; set; }
        public string CONTRACT_DATE1_IN { get; set; }
        public string CONTRACT_DATE2_IN { get; set; }
        public string INFORMER_IN { get; set; }
        public string SUBJECT_NAME_IN { get; set; }

    }
    public partial class RP_108 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var service = new ReportService();
                ReportViewer1.PageCountMode = new PageCountMode();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                var report = ReportViewer1.LocalReport;
                var assembly = typeof(_61AReport).Assembly;
                report.DataSources.Clear();

                var json = Request["JsonParameter"];
                var obj1 = string.Empty;
                var examID = string.Empty;
                var PROVINCE_ID = string.Empty;
                var PROJECT_ID_IN = string.Empty;
                var PROJECT_NAME_IN = string.Empty;
                
                var PROJECT_PROVINCE_ID = string.Empty;
                var D_DEED_NO1 = string.Empty;
                var D_DEED_NO2 = string.Empty;
                var CONTRACT_NO_IN = string.Empty;
                var CONTRACT_DATE1_IN = string.Empty;
                var CONTRACT_DATE2_IN = string.Empty;
                var INFORMER_IN = string.Empty;
                var SUBJECT_NAME_IN = string.Empty;


                if (!string.IsNullOrEmpty(json))
                {
                    var obj = JsonConvert.DeserializeObject<ReportRP108CriteriaData>(json);

                    PROJECT_ID_IN = obj.PROJECT_ID_IN;
                    PROJECT_NAME_IN = obj.PROJECT_NAME_IN;
                    
                    PROJECT_PROVINCE_ID = obj.PROJECT_PROVINCE_ID;
                    D_DEED_NO1 = obj.D_DEED_NO1;
                    D_DEED_NO2 = obj.D_DEED_NO2;
                    CONTRACT_NO_IN = obj.CONTRACT_NO_IN;
                    CONTRACT_DATE1_IN = obj.CONTRACT_DATE1_IN;
                    CONTRACT_DATE2_IN = obj.CONTRACT_DATE2_IN;
                    INFORMER_IN = obj.INFORMER_IN;
                    SUBJECT_NAME_IN = obj.SUBJECT_NAME_IN;

                }

                if (PROJECT_ID_IN != "")
                {

                    var ds = new DataSet() as DataSet;
                    ds = service.GetDataRT108(
                        PROJECT_ID_IN,
                        PROJECT_NAME_IN,
                        PROJECT_PROVINCE_ID,
                        D_DEED_NO1,
                        D_DEED_NO2,
                        CONTRACT_NO_IN,
                        CONTRACT_DATE1_IN,
                        CONTRACT_DATE2_IN,
                        INFORMER_IN,
                        SUBJECT_NAME_IN);

                    string path = "ITS_61A.Reports.RDLC.RP_1.RP_108.rdlc";

                    var stream = assembly.GetManifestResourceStream(path);
                    report.LoadReportDefinition(stream);

                    report.DisplayName = "RT108";

                    //var parameters = new List<ReportParameter>
                    //    {
                    //        new ReportParameter("DateStart", Startdate),
                    //        new ReportParameter("DateEnd", Enddate),
                    //        new ReportParameter("ToDay", ToDay),
                    //    };
                    //report.SetParameters(parameters);


                    List<RP_108ViewModel>ListRT108= new List<RP_108ViewModel>();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ListRT108.Add(new RP_108ViewModel
                        {

                            idProject = row["project_name"].ToString(),
                            agency = row["informer"].ToString(),
                            deedNumber = row["deed_no"].ToString(),
                            province = row["province_id"].ToString(),
                            promiseNumber = row["contract_no"].ToString(),
                            promiseDate = row["contract_date"].ToString(),
                            subject = row["subject_name"].ToString()
                            
                        });
                    }


                    var reportDs = new ReportDataSource("RP_108DataSet", ListRT108);
                    report.DataSources.Add(reportDs);
                    report.Refresh();
                }

            }
        }
    }
}