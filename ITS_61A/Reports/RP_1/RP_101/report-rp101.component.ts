﻿// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
class reportRP101CriteriaController implements ng.IController {


    constructor(private $location: ng.ILocationService,
        private $scope: ng.IScope,
        private $filter: ng.IFilterService,
        private dialogService: DialogService,
        private webConfigService: WebConfigService,
        private $http: ng.IHttpService,
    ) { }


    $onInit() {

    }

    public getReport() {

        let hdJson = angular.element("#JsonParameter");


        let paramObj = {
            countryID: ''
        };

        hdJson.val(angular.toJson(paramObj));
        angular.element("#frmForSubmitParameter").submit();

    }
}



angular.module("reportRp101App", ['ngRoute', 'ngCookies', 'ngMaterial'])
    .service("WebConfig", ["$location", WebConfigService])
    .service("Dialog", ["$mdBottomSheet", "$mdDialog", DialogService])
    .config(function ($mdDateLocaleProvider) {
        var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
        $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
        $mdDateLocaleProvider.shortMonths = shortMonths;
        $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
        $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
        $mdDateLocaleProvider.monthHeaderFormatter = function (date) {
            return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
        };
        $mdDateLocaleProvider.formatDate = function (date) {
            return `${moment(date).format('DD')}/${moment(date).format('MM')}/${moment(date).get('year') + 543}`;
        };
        $mdDateLocaleProvider.parseDate = function (dateString) {
            var dateArray = dateString.split("/");
            dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
            var m = moment(dateString, 'L', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
    })
    .component("reportRp101",
    {
        templateUrl: "report-rp101.component.html",
        controllerAs: "rtReportCtrl",
        controller: ["$location", "$scope", "$filter", "Dialog", "WebConfig", "$http", reportRP101CriteriaController]
    });

