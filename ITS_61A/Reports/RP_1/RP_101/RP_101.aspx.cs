﻿using ITS_61A.Core.Services;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ITS_61A.Reports.RP_1
{
    public partial class RP_101 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var service = new ReportService();
                ReportViewer1.PageCountMode = new PageCountMode();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                var report = ReportViewer1.LocalReport;
                var assembly = typeof(_61AReport).Assembly;
                report.DataSources.Clear();

                var json = Request["JsonParameter"];
            }
        }
    }
}