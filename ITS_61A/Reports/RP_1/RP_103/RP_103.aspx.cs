﻿using ITS_61A.Core.Services;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Reports.ViewModel.RP_1;
using Reports.ViewModel.RP_2;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ITS_61A.Reports.RP_1
{
    public class ReportRP103CriteriaData
    {
        public string LANDLORD_DEPARTMENT { get; set; }
        //public string DEED_NO { get; set; }
        public string REGISTRY_NO { get; set; }
        public string SINGLE_REGISTRY { get; set; }
        public string CONTRACT_FROM_DATE_ST { get; set; }
        public string CONTRACT_TO_DATE_ST { get; set; }
        public string CONTRACT_TO_DATE_ED { get; set; }
        //public string BUILDING_MODEL { get; set; }
        //public string ORG_NAME { get; set; }
        public string document_id { get; set; }
        //public string COST_OF_WA_FOR_RENT { get; set; }
        //public string CONTRACT_AREA_RAI { get; set; }
        //public string CONTRACT_AREA_NGAN { get; set; }
        public string CONTRACT_FROM_DATE_ED { get; set; }
        public string PROVINCE_ID { get; set; }
        //public string REGISTRY_NO { get; set; }
    
    }
    public partial class RP_103 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var service = new ReportService();
                ReportViewer1.PageCountMode = new PageCountMode();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                var report = ReportViewer1.LocalReport;
                var assembly = typeof(_61AReport).Assembly;
                report.DataSources.Clear();

                var json = Request["JsonParameter"];
                var obj1 = string.Empty;
                var examID = string.Empty;

                var LANDLORD_DEPARTMENT = string.Empty;
                //var DEED_NO = string.Empty;
                var REGISTRY_NO = string.Empty;
                var SINGLE_REGISTRY = string.Empty;
                var CONTRACT_FROM_DATE_ST = string.Empty;
                var CONTRACT_TO_DATE_ST = string.Empty;
                var CONTRACT_TO_DATE_ED = string.Empty;
                //var BUILDING_MODEL = string.Empty;
                //var ORG_NAME = string.Empty;
                var document_id = string.Empty;
                //var COST_OF_WA_FOR_RENT = string.Empty;
                //var CONTRACT_AREA_RAI = string.Empty;
                //var CONTRACT_AREA_NGAN = string.Empty;
                var CONTRACT_FROM_DATE_ED = string.Empty;
                var PROVINCE_ID = string.Empty;
                //var REGISTRY_NO = string.Empty;

                if (!string.IsNullOrEmpty(json))
                {
                    var obj = JsonConvert.DeserializeObject<ReportRP103CriteriaData>(json);
                    PROVINCE_ID = obj.PROVINCE_ID;
                    REGISTRY_NO = obj.REGISTRY_NO;
                    LANDLORD_DEPARTMENT = obj.LANDLORD_DEPARTMENT;
                    //DEED_NO = obj.DEED_NO;
                    //REGISTRY_NO = obj.SINGLE_REGISTRY;
                    SINGLE_REGISTRY = obj.SINGLE_REGISTRY;
                    CONTRACT_FROM_DATE_ST = obj.CONTRACT_FROM_DATE_ST;
                    CONTRACT_TO_DATE_ST = obj.CONTRACT_TO_DATE_ST;
                    CONTRACT_TO_DATE_ED = obj.CONTRACT_TO_DATE_ED;
                    CONTRACT_FROM_DATE_ED = obj.CONTRACT_FROM_DATE_ED;
                    //ORG_NAME = obj.ORG_NAME;
                    document_id = obj.document_id;
                    //COST_OF_WA_FOR_RENT = obj.COST_OF_WA_FOR_RENT;
                    //CONTRACT_AREA_RAI = obj.CONTRACT_AREA_RAI;
                    //CONTRACT_AREA_NGAN = obj.CONTRACT_AREA_NGAN;
                    //CONTRACT_AREA_wa = obj.CONTRACT_AREA_wa;

                    //LANDLORD_DEPARTMENT = "44CB0A25CB8A435CB48477DBFEF153F1";
                    //DEED_NO = "A0F1527332E14B20AD00308E0A749F81";
                    //REGISTRY_NO = "A0F1527332E14B20AD00308E0A749F81";
                    //SINGLE_REGISTRY = "A0F1527332E14B20AD00308E0A749F81";
                    //CONTRACT_FROM_DATE = "A0F1527332E14B20AD00308E0A749F81";
                    //CONTRACT_TO_DATE = "A0F1527332E14B20AD00308E0A749F81";
                    //BUILDING_MODEL = "A0F1527332E14B20AD00308E0A749F81";
                    //ORG_NAME = "A0F1527332E14B20AD00308E0A749F81";
                    //DATA_TOPIC = "A0F1527332E14B20AD00308E0A749F81";
                    //COST_OF_WA_FOR_RENT = "A0F1527332E14B20AD00308E0A749F81";
                    //CONTRACT_AREA_RAI = "A0F1527332E14B20AD00308E0A749F81";
                    //CONTRACT_AREA_NGAN = "A0F1527332E14B20AD00308E0A749F81";
                    //CONTRACT_AREA_wa = "A0F1527332E14B20AD00308E0A749F81";
                }

                //    if(REGISTRY_NO != "")
                //    {

                //        var ds = new DataSet() as DataSet;
                //        ds = service.GetDataRT103(REGISTRY_NO);

                //        string path = "ITS_61A.Reports.RDLC.RP_1.RP_103.rdlc";

                //        var stream = assembly.GetManifestResourceStream(path);
                //        report.LoadReportDefinition(stream);

                //        report.DisplayName = "RT103";

                //        List<RP_103ViewModel> ListRT103 = new List<RP_103ViewModel>();

                //        foreach (DataRow row in ds.Tables[0].Rows)
                //        {
                //            ListRT103.Add(new RP_103ViewModel
                //            {
                //                nameOfOrganization = row["LANDLORD_DEPARTMENT"].ToString(),
                //                deedNumber = row["DEED_NO"].ToString(),
                //                registerAt = row["REGISTRY_NO"].ToString(),
                //                individualRegistration = row["SINGLE_REGISTRY"].ToString(),
                //                rai = row["CONTRACT_AREA_RAI"].ToString(),
                //                garn = row["CONTRACT_AREA_NGAN"].ToString(),
                //                wa = row["CONTRACT_AREA_wa"].ToString(),
                //                contractStartDate = row["CONTRACT_FROM_DATE"].ToString(),
                //                contractTerm = row["CONTRACT_TO_DATE"].ToString(),
                //                buildingStyle = row["BUILDING_MODEL"].ToString(),
                //                agencyResponsible = row["ORG_NAME"].ToString(),
                //                category = row["DATA_TOPIC"].ToString(),
                //                sqWa = row["COST_OF_WA_FOR_RENT"].ToString()
                //            });
                //        }


                //        var reportDs = new ReportDataSource("RP_103DataSet", ListRT103);
                //        report.DataSources.Add(reportDs);
                //        report.Refresh();



                //}
                if (PROVINCE_ID != "")
                {

                    var ds = new DataSet() as DataSet;
                    ds = service.GetDataRT103(
                        PROVINCE_ID , 
                        REGISTRY_NO , 
                        SINGLE_REGISTRY ,
                        LANDLORD_DEPARTMENT, 
                        CONTRACT_FROM_DATE_ST , 
                        CONTRACT_FROM_DATE_ED,
                        CONTRACT_TO_DATE_ST,
                        CONTRACT_TO_DATE_ED,
                        document_id);

                    string path = "ITS_61A.Reports.RDLC.RP_1.RP_103.rdlc";

                    var stream = assembly.GetManifestResourceStream(path);
                    report.LoadReportDefinition(stream);

                    report.DisplayName = "RT103";

                    //var parameters = new List<ReportParameter>
                    //    {
                    //        new ReportParameter("DateStart", Startdate),
                    //        new ReportParameter("DateEnd", Enddate),
                    //        new ReportParameter("ToDay", ToDay),
                    //    };
                    //report.SetParameters(parameters);


                    List<RP_103ViewModel> ListRT103 = new List<RP_103ViewModel>();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ListRT103.Add(new RP_103ViewModel
                        {
                            nameOfOrganization = row["LANDLORD_DEPARTMENT"].ToString(),
                            deedNumber = row["DEED_NO"].ToString(),
                            registerAt = row["REGISTRY_NO"].ToString(),
                            individualRegistration = row["SINGLE_REGISTRY"].ToString(),
                            rai = row["CONTRACT_AREA_RAI"].ToString(),
                            garn = row["CONTRACT_AREA_NGAN"].ToString(),
                            wa = row["CONTRACT_AREA_wa"].ToString(),
                            contractStartDate = row["CONTRACT_FROM_DATE"].ToString(),
                            contractTerm = row["CONTRACT_TO_DATE"].ToString(),
                            buildingStyle = row["BUILDING_MODEL"].ToString(),
                            agencyResponsible = row["ORG_NAME"].ToString(),
                            category = row["DATA_TOPIC"].ToString(),
                            sqWa = row["COST_OF_WA_FOR_RENT"].ToString()
                        });
                    }


                    var reportDs = new ReportDataSource("RP_103DataSet", ListRT103);
                    report.DataSources.Add(reportDs);
                    report.Refresh();
                }

            }
        }
    }
}