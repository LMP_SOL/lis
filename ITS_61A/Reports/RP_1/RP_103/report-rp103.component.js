var reportRP103CriteriaController = (function () {
    function reportRP103CriteriaController($location, $scope, $filter, dialogService, webConfigService, $http, CommonService, _masterDataServic) {
        this.$location = $location;
        this.$scope = $scope;
        this.$filter = $filter;
        this.dialogService = dialogService;
        this.webConfigService = webConfigService;
        this.$http = $http;
        this.CommonService = CommonService;
        this._masterDataServic = _masterDataServic;
        this.session_id_department = "6A469DE811EC44A5A6826D8D7AFC4EBB";
        this.topic_id_department = "7C8FE4A791124CF595EFDE0F4F08F2D5";
        this.CONTRACT_FROM_DATE_ST = null;
        this.CONTRACT_TO_DATE_ST = null;
        this.CONTRACT_FROM_DATE_ED = null;
        this.CONTRACT_TO_DATE_ED = null;
        this.configMultiSelect_POSITION_LEVEL = {
            POSITION_LEVEL: [],
            searchText: '',
        };
        this.configProvinceAutoComplete = {
            isDisabled: false,
            noCache: true,
            selectedItem: undefined,
            searchText: undefined,
        };
    }
    reportRP103CriteriaController.prototype.$onInit = function () {
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_department, this.topic_id_department);
        this.getDataProvince();
        this.getDatadocument_id();
        this.getLevelData();
        angular.element('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });
    };
    reportRP103CriteriaController.prototype.getLevelData = function () {
        var arr = [];
        for (var i = 0; i < 10; i++) {
            arr.push({ ID: i + 1, NAME: "TEST" + (i + 1) });
        }
        this.POSITION_LEVEL_LIST = arr;
    };
    reportRP103CriteriaController.prototype.getdataFromMasterInsertWorkerManagementPosition = function (session_id, topic_id) {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + ("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id)).then(function (res) {
            console.log('res7-->', res);
            _this.managementPositionList = res.data.Table;
        });
    };
    reportRP103CriteriaController.prototype.clearSearchPOSITION_LEVEL = function () {
        this.configMultiSelect_POSITION_LEVEL.searchText = '';
    };
    reportRP103CriteriaController.prototype.searchTextChange = function (item) {
        console.log('itemmmmmmmmmmmmmm', item);
    };
    reportRP103CriteriaController.prototype.getDatadocument_id = function () {
        var _this = this;
        this._masterDataServic.getMasterData('3F841FD59BB548E98D1D480B6E0AC1A9', '098B5A17EDAA47A9ADA5274861668401')
            .then(function (res) {
            console.log('document_id-->', res);
            _this.document_idList = res.data.Table;
            _this.document_idList.splice(0, 0, { ID: 0, DATA_TOPIC: "ทั้งหมด" });
        });
    };
    reportRP103CriteriaController.prototype.selectedItemChangeProvince = function (item) {
        if (item) {
            this.configProvinceAutoComplete.selectedItem = item;
            this.ProvinceID = item.ID;
        }
        else {
            this.configProvinceAutoComplete.selectedItem = undefined;
            this.ProvinceID = null;
        }
    };
    reportRP103CriteriaController.prototype.querySearchProvince = function (query) {
        var results = query ? this.porvinceList.filter(this.createFilterForProvince(query)) : this.porvinceList;
        return results;
    };
    reportRP103CriteriaController.prototype.createFilterForProvince = function (query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    };
    reportRP103CriteriaController.prototype.getDataProvince = function () {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + "api/Report/getDataProvince").then(function (res) {
            console.log('province-->', res);
            _this.porvinceList = res.data.Table;
        });
    };
    reportRP103CriteriaController.prototype.getReport = function () {
        debugger;
        if (this.CONTRACT_FROM_DATE_ST == null && this.CONTRACT_FROM_DATE_ED != null) {
            this.dialogService.dialogMessage(Constants.messageDialog.systemMesage, "กรุณากรอกข้อมูล1");
        }
        else if (this.CONTRACT_TO_DATE_ST == null && this.CONTRACT_TO_DATE_ED != null) {
            this.dialogService.dialogMessage(Constants.messageDialog.systemMesage, "กรุณากรอกข้อมูล2");
        }
        else if (this.CONTRACT_FROM_DATE_ED == null && this.CONTRACT_FROM_DATE_ST != null) {
            this.dialogService.dialogMessage(Constants.messageDialog.systemMesage, "กรุณากรอกข้อมูล3");
        }
        else if (this.CONTRACT_TO_DATE_ED == null && this.CONTRACT_TO_DATE_ST != null) {
            this.dialogService.dialogMessage(Constants.messageDialog.systemMesage, "กรุณากรอกข้อมูล4");
        }
        else {
            var hdJson = angular.element("#JsonParameter");
            console.log("this.configProvinceAutoComplete.selectedItem", this.configProvinceAutoComplete.selectedItem);
            var tmpST = this.CommonService.formatDate(this.CONTRACT_FROM_DATE_ST, "");
            var tmpED = this.CommonService.formatDate(this.CONTRACT_FROM_DATE_ED, "");
            var tmpTST = this.CommonService.formatDate(this.CONTRACT_TO_DATE_ST, "");
            var tmpTED = this.CommonService.formatDate(this.CONTRACT_TO_DATE_ED, "");
            var paramObj = {
                obj1: "test",
                document_id: this.document_id,
                PROVINCE_ID: this.ProvinceID,
                REGISTRY_NO: this.REGISTRY_NO,
                SINGLE_REGISTRY: this.SINGLE_REGISTRY,
                LANDLORD_DEPARTMENT: this.LANDLORD_DEPARTMENT,
                CONTRACT_FROM_DATE_ST: this.CommonService.formatDateEn(tmpST, ""),
                CONTRACT_TO_DATE_ST: this.CommonService.formatDateEn(tmpTST, ""),
                CONTRACT_FROM_DATE_ED: this.CommonService.formatDateEn(tmpED, ""),
                CONTRACT_TO_DATE_ED: this.CommonService.formatDateEn(tmpTED, "")
            };
            debugger;
            hdJson.val(angular.toJson(paramObj));
            angular.element("#frmForSubmitParameter").submit();
            console.log('paramObj', paramObj);
        }
    };
    reportRP103CriteriaController.prototype.clear = function () {
        this.REGISTRY_NO = undefined;
        this.ProvinceID = undefined;
        this.SINGLE_REGISTRY = undefined;
        this.LANDLORD_DEPARTMENT = undefined;
        this.CONTRACT_FROM_DATE_ST = null;
        this.CONTRACT_TO_DATE_ST = null;
        this.CONTRACT_FROM_DATE_ED = null;
        this.CONTRACT_TO_DATE_ED = null;
        this.document_id = undefined;
        this.configProvinceAutoComplete.selectedItem = null;
    };
    return reportRP103CriteriaController;
}());
angular.module("reportRp103App", ['ngRoute', 'ngCookies', 'ngMaterial'])
    .service("WebConfig", ["$location", WebConfigService])
    .service("Dialog", ["$mdBottomSheet", "$mdDialog", DialogService])
    .service(Constants.services.WebConfig, ["$location", WebConfigService])
    .service(Constants.services.MasterDataService, ["$http", Constants.services.WebConfig, "$q", MasterDataService])
    .service(Constants.services.Common, [CommonService])
    .config(function ($mdDateLocaleProvider) {
    var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
    $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
    $mdDateLocaleProvider.shortMonths = shortMonths;
    $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
    $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
    $mdDateLocaleProvider.monthHeaderFormatter = function (date) {
        return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
    };
    $mdDateLocaleProvider.formatDate = function (date) {
        return (date) ? moment(date).format('DD') + "/" + moment(date).format('MM') + "/" + (moment(date).get('year') + 543) : null;
    };
    $mdDateLocaleProvider.parseDate = function (dateString) {
        var dateArray = dateString.split("/");
        dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
        var m = moment(dateString, 'L', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
})
    .component("reportRp103", {
    templateUrl: "report-rp103.component.html",
    controllerAs: "rtReportCtrl",
    controller: ["$location", "$scope", "$filter", "Dialog", "WebConfig", "$http", Constants.services.Common, Constants.services.MasterDataService, reportRP103CriteriaController]
});
//# sourceMappingURL=report-rp103.component.js.map