﻿using ITS_61A.Core.Services;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Reports.ViewModel.RP_1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ITS_61A.Reports.RP_1
{
    public class ReportRP106CriteriaData
    {
        //public string OFFER_SALE_DATE { get; set; }
        public string OFFER_SALE_NAME { get; set; }
        //public string OFFER_SALE_ADDRESS { get; set; }
        //public string SALE_LAND_RAI { get; set; }
        //public string SALE_LAND_NGAN { get; set; }
        //public string SALE_LAND_WA { get; set; }
        //public string SUBDISTRICT_NAME_TH { get; set; }
        //public string SALE_AMOUNT_OF_RAI { get; set; }
        //public string DISTRICT_NAME_TH { get; set; }
        //public string PROVINCE_NAME_TH { get; set; }
        public string OFFER_SALE_DATE_ST { get; set; }
        public string OFFER_SALE_DATE_ED { get; set; }
        public string PROVINCE_ID { get; set; }
        public string DISTRICT_ID { get; set; }
        public string SUBDISTRICT_ID { get; set; }
        public string DEED_NO { get; set; }
        public string ToDEED_NO { get; set; }
        public string PARCEL_NO { get; set; }
        public string ANNOUNCEMENT_NO { get; set; }
    }
    public partial class RP_106 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var service = new ReportService();
                ReportViewer1.PageCountMode = new PageCountMode();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                var report = ReportViewer1.LocalReport;
                var assembly = typeof(_61AReport).Assembly;
                report.DataSources.Clear();

                var json = Request["JsonParameter"];
                var obj1 = string.Empty;
                var examID = string.Empty;

                //var OFFER_SALE_DATE = string.Empty;
                var OFFER_SALE_NAME = string.Empty;
                var OFFER_SALE_DATE_ST = string.Empty;
                var OFFER_SALE_DATE_ED = string.Empty;
                //var SALE_LAND_NGAN = string.Empty;
                //var SALE_LAND_WA = string.Empty;
                //var SUBDISTRICT_NAME_TH = string.Empty;
                //var SALE_AMOUNT_OF_RAI = string.Empty;
                //var DISTRICT_NAME_TH = string.Empty;
                //var PROVINCE_NAME_TH = string.Empty;
                var PROVINCE_ID = string.Empty;
                var DISTRICT_ID = string.Empty;
                var SUBDISTRICT_ID = string.Empty;
                var DEED_NO = string.Empty;
                var ToDEED_NO = string.Empty;
                var PARCEL_NO = string.Empty;
                var ANNOUNCEMENT_NO = string.Empty;

                if (!string.IsNullOrEmpty(json))
                {
                    var obj = JsonConvert.DeserializeObject<ReportRP106CriteriaData>(json);
                    PROVINCE_ID = obj.PROVINCE_ID;
                    DISTRICT_ID = obj.DISTRICT_ID;
                    SUBDISTRICT_ID = obj.SUBDISTRICT_ID;
                    OFFER_SALE_NAME = obj.OFFER_SALE_NAME;
                    DEED_NO = obj.DEED_NO;
                    ToDEED_NO = obj.ToDEED_NO;
                    PARCEL_NO = obj.PARCEL_NO;
                    ANNOUNCEMENT_NO = obj.ANNOUNCEMENT_NO;

                    OFFER_SALE_DATE_ST = obj.OFFER_SALE_DATE_ST;
                    OFFER_SALE_DATE_ED = obj.OFFER_SALE_DATE_ED;
                    //OFFER_SALE_ADDRESS = "A0F1527332E14B20AD00308E0A749F81";
                    //SALE_LAND_RAI = "A0F1527332E14B20AD00308E0A749F81";
                    //SALE_LAND_NGAN = "A0F1527332E14B20AD00308E0A749F81";
                    //SALE_LAND_WA = "A0F1527332E14B20AD00308E0A749F81";
                    //SUBDISTRICT_NAME_TH = "A0F1527332E14B20AD00308E0A749F81";
                    //SALE_AMOUNT_OF_RAI = "A0F1527332E14B20AD00308E0A749F81";
                    //DISTRICT_NAME_TH = "A0F1527332E14B20AD00308E0A749F81";
                    //PROVINCE_NAME_TH = "A0F1527332E14B20AD00308E0A749F81";
                }

                if (PROVINCE_ID != "")
                {

                    var ds = new DataSet() as DataSet;
                    ds = service.GetDataRT106(PROVINCE_ID, DISTRICT_ID, SUBDISTRICT_ID, OFFER_SALE_NAME, DEED_NO, ToDEED_NO, PARCEL_NO, ANNOUNCEMENT_NO , OFFER_SALE_DATE_ST , OFFER_SALE_DATE_ED);

                    string path = "ITS_61A.Reports.RDLC.RP_1.RP_106.rdlc";

                    var stream = assembly.GetManifestResourceStream(path);
                    report.LoadReportDefinition(stream);

                    report.DisplayName = "RT106";

                    //var parameters = new List<ReportParameter>
                    //    {
                    //        new ReportParameter("DateStart", Startdate),
                    //        new ReportParameter("DateEnd", Enddate),
                    //        new ReportParameter("ToDay", ToDay),
                    //    };
                    //report.SetParameters(parameters);


                    List<RP_106ViewModel> ListRT106 = new List<RP_106ViewModel>();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ListRT106.Add(new RP_106ViewModel
                        {
                            dateSele = row["OFFER_SALE_DATE"].ToString(),
                            nameAddressSele = row["OFFER_SALE_NAME"].ToString(),
                            addressSele = row["OFFER_SALE_ADDRESS"].ToString(),
                            area = row["SUBDISTRICT_NAME_TH"].ToString(),
                            province = row["PROVINCE_NAME_TH"].ToString(),
                            landPriceSele = row["DISTRICT_NAME_TH"].ToString(),
                            priceSele = row["SALE_AMOUNT_OF_RAI"].ToString(),
                            rai = row["SALE_LAND_RAI"].ToString(),
                            garn = row["SALE_LAND_NGAN"].ToString(),
                            wa = row["SALE_LAND_WA"].ToString()
                        });
                    }


                    var reportDs = new ReportDataSource("RP_106DataSet", ListRT106);
                    report.DataSources.Add(reportDs);
                    report.Refresh();
                }

            }
        }
    }

}    