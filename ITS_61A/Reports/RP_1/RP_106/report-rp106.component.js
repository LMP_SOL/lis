var reportRP106CriteriaController = (function () {
    function reportRP106CriteriaController($location, $scope, $filter, dialogService, webConfigService, $http, CommonService) {
        this.$location = $location;
        this.$scope = $scope;
        this.$filter = $filter;
        this.dialogService = dialogService;
        this.webConfigService = webConfigService;
        this.$http = $http;
        this.CommonService = CommonService;
        this.session_id_department = "6A469DE811EC44A5A6826D8D7AFC4EBB";
        this.topic_id_department = "7C8FE4A791124CF595EFDE0F4F08F2D5";
        this.OFFER_SALE_DATE_ST = null;
        this.OFFER_SALE_DATE_ED = null;
        this.configProvinceAutoComplete = {
            isDisabled: false,
            noCache: true,
            selectedItem: undefined,
            searchText: undefined,
        };
        //public configOffersalenameAutoComplete = {
        //    isDisabled: false,
        //    noCache: true,
        //    selectedItem: undefined,
        //    searchText: undefined,
        //};
        this.configParcelAutoComplete = {
            isDisabled: false,
            noCache: true,
            selectedItem: undefined,
            searchText: undefined,
        };
    }
    reportRP106CriteriaController.prototype.$onInit = function () {
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_department, this.topic_id_department);
        this.getDataProvince();
        //this.getDataDistrict();
        //this.getDataSubdistrict();
        //this.getDataOffersalename();
        this.getDataParcelNO();
    };
    reportRP106CriteriaController.prototype.getDataProvince = function () {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + "api/Report/getDataProvince").then(function (res) {
            console.log('province-->', res);
            _this.porvinceList = res.data.Table;
        });
    };
    //public getDataDistrict() {
    //    this.$http.get(this.webConfigService.getBaseUrl() + `api/Report/getDataDistrict`).then((res: any) => {
    //        console.log('district-->', res)
    //        this.districtList = res.data.Table;
    //    })
    //}
    //public getDataSubdistrict() {
    //    this.$http.get(this.webConfigService.getBaseUrl() + `api/Report/getDataSubdistrict`).then((res: any) => {
    //        console.log('subdistrict-->', res)
    //        this.subdistrictList = res.data.Table;
    //    })
    //}
    //public getDataOffersalename() {
    //    this.$http.get(this.webConfigService.getBaseUrl() + `api/Report/getDataOffersalename`).then((res: any) => {
    //        console.log('offersalename-->', res)
    //        this.offersalenameList = res.data.Table;
    //    })
    //}
    reportRP106CriteriaController.prototype.getDataParcelNO = function () {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + "api/Report/getDataParcelNO").then(function (res) {
            console.log('parcel-->', res);
            res.data.Table.forEach(function (item) {
                item.PARCEL_NO = item.PARCEL_NO.toString();
            });
            _this.parcelList = res.data.Table;
        });
    };
    reportRP106CriteriaController.prototype.getdataFromMasterInsertWorkerManagementPosition = function (session_id, topic_id) {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + ("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id)).then(function (res) {
            console.log('res7-->', res);
            _this.managementPositionList = res.data.Table;
        });
    };
    /////***** Province autocomplete
    //public searchTextChange(item: string) {
    //    console.log('itemmmmmmmmmmmmmm', item)
    //}
    //public selectedItemChangeProvince(item) {
    //    // console.log('selectedItemChange ', item);
    //    if (item) {
    //        this.configProvinceAutoComplete.selectedItem = item;
    //    }
    //}
    //public querySearchProvince(query) {
    //    var results = query ? this.porvinceList.filter(this.createFilterForProvince(query)) : this.porvinceList;
    //    return results;
    //}
    //public createFilterForProvince(query) {
    //    var lowercaseQuery = angular.lowercase(query);
    //    return function filterFn(item) {
    //        return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
    //    };
    //}
    /////******* end autocomplete *****////
    ///***** Parcel autocomplete
    reportRP106CriteriaController.prototype.searchTextChange = function (item) {
        console.log('itemmmmmmmmmmmmmm', item);
    };
    reportRP106CriteriaController.prototype.selectedItemChangeParcel = function (item) {
        // console.log('selectedItemChange ', item);
        debugger;
        if (item) {
            this.configParcelAutoComplete.selectedItem = item;
        }
    };
    reportRP106CriteriaController.prototype.querySearchParcel = function (query) {
        var results = query ? this.parcelList.filter(this.createFilterForParcel(query)) : this.parcelList;
        return results;
    };
    reportRP106CriteriaController.prototype.createFilterForParcel = function (query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.PARCEL_NO.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    };
    ///******* end autocomplete *****////
    reportRP106CriteriaController.prototype.getReport = function () {
        if (this.OFFER_SALE_DATE_ST == null && this.OFFER_SALE_DATE_ED != null) {
            this.dialogService.dialogMessage(Constants.messageDialog.systemMesage, "กรุณากรอกข้อมูล1");
        }
        else if (this.OFFER_SALE_DATE_ED == null && this.OFFER_SALE_DATE_ST != null) {
            this.dialogService.dialogMessage(Constants.messageDialog.systemMesage, "กรุณากรอกข้อมูล3");
        }
        else {
            var hdJson = angular.element("#JsonParameter");
            console.log("this.configProvinceAutoComplete.selectedItem", this.configProvinceAutoComplete.selectedItem);
            console.log("this.configParcelAutoComplete.selectedItem", this.configParcelAutoComplete.selectedItem);
            //console.log("this.configOffersalenameAutoComplete.selectedItem", this.configOffersalenameAutoComplete.selectedItem);
            debugger;
            var tmpST = this.CommonService.formatDate(this.OFFER_SALE_DATE_ST, "");
            var tmpED = this.CommonService.formatDate(this.OFFER_SALE_DATE_ED, "");
            var paramObj = {
                PROVINCE_ID: this.ProvinceID,
                DISTRICT_ID: this.DistrictID,
                SUBDISTRICT_ID: this.SubdistrictID,
                OFFER_SALE_NAME: this.OffersalenameID,
                DEED_NO: this.DeedID,
                ToDEED_NO: this.ToDeedID,
                PARCEL_NO: this.ParcelID,
                ANNOUNCEMENT_NO: this.AnnoucementID,
                OFFER_SALE_DATE_ST: this.CommonService.formatDateEn(tmpST, ""),
                OFFER_SALE_DATE_ED: this.CommonService.formatDateEn(tmpED, "")
            };
            debugger;
            hdJson.val(angular.toJson(paramObj));
            angular.element("#frmForSubmitParameter").submit();
            console.log('paramObj', paramObj);
        }
    };
    reportRP106CriteriaController.prototype.clear = function () {
        this.managementPositionID = undefined;
        //this.configOffersalenameAutoComplete.selectedItem = null;
        this.configProvinceAutoComplete.selectedItem = null;
        this.configParcelAutoComplete.selectedItem = undefined;
        this.OffersalenameID = null;
        this.ParcelID = undefined;
        this.AnnoucementID = null;
        this.DeedID = null;
        this.ToDeedID = null;
        this.OFFER_SALE_DATE_ST = null;
        this.OFFER_SALE_DATE_ED = null;
        this.onClear.clear();
    };
    return reportRP106CriteriaController;
}());
angular.module("reportRp106App", ['ngRoute', 'ngCookies', 'ngMaterial'])
    .service("WebConfig", ["$location", WebConfigService])
    .service("Dialog", ["$mdBottomSheet", "$mdDialog", DialogService])
    .service(Constants.services.WebConfig, ["$location", WebConfigService])
    .service(Constants.services.Common, [CommonService])
    .service(Constants.services.MasterDataService, ["$http", "WebConfig", "$q", MasterDataService])
    .config(function ($mdDateLocaleProvider) {
    var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
    $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
    $mdDateLocaleProvider.shortMonths = shortMonths;
    $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
    $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
    $mdDateLocaleProvider.monthHeaderFormatter = function (date) {
        return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
    };
    $mdDateLocaleProvider.formatDate = function (date) {
        return (date) ? moment(date).format('DD') + "/" + moment(date).format('MM') + "/" + (moment(date).get('year') + 543) : null;
    };
    $mdDateLocaleProvider.parseDate = function (dateString) {
        var dateArray = dateString.split("/");
        dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
        var m = moment(dateString, 'L', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
})
    .component("reportRp106", {
    templateUrl: "report-rp106.component.html",
    controllerAs: "rtReportCtrl",
    controller: ["$location", "$scope", "$filter", "Dialog", "WebConfig", "$http", Constants.services.Common, reportRP106CriteriaController]
})
    .component(Constants.components.SelectionAreaAddress, {
    templateUrl: "../../../app/shared/selectionAreaAddress/selection-area-address.component.html",
    controllerAs: "ctrl",
    controller: [Constants.services.MasterDataService, "$location", "$http", "$element", selectionAreaAddressController],
    bindings: {
        form: "=",
        mode: "=",
        required: "=",
        provinceSelected: "=",
        districtSelected: "=",
        subdistrictSelected: "=",
        onClear: "="
    }
});
//# sourceMappingURL=report-rp106.component.js.map