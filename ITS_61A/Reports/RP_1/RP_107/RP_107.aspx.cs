﻿using ITS_61A.Core.Services;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Reports.ViewModel.RP_1;
using Reports.ViewModel.RP_2;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ITS_61A.Reports.RP_1
{
    public class ReportRP107CriteriaData
    {
        public string MAINTENANCE_DEPARTMENT_ID { get; set; }
        public string PROJECT_ID { get; set; }
        public string LAND_ADMINISTRATOR { get; set; }
        public string CONTRACT_START_DATE_ST { get; set; }
        public string CONTRACT_START_DATE_ED { get; set; }
        public string CONTRACT_NO { get; set; }
        public string PROVINCE_ID { get; set; }
        public string DEED_NO { get; set; }
        public string DEED_TO_NO { get; set; }
        public string projectName { get; set; }

    }
    public partial class RP_107 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var service = new ReportService();
                ReportViewer1.PageCountMode = new PageCountMode();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                var report = ReportViewer1.LocalReport;
                var assembly = typeof(_61AReport).Assembly;
                report.DataSources.Clear();

                var json = Request["JsonParameter"];
                var obj1 = string.Empty;
                var examID = string.Empty;

                var MAINTENANCE_DEPARTMENT_ID = string.Empty;
                var PROJECT_ID = string.Empty;
                var LAND_ADMINISTRATOR = string.Empty;
                var CONTRACT_START_DATE_ST = string.Empty;
                var CONTRACT_START_DATE_ED = string.Empty;
                var CONTRACT_NO = string.Empty;
                var PROVINCE_ID = string.Empty;
                var DEED_TO_NO = string.Empty;
                var DEED_NO = string.Empty;
                var projectName = string.Empty;

                if (!string.IsNullOrEmpty(json))
                {
                    var obj = JsonConvert.DeserializeObject<ReportRP107CriteriaData>(json);
                    PROVINCE_ID = obj.PROVINCE_ID;
                    MAINTENANCE_DEPARTMENT_ID = obj.MAINTENANCE_DEPARTMENT_ID;
                    PROJECT_ID = obj.PROJECT_ID;
                    LAND_ADMINISTRATOR = obj.LAND_ADMINISTRATOR;
                    CONTRACT_START_DATE_ST = obj.CONTRACT_START_DATE_ST;
                    CONTRACT_START_DATE_ED = obj.CONTRACT_START_DATE_ED;
                    CONTRACT_NO = obj.CONTRACT_NO;
                    DEED_TO_NO = obj.DEED_TO_NO;
                    DEED_NO = obj.DEED_NO;
                    projectName = obj.projectName;
                }

                if (PROVINCE_ID != "")
                {

                    var ds = new DataSet() as DataSet;
                    ds = service.GetDataRT107(
                        PROVINCE_ID,
                        MAINTENANCE_DEPARTMENT_ID,
                        PROJECT_ID,
                        LAND_ADMINISTRATOR,
                        CONTRACT_START_DATE_ST,
                        CONTRACT_START_DATE_ED,
                        CONTRACT_NO,
                        DEED_TO_NO,
                        DEED_NO);

                    string path = "ITS_61A.Reports.RDLC.RP_1.RP_107.rdlc";

                    var stream = assembly.GetManifestResourceStream(path);
                    report.LoadReportDefinition(stream);

                    report.DisplayName = "RT107";

                    //var parameters = new List<ReportParameter>
                    //    {
                    //        new ReportParameter("DateStart", Startdate),
                    //        new ReportParameter("DateEnd", Enddate),
                    //        new ReportParameter("ToDay", ToDay),
                    //    };
                    //report.SetParameters(parameters);


                    List<RP_107ViewModel> ListRT107 = new List<RP_107ViewModel>();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ListRT107.Add(new RP_107ViewModel
                        {
                            idProject = row["project_code"].ToString(),
                            projectName = row["project_name"].ToString(),
                            deedNumber = row["DEED_NO"].ToString(),
                            rai = row["SIZE_IN_DEED_RAI"].ToString(),
                            garn = row["SIZE_IN_DEED_NGAN"].ToString(),
                            wa = row["SIZE_IN_DEED_WA"].ToString(),
                            province = row["province_name_th"].ToString(),
                            promiseNumber = row["CONTRACT_NO"].ToString(),
                            promiseDateStart = row["CONTRACT_START_DATE"].ToString(),
                            promiseDateOver = row["CONTRACT_END_DATE"].ToString(),
                            landPriceMonth = row["MONTHLY_LAND_MAINTENANCE"].ToString(),
                            LandAdmin = row["LAND_ADMINISTRATOR"].ToString(),
                            landDepartment = row["MAINTENANCE_DEPARTMENT_ID"].ToString()
                        });
                    }


                    var reportDs = new ReportDataSource("RP_107DataSet", ListRT107);
                    report.DataSources.Add(reportDs);
                    report.Refresh();
                }

            }
        }
    }
}