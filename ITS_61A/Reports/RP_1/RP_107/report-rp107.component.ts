﻿class reportRP107CriteriaController implements ng.IController {
    public session_id_department: string = "6A469DE811EC44A5A6826D8D7AFC4EBB";
    public topic_id_department: string = "7C8FE4A791124CF595EFDE0F4F08F2D5";
    public managementPositionList: any[];
    public managementPositionID: string;
    public porvinceList: any[];
    public ProvinceID: string;
    public POSITION_LEVEL_LIST: any[];
    public MAINTENANCE_DEPARTMENT_IDList: any[];
    public MAINTENANCE_DEPARTMENT_ID: string;
    public PROJECT_IDList: any[];
    public PROJECT_ID: string;
    public LAND_ADMINISTRATOR: string;
    public DEED_NO: string;
    public DEED_TO_NO: string;
    public CONTRACT_START_DATE_ST: any = null;
    public CONTRACT_START_DATE_ED: any = null;
    public CONTRACT_NO: string

    constructor(private $location: ng.ILocationService,
        private $scope: ng.IScope,
        private $filter: ng.IFilterService,
        private dialogService: DialogService,
        private webConfigService: WebConfigService,
        private $http: ng.IHttpService,
        private CommonService: CommonService,
        private _masterDataServic: MasterDataService
    ) { }

    $onInit() {
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_department, this.topic_id_department);
        this.getDataProvince();
        this.getLevelData();
        angular.element('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });
    }

    private getLevelData() {
        let arr = [];
        for (let i = 0; i < 10; i++) {
            arr.push({ ID: i + 1, NAME: "TEST" + (i+1)});
        }
        this.POSITION_LEVEL_LIST = arr;
    }
    public getdataFromMasterInsertWorkerManagementPosition(session_id, topic_id) {
        this.$http.get(this.webConfigService.getBaseUrl() + `api/SystemManagement/GetdataFromMasterInsertWorker?session_id=${session_id}&topic_id=${topic_id}`).then((res: any) => {
            console.log('res7-->', res)
            this.managementPositionList = res.data.Table;
        })
    }
    public configMultiSelect_POSITION_LEVEL = {
        POSITION_LEVEL: [],
        searchText: '',
    };
    public clearSearchPOSITION_LEVEL() {
        this.configMultiSelect_POSITION_LEVEL.searchText = '';
    }
    public searchTextChange(item: string) {
        console.log('itemmmmmmmmmmmmmm', item)
    }

    public configProvinceAutoComplete = {
        isDisabled: false,
        noCache: true,
        selectedItem: undefined,
        searchText: undefined,
    };
    public selectedItemChangeProvince(item) {
        if (item)
        {
            this.configProvinceAutoComplete.selectedItem = item;
            this.ProvinceID = item.ID;
        }
        else
        {
            this.configProvinceAutoComplete.selectedItem = undefined;
            this.ProvinceID = null;
        }
    }
    public querySearchProvince(query)
    {
        var results = query ? this.porvinceList.filter(this.createFilterForProvince(query)) : this.porvinceList;
        return results;
    }
    public createFilterForProvince(query)
    {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.PROVINCE_NAME_TH.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    }
    public getDataProvince()
    {
        this.$http.get(this.webConfigService.getBaseUrl() + `api/Report/getDataProvince`).then((res: any) => {
            console.log('province-->', res)
            this.porvinceList = res.data.Table;
        })
    }

    //public getDatadocument_id() {
    //    this._masterDataServic.getMasterData('3F841FD59BB548E98D1D480B6E0AC1A9', '098B5A17EDAA47A9ADA5274861668401')
    //        .then((res: any) => {
    //            console.log('document_id-->', res)
    //            this.document_idList = res.data.Table;
    //            this.document_idList.splice(0, 0, { ID: 0, DATA_TOPIC : "ทั้งหมด"})
    //        })
    //}

    public configMAINTENANCE_DEPARTMENT_IDAutoComplete = {
        isDisabled: false,
        noCache: true,
        selectedItem: undefined,
        searchText: undefined,
    };
    public selectedItemChangeMAINTENANCE_DEPARTMENT_ID(item) {
        if (item)
        {
            this.configMAINTENANCE_DEPARTMENT_IDAutoComplete.selectedItem = item;
            this.MAINTENANCE_DEPARTMENT_ID = item.ID;
        }
        else
        {
            this.configMAINTENANCE_DEPARTMENT_IDAutoComplete.selectedItem = undefined;
            this.MAINTENANCE_DEPARTMENT_ID = null;
        }
    }
    public querySearchMAINTENANCE_DEPARTMENT_ID(query)
    {
        var results = query ? this.MAINTENANCE_DEPARTMENT_IDList.filter(this.createFilterForMAINTENANCE_DEPARTMENT_ID(query)) : this.MAINTENANCE_DEPARTMENT_IDList;
        return results;
    }
    public createFilterForMAINTENANCE_DEPARTMENT_ID(query)
    {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.MAINTENANCE_DEPARTMENT_ID.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    }
    public getDataMAINTENANCE_DEPARTMENT_ID()
    {
        this.$http.get(this.webConfigService.getBaseUrl() + `api/Report/getDataMAINTENANCE_DEPARTMENT_ID`).then((res: any) => {
            console.log('MAINTENANCE_DEPARTMENT_ID-->', res)
            this.MAINTENANCE_DEPARTMENT_IDList = res.data.Table;
        })
    }

    public configPROJECT_IDAutoComplete = {
        isDisabled: false,
        noCache: true,
        selectedItem: undefined,
        searchText: undefined,
    };
    public selectedItemChangePROJECT_ID(item) {
        if (item) {
            this.configPROJECT_IDAutoComplete.selectedItem = item;
            this.PROJECT_ID = item.ID;
        }
        else {
            this.configPROJECT_IDAutoComplete.selectedItem = undefined;
            this.PROJECT_ID = null;
        }
    }
    public querySearchPROJECT_ID(query) {
        var results = query ? this.PROJECT_IDList.filter(this.createFilterForPROJECT_ID(query)) : this.PROJECT_IDList;
        return results;
    }
    public createFilterForPROJECT_ID(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.PROJECT_ID.search(new RegExp('(' + lowercaseQuery + ')', 'gi')) != -1);
        };
    }
    public getDataPROJECT_ID() {
        this.$http.get(this.webConfigService.getBaseUrl() + `api/Report/PROJECT_ID`).then((res: any) => {
            console.log('PROJECT_ID-->', res)
            this.MAINTENANCE_DEPARTMENT_IDList = res.data.Table;
        })
    }


    public getReport()
    {
        debugger
        if (this.CONTRACT_START_DATE_ST == null && this.CONTRACT_START_DATE_ED != null)
        {
            this.dialogService.dialogMessage(Constants.messageDialog.systemMesage, "กรุณากรอกข้อมูล1");
        }
        else if (this.CONTRACT_START_DATE_ED == null && this.CONTRACT_START_DATE_ST != null) {
            this.dialogService.dialogMessage(Constants.messageDialog.systemMesage, "กรุณากรอกข้อมูล3");
        }
        else
        {
            let hdJson = angular.element("#JsonParameter");
            console.log("this.configProvinceAutoComplete.selectedItem", this.configProvinceAutoComplete.selectedItem);
            console.log("this.configMAINTENANCE_DEPARTMENT_IDAutoComplete.selectedItem", this.configMAINTENANCE_DEPARTMENT_IDAutoComplete.selectedItem);
            let tmpST = this.CommonService.formatDate(this.CONTRACT_START_DATE_ST, "");
            let tmpED = this.CommonService.formatDate(this.CONTRACT_START_DATE_ED, "");
            let paramObj =
                {
                obj1: "test",
                MAINTENANCE_DEPARTMENT_ID: this.MAINTENANCE_DEPARTMENT_ID,
                PROVINCE_ID: this.ProvinceID,
                PROJECT_ID: this.PROJECT_ID,
                LAND_ADMINISTRATOR: this.LAND_ADMINISTRATOR,
                DEED_NO: this.DEED_NO,
                DEED_TO_NO: this.DEED_TO_NO,
                CONTRACT_NO: this.CONTRACT_NO,
                CONTRACT_START_DATE_ST: this.CommonService.formatDateEn(tmpST, ""),
                CONTRACT_START_DATE_ED: this.CommonService.formatDateEn(tmpED, ""),
            };
            debugger
            hdJson.val(angular.toJson(paramObj));
            angular.element("#frmForSubmitParameter").submit();
            console.log('paramObj', paramObj);
        }
    }

    public clear() {
        //this.REGISTRY_NO = undefined;
        //this.ProvinceID = undefined;
        //this.SINGLE_REGISTRY = undefined;
        //this.LANDLORD_DEPARTMENT = undefined;
        //this.CONTRACT_FROM_DATE_ST = null;
        //this.CONTRACT_TO_DATE_ST = null;
        //this.CONTRACT_FROM_DATE_ED = null;
        //this.CONTRACT_TO_DATE_ED = null;
        //this.document_id = undefined;
        //this.configProvinceAutoComplete.selectedItem = null;
    }
}

angular.module("reportRp107App", ['ngRoute', 'ngCookies', 'ngMaterial'])
    .service("WebConfig", ["$location", WebConfigService])
    .service("Dialog", ["$mdBottomSheet", "$mdDialog", DialogService])
    .service(Constants.services.WebConfig, ["$location", WebConfigService])
    .service(Constants.services.MasterDataService, ["$http", Constants.services.WebConfig, "$q", MasterDataService])
    .service(Constants.services.Common, [CommonService])
    .config(function ($mdDateLocaleProvider)
    {
        var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
        $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
        $mdDateLocaleProvider.shortMonths = shortMonths;
        $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
        $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
        $mdDateLocaleProvider.monthHeaderFormatter = function (date)
        {
            return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
        };
        $mdDateLocaleProvider.formatDate = function (date)
        {
            return (date) ? `${moment(date).format('DD')}/${moment(date).format('MM')}/${moment(date).get('year') + 543}` : null;
        };
        $mdDateLocaleProvider.parseDate = function (dateString)
        {
            var dateArray = dateString.split("/");
            dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
            var m = moment(dateString, 'L', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
    })
    .component("reportRp107",
    {
        templateUrl: "report-rp107.component.html",
        controllerAs: "rtReportCtrl",
        controller: ["$location", "$scope", "$filter", "Dialog", "WebConfig", "$http", Constants.services.Common, Constants.services.MasterDataService, reportRP107CriteriaController]
    });

