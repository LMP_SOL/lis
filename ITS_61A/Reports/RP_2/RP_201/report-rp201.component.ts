﻿class reportRP201CriteriaController implements ng.IController {
    public session_id_department: string = "6A469DE811EC44A5A6826D8D7AFC4EBB";
    public topic_id_department: string = "7C8FE4A791124CF595EFDE0F4F08F2D5";
    public managementPositionList: any[];
    public porvinceList: any[];
    public managementPositionID: string;
    public ProvinceID: string;
    constructor(private $location: ng.ILocationService,
        private $scope: ng.IScope,
        private $filter: ng.IFilterService,
        private dialogService: DialogService,
        private webConfigService: WebConfigService,
        private $http: ng.IHttpService
    ) { }


    $onInit() {
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_department, this.topic_id_department);
        this.getDataProvince();
    }

    public getdataFromMasterInsertWorkerManagementPosition(session_id, topic_id) {
        this.$http.get(this.webConfigService.getBaseUrl() + `api/SystemManagement/GetdataFromMasterInsertWorker?session_id=${session_id}&topic_id=${topic_id}`).then((res: any) => {
            console.log('res7-->', res)
            this.managementPositionList = res.data.Table;
        })
    }

    public getDataProvince() {
        this.$http.get(this.webConfigService.getBaseUrl() + `api/Report/getDataProvince`).then((res: any) => {
            console.log('province-->', res)
            this.porvinceList = res.data.Table;
        })
    }
  
    public getReport() {
        let hdJson = angular.element("#JsonParameter");
        //this.managementPositionID
            let paramObj = {
                obj1: "test",
                managementPositionID: this.managementPositionID,
                PROVINCE_ID: this.ProvinceID
            };

            hdJson.val(angular.toJson(paramObj));
            angular.element("#frmForSubmitParameter").submit();
            console.log('paramObj', paramObj);
    }

    public clear() {
        this.managementPositionID = undefined;
    }
}



angular.module("reportRp201App", ['ngRoute', 'ngCookies', 'ngMaterial'])
    .service("WebConfig", ["$location", WebConfigService])
    .service("Dialog", ["$mdBottomSheet", "$mdDialog", DialogService])
    .config(function ($mdDateLocaleProvider) {
        var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
        $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
        $mdDateLocaleProvider.shortMonths = shortMonths;
        $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
        $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
        $mdDateLocaleProvider.monthHeaderFormatter = function (date) {
            return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
        };
        $mdDateLocaleProvider.formatDate = function (date) {
            return `${moment(date).format('DD')}/${moment(date).format('MM')}/${moment(date).get('year') + 543}`;
        };
        $mdDateLocaleProvider.parseDate = function (dateString) {
            var dateArray = dateString.split("/");
            dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
            var m = moment(dateString, 'L', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
    })
    .component("reportRp201",
    {
        templateUrl: "report-rp201.component.html",
        controllerAs: "rtReportCtrl",
        controller: ["$location", "$scope", "$filter", "Dialog", "WebConfig", "$http", reportRP201CriteriaController]
    });

