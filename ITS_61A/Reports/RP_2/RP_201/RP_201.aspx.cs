﻿using ITS_61A.Core.Services;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Reports.ViewModel.RP_2;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ITS_61A.Reports.RP_2
{
    public class ReportRP201CriteriaData
    {
        public string obj1 { get; set; }
        public string examID { get; set; }
        public string managementPositionID { get; set; }
        public string PROVINCE_ID { get; set; }
    }
    public partial class RP_201 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var service = new ReportService();
                ReportViewer1.PageCountMode = new PageCountMode();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                var report = ReportViewer1.LocalReport;
                var assembly = typeof(_61AReport).Assembly;
                report.DataSources.Clear();

                var json = Request["JsonParameter"];
                var obj1 = string.Empty;
                var examID = string.Empty;
                var managementPositionID = string.Empty;
                var PROVINCE_ID = string.Empty;
                if (!string.IsNullOrEmpty(json))
                {
                    var obj = JsonConvert.DeserializeObject<ReportRP201CriteriaData>(json);

                    obj1 = obj.obj1;
                    PROVINCE_ID = obj.PROVINCE_ID;
                    managementPositionID = obj.managementPositionID;
                }

                if (obj1 != "")
                {

                    var ds = new DataSet() as DataSet;
                    ds = service.GetDataForRP201(PROVINCE_ID);

                    string path = "ITS_61A.Reports.RDLC.RP_2.RP_201.rdlc";

                    var stream = assembly.GetManifestResourceStream(path);
                    report.LoadReportDefinition(stream);

                    report.DisplayName = "RT201";

                    //var parameters = new List<ReportParameter>
                    //    {
                    //        new ReportParameter("DateStart", Startdate),
                    //        new ReportParameter("DateEnd", Enddate),
                    //        new ReportParameter("ToDay", ToDay),
                    //    };
                    //report.SetParameters(parameters);


                    List<RP_201ViewModel> demoData = new List<RP_201ViewModel>();

                    for (int i = 0; i <= 50; i++)
                    {
                        demoData.Add(new RP_201ViewModel
                        {
                            recordNumber = (i + 1).ToString(),
                            prejectName = "โครงการ " + (i + 1).ToString(),
                            sv = "222",
                            surveyType = "เดินสำรวจ",
                            departmentalNumber = "กองลำรวจ",
                            dateOfCase = "2017-08-30",
                            operatorName = "สนจิต จงจอหอ",
                            progress = "เริ่มโครงการได้ 70%",
                            history = "ออกสำรวจ",
                            dateOfComplete = "2018-09-20",
                            remark = "หมายเหตุ",
                        });
                    }


                    var reportDs = new ReportDataSource("RP201DataSet", demoData);
                    report.DataSources.Add(reportDs);
                    report.Refresh();
                }

            }
        }
    }
}