var reportRP201CriteriaController = (function () {
    function reportRP201CriteriaController($location, $scope, $filter, dialogService, webConfigService, $http) {
        this.$location = $location;
        this.$scope = $scope;
        this.$filter = $filter;
        this.dialogService = dialogService;
        this.webConfigService = webConfigService;
        this.$http = $http;
        this.session_id_department = "6A469DE811EC44A5A6826D8D7AFC4EBB";
        this.topic_id_department = "7C8FE4A791124CF595EFDE0F4F08F2D5";
    }
    reportRP201CriteriaController.prototype.$onInit = function () {
        this.getdataFromMasterInsertWorkerManagementPosition(this.session_id_department, this.topic_id_department);
        this.getDataProvince();
    };
    reportRP201CriteriaController.prototype.getdataFromMasterInsertWorkerManagementPosition = function (session_id, topic_id) {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + ("api/SystemManagement/GetdataFromMasterInsertWorker?session_id=" + session_id + "&topic_id=" + topic_id)).then(function (res) {
            console.log('res7-->', res);
            _this.managementPositionList = res.data.Table;
        });
    };
    reportRP201CriteriaController.prototype.getDataProvince = function () {
        var _this = this;
        this.$http.get(this.webConfigService.getBaseUrl() + "api/Report/getDataProvince").then(function (res) {
            console.log('province-->', res);
            _this.porvinceList = res.data.Table;
        });
    };
    reportRP201CriteriaController.prototype.getReport = function () {
        var hdJson = angular.element("#JsonParameter");
        //this.managementPositionID
        var paramObj = {
            obj1: "test",
            managementPositionID: this.managementPositionID,
            PROVINCE_ID: this.ProvinceID
        };
        hdJson.val(angular.toJson(paramObj));
        angular.element("#frmForSubmitParameter").submit();
        console.log('paramObj', paramObj);
    };
    reportRP201CriteriaController.prototype.clear = function () {
        this.managementPositionID = undefined;
    };
    return reportRP201CriteriaController;
}());
angular.module("reportRp201App", ['ngRoute', 'ngCookies', 'ngMaterial'])
    .service("WebConfig", ["$location", WebConfigService])
    .service("Dialog", ["$mdBottomSheet", "$mdDialog", DialogService])
    .config(function ($mdDateLocaleProvider) {
    var shortMonths = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
    $mdDateLocaleProvider.months = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
    $mdDateLocaleProvider.shortMonths = shortMonths;
    $mdDateLocaleProvider.days = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
    $mdDateLocaleProvider.shortDays = ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'];
    $mdDateLocaleProvider.monthHeaderFormatter = function (date) {
        return shortMonths[date.getMonth()] + ' ' + (date.getFullYear() + 543);
    };
    $mdDateLocaleProvider.formatDate = function (date) {
        return moment(date).format('DD') + "/" + moment(date).format('MM') + "/" + (moment(date).get('year') + 543);
    };
    $mdDateLocaleProvider.parseDate = function (dateString) {
        var dateArray = dateString.split("/");
        dateString = dateArray[1] + "/" + dateArray[0] + "/" + (dateArray[2] - 543);
        var m = moment(dateString, 'L', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
})
    .component("reportRp201", {
    templateUrl: "report-rp201.component.html",
    controllerAs: "rtReportCtrl",
    controller: ["$location", "$scope", "$filter", "Dialog", "WebConfig", "$http", reportRP201CriteriaController]
});
//# sourceMappingURL=report-rp201.component.js.map