﻿<!DOCTYPE html><%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RP_201.aspx.cs" Inherits="ITS_61A.Reports.RP_2.RP_201" %>

 <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link href="../../../Scripts/angular-material/angular-material.min.css" rel="stylesheet" />
        <link href="../../../Scripts/kendo-ui/2017.1.223/css/kendo.common-material.css" rel="stylesheet"/>
        <link href="../../../Scripts/kendo-ui/2017.1.223/css/kendo.material.css" rel="stylesheet" />
        <link href="../../../Scripts/kendo-ui/2017.1.223/css/kendo.material.mobile.css" rel="stylesheet" />
       <link href="../../../Style/main.css" rel="stylesheet" />

           <style>
                #ReportViewer1_ctl09,
                #ReportViewer1 {
                    height: calc(100vh - 105px) !important;
                }

                #VisibleReportContentReportViewer1_ctl09>div {
                    overflow: auto;
                }

                @media only screen and (max-width: 1366px) {

                    #ReportViewer1_ctl09 {
                        height: calc(100vh - 161px) !important;
                    }

                    #ReportViewer1 {
                        height: calc(100vh - 129px) !important;
                        overflow-y: hidden;
                    }

                    #VisibleReportContentReportViewer1_ctl09>div {
                        height: calc(100vh - 160px) !important;
                    }
                }

                @media only screen and (max-width: 959px) {

                    #ReportViewer1_ctl09 {
                        height: calc(100vh - 241px) !important;
                    }

                    #ReportViewer1 {
                        height: calc(100vh - 187px) !important;
                        overflow-y: hidden;
                    }

                    #VisibleReportContentReportViewer1_ctl09>div {
                        height: calc(100vh - 247px) !important;
                    }
                }

                .md-complex-dialog .main-btn-dialog img {
                    display: none;
                }

            </style>

</head>
       <body style="background-color: #fff;">

            <div ng-app="reportRp201App">

                <report-rp201></report-rp201>

            </div>

            <form id="form1" runat="server">
                 <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%">
                </rsweb:ReportViewer>
            </form>

            <script src="../../../Scripts/Jquery/jquery-2-2.4.min.js"></script>
            <script src="../../../Scripts/lodash/lodash.min.js"></script>
            <script src="../../../Scripts/angularJs/angular.js"></script>
            <script src="../../../Scripts/angularJs/angular-sanitize.min.js"></script>
            <script src="../../../Scripts/angularJs/angular-messages.min.js"></script>
            <script src="../../../Scripts/angularJs/angular-route.min.js"></script>
            <script src="../../../Scripts/angularJs/angular-cookies.js"></script>
            <script src="../../../Scripts/angularJs/angular-animate.min.js"></script>
            <script src="../../../Scripts/angularJs/angular-aria.min.js"></script>
            <script src="../../../Scripts/angularJs/angular-touch.min.js"></script>
            <script src="../../../Scripts/angular-material/angular-material.min.js"></script>
            <script src="../../../Scripts/moment/moment.min.js"></script>
            <script src="../../../Scripts/jszip.js"></script>
            <script src="../../../bundles/js?v=f-rOZpG8nqcdBI9IS1kiTRlij7Eim7N9U1_RJYwd4_w1" type="text/javascript" language="text/javascript"></script>
            
           <script src="report-rp201.component.js"></script>

            <script type="text/javascript">
                $(document).ready(function () {
                    $("#ReportViewer1_ReportViewer").hide();
                    Sys.Application.add_load(function () {
                        $find("ReportViewer1").add_propertyChanged(viewerPropertyChanged);
                    });
                });

                function viewerPropertyChanged(sender, e) {
                    $("#ReportViewer1_ReportViewer").show();
                    if (e.get_propertyName() == "isLoading") {
                        if ($find("ReportViewer1").get_isLoading()) {
                            $("#VisibleReportContentReportViewer1_ctl09").find('div').find("table:first").attr("align", "center");
                        } else {
                            $("#VisibleReportContentReportViewer1_ctl09").find('div').find("table:first").attr("align", "center");
                        }
                    }
                };
            </script>
        </body>
</html>
