﻿using ITS_61A.Core.ServiceDataModel.LogDataModel;
using ITS_61A.Core.Services;
using ITS_61A.Web.apis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace ITS_61A.Apis
{
    public class logController : BaseApiController
    {
        public string getHost() {
                return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        }
        public string getClientIPAddress()
        {
            string VisitorsIPAddr = string.Empty;
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
            }
            return VisitorsIPAddr;
        }

        public string getUserAgentSrting() {
            return HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToString();
        }

        public void insterLogData( string menuId = null, string buttonId = null, string objectId = null) {
            
            LisLogDataModel item = new LisLogDataModel();
            LogDataService _service = new LogDataService();

            item.LOG_HOST = getHost();
            item.CLIENT_IP = getClientIPAddress();
            item.USER_AGENT_STRING = getUserAgentSrting();
            item.IDENTITY_USER_ID = "AA18BA7B99FF403FAFD8529B35F94370";
            item.MENU_ID = menuId;
            item.BUTTON_ID = buttonId; //1= view, 2=insert,3=update, 4 = delete, 5 = report  
            item.OBJECT_ID = objectId;
            _service.saveLog(item);
        }
    }
}