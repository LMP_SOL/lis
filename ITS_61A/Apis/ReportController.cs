﻿using ITS_61A.Core.Services;
using ITS_61A.Web.apis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ITS_61A.Apis
{
    public class ReportController : BaseApiController
    {

        [HttpGet]
        [Route("api/Report/getDataProvince")]
        public HttpResponseMessage getDataProvince()
        {
            return ExecuteWebApiCall(() =>
            {
                var ds = new ReportService().getDataProvince();
                return ds;
            });
        }

        [HttpGet]
        [Route("api/Report/getDataDistrict")]
        public HttpResponseMessage getDataDistrict()
        {
            return ExecuteWebApiCall(() =>
            {
                var ds = new ReportService().getDataDistrict();
                return ds;
            });
        }

        [HttpGet]
        [Route("api/Report/getDataSubdistrict")]
        public HttpResponseMessage getDataSubdistrict()
        {
            return ExecuteWebApiCall(() =>
            {
                var ds = new ReportService().getDataSubdistrict();
                return ds;
            });
        }

        //[HttpGet]
        //[Route("api/Report/getDataOffersalename")]
        //public HttpResponseMessage getDataOffersalename()
        //{
        //    return ExecuteWebApiCall(() =>
        //    {
        //        var ds = new ReportService().getDataOffersalename();
        //        return ds;
        //    });
        //}

        [HttpGet]
        [Route("api/Report/getDataParcelNO")]
        public HttpResponseMessage getDataParcelNO()
        {
            return ExecuteWebApiCall(() =>
            {
                var ds = new ReportService().getDataParcelNO();
                return ds;
            });
        }

        [HttpGet]
        [Route("api/Report/getDataREGISTRY")]
        public HttpResponseMessage getDataREGISTRY()
        {
            return ExecuteWebApiCall(() =>
            {
                var ds = new ReportService().getDataREGISTRY();
                return ds;
            });
        }
        [HttpGet]
        [Route("api/Report/getDataSINGLEREGISTRY")]
        public HttpResponseMessage getDataSINGLEREGISTRY()
        {
            return ExecuteWebApiCall(() =>
            {
                var ds = new ReportService().getDataSINGLEREGISTRY();
                return ds;
            });
        }
        [HttpGet]
        [Route("api/Report/getDatadocument_id")]
        public HttpResponseMessage getDatadocument_id()
        {
            return ExecuteWebApiCall(() =>
            {
                var ds = new ReportService().getDatadocument_id();
                return ds;
            });
        }

        [HttpGet]
        [Route("api/Report/getDataMAINTENANCE_DEPARTMENT_ID")]
        public HttpResponseMessage getDataMAINTENANCE_DEPARTMENT_ID()
        {
            return ExecuteWebApiCall(() =>
            {
                var ds = new ReportService().getDataMAINTENANCE_DEPARTMENT_ID();
                return ds;
            });
        }

        [HttpGet]
        [Route("api/Report/getDataPROJECT_ID")]
        public HttpResponseMessage getDataPROJECT_ID()
        {
            return ExecuteWebApiCall(() =>
            {
                var ds = new ReportService().getDataPROJECT_ID();
                return ds;
            });
        }

    }
}