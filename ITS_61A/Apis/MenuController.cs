﻿using ITS_61A.Core.Services;
using ITS_61A.Web.apis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ITS_61A.Apis
{
    public class MenuController : BaseApiController
    {
        [HttpGet]
        [Route("api/Menu/getMenuUser")]
        public HttpResponseMessage getMenuUser()
        {
            return ExecuteWebApiCall(() =>
            {
                MenuDataservice _service = new MenuDataservice();
                var id = "93A71B7136404C419EEBDF6CCB781CDF";
                return _service.getMenuOfUser(id);
            });
        }
    }
}