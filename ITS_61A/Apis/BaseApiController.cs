﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Controllers;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using ITS_61A.Core.Exceptions;

namespace ITS_61A.Web.apis
{
    public class BaseApiController : ApiController
    {
        protected BusinessException CreateBusinessException(ModelStateDictionary modelState)
        {
            var sb = new StringBuilder();
            foreach (var key in modelState.Keys)
            {
                var item = modelState[key];
                if (item.Errors.Count >= 0)
                {
                    var errorMessages = item.Errors.Select(x => x.ErrorMessage).ToList();
                    var error = string.Join(",", errorMessages);
                    sb.AppendLine($"{item.Value}:{error}");
                }
            }
            var bizException = new BusinessException(sb.ToString());
            return bizException;
        }

        protected async Task<HttpResponseMessage> ExecuteWebApiCallAsync<T>(Func<Task<T>> func)
        {
            try
            {
                var result = await func();
                return ConvertToJsonResponseMessage(result);
            }
            catch (BusinessException bizErr)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, bizErr.Message);
            }
            catch (Exception err)
            {
                //Warning CS0168  The variable 'err' is declared but never used by m.sakchai
                var dummy = err;
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Something went wrong.");
            }
        }

        protected HttpResponseMessage ExecuteWebApiCall<T>(Func<T> func)
        {
            try
            {
                var result = func();
                return ConvertToJsonResponseMessage(result);
            }
            catch (BusinessException bizErr)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, bizErr.Message);
            }
            catch (Exception err)
            {
                //Warning CS0168  The variable 'err' is declared but never used by m.sakchai
                var dummy = err;
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Something went wrong." + err + "");
            }
        }
        protected HttpResponseMessage ConvertToJsonResponseMessage(object data)
        {
            var mediaFormatter = new JsonMediaTypeFormatter
            {
                //SerializerSettings = new JsonSerializerSettings
                //{
                //    ContractResolver = new CamelCasePropertyNamesContractResolver()
                //}
            };
            var response = Request.CreateResponse(HttpStatusCode.OK, data, mediaFormatter);
            //response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
    }


    public class AuthorizeWithToken : AuthorizeAttribute
    {
        //private UserService userService = new UserService();
        //public override void OnAuthorization(HttpActionContext actionContext)
        //{
        //    var username = HttpContext.Current.User.Identity as ClaimsIdentity;
        //    var u = username.Claims.Where(c => c.Type == ClaimTypes.Sid)
        //           .Select(c => c.Value).SingleOrDefault();
        //    if (u == null)
        //    {
        //        HandleUnauthorizedRequest(actionContext);
        //    }
        //    if (Authorize(actionContext, u))
        //    {
        //        return;
        //    }
        //    HandleUnauthorizedRequest(actionContext);
        //}
        //protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        //{
        //    actionContext.Response = new HttpResponseMessage
        //    {
        //        StatusCode = HttpStatusCode.Forbidden,
        //        Content = new StringContent("You are unauthorized to access this resource")
        //    };
        //}
        //private bool Authorize(HttpActionContext actionContext, string userId)
        //{
        //    try
        //    {
        //        IEnumerable<string> values = new List<string>();
        //        actionContext.Request.Headers.TryGetValues("Authorization", out values);
        //        if (values.Count() != 1)
        //        {
        //            return false;
        //        }

        //        return userService.CheckUserToken(userId, values.FirstOrDefault());

        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}
    }
}