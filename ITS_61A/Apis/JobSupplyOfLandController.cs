﻿using ITS_61A.Web.apis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using ITS_61A.Core.Entities;
using ITS_61A.Core.Services;
using ITS_61A.Web.apis;
using ITS_61A.Core.ServiceDataModel.JobSupplyOfLandDataModel;

namespace ITS_61A.Apis
{
    public class JobSupplyOfLandController : BaseApiController
    {
        logController log = new logController();

        [HttpGet]
        [Route("api/JobSupplyOfLand/GetCriteriaForSearchPayTaxLand")]
        public HttpResponseMessage GetCriteriaForSearchPayTaxLand()
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.GetCriteriaForSearchPayTaxLand();
                return ds;
            });
        }

        [HttpGet]
        [Route("api/JobSupplyOfLand/GetDataForPayTaxLand")]
        public HttpResponseMessage GetDataForPayTaxLand(string year_tax,string project_id,string deed_id_from,string deed_id_to,string province_id,string taxReceive_Department)
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.GetDataForPayTaxLand(year_tax, project_id, deed_id_from, deed_id_to, province_id, taxReceive_Department);
                return ds;
            });
        }
        [HttpGet]
        [Route("api/JobSupplyOfLand/GetDataForAnnouncementLand")]
        public HttpResponseMessage GetDataForAnnouncementLand()
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.GetDataForAnnouncementLand();
                return ds;
            });
        }

        [HttpGet]
        [Route("api/JobSupplyOfLand/GetDataForLandRegistration")]
        public HttpResponseMessage GetDataForLandRegistration(string ANNOUNCEMENT_LAND_ID, string PROJECT_NAME, string DEED_NO_FROM, string DEED_NO_TO, string PROVINCE_ID)
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.GetDataForLandRegistration(ANNOUNCEMENT_LAND_ID,PROJECT_NAME,DEED_NO_FROM,DEED_NO_TO,PROVINCE_ID);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/JobSupplyOfLand/DeleteDataTaxLand")]
        public HttpResponseMessage DeleteDataTaxLand(string id_p_tax_land)
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.DeleteDataTaxLand(id_p_tax_land);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/JobSupplyOfLand/GetDataForSearchPurchasingProject")]
        public HttpResponseMessage GetDataForSearchPurchasingProject(string PROJECT_ID, string PROVINCE_ID, string ANNOUNCEMENTS_NO, string DEED_NO_FROM, string DEED_NO_TO, string PARCEL_NO, string DEALING_FILE_NO)
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.GetDataForSearchPurchasingProject(PROJECT_ID, PROVINCE_ID, ANNOUNCEMENTS_NO, DEED_NO_FROM, DEED_NO_TO, PARCEL_NO, DEALING_FILE_NO);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/JobSupplyOfLand/getCriteriaforsearcPurchasingProject")]
        public HttpResponseMessage getCriteriaforsearcPurchasingProject()
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.getCriteriaforsearcPurchasingProject();
                return ds;
            });
        }
        [HttpPost]
        [Route("api/JobSupplyOfLand/GetDataSearchAnnouncementLand")]
        public HttpResponseMessage GetDataSearchAnnouncementLand(AnnoucementLandSearchDataModel model)
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.GetDataSearchAnnouncementLand(model);
                return ds;
            });
        }
        [HttpGet]
        [Route("api/JobSupplyOfLand/DeleteAnnouncementLand")]
        public HttpResponseMessage DeleteAnnouncementLand( string AnnouncementLandId)
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.DeleteAnnouncementLand(AnnouncementLandId);
                return ds;
            });
        }
        [HttpGet]
        [Route("api/JobSupplyOfLand/DeleteDDeed")]
        public HttpResponseMessage DeleteDDeed(string DId)
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.DeleteDDeed(DId);
                return ds;
            });
        }
    }
}