﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ITS_61A.Core.Entities;
using ITS_61A.Core.Services;

namespace ITS_61C.Web.Apis
{
    public class DemoController : ApiController
    {
        [HttpGet]
        [Route("api/demo/ListFile")]
        public List<demoModel> ListFile()
        {
            DemoDataservice file = new DemoDataservice();
            return file.demo();
        }
    }
}
