﻿using ITS_61A.Core.Services;
using ITS_61A.Web.apis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ITS_61A.Apis
{
    public class IdentityUserController : BaseApiController
    {
        [HttpGet]
        [Route("api/IdentityUser/getUserFromHRIS")]
        public HttpResponseMessage getUserFromHRIS()
        {
            return ExecuteWebApiCall(() =>
            {
                HRISDataservice _service = new HRISDataservice();
                return _service.getUser();
            });
        }
    }
}