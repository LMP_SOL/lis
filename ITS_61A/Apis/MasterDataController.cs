﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ITS_61A.Core.Entities;
using ITS_61A.Core.Services;
using ITS_61A.Web.apis;
using ITS_61A.Core.ServiceDataModel.MasterData;

namespace ITS_61A.Apis
{
    public class MasterDataController: BaseApiController
    {
        logController log = new logController();

        [HttpGet]
        [Route("api/MasterData/getSectionOfMasterMenu")]
        public HttpResponseMessage getSectionOfMasterMenu(string menuId)
        {
            return ExecuteWebApiCall(() =>
            {
                MasterDataManagementDataservice _service = new MasterDataManagementDataservice();

                return _service.getSectionOfMasterMenu(menuId);
            });
        }

        [HttpGet]
        [Route("api/MasterData/getDataIntab")]
        public HttpResponseMessage getDataIntab(string menuId, string sessionId)
        {
            return ExecuteWebApiCall(() =>
            {
                MasterDataManagementDataservice _service = new MasterDataManagementDataservice();

                return _service.getDataIntab(menuId, sessionId);
            });
        }

        [HttpGet]
        [Route("api/MasterData/Datatopic")]
        public HttpResponseMessage getDatatopic(string docId,string menuId)
        {
            return ExecuteWebApiCall(() =>
            {
                MasterDataManagementDataservice _service = new MasterDataManagementDataservice();

                var ds = _service.getDataInTopic(docId);

                log.insterLogData(null, "1", docId);

                return ds;
            });
        }

        [HttpGet]
        [Route("api/MasterData/Mastertopic")]
        public HttpResponseMessage getMastertopicc(string sessionId)
        {
            return ExecuteWebApiCall(() =>
            {
                MasterDataManagementDataservice _service = new MasterDataManagementDataservice();

                var ds = _service.getMastertopic(sessionId);
                return ds;
            });
        }
        [HttpPost]
        [Route("api/MasterData/Sortdata")]
        public HttpResponseMessage Sortdata(masterDataSort[] masterDataList)
        {
            return ExecuteWebApiCall(() =>
            {
                MasterDataManagementDataservice _service = new MasterDataManagementDataservice();
      
                return  _service.saveSortData(masterDataList);
            });
        }

        [HttpPost]
        [Route("api/MasterData/Mastersave")]
        public HttpResponseMessage Mastersave(masterDataSave masterDataSave)
        {
            return ExecuteWebApiCall(() =>
            {
                MasterDataManagementDataservice _service = new MasterDataManagementDataservice();
                masterDataSave.user_id = "AA18BA7B99FF403FAFD8529B35F94370";
                //string menuId = masterDataSave.menu_id;
                string menuId = null;
                string buttonId = masterDataSave.doc_id == null|| masterDataSave.doc_id == string.Empty ? "2":"3";
              
                var res = _service.saveMasterData(masterDataSave);
                if (res) {
                    string objectId = masterDataSave.doc_id;
                    log.insterLogData(menuId, buttonId, objectId);
                    return res;
                }
                else{
                    return res;
                }
                 
            });
        }

        
        [HttpGet]
        [Route("api/MasterData/GetDataDepartment")]
        public HttpResponseMessage GetDataDepartment()
        {
            return ExecuteWebApiCall(() =>
            {
                MasterDataManagementDataservice _service = new MasterDataManagementDataservice();

                return _service.GetDataDepartment();
            });
        }

        

        [HttpGet]
        [Route("api/MasterData/getDataSubdistrictOffset")]
        public HttpResponseMessage getDataSubdistrictOffset(int limit, int offset, string keyword, string district)
        {
            return ExecuteWebApiCall(() =>
            {
                MasterDataManagementDataservice _service = new MasterDataManagementDataservice();

                return _service.getDataSubdistrictOffset(limit, offset, keyword, district);
            });
        }
        

        [HttpGet]
        [Route("api/MasterData/getDataDistrictAndProvince")]
        public HttpResponseMessage getDataDistrictAndProvince()
        {
            return ExecuteWebApiCall(() =>
            {
                MasterDataManagementDataservice _service = new MasterDataManagementDataservice();

                return _service.getDataDistrictAndProvince();
            });
        }

    }
}