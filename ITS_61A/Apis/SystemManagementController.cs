﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ITS_61A.Core.Entities;
using ITS_61A.Core.Services;
using ITS_61A.Web.apis;
using ITS_61A.Core.ServiceDataModel.SystemManagement;

namespace ITS_61A.Apis
{
    public class SystemManagementController : BaseApiController
    {
        logController log = new logController();

        [HttpGet]
        [Route("api/SystemManagement/GetdataIdentitySetPassword")]
        public HttpResponseMessage GetdataIdentitySetPassword()
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
              
                return _service.GetdataIdentitySetPassword();
            });
        }
        //[HttpGet]
        //[Route("api/SystemManagement/SetdataIdentitySetPassword")]
        //public HttpResponseMessage SetdataIdentitySetPassword(string ageDate)
        //{
        //    return ExecuteWebApiCall(() =>
        //    {
        //        SystemManagementDataservicecs _service = new SystemManagementDataservicecs();

        //        var ds = _service.SetdataIdentitySetPassword(ageDate);
        //        return ds;
        //    });
        //}


        //-------------

        [HttpGet]
        [Route("api/SystemManagement/GetSettingConnectdatabase")]
        public HttpResponseMessage GetSettingConnectdatabase()
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();

                return _service.GetSettingConnectdatabase();
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/GetDataIdentitySMTP")]
        public HttpResponseMessage GetDataIdentitySMTP()
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
              
                return _service.GetDataIdentitySMTP();
            });
        }
        [HttpPost]
        [Route("api/SystemManagement/saveDataIdentitySMTP")]
        public HttpResponseMessage saveDataIdentitySMTP(SMTP_dataModel SMTP)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                string user_id = "AA18BA7B99FF403FAFD8529B35F94370";
                return _service.save_SMTP(SMTP, user_id);
            });
        }

        [HttpPost]
        [Route("api/SystemManagement/settingPasswordDataModel")]
        public HttpResponseMessage settingPasswordDataModel(setting_password_dataModel SPD)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                string user_id = "AA18BA7B99FF403FAFD8529B35F94370"; 
                return _service.setting_password_dataModel(SPD, user_id);
            });
        }

        [HttpPost]
        [Route("api/SystemManagement/settingConnectDatabaseDataModel")]
        public HttpResponseMessage settingConnectDatabaseDataModel(setting_connectDatabase_dataModel SCDB)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                string user_id = "AA18BA7B99FF403FAFD8529B35F94370";
                return _service.setting_connectDatabase_dataModel(SCDB, user_id);
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/GetdataInsertWorker")]
        public HttpResponseMessage GetdataInsertWorker(string id)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();

                var ds = _service.GetdataInsertWorker(id);

                string menuId = null;
                log.insterLogData(menuId, "1", id);

                return ds;
            });
        }
        [HttpGet]
        [Route("api/SystemManagement/GetdataFromMasterInsertWorker")]
        public HttpResponseMessage GetdataFromMasterInsertWorker(string session_id, string topic_id)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.GetdataFromMasterInsertWorker(session_id, topic_id);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/GetdataIdentityGroup")]
        public HttpResponseMessage GetdataIdentityGroup()
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.GetdataIdentityGroup();
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/getdataFromSettingAlertUser")]
        public HttpResponseMessage getdataFromSettingAlertUser(string bigTopic, string startDate, string endDate, string notifier)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.getdataFromSettingAlertUser(bigTopic, startDate, endDate, notifier);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/DeleteDataSettingAlertUser")]
        public HttpResponseMessage DeleteDataSettingAlertUser(string alert_id)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.DeleteDataSettingAlertUser(alert_id);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/getdataForsearchUser")]
        public HttpResponseMessage getdataForsearchUser(string PersonCode, string PersonName, string Username, string Office, int Status, string Position)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.getdataForsearchUser(PersonCode, PersonName, Username, Office, Status, Position);
                return ds;
            });
        }

        [HttpGet]
        //[Route("api/SystemManagement/getCriteriaforsearcUser")]
        public HttpResponseMessage getCriteriaforsearcUser()
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.getCriteriaforsearcUser();
                return ds;
            });
        }
        
        [HttpGet]
        [Route("api/SystemManagement/GetdataIdentityAlertUser")]
        public HttpResponseMessage GetdataIdentityAlertUser(string id)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.GetdataIdentityAlertUser(id);
                return ds;
            });
        }

        [HttpGet]
        public HttpResponseMessage SendEmailforChangepassword(string email)
        {

            //SystemManagementDataservicecs system = new SystemManagementDataservicecs();
            //if (!system.SendEmailforChangepassword(email))
            //{
            //    return Request.CreateResponse(HttpStatusCode.BadRequest);
            //}
            //return Request.CreateResponse(HttpStatusCode.OK, 1);

            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.SendEmailforChangepassword(email);
                return ds;
            });
        }



        [HttpPost]
        [Route("api/SystemManagement/InsertAlertSave")]
        public HttpResponseMessage InsertAlertSave(notificationMessage notificationMessage)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.saveNotificationMessage(notificationMessage);
                return ds;

            });
        }
        [HttpPost]
        [Route("api/SystemManagement/InsertWorkerSave")]
        public HttpResponseMessage InsertWorkerSave(insertWorkerMessage insertWorkerMessage)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                //insertWorkerMessage.CREATE_BY = "AA18BA7B99FF403FAFD8529B35F94370";
                var ds = _service.InsertWorkerSave(insertWorkerMessage);
                return ds;

            });
        }

        [HttpPost]
        [Route("api/SystemManagement/updateWorkerDataModel")]
        public HttpResponseMessage update_worker_dataModel(update_worker_dataModel UPDW)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                string user_id = "AA18BA7B99FF403FAFD8529B35F94370";
                var ds = _service.update_worker_dataModel(UPDW, user_id);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/GetDataSortAlertUser")]
        public HttpResponseMessage GetDataSortAlertUser()
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.GetDataSortAlertUser();
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/getdataGroupPrivilege")]
        public HttpResponseMessage getdataGroupPrivilege(string groupName)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.GetdataGroupPrivilege(groupName);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/DeleteDataGroupPrivilege")]
        public HttpResponseMessage DeleteDataGroupPrivilege(string group_id, string group_update_by)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.DeleteDataGroupPrivilege(group_id,group_update_by);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/getdataLogStatusPerson")]
        public HttpResponseMessage GetdataLogStatusPerson(string Name_user, string startDate_From,string start_DateTo)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.GetdataLogStatusPerson(Name_user, startDate_From, start_DateTo);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/getdataLogByDate")]
        public HttpResponseMessage GetdataLogByDate(string startDate_From, string start_DateTo)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.GetdataLogByDate(startDate_From, start_DateTo);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/getdataLogByUser")]
        public HttpResponseMessage GetdataLogByUser(string id,string startDate_From,string startDate_To)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.GetdataLogByUser(id, startDate_From, startDate_To);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/SystemManagement/getdataUser")]
        public HttpResponseMessage GetdataUser(string Name, string Username, string SelectDepartment)
        {
            return ExecuteWebApiCall(() =>
            {
                SystemManagementDataservicecs _service = new SystemManagementDataservicecs();
                var ds = _service.GetdataUser(Name, Username, SelectDepartment);
                return ds;
            });
        }

        [HttpGet]
        [Route("api/JobSupplyOfLand/GetDataForSearchPurchasingProject")]
        public HttpResponseMessage GetDataForSearchPurchasingProject(string PROJECT_ID, string PROVINCE_ID, string ANNOUNCEMENTS_NO, string DEED_NO_FROM, string DEED_NO_TO, string PARCEL_NO, string DEALING_FILE_NO)
        {
            return ExecuteWebApiCall(() =>
            {
                JobSupplyOfLandDataservicecs _service = new JobSupplyOfLandDataservicecs();
                var ds = _service.GetDataForSearchPurchasingProject(PROJECT_ID, PROVINCE_ID, ANNOUNCEMENTS_NO, DEED_NO_FROM, DEED_NO_TO, PARCEL_NO, DEALING_FILE_NO);
                return ds;
            });
        }
    }
}
