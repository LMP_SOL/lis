var is = {
    string: function (msg) {
        return true;
    }
};
// TreeView
$(function () {
    var treeview = $("#treeview").data("kendoTreeView");
    is.string(treeview.text("#foo"));
    treeview.text("#foo", "bar");
});
// Window
$(function () {
    var window = $("#window").data("kendoWindow");
    var dom = $("<em>Foo</em>");
    window.content(dom);
});
//# sourceMappingURL=kendo-ui-tests.js.map