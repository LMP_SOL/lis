﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;
using Newtonsoft.Json;

namespace ITS_61A.Tests.Test1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            GetdataInsertWorker();
        }
        public DataSet GetdataInsertWorker()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            string sql = "GETDATA_MASTER_DATA";

            using (var con = new OracleConnection(ConfigurationManager.ConnectionStrings["LISConnection"].ConnectionString))
            {
                using (var cmd = new OracleCommand(sql, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DATA_SELECT", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("SESSION_ID", OracleDbType.NVarchar2).Value = "44CB0A25CB8A435CB48477DBFEF153F1";
                    cmd.Parameters.Add("USERTOPIC_ID_ID", OracleDbType.NVarchar2).Value = "A0F1527332E14B20AD00308E0A749F81";
                    //cmd.Parameters.Add("ID_", OracleDbType.NVarchar2).Value = "73EB039BF2A6595AE05408002713F738";
                    var da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                    }
                }
            }

            return ds;
        }
    }
}
